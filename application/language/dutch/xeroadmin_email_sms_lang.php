<?php
$lang = array(
	"Subscribers Counter" => "Subscribers Counter",
	"Total Reach" => "Total Reach",
    "My Email Campaign" => "My Email Campaign",
	"My SMS Campaign" => "My SMS Campaign",
    "My Contact Book" => "My Contact Book",
	"Manual List" => "Manual List",
    "Contact List" => "Contact List",
	"My Automate Posting" => "My Automate Posting",
    "Carousel Posting" => "Carousel Posting",
	"Call to Action Posting" => "Call to Action Posting",
);