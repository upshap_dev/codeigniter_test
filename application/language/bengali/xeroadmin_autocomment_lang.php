<?php
$lang = array(
    "Send Inbox reply multiple times?" => "Send Inbox reply multiple times?",
    "Enable comment reply?" => "Enable comment reply?",
	"Auto like comment?" => "Auto like comment?",
    "Do you want to hide comments?" => "Do you want to hide comments?",
    "Type of Auto Reply Comment" => "Type of Auto Reply Comment",
    "Write keywords in comma separated" => "Write keywords in comma separated",
	"Select message template for Inbox reply after delete or hide comment" => "Select message template for Inbox reply after delete or hide comment",
	"Select private reply" => "Select private reply",
    "Create Auto Reply Template" => "Create Auto Reply Template",
	"Connect  Pages Now" => "Connect  Pages Now",
);