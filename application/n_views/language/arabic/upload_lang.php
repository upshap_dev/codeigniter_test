<?php
$lang = array(
    "upload_userfile_not_set" => "الملف الشخصى غير موجود",
    "upload_file_exceeds_limit" => "ملف يتجاوز الحدود",
    "upload_file_exceeds_form_limit" => "ملف يتجاوز حد النموذج",
    "upload_file_partial" => "ملف جزئى",
    "upload_no_temp_directory" => "لايوجد دليل مؤقت",
    "upload_unable_to_write_file" => "غير قادر على كتابة الملف",
    "upload_stopped_by_extension" => "توقف بالتمديد",
    "upload_no_file_selected" => "لم يتم اختيار ملف",
    "upload_invalid_filetype" => "نوع الملف غير صالح",
    "upload_invalid_filesize" => "حجم الملف غير صالح",
    "upload_invalid_dimensions" => "ابعاد غير صالحة",
    "upload_destination_error" => "خطأ الواجهة",
    "upload_no_filepath" => "لايوجد مسار",
    "upload_no_file_types" => "لاتوجد انواع ملفات",
    "upload_bad_filename" => "اسم غير جيد",
    "upload_not_writable" => "غير قابل للكتابة",
);