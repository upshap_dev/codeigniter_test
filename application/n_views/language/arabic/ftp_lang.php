<?php
$lang = array(
    "ftp_no_connection" => "لايوجد اتصال",
    "ftp_no_source_file" => "ملف مجهول المصدر",
    "ftp_unable_to_changedir" => "غير قابل لتغيير الاتجاه",
    "ftp_unable_to_chmod" => "غير قابل لتغيير الصلاحية",
    "ftp_unable_to_connect" => "غير قادر على الاتصال",
    "ftp_unable_to_delete" => "غير قابل للحذف",
    "ftp_unable_to_download" => "غير قابل للتحميل",
    "ftp_unable_to_login" => "غير قادر على تسجيل الدخول",
    "ftp_unable_to_makdir" => "غير قابل",
    "ftp_unable_to_move" => "غير قابل للحركة",
    "ftp_unable_to_rename" => "غير قابل لاعادة التسمية",
    "ftp_unable_to_upload" => "غير قابل للرفع",
);