<?php
$lang = array(
    "migration_none_found" => "ليس متواجد",
    "migration_not_found" => "غير موجود",
    "migration_multiple_version" => "نسخ متعددة",
    "migration_class_doesnt_exist" => "الكلاس غير موجود",
    "migration_missing_up_method" => "الطريقة مفقودة",
    "migration_missing_down_method" => "الطريقة مفقودة ايضا",
    "migration_invalid_filename" => "اسم الملف غير صالح",
);