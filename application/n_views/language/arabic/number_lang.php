<?php
$lang = array(
    "bytes" => "بايت",
    "gigabyte_abbr" => "جيجابايت",
    "kilobyte_abbr" => "كيلوبايت",
    "megabyte_abbr" => "ميجابايت",
    "terabyte_abbr" => "تيرابايت",
);