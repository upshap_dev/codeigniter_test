<?php include("application/n_views/instagram_reply/comment_reply/comment_reply_js.php"); ?>
<?php include("application/n_views/instagram_reply/comment_reply/full_mentions_campaign_modals.php"); ?>
<!-- start of auto comment javascript section -->
<?php include(FCPATH.'application/n_views/comment_automation/autocomment_javascript_section.php'); ?>
<?php include(FCPATH.'application/n_views/comment_automation/autocomment_modal_section.php'); ?>
<!-- end of auto comment javascript section -->

<script>
    $(document).ready(function(){
        $('body').addClass('menu-collapsed');
        $('.brand-logo').removeClass('d-none');
    });
</script>

<script type="text/javascript">
    $("document").ready(function(){
        var base_url = "<?php echo base_url(); ?>";

        $('.modal').on("hidden.bs.modal", function (e) {
            if ($('.modal:visible').length) {
                $('body').addClass('modal-open');
            }
        });

        $(document).on('click','.add_template',function(e){
            e.preventDefault();
            var current_id=$(this).prev().prev().attr('id');
            var current_val=$(this).prev().prev().val();
            var page_id = get_page_id();
            if(page_id=="")
            {
                swal.fire('<?php echo $this->lang->line("Error"); ?>', "<?php echo $this->lang->line('Please select a page first')?>", 'error');
                return false;
            }
            $("#add_template_modal").attr("current_id",current_id);
            $("#add_template_modal").attr("current_val",current_val);
            $("#add_template_modal").modal();
        });

        $(document).on('click','.ref_template',function(e){
            e.preventDefault();
            var current_val=$(this).prev().prev().prev().val();
            var current_id=$(this).prev().prev().prev().attr('id');
            var page_id = get_page_id();
            if(page_id=="")
            {
                swal.fire('<?php echo $this->lang->line("Error"); ?>', "<?php echo $this->lang->line('Please select a page first')?>", 'error');
                return false;
            }
            $.ajax({
                type:'POST',
                url: base_url+"instagram_reply/get_private_reply_postbacks",
                data: {page_table_ids:page_id},
                dataType: 'JSON',
                success:function(response){
                    $("#"+current_id).html(response.options).val(current_val);
                }
            });
        });

        $('#add_template_modal').on('hidden.bs.modal', function (e) {
            var current_id=$("#add_template_modal").attr("current_id");
            var current_val=$("#add_template_modal").attr("current_val");
            var page_id = get_page_id();
            if(page_id=="")
            {
                swal.fire('<?php echo $this->lang->line("Error"); ?>', "<?php echo $this->lang->line('Please select a page first')?>", 'error');
                return false;
            }
            $.ajax({
                type:'POST' ,
                url: base_url+"instagram_reply/get_private_reply_postbacks",
                data: {page_table_ids:page_id,is_from_add_button:'1'},
                dataType: 'JSON',
                success:function(response){
                    $("#"+current_id).html(response.options);
                }
            });
        });

        // getting postback list and making iframe
        $('#add_template_modal').on('shown.bs.modal',function(){
            var page_id = get_page_id();
            var rand_time="<?php echo time(); ?>";
            var media_type = "ig";
            var iframe_link="<?php echo base_url('messenger_bot/create_new_template/1/');?>"+page_id+"/0/"+media_type+"?lev="+rand_time;
            $(this).find('iframe').attr('src',iframe_link);
        });
        // getting postback list and making iframe

    });

    function get_page_id()
    {
        var page_id = $('#bot_list_select').val();
        return page_id;
    }
</script>


<!-- postback add/refresh button section -->
<div class="modal fade" id="add_template_modal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><i class="fa fa-plus-circle"></i> <?php echo $this->lang->line('Add Template'); ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <iframe src="" frameborder="0" width="100%" onload="resizeIframe(this)"></iframe>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" type="button" class="btn-lg btn btn-dark"><i class="fa fa-refresh"></i> <?php echo $this->lang->line("Close & Refresh List");?></button>
            </div>
        </div>
    </div>
</div>