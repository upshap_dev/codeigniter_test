<?php
$n_config = array();

//basic_config
$n_config['theme_version'] = 1.738; //change version to correct

//developer_function
$n_config['dev_mode'] = true; //production change to false
$n_config['extra_function'] = true; //production change to false

//theme_configuration
$n_config['use_nviews_login_page'] = 'true';
$n_config['light_logo'] = '';
$n_config['dark_logo'] = '';
$n_config['dark_logo_big_favicon_png'] = '';
$n_config['rtl_langs'] = 'arabic,hebrew';
$n_config['set_icons'] = '';
$n_config['current_theme'] = 'semi-dark-layout';
$n_config['sidebar_icons'] = 'boxicons';
$n_config['recommend_photoswipe_resolution'] = '0x0';
$n_config['hide_login_via_email'] = 'false';
$n_config['show_renew_button'] = 'true';
$n_config['show_renew_button_days'] = 14;
$n_config['livicon_icon_style'] = 'lines';

$n_config['arabic_lang_icon'] = 'sa';
$n_config['hebrew_lang_icon'] = 'il';
$n_config['spain_lang_icon'] = 'es';


// v1.4

$n_config['page_faq_view'] = 'false';
$n_config['page_faq_only_admin'] = 'false';
$n_config['page_faq_default'] = '';

$n_config['page_help_view'] = 'false';
$n_config['page_help_only_admin'] = 'false';
$n_config['page_help_default'] = '';

$n_config['dashboard_section_1_on'] = 'false';
$n_config['dashboard_section_1_only_admin'] = 'false';
$n_config['dashboard_section_1_default'] = '';

$n_config['greetings_on'] = 'false';
$n_config['greetings_random'] = 'true';

//v1.5

$n_config['start_modal_show'] = 'false';
$n_config['start_modal_only_admin'] = 'false';
$n_config['start_modal_default'] = '';
$n_config['start_modal_always_show'] = 'false';

$n_config['login_page_text_show'] = 'false';
$n_config['login_page_text_default'] = '';

$n_config['package_qa_show'] = 'false';
$n_config['package_qa_only_admin'] = 'false';
$n_config['package_qa_default'] = '';

$n_config['disable_example_dashboard'] = 'true';

$n_config['ecommerce_product_gallery'] = 8;

$n_config['default_lang_flowbuilder'] = 'english';
$n_config['default_flowbuilder'] = 'true';
$n_config['show_lang_selector'] = 'true';

$n_config['is_external_off'] = 'false';
$n_config['payment_text_header_sidebar'] = 'Payment';
$n_config['payment_text_sidebar'] = 'Payment';


//PWA settings
$n_config['pwa_on'] = 'false';
$n_config['pwa_name'] = '';
$n_config['pwa_short_name'] = '';
$n_config['pwa_description'] = '';
$n_config['pwa_theme_color'] = '';
$n_config['pwa_background_color'] = '';
$n_config['pwa_icon_512'] = '';
$n_config['pwa_apple_status_bar'] = 'black-translucent';

$n_config['eco_custom_domain'] = 'false';
$n_config['custom_domain_host'] = '';

$n_config['theme_appeareance_on'] = 'false';
$n_config['theme_sidebar_color'] = '#1a233a';
$n_config['dark_icon_color'] = '#8baff3';
$n_config['sidebar_text_color'] = '#8494a7';
$n_config['primary_color'] = '#5A8DEE';
$n_config['primary_outline_color'] = '#5A8DEE';
$n_config['light_primary_color'] = 'rgba(90, 141, 238, 0.17)';
$n_config['danger_color'] = '#FF5B5C';
$n_config['success_color'] = '#39DA8A';
$n_config['warning_color'] = '#FDAC41';
$n_config['primary_color_hover'] = '#175ee4';
$n_config['nav_font'] = 'Rubik';
$n_config['body_font'] = 'IBM Plex Sans';
$n_config['btn_primary_color_hover'] = '#719df0';
$n_config['dashboard_background'] = '#F2F4F4';

$n_config['sidebar_icon_help_bx'] = 'bx bx-help-circle';
$n_config['sidebar_icon_help_livicons'] = 'help';
$n_config['sidebar_icon_faq_bx'] = 'bx bx-question-mark';
$n_config['sidebar_icon_faq_livicons'] = 'question';

//rtl
$n_config['nav_font_rtl'] = 'Rubik';
$n_config['body_font_rtl'] = 'IBM Plex Sans';
$n_config['light_icon_rtl'] = '';
$n_config['dark_icon_rtl'] = '';
$n_config['dark_logo_rtl'] = '';
$n_config['light_logo_rtl'] = '';


//v1.6
$n_config['body_font_font_size'] = '1rem';
$n_config['card_title_font_size'] = '1.2rem';
$n_config['body_font_font_size_rtl'] = '1rem';
$n_config['card_title_font_size_rtl'] = '1.2rem';

$n_config['signup_page_view'] = 'false';
$n_config['signup_page_default_view'] = '';

$n_config['helper_default_lang'] = '';

$n_config['helper_animation'] = 'true';

$n_config['omise_on'] = 'false';
$n_config['omise_public_key'] = '';
$n_config['omise_secret_key'] = '';

$n_config['n_paymongo_gateway_enabled'] = 'false';
$n_config['n_paymongo_gateway_gcash_enabled'] = 'false';
$n_config['n_paymongo_gateway_paymaya_enabled'] = 'false';
$n_config['n_paymongo_gateway_grab_enabled'] = 'false';
$n_config['n_paymongo_sec'] = '';
$n_config['n_paymongo_pub'] = '';

$n_config['theme_mobile_full_width'] = 'false';
$n_config['import_account_fb_alert'] = 'false';

$n_config['welcome_modal_button_text_arabic'] = '';
$n_config['welcome_modal_button_text_english'] = '';
$n_config['welcome_modal_button_text_bengali'] = '';
$n_config['welcome_modal_button_text_dutch'] = '';
$n_config['welcome_modal_button_text_english'] = '';
$n_config['welcome_modal_button_text_french'] = '';
$n_config['welcome_modal_button_text_german'] = '';
$n_config['welcome_modal_button_text_greek'] = '';
$n_config['welcome_modal_button_text_italian'] = '';
$n_config['welcome_modal_button_text_polish'] = '';
$n_config['welcome_modal_button_text_portuguese'] = '';
$n_config['welcome_modal_button_text_russian'] = '';
$n_config['welcome_modal_button_text_spanish'] = '';
$n_config['welcome_modal_button_text_turkish'] = '';
$n_config['welcome_modal_button_text_vietnamese'] = '';


//{color:data.task.task_color}

//dont_change_this_add_new_configs_here

include(FCPATH.'application/n_views/config_user.php');