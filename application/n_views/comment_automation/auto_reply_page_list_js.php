<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('body').addClass('menu-collapsed');
        $('.brand-logo').removeClass('d-none');

    });
    $(document).ready(function(){

        var base_url = "<?php echo base_url(); ?>";
        $(".private_reply_postback").select2({ width: "100%" });
        $("#bot_list_select").select2();


        $('#hide_comment_automation_message').on('close.bs.alert', function (e) {
            e.preventDefault();
            $.ajax({
                type:'POST' ,
                url:"<?php echo site_url();?>comment_automation/hide_comment_automation_message",
                data:{},
                dataType:'JSON',
                success:function(response){
                    $("#hide_comment_automation_message").parent().parent().hide();
                }
            });
        });

        $('#bot_list_select').on('change', function (e) {
            var waiting_div_content = '<div class="text-center waiting"><i class="bx bx-loader-alt bx-spin blue text-center"></i></div>';
            $("#middle_column").html(waiting_div_content);
            $("#right_column").html(waiting_div_content);

            var page_table_id = $('#bot_list_select').val();
            $("#dynamic_page_id").val($('#bot_list_select').val());
            $.ajax({
                type:'POST' ,
                url:"<?php echo site_url();?>comment_automation/get_page_details",
                data:{page_table_id:page_table_id},
                dataType:'JSON',
                success:function(response){
                    data = response.middle_column_content;
                    data = data.replaceAll('mr-3', '');
                    data = data.replaceAll('fas fa-code', 'bx bx-code');
                    data = data.replaceAll('fas fa-edit', 'bx bx-edit');
                    data = data.replaceAll('fa fa-edit', 'bx bx-edit');
                    data = data.replaceAll('fa  fa-edit', 'bx bx-edit');
                    data = data.replaceAll('far fa-copy', 'bx bx-copy');
                    data = data.replaceAll('fa fa-trash', 'bx bx-trash');
                    data = data.replaceAll('fas fa-trash', 'bx bx-trash');
                    data = data.replaceAll('fa fa-eye', 'bx bxs-show');
                    data = data.replaceAll('fas fa-eye', 'bx bxs-show');
                    data = data.replaceAll('fas fa-trash-alt', 'bx bx-trash');
                    data = data.replaceAll('fa fa-wordpress', 'bx bxl-wordpress');
                    data = data.replaceAll('fa fa-briefcase', 'bx bx-briefcase');
                    data = data.replaceAll('fab fa-wpforms', 'bx bx-news');
                    data = data.replaceAll('fas fa-file-export', 'bx bx-export');
                    data = data.replaceAll('fa fa-comment', 'bx bx-comment');
                    data = data.replaceAll('fa fa-user', 'bx bx-user');
                    data = data.replaceAll('fa fa-refresh', 'bx bx-refresh');
                    data = data.replaceAll('fa fa-plus-circle', 'bx bx-plus-circle');
                    data = data.replaceAll('fas fa-comments', 'bx bx-comment');
                    data = data.replaceAll('fa fa-hand-o-right', 'bx bx-link-external');
                    data = data.replaceAll('fab fa-facebook-square', 'bx bxl-facebook-square');
                    data = data.replaceAll('fas fa-exchange-alt', 'bx bx-repost');
                    data = data.replaceAll('fa fa-sync-alt', 'bx bx-sync');
                    data = data.replaceAll('fas fa-key', 'bx bx-key');
                    data = data.replaceAll('fas fa-bolt', 'bx bxs-bolt');
                    data = data.replaceAll('fas fa-clone', 'bx bxs-copy-alt');
                    data = data.replaceAll('fas fa-receipt', 'bx bx-receipt');
                    data = data.replaceAll('fa fa-paper-plane', 'bx bx-paper-plane');
                    data = data.replaceAll('fa fa-send', 'bx bx-send');
                    data = data.replaceAll('fas fa-hand-point-right', 'bx bx-news');
                    data = data.replaceAll('fa fa-code', 'bx bx-code');
                    data = data.replaceAll('fa fa-clone', 'bx bx-duplicate');
                    data = data.replaceAll('fas fa-pause', 'bx bx-pause');
                        data = data.replaceAll('fas fa-play', 'bx bx-play');
                    data = data.replaceAll('fa fa-cog', 'bx bx-cog');
                    data = data.replaceAll('fa fa-check', 'bx bx-check');



                    $("#middle_column").html(data);

                    data = response.right_column_content;
                    data = data.replaceAll('fas fa-code', 'bx bx-code');
                    data = data.replaceAll('fas fa-edit', 'bx bx-edit');
                    data = data.replaceAll('fa fa-edit', 'bx bx-edit');
                    data = data.replaceAll('fa  fa-edit', 'bx bx-edit');
                    data = data.replaceAll('far fa-copy', 'bx bx-copy');
                    data = data.replaceAll('fa fa-trash', 'bx bx-trash');
                    data = data.replaceAll('fas fa-trash', 'bx bx-trash');
                    data = data.replaceAll('fa fa-eye', 'bx bxs-show');
                    data = data.replaceAll('fas fa-eye', 'bx bxs-show');
                    data = data.replaceAll('fas fa-trash-alt', 'bx bx-trash');
                    data = data.replaceAll('fa fa-wordpress', 'bx bxl-wordpress');
                    data = data.replaceAll('fa fa-briefcase', 'bx bx-briefcase');
                    data = data.replaceAll('fab fa-wpforms', 'bx bx-news');
                    data = data.replaceAll('fas fa-file-export', 'bx bx-export');
                    data = data.replaceAll('fa fa-comment', 'bx bx-comment');
                    data = data.replaceAll('fa fa-user', 'bx bx-user');
                    data = data.replaceAll('fa fa-refresh', 'bx bx-refresh');
                    data = data.replaceAll('fa fa-plus-circle', 'bx bx-plus-circle');
                    data = data.replaceAll('fas fa-comments', 'bx bx-comment');
                    data = data.replaceAll('fa fa-hand-o-right', 'bx bx-link-external');
                    data = data.replaceAll('fab fa-facebook-square', 'bx bxl-facebook-square');
                    data = data.replaceAll('fas fa-exchange-alt', 'bx bx-repost');
                    data = data.replaceAll('fa fa-sync-alt', 'bx bx-sync');
                    data = data.replaceAll('fas fa-key', 'bx bx-key');
                    data = data.replaceAll('fas fa-bolt', 'bx bxs-bolt');
                    data = data.replaceAll('fas fa-clone', 'bx bxs-copy-alt');
                    data = data.replaceAll('fas fa-receipt', 'bx bx-receipt');
                    data = data.replaceAll('fa fa-paper-plane', 'bx bx-paper-plane');
                    data = data.replaceAll('fa fa-send', 'bx bx-send');
                    data = data.replaceAll('fas fa-hand-point-right', 'bx bx-news');
                    data = data.replaceAll('fa fa-code', 'bx bx-code');
                    data = data.replaceAll('fa fa-clone', 'bx bx-duplicate');
                    data = data.replaceAll('fas fa-pause', 'bx bx-pause');
                        data = data.replaceAll('fas fa-play', 'bx bx-play');
                    data = data.replaceAll('fa fa-cog', 'bx bx-cog');
                    data = data.replaceAll('fa fa-check-circle', 'bx bx-check-circle');
                    data = data.replaceAll('fas fa-comment', 'bx bx-comment');
                    data = data.replaceAll('fa fa-check', 'bx bx-check');

                    $("#right_column").html(data);
                    $("#auto_reply_template").html(response.template_list);
                    $(".private_reply_postback").html(response.autoreply_postbacks);
                }
            });

            $.ajax({
                type:'POST' ,
                url: base_url+'comment_automation/get_label_dropdown',
                data: {page_table_id:page_table_id},
                dataType : 'JSON',
                success:function(response){
                    $('.dropdown_con').removeClass('hidden');
                    $('#first_dropdown').html(response.first_dropdown);
                    $('#edit_first_dropdown').html(response.edit_first_dropdown);
                }
            });

        });


        var content_counter = 1;
        var edit_content_counter = 1;

        $('[data-toggle="popover"]').popover();
        $('[data-toggle="popover"]').on('click', function(e) {e.preventDefault(); return true;});


        // enable and edit auto reply by post id
        $(document).on('click','.manual_auto_reply',function(){
            var page_name = $(this).attr('page_name');
            var page_table_id = $('#bot_list_select').val();
            $("#manual_reply_error").html('');
            $("#manual_page_name").html(page_name);
            $("#manual_table_id").val(page_table_id);
            $("#manual_post_id").val('');

            $("#check_post_id").show();

            $("#manual_reply_by_post").addClass('modal');
            $("#manual_reply_by_post").modal();
        });

        $(document).on('click','#check_post_id',function(){
            $("#manual_reply_error").html('');
            var post_id = $("#manual_post_id").val();
            var page_table_id = $("#manual_table_id").val();
            if(post_id=="")
            {
                swal.fire('<?php echo $this->lang->line("Warning"); ?>', '<?php echo $this->lang->line("Please provide a post ID");?>', 'warning');
                return false;
            }

            $(this).addClass('btn-progress');
            $.ajax({
                type:'POST' ,
                url:"<?php echo site_url();?>comment_automation/checking_post_id",
                data:{page_table_id:page_table_id,post_id:post_id},
                dataType:'JSON',
                context: this,
                success:function(response){
                    $(this).removeClass('btn-progress');
                    if(response.error == 'yes')
                        swal.fire('<?php echo $this->lang->line("Error"); ?>', response.error_msg, 'error');
                    else
                    {
                        $("#manual_check_button_div").html(response.buttons);
                        $("#check_post_id").hide();
                    }
                }
            });
        });


        $("#enable_auto_tag").click(function(){
            $("#manual_reply_error").html('');
            var post_id = $("#manual_post_id").val();
            var page_id = $(this).attr('page_table_id');
            $(this).addClass('disabled');
            $.ajax({
                type:'POST' ,
                url:"<?php echo site_url();?>comment_reply_enhancers/manual_sync_commenter_info",
                data:{page_id:page_id,post_id:post_id},
                dataType:'JSON',
                success:function(response){
                    if(response.status != '1')
                        $("#manual_reply_error").html("<div class='alert alert-danger text-center'><i class='bx bx-x-circle'></i> "+response.message+"</div><br/>");
                    else
                    {
                        $("#manual_reply_error").html("<div class='alert alert-success text-center'><i class='bx bx-check'></i> "+response.message+"</div><br/>");
                        // $("#manual_post_id").val();
                    }
                    $("#enable_auto_tag").removeClass('disabled');
                }
            });
        });


        // end of enable and edit auto reply by post id



        $(document).on('click','.enable_auto_commnet',function(){

            /** emoji load for offensive private reply  **/

            var page_table_id = $('#bot_list_select').val();
            var post_id = $(this).attr('post_id');
            var post_permalink = $(this).attr('post_permalink');
            var manual_enable = $(this).attr('manual_enable');
            var Pleaseprovidepostid = "<?php echo $Pleaseprovidepostid; ?>";

            if(typeof(post_id) === 'undefined' || post_id == '')
            {
                alertify.alert('<?php echo $this->lang->line("Alert")?>',Pleaseprovidepostid,function(){});
                return false;
            }

            $("#auto_reply_page_id").val(page_table_id);
            $("#auto_reply_post_id").val(post_id);
            $("#auto_reply_post_permalink").val(post_permalink);
            $("#manual_enable").val(manual_enable);

            $("#create_label_auto_reply").attr("page_id_for_label",page_table_id);

            $(".message").val('').click();
            $(".filter_word").val('');
            $("#auto_campaign_name").val('');
            $("#template_select").prop("checked", true);
            $("#auto_reply_template").val('0');
            $("#auto_reply_templates_section").show();
            $("#new_template_section").hide();
            $("#save_and_create").hide();
            $("#comment_reply_enabled").prop("checked", true);
            $("#delete_offensive_comment_hide").prop("checked", true);
            $("#multiple_reply").prop("checked", false);
            $("#auto_like_comment").prop("checked", false);
            $("#hide_comment_after_comment_reply").prop("checked", false);

            $("#generic").prop("checked", false);
            $("#filter").prop("checked", false);
            $("#generic_message_div").hide();
            $("#filter_message_div").hide();

            $('#label_ids').val(null).trigger('change');



            for(var i=2;i<=20;i++)
            {
                $("#filter_div_"+i).hide();
            }
            content_counter = 1;
            $("#content_counter").val(content_counter);
            $("#add_more_button").show();

            $("#response_status").html('');

            $("#auto_reply_message_modal").addClass("modal");
            $("#auto_reply_message_modal").modal();

            $("#manual_reply_by_post").removeClass('modal');
        });



        $("#content_counter").val(content_counter);

        $(document).on('click','#add_more_button',function(){
            content_counter++;
            if(content_counter == 20)
                $("#add_more_button").hide();
            $("#content_counter").val(content_counter);

            $("#filter_div_"+content_counter).show();

            /** Load Emoji For Filter Word when click on add more button **/

            $("#comment_reply_msg_"+content_counter).emojioneArea({
                autocomplete: false,
                pickerPosition: "bottom"
            });


        });


        $(document).on('change','input[name=message_type]',function(){
            if($("input[name=message_type]:checked").val()=="generic")
            {
                $("#generic_message_div").show();
                $("#filter_message_div").hide();

                /*** Load Emoji for generic message when clicked ***/

                $("#generic_message").emojioneArea({
                    autocomplete: false,
                    pickerPosition: "bottom"
                });


            }
            else
            {
                $("#generic_message_div").hide();
                $("#filter_message_div").show();

                /*** Load Emoji When Filter word click , by defualt first textarea are loaded & No match found field****/

                $("#comment_reply_msg_1, #nofilter_word_found_text").emojioneArea({
                    autocomplete: false,
                    pickerPosition: "bottom"
                });

            }
        });


        $(document).on('click','.lead_first_name',function(){

            var textAreaTxt = $(this).parent().next().next().next().children('.emojionearea-editor').html();

            var lastIndex = textAreaTxt.lastIndexOf("<br>");
            var lastTag = textAreaTxt.substr(textAreaTxt.length - 4);
            lastTag=lastTag.trim(lastTag);

            if(lastTag=="<br>")
                textAreaTxt = textAreaTxt.substring(0, lastIndex);



            var txtToAdd = " #LEAD_USER_FIRST_NAME# ";
            var new_text = textAreaTxt + txtToAdd;
            $(this).parent().next().next().next().children('.emojionearea-editor').html(new_text);
            $(this).parent().next().next().next().children('.emojionearea-editor').click();


        });

        $(document).on('click','.lead_last_name',function(){

            var textAreaTxt = $(this).parent().next().next().next().next().children('.emojionearea-editor').html();

            var lastIndex = textAreaTxt.lastIndexOf("<br>");
            var lastTag = textAreaTxt.substr(textAreaTxt.length - 4);
            lastTag=lastTag.trim(lastTag);

            if(lastTag=="<br>")
                textAreaTxt = textAreaTxt.substring(0, lastIndex);



            var txtToAdd = " #LEAD_USER_LAST_NAME# ";
            var new_text = textAreaTxt + txtToAdd;
            $(this).parent().next().next().next().next().children('.emojionearea-editor').html(new_text);
            $(this).parent().next().next().next().next().children('.emojionearea-editor').click();

        });

        $(document).on('click','.lead_tag_name',function(){

            var textAreaTxt = $(this).parent().next().next().next().next().next().children('.emojionearea-editor').html();

            var lastIndex = textAreaTxt.lastIndexOf("<br>");
            var lastTag = textAreaTxt.substr(textAreaTxt.length - 4);
            lastTag=lastTag.trim(lastTag);

            if(lastTag=="<br>")
                textAreaTxt = textAreaTxt.substring(0, lastIndex);




            var txtToAdd = " #TAG_USER# ";
            var new_text = textAreaTxt + txtToAdd;
            $(this).parent().next().next().next().next().next().children('.emojionearea-editor').html(new_text);
            $(this).parent().next().next().next().next().next().children('.emojionearea-editor').click();
        });



        $(document).on('click','.save_button',function(){
            var button_type = $(this).attr('button_name');
            var use_template = $("input[name=auto_template_selection]:checked").val();
            var post_id = $("#auto_reply_post_id").val();

            if(button_type == "submit_create_button") $("#submit_btn_values").val("submit_create_button");

            if(button_type == "only_submit") $("#submit_btn_values").val("only_submit");

            if(typeof(use_template)==='undefined')
            {
                var reply_type = $("input[name=message_type]:checked").val();
                var Youdidntselectanyoption = "<?php echo $Youdidntselectanyoption; ?>";
                var Youdidntprovideallinformation = "<?php echo $Youdidntprovideallinformation; ?>";
                if (typeof(reply_type)==='undefined')
                {
                    swal.fire('<?php echo $this->lang->line("Warning"); ?>', Youdidntselectanyoption, 'warning');
                    return false;
                }
                var auto_campaign_name = $("#auto_campaign_name").val().trim();

                if(reply_type == 'generic')
                {
                    if(auto_campaign_name == ''){
                        swal.fire('<?php echo $this->lang->line("Warning"); ?>', Youdidntprovideallinformation, 'warning');
                        return false;
                    }
                }
                else
                {
                    if(auto_campaign_name == ''){
                        swal.fire('<?php echo $this->lang->line("Warning"); ?>', Youdidntprovideallinformation, 'warning');
                        return false;
                    }
                }

            } else if(use_template == 'yes')
            {
                var template_selection = $("#auto_reply_template").val();

                if(template_selection == '0')
                {
                    var Youdidntselectanytemplate = "<?php echo $Youdidntselectanytemplate; ?>";
                    swal.fire('<?php echo $this->lang->line("Warning"); ?>', Youdidntselectanytemplate, 'warning');
                    return false;
                }
            }


            $(this).addClass('btn-progress');

            var queryString = new FormData($("#auto_reply_info_form")[0]);
            var AlreadyEnabled = "<?php echo $AlreadyEnabled; ?>";
            $.ajax({
                type:'POST' ,
                url: base_url+"comment_automation/ajax_autoreply_submit",
                data: queryString,
                dataType : 'JSON',
                // async: false,
                cache: false,
                contentType: false,
                processData: false,
                context: this,
                success:function(response){
                    $(this).removeClass('btn-progress');
                    if(response.status=="1")
                    {
                        swal.fire('<?php echo $this->lang->line("Success"); ?>', response.message, 'success').then((value) => {
                            $('#bot_list_select').val($('#bot_list_select').val()).trigger("change");
                            $("#auto_reply_message_modal").modal('hide');
                            $("#pageresponse_auto_reply_message_modal").modal('hide');
                            $("#manual_reply_by_post").modal('hide');
                            $("#manual_check_button_div").html('');
                            $("#create_label_auto_reply").attr("page_id_for_label","");
                        });
                        $("button[post_id="+post_id+"][manual_enable='no']").removeClass('btn-outline-success').addClass('btn-outline-warning disabled').html(AlreadyEnabled);
                    }
                    else
                    {
                        swal.fire('<?php echo $this->lang->line("Error"); ?>', response.message, 'error');
                    }
                }

            });



        });


        // create an new label and put inside label list
        $(document).on('click','#create_label_auto_reply',function(e){
            e.preventDefault();

            var page_id=$(this).attr('page_id_for_label');

            swal.fire({
                title: "<?php echo $this->lang->line('Label Name'); ?>",
                input: "text",
                confirmButtonText: "<?php echo $this->lang->line('Create'); ?>",
                cancelButtonText: "<?php echo $this->lang->line('Cancel'); ?>",
                showCancelButton: true,
            })
                .then((value) => {
                    if (value.isDenied || value.isDismissed) {
                        return;
                    }
                    var label_name = `${value.value}`;
                    if(label_name!="" && label_name!='null')
                    {
                        $("#save_changes").addClass("btn-progress");
                        $.ajax({
                            context: this,
                            type:'POST',
                            dataType:'JSON',
                            url:"<?php echo site_url();?>home/common_create_label_and_assign",
                            data:{page_id:page_id,label_name:label_name},
                            success:function(response){

                                $("#save_changes").removeClass("btn-progress");

                                if(response.error) {
                                    var span = document.createElement("span");
                                    span.innerHTML = response.error;

                                    swal.fire({
                                        icon: 'error',
                                        title: '<?php echo $this->lang->line('Error'); ?>',
                                        html:span,
                                    });

                                } else {
                                    var newOption = new Option(response.text, response.id, true, true);
                                    $('#label_ids').append(newOption).trigger('change');
                                }
                            }
                        });
                    }
                });
        });


        // create an new label and put inside label list
        $(document).on('click','#create_label_edit_auto_reply',function(e){
            e.preventDefault();

            var page_id=$(this).attr('page_id_for_label');

            swal.fire({
                title: "<?php echo $this->lang->line('Label Name'); ?>",
                input: "text",
                confirmButtonText: "<?php echo $this->lang->line('Create'); ?>",
                cancelButtonText: "<?php echo $this->lang->line('Cancel'); ?>",
                showCancelButton: true,
            })
                .then((value) => {
                    if (value.isDenied || value.isDismissed) {
                        return;
                    }
                    var label_name = `${value.value}`;
                    if(label_name!="" && label_name!='null')
                    {
                        $("#save_changes").addClass("btn-progress");
                        $.ajax({
                            context: this,
                            type:'POST',
                            dataType:'JSON',
                            url:"<?php echo site_url();?>home/common_create_label_and_assign",
                            data:{page_id:page_id,label_name:label_name},
                            success:function(response){

                                $("#save_changes").removeClass("btn-progress");

                                if(response.error) {
                                    var span = document.createElement("span");
                                    span.innerHTML = response.error;

                                    swal.fire({
                                        icon: 'error',
                                        title: '<?php echo $this->lang->line('Error'); ?>',
                                        html:span,
                                    });

                                } else {
                                    var newOption = new Option(response.text, response.id, true, true);
                                    $('#edit_label_ids').append(newOption).trigger('change');
                                }
                            }
                        });
                    }
                });
        });

        $(document).on('click','#modal_close',function(){
            $('#bot_list_select').val($('#bot_list_select').val()).trigger("change");
            $("#auto_reply_message_modal").modal('hide');
            $("#pageresponse_auto_reply_message_modal").modal('hide');
            $("#manual_reply_by_post").modal('hide');
            $("#create_label_auto_reply").attr('page_id_for_label','');
        });

        $(document).on('click','#edit_modal_close',function(){
            $('#bot_list_select').val($('#bot_list_select').val()).trigger("change");
            $("#edit_auto_reply_message_modal").modal('hide');
            $("#pageresponse_edit_auto_reply_message_modal").modal('hide');
            $("#create_label_edit_auto_reply").attr("page_id_for_label","");
        });



        $('#post_synch_modal').on('hidden.bs.modal', function () {
            $('#bot_list_select').val($('#bot_list_select').val()).trigger("change");
        });

        $('#manual_reply_by_post').on('hidden.bs.modal', function () {
            $('#bot_list_select').val($('#bot_list_select').val()).trigger("change");
            $("#manual_check_button_div").html('');
        });


        $(document).on('click','.edit_reply_info',function(){

            //$(".emojionearea-editor").html('');

            var emoji_load_div_list="";

            $("#manual_edit_reply_by_post").removeClass('modal');
            $("#edit_auto_reply_message_modal").addClass("modal");
            $("#edit_response_status").html("");

            var table_id = $(this).attr('table_id');
            $(".previewLoader").show();
            $.ajax({
                type:'POST' ,
                url:"<?php echo site_url();?>comment_automation/ajax_edit_reply_info",
                data:{table_id:table_id},
                dataType:'JSON',
                success:function(response)
                {
                    $("#edit_private_message_offensive_words").html(response.postbacks);
                    $("#edit_generic_message_private").html(response.postbacks).select2({ width: "100%" });
                    $("#edit_nofilter_word_found_text_private").html(response.postbacks);
                    for(var j=1;j<=20;j++)
                    {
                        $("#edit_filter_div_"+j).hide();
                        $("#edit_filter_message_"+j).html(response.postbacks);
                    }

                    var edit_label_ids = response.edit_label_ids;
                    if(edit_label_ids != '')
                    {
                        edit_label_ids = edit_label_ids.split(",");
                        $('#edit_label_ids').val(edit_label_ids).trigger('change');
                    }
                    else
                        $('#edit_label_ids').val(null).trigger('change');

                    $("#edit_auto_reply_page_id").val(response.edit_auto_reply_page_id);
                    $("#create_label_edit_auto_reply").attr('page_id_for_label',response.edit_auto_reply_page_id);
                    $("#edit_auto_reply_post_id").val(response.edit_auto_reply_post_id);
                    $("#edit_auto_reply_post_permalink").val(response.edit_auto_reply_post_permalink);
                    $("#edit_auto_campaign_name").val(response.edit_auto_campaign_name);

                    // comment hide and delete section
                    if(response.is_delete_offensive == 'hide')
                    {
                        $("#edit_delete_offensive_comment_hide").attr('checked','checked');
                    }
                    else
                    {
                        $("#edit_delete_offensive_comment_delete").attr('checked','checked');
                    }
                    $("#edit_delete_offensive_comment_keyword").val(response.offensive_words);
                    $("#edit_private_message_offensive_words").val(response.private_message_offensive_words).click();




                    /**	make the emoji loads div id in a string for selection . This is the first add. **/
                    // emoji_load_div_list=emoji_load_div_list+"";

                    if(response.hide_comment_after_comment_reply == 'no')
                        $("#edit_hide_comment_after_comment_reply").removeAttr('checked','checked');
                    else
                        $("#edit_hide_comment_after_comment_reply").attr('checked','checked');
                    // comment hide and delete section


                    $("#edit_"+response.reply_type).prop('checked', true);
                    // added by mostofa on 27-04-2017
                    if(response.comment_reply_enabled == 'no')
                        $("#edit_comment_reply_enabled").removeAttr('checked','checked');
                    else
                        $("#edit_comment_reply_enabled").attr('checked','checked');

                    if(response.multiple_reply == 'no')
                        $("#edit_multiple_reply").removeAttr('checked','checked');
                    else
                        $("#edit_multiple_reply").attr('checked','checked');

                    if(response.auto_like_comment == 'no')
                        $("#edit_auto_like_comment").removeAttr('checked','checked');
                    else
                        $("#edit_auto_like_comment").attr('checked','checked');

                    var inner_content = '<i class="bx bx-time"></i> Remove';

                    if(response.reply_type == 'generic')
                    {
                        $("#edit_generic_message_div").show();
                        $("#edit_filter_message_div").hide();
                        var i=1;
                        edit_content_counter = i;
                        var auto_reply_text_array_json = JSON.stringify(response.auto_reply_text);
                        auto_reply_text_array = JSON.parse(auto_reply_text_array_json,'true');
                        $("#edit_generic_message").val(auto_reply_text_array[0]['comment_reply']).click();
                        $("#edit_generic_message_private").val(auto_reply_text_array[0]['private_reply']).click();

                        /** Add generic reply textarea id into the emoji load div list***/
                        if(emoji_load_div_list == '')
                            emoji_load_div_list=emoji_load_div_list+"#edit_generic_message";
                        else
                            emoji_load_div_list=emoji_load_div_list+", #edit_generic_message";

                        // comment hide and delete section

                        $("#edit_generic_image_for_comment_reply_display").attr('src',auto_reply_text_array[0]['image_link']).show();
                        if(auto_reply_text_array[0]['image_link']=="")
                        {
                            $("#edit_generic_image_for_comment_reply_display").prev('span').removeClass('remove_media').html('');
                            $("#edit_generic_image_for_comment_reply_display").hide();
                        }
                        else
                            $("#edit_generic_image_for_comment_reply_display").prev('span').addClass('remove_media').html(inner_content);


                        var vidreplace='<source src="'+auto_reply_text_array[0]['video_link']+'" id="edit_generic_video_comment_reply_display" type="video/mp4">';
                        $("#edit_generic_video_comment_reply_display").parent().html(vidreplace).show();

                        if(auto_reply_text_array[0]['video_link']=='')
                        {
                            $("#edit_generic_video_comment_reply_display").parent().prev('span').removeClass('remove_media').html('');
                            $("#edit_generic_video_comment_reply_display").parent().hide();
                        }
                        else
                            $("#edit_generic_video_comment_reply_display").parent().prev('span').addClass('remove_media').html(inner_content);


                        $("#edit_generic_image_for_comment_reply").val(auto_reply_text_array[0]['image_link']);
                        $("#edit_generic_video_comment_reply").val(auto_reply_text_array[0]['video_link']);
                        // comment hide and delete section
                    }
                    else
                    {
                        var edit_nofilter_word_found_text = JSON.stringify(response.edit_nofilter_word_found_text);
                        edit_nofilter_word_found_text = JSON.parse(edit_nofilter_word_found_text,'true');
                        $("#edit_nofilter_word_found_text").val(edit_nofilter_word_found_text[0]['comment_reply']).click();
                        $("#edit_nofilter_word_found_text_private").val(edit_nofilter_word_found_text[0]['private_reply']).click();

                        /**Add no match found textarea into emoji load div list***/
                        if(emoji_load_div_list == '')
                            emoji_load_div_list=emoji_load_div_list+"#edit_nofilter_word_found_text";
                        else
                            emoji_load_div_list=emoji_load_div_list+", #edit_nofilter_word_found_text";

                        // comment hide and delete section

                        $("#edit_nofilter_image_upload_reply_display").attr('src',edit_nofilter_word_found_text[0]['image_link']).show();
                        if(edit_nofilter_word_found_text[0]['image_link']=="")
                        {
                            $("#edit_nofilter_image_upload_reply_display").prev('span').removeClass('remove_media').html('');
                            $("#edit_nofilter_image_upload_reply_display").hide();
                        }
                        else
                            $("#edit_nofilter_image_upload_reply_display").prev('span').addClass('remove_media').html(inner_content);


                        var vidreplace='<source src="'+edit_nofilter_word_found_text[0]['video_link']+'" id="edit_nofilter_video_upload_reply_display" type="video/mp4">';
                        $("#edit_nofilter_video_upload_reply_display").parent().html(vidreplace).show();

                        if(edit_nofilter_word_found_text[0]['video_link']=='')
                        {
                            $("#edit_nofilter_video_upload_reply_display").parent().prev('span').removeClass('remove_media').html('');
                            $("#edit_nofilter_video_upload_reply_display").parent().hide();
                        }
                        else
                            $("#edit_nofilter_video_upload_reply_display").parent().prev('span').addClass('remove_media').html(inner_content);


                        $("#edit_nofilter_image_upload_reply").val(edit_nofilter_word_found_text[0]['image_link']);
                        $("#edit_nofilter_video_upload_reply").val(edit_nofilter_word_found_text[0]['video_link']);
                        // comment hide and delete section

                        $("#edit_filter_message_div").show();
                        $("#edit_generic_message_div").hide();
                        var auto_reply_text_array = JSON.stringify(response.auto_reply_text);
                        auto_reply_text_array = JSON.parse(auto_reply_text_array,'true');

                        for(var i = 0; i < auto_reply_text_array.length; i++) {
                            var j = i+1;
                            $("#edit_filter_div_"+j).show();
                            $("#edit_filter_word_"+j).val(auto_reply_text_array[i]['filter_word']);
                            var unscape_reply_text = auto_reply_text_array[i]['reply_text'];
                            $("#edit_filter_message_"+j).val(unscape_reply_text).click();
                            // added by mostofa 25-04-2017
                            var unscape_comment_reply_text = auto_reply_text_array[i]['comment_reply_text'];
                            $("#edit_comment_reply_msg_"+j).val(unscape_comment_reply_text).click();

                            if(emoji_load_div_list == '')
                                emoji_load_div_list=emoji_load_div_list+"#edit_comment_reply_msg_"+j;
                            else
                                emoji_load_div_list=emoji_load_div_list+", #edit_comment_reply_msg_"+j;

                            // comment hide and delete section

                            $("#edit_filter_image_upload_reply_display_"+j).attr('src',auto_reply_text_array[i]['image_link']).show();
                            if(auto_reply_text_array[i]['image_link']=="")
                            {
                                $("#edit_filter_image_upload_reply_display_"+j).prev('span').removeClass('remove_media').html('');
                                $("#edit_filter_image_upload_reply_display_"+j).hide();
                            }
                            else
                                $("#edit_filter_image_upload_reply_display_"+j).prev('span').addClass('remove_media').html(inner_content);


                            var vidreplace='<source src="'+auto_reply_text_array[i]['video_link']+'" id="edit_filter_video_upload_reply_display'+j+'" type="video/mp4">';
                            $("#edit_filter_video_upload_reply_display"+j).parent().html(vidreplace).show();
                            if(auto_reply_text_array[i]['video_link']=='')
                            {
                                $("#edit_filter_video_upload_reply_display"+j).parent().prev('span').removeClass('remove_media').html('');
                                $("#edit_filter_video_upload_reply_display"+j).parent().hide();
                            }
                            else
                                $("#edit_filter_video_upload_reply_display"+j).parent().prev('span').addClass('remove_media').html(inner_content);

                            $("#edit_filter_image_upload_reply_"+j).val(auto_reply_text_array[i]['image_link']);
                            $("#edit_filter_video_upload_reply_"+j).val(auto_reply_text_array[i]['video_link']);
                            // comment hide and delete section
                        }

                        edit_content_counter = i+1;
                        $("#edit_content_counter").val(edit_content_counter);
                    }
                    $("#edit_auto_reply_message_modal").modal();
                }
            });


            setTimeout(function(){

                $(emoji_load_div_list).emojioneArea({
                    autocomplete: false,
                    pickerPosition: "bottom"
                });
            },2000);

            setTimeout(function(){

                $(".previewLoader").hide();

            },2000);



        });


        $(document).on('click','#edit_add_more_button',function(){
            if(edit_content_counter == 21)
                $("#edit_add_more_button").hide();
            $("#edit_content_counter").val(edit_content_counter);

            $("#edit_filter_div_"+edit_content_counter).show();

            /** Load Emoji For Filter Word when click on add more button during Edit**/


            $("#edit_comment_reply_msg_"+edit_content_counter).emojioneArea({
                autocomplete: false,
                pickerPosition: "bottom"
            });

            edit_content_counter++;

        });


        $(document).on('click','#edit_save_button',function(){

            var post_id = $("#edit_auto_reply_post_id").val();
            var edit_auto_campaign_name = $("#edit_auto_campaign_name").val();
            var reply_type = $("input[name=edit_message_type]:checked").val();
            var Youdidntselectanyoption = "<?php echo $Youdidntselectanyoption; ?>";
            var Youdidntprovideallinformation = "<?php echo $Youdidntprovideallinformation; ?>";
            if (typeof(reply_type)==='undefined')
            {
                swal.fire('<?php echo $this->lang->line("Warning"); ?>', Youdidntselectanyoption, 'warning');
                return false;
            }
            if(reply_type == 'generic')
            {
                if(edit_auto_campaign_name == ''){
                    swal.fire('<?php echo $this->lang->line("Warning"); ?>', Youdidntprovideallinformation, 'warning');
                    return false;
                }
            }
            else
            {
                if(edit_auto_campaign_name == ''){
                    swal.fire('<?php echo $this->lang->line("Warning"); ?>', Youdidntprovideallinformation, 'warning');
                    return false;
                }
            }

            $(this).addClass('btn-progress');

            var queryString = new FormData($("#edit_auto_reply_info_form")[0]);
            $.ajax({
                type:'POST' ,
                url: base_url+"comment_automation/ajax_update_autoreply_submit",
                data: queryString,
                dataType : 'JSON',
                // async: false,
                cache: false,
                contentType: false,
                processData: false,
                context: this,
                success:function(response){
                    $(this).removeClass('btn-progress');
                    if(response.status=="1")
                    {
                        swal.fire('<?php echo $this->lang->line("Success"); ?>', response.message, 'success').then((value) => {
                            $('#bot_list_select').val($('#bot_list_select').val()).trigger("change");
                            $("#edit_auto_reply_message_modal").modal('hide');
                            $("#pageresponse_edit_auto_reply_message_modal").modal('hide');
                            $("#create_label_edit_auto_reply").attr("page_id_for_label","");
                        });
                    }
                    else
                    {
                        swal.fire('<?php echo $this->lang->line("Error"); ?>', response.message, 'error');
                    }
                }

            });

        });


        $(document).on('change','input[name=edit_message_type]',function(){
            if($("input[name=edit_message_type]:checked").val()=="generic")
            {
                $("#edit_generic_message_div").show();
                $("#edit_filter_message_div").hide();

            }
            else
            {
                $("#edit_generic_message_div").hide();
                $("#edit_filter_message_div").show();


                /*** Load Emoji When Filter word click during Edit , by defualt first textarea are loaded & No match found field****/

                $("#edit_comment_reply_msg_1, #edit_nofilter_word_found_text").emojioneArea({
                    autocomplete: false,
                    pickerPosition: "bottom"
                });
            }
        });



        // start comment tag machine section
        $(document).on('click','.sync_commenter_info',function(){
            var page_id = $(this).attr('page_table_id');
            var post_id = $(this).attr('post_id');
            var post_description = $(this).attr('post-description');
            var post_created_at = $(this).attr('post-created-at');
            var Pleaseprovidepostid = "<?php echo $Pleaseprovidepostid; ?>";

            if(typeof(post_id) === 'undefined' || post_id == '')
            {
                swal.fire('<?php echo $this->lang->line("Warning"); ?>', Pleaseprovidepostid, 'warning');
                return false;
            }
            var button_id=page_id+"-"+post_id;
            $("#"+button_id).addClass('disabled');


            $(this).addClass('btn-progress');

            $.ajax({
                type:'POST' ,
                url:"<?php echo site_url();?>comment_reply_enhancers/sync_commenter_info",
                data:{page_id:page_id,post_id:post_id,post_description:post_description,post_created_at:post_created_at},
                dataType:'JSON',
                context: this,
                success:function(response)
                {
                    $(this).removeClass('btn-progress');

                    if(response.status=='1')
                    {
                        var success_message=response.message;
                        var span = document.createElement("span");
                        span.innerHTML = success_message;

                        swal.fire({title:'<?php echo $this->lang->line("Success"); ?>', html:span, icon:'success'}).then((value) => {
                            $('#bot_list_select').val($('#bot_list_select').val()).trigger("change");
                            $("#manual_reply_by_post").modal('hide');
                            $("#manual_check_button_div").html('');
                        });
                        $("#"+button_id).removeClass('blue');
                        $("#"+button_id).removeClass('sync_commenter_info');
                        $("#"+button_id).html(response.button_replace);
                    }
                    else
                    {
                        swal.fire('<?php echo $this->lang->line("Error"); ?>', response.message, 'error');
                        $("#"+button_id).removeClass('disabled');
                    }
                }
            });

        });
        // end comment tag machine section

        $(document).on('click','.remove_media',function(){
            $(this).parent().prev('input').val('');
            $(this).parent().hide();
        });

    });
</script>

<script>
    $("document").ready(function(){

        $(document).on('click','.instant_comment',function(){
            var page_table_id = $('#bot_list_select').val();
            $("#instant_comment_page_id").val(page_table_id);
            var post_id = $(this).attr('post_id');
            $("#instant_comment_post_id").val(post_id);
            $("#instant_comment_message").val('');
            $("#instant_comment_modal").modal();
        });

        $(document).on('click','.submit_instant_comment',function(){
            var page_table_id = $("#instant_comment_page_id").val();
            var post_id = $("#instant_comment_post_id").val();
            var message = $("#instant_comment_message").val();
            if(message == '')
            {
                swal.fire('<?php echo $this->lang->line("Warning"); ?>', '<?php echo $this->lang->line("Please provide your comment first."); ?>', 'warning');
                return false;
            }

            $(this).addClass('btn-progress');

            $.ajax({
                context: this,
                type:'POST' ,
                url:"<?php echo site_url();?>comment_automation/instant_commnet_submit",
                data: {page_table_id:page_table_id,post_id:post_id,message:message},
                dataType: 'json',
                success:function(response){
                    if(response.status == 1)
                    {
                        $(this).removeClass('btn-progress');
                        var span = document.createElement("span");
                        span.innerHTML = response.message;
                        swal.fire({ title:'<?php echo $this->lang->line("Success"); ?>', html:span,icon:'success'}).then((value) => {
                            $("#instant_comment_modal").modal('hide');
                        });
                    }
                    else
                    {
                        swal.fire('<?php echo $this->lang->line("Error!"); ?>', response.message, 'error');
                    }
                },
                error:function(response){
                    var span = document.createElement("span");
                    span.innerHTML = response.responseText;
                    swal.fire({ title:'<?php echo $this->lang->line("Error!"); ?>', html:span,icon:'error'});
                }
            });


        });

        $("#auto_reply_template").select2({width: "100%"});

        $("#filemanager_close").click(function(){
            $("#modal-live-video-library").removeClass('modal');
        });

        $(document).on('click','.cancel_button',function(){
            $("#pageresponse_auto_reply_message_modal").modal('hide');
            $("#pageresponse_edit_auto_reply_message_modal").modal('hide');
            $("#edit_auto_reply_message_modal").modal('hide');
            $("#auto_reply_message_modal_template").modal('hide');
            $("#edit_auto_reply_message_modal_template").modal('hide');
            $('#bot_list_select').val($('#bot_list_select').val()).trigger("change");
        });

        // ===== auto click to the page name box to trigger click event [click function is in top of the javascript code] =========//
        var session_value = "<?php echo $this->session->userdata('get_page_details_page_table_id'); ?>";
        if(session_value==''){

            $('#bot_list_select').val($('#bot_list_select').val()).trigger("change");
        }
        else
        $('#bot_list_select').val(session_value).trigger("change");



        // ================== use saved template or not =================================
        if($("input[name=auto_template_selection]:checked").val()=="yes")
        {
            $("#auto_reply_templates_section").show();
            $("#new_template_section").hide();
            $("#save_and_create").hide();
        }
        else
        {
            $("#auto_reply_templates_section").hide();
            $("#new_template_section").show();
            $("#save_and_create").show();
        }

        $(document).on('change','input[name=auto_template_selection]',function(){
            if($("input[name=auto_template_selection]:checked").val()=="yes")
            {
                $("#auto_reply_templates_section").show();
                $("#new_template_section").hide();
                $("#save_and_create").hide();

            }
            else
            {
                $("#auto_reply_templates_section").hide();
                $("#new_template_section").show();
                $("#save_and_create").show();

            }
        });
        // ========================= end of use saved template or not =================

        var image_upload_limit = "<?php echo $image_upload_limit; ?>";
        var video_upload_limit = "<?php echo $video_upload_limit; ?>";

        var base_url="<?php echo site_url(); ?>";
        var user_id = "<?php echo $this->session->userdata('user_id'); ?>";
        <?php for($k=1;$k<=20;$k++) : ?>
        $("#edit_filter_video_upload_<?php echo $k; ?>").uploadFile({
            url:base_url+"comment_automation/upload_live_video",
            fileName:"myfile",
            maxFileSize:video_upload_limit*1024*1024,
            uploadButtonClass: 'btn btn-sm btn-primary mr-10',
            showPreview:false,
            returnType: "json",
            dragDrop: true,
            showDelete: true,
            multiple:false,
            maxFileCount:1,
            acceptFiles:".flv,.mp4,.wmv,.WMV,.MP4,.FLV",
            deleteCallback: function (data, pd) {
                var delete_url="<?php echo site_url('comment_automation/delete_uploaded_live_file');?>";
                $.post(delete_url, {op: "delete",name: data},
                    function (resp,textStatus, jqXHR) {
                        $("#edit_filter_video_upload_reply_<?php echo $k; ?>").val('');
                    });

            },
            onSuccess:function(files,data,xhr,pd)
            {
                var file_path = base_url+"upload/video/"+data;
                $("#edit_filter_video_upload_reply_<?php echo $k; ?>").val(file_path);
            }
        });


        $("#edit_filter_image_upload_<?php echo $k; ?>").uploadFile({
            url:base_url+"comment_automation/upload_image_only",
            fileName:"myfile",
            maxFileSize:image_upload_limit*1024*1024,
            uploadButtonClass: 'btn btn-sm btn-primary mr-10',
            showPreview:false,
            returnType: "json",
            dragDrop: true,
            showDelete: true,
            multiple:false,
            maxFileCount:1,
            acceptFiles:".png,.jpg,.jpeg,.JPEG,.JPG,.PNG,.gif,.GIF",
            deleteCallback: function (data, pd) {
                var delete_url="<?php echo site_url('comment_automation/delete_uploaded_file');?>";
                $.post(delete_url, {op: "delete",name: data},
                    function (resp,textStatus, jqXHR) {
                        $("#edit_filter_image_upload_reply_<?php echo $k; ?>").val('');
                    });

            },
            onSuccess:function(files,data,xhr,pd)
            {
                var data_modified = base_url+"upload/image/"+user_id+"/"+data;
                $("#edit_filter_image_upload_reply_<?php echo $k; ?>").val(data_modified);
            }
        });
        <?php endfor; ?>

        <?php for($k=1;$k<=20;$k++) : ?>
        $("#filter_video_upload_<?php echo $k; ?>").uploadFile({
            url:base_url+"comment_automation/upload_live_video",
            fileName:"myfile",
            maxFileSize:video_upload_limit*1024*1024,
            uploadButtonClass: 'btn btn-sm btn-primary mr-10',
            showPreview:false,
            returnType: "json",
            dragDrop: true,
            showDelete: true,
            multiple:false,
            maxFileCount:1,
            acceptFiles:".flv,.mp4,.wmv,.WMV,.MP4,.FLV",
            deleteCallback: function (data, pd) {
                var delete_url="<?php echo site_url('comment_automation/delete_uploaded_live_file');?>";
                $.post(delete_url, {op: "delete",name: data},
                    function (resp,textStatus, jqXHR) {
                        $("#filter_video_upload_reply_<?php echo $k; ?>").val('');
                    });

            },
            onSuccess:function(files,data,xhr,pd)
            {
                var file_path = base_url+"upload/video/"+data;
                $("#filter_video_upload_reply_<?php echo $k; ?>").val(file_path);
            }
        });


        $("#filter_image_upload_<?php echo $k; ?>").uploadFile({
            url:base_url+"comment_automation/upload_image_only",
            fileName:"myfile",
            maxFileSize:image_upload_limit*1024*1024,
            uploadButtonClass: 'btn btn-sm btn-primary mr-10',
            showPreview:false,
            returnType: "json",
            dragDrop: true,
            showDelete: true,
            multiple:false,
            maxFileCount:1,
            acceptFiles:".png,.jpg,.jpeg,.JPEG,.JPG,.PNG,.gif,.GIF",
            deleteCallback: function (data, pd) {
                var delete_url="<?php echo site_url('comment_automation/delete_uploaded_file');?>";
                $.post(delete_url, {op: "delete",name: data},
                    function (resp,textStatus, jqXHR) {
                        $("#filter_image_upload_reply_<?php echo $k; ?>").val('');
                    });

            },
            onSuccess:function(files,data,xhr,pd)
            {
                var data_modified = base_url+"upload/image/"+user_id+"/"+data;
                $("#filter_image_upload_reply_<?php echo $k; ?>").val(data_modified);
            }
        });
        <?php endfor; ?>

        $("#generic_video_upload").uploadFile({
            url:base_url+"comment_automation/upload_live_video",
            fileName:"myfile",
            maxFileSize:video_upload_limit*1024*1024,
            uploadButtonClass: 'btn btn-sm btn-primary mr-10',
            showPreview:false,
            returnType: "json",
            dragDrop: true,
            showDelete: true,
            multiple:false,
            maxFileCount:1,
            acceptFiles:".flv,.mp4,.wmv,.WMV,.MP4,.FLV",
            deleteCallback: function (data, pd) {
                var delete_url="<?php echo site_url('comment_automation/delete_uploaded_live_file');?>";
                $.post(delete_url, {op: "delete",name: data},
                    function (resp,textStatus, jqXHR) {
                        $("#generic_video_comment_reply").val('');
                    });

            },
            onSuccess:function(files,data,xhr,pd)
            {
                var file_path = base_url+"upload/video/"+data;
                $("#generic_video_comment_reply").val(file_path);
            }
        });


        $("#generic_comment_image").uploadFile({
            url:base_url+"comment_automation/upload_image_only",
            fileName:"myfile",
            maxFileSize:image_upload_limit*1024*1024,
            uploadButtonClass: 'btn btn-sm btn-primary mr-10',
            showPreview:false,
            returnType: "json",
            dragDrop: true,
            showDelete: true,
            multiple:false,
            maxFileCount:1,
            acceptFiles:".png,.jpg,.jpeg,.JPEG,.JPG,.PNG,.gif,.GIF",
            deleteCallback: function (data, pd) {
                var delete_url="<?php echo site_url('comment_automation/delete_uploaded_file');?>";
                $.post(delete_url, {op: "delete",name: data},
                    function (resp,textStatus, jqXHR) {
                        $("#generic_image_for_comment_reply").val('');
                    });

            },
            onSuccess:function(files,data,xhr,pd)
            {
                var data_modified = base_url+"upload/image/"+user_id+"/"+data;
                $("#generic_image_for_comment_reply").val(data_modified);
            }
        });


        $("#nofilter_video_upload").uploadFile({
            url:base_url+"comment_automation/upload_live_video",
            fileName:"myfile",
            maxFileSize:video_upload_limit*1024*1024,
            uploadButtonClass: 'btn btn-sm btn-primary mr-10',
            showPreview:false,
            returnType: "json",
            dragDrop: true,
            showDelete: true,
            multiple:false,
            maxFileCount:1,
            acceptFiles:".flv,.mp4,.wmv,.WMV,.MP4,.FLV",
            deleteCallback: function (data, pd) {
                var delete_url="<?php echo site_url('comment_automation/delete_uploaded_live_file');?>";
                $.post(delete_url, {op: "delete",name: data},
                    function (resp,textStatus, jqXHR) {
                        $("#nofilter_video_upload_reply").val('');
                    });

            },
            onSuccess:function(files,data,xhr,pd)
            {
                var file_path = base_url+"upload/video/"+data;
                $("#nofilter_video_upload_reply").val(file_path);
            }
        });


        $("#nofilter_image_upload").uploadFile({
            url:base_url+"comment_automation/upload_image_only",
            fileName:"myfile",
            maxFileSize:image_upload_limit*1024*1024,
            uploadButtonClass: 'btn btn-sm btn-primary mr-10',
            showPreview:false,
            returnType: "json",
            dragDrop: true,
            showDelete: true,
            multiple:false,
            maxFileCount:1,
            acceptFiles:".png,.jpg,.jpeg,.JPEG,.JPG,.PNG,.gif,.GIF",
            deleteCallback: function (data, pd) {
                var delete_url="<?php echo site_url('comment_automation/delete_uploaded_file');?>";
                $.post(delete_url, {op: "delete",name: data},
                    function (resp,textStatus, jqXHR) {
                        $("#nofilter_image_upload_reply").val('');
                    });

            },
            onSuccess:function(files,data,xhr,pd)
            {
                var data_modified = base_url+"upload/image/"+user_id+"/"+data;
                $("#nofilter_image_upload_reply").val(data_modified);
            }
        });

        $("#edit_generic_video_upload").uploadFile({
            url:base_url+"comment_automation/upload_live_video",
            fileName:"myfile",
            maxFileSize:video_upload_limit*1024*1024,
            uploadButtonClass: 'btn btn-sm btn-primary mr-10',
            showPreview:false,
            returnType: "json",
            dragDrop: true,
            showDelete: true,
            multiple:false,
            maxFileCount:1,
            acceptFiles:".flv,.mp4,.wmv,.WMV,.MP4,.FLV",
            deleteCallback: function (data, pd) {
                var delete_url="<?php echo site_url('comment_automation/delete_uploaded_live_file');?>";
                $.post(delete_url, {op: "delete",name: data},
                    function (resp,textStatus, jqXHR) {
                        $("#edit_generic_video_comment_reply").val('');
                    });

            },
            onSuccess:function(files,data,xhr,pd)
            {
                var file_path = base_url+"upload/video/"+data;
                $("#edit_generic_video_comment_reply").val(file_path);
            }
        });


        $("#edit_generic_comment_image").uploadFile({
            url:base_url+"comment_automation/upload_image_only",
            fileName:"myfile",
            maxFileSize:image_upload_limit*1024*1024,
            uploadButtonClass: 'btn btn-sm btn-primary mr-10',
            showPreview:false,
            returnType: "json",
            dragDrop: true,
            showDelete: true,
            multiple:false,
            maxFileCount:1,
            acceptFiles:".png,.jpg,.jpeg,.JPEG,.JPG,.PNG,.gif,.GIF",
            deleteCallback: function (data, pd) {
                var delete_url="<?php echo site_url('comment_automation/delete_uploaded_file');?>";
                $.post(delete_url, {op: "delete",name: data},
                    function (resp,textStatus, jqXHR) {
                        $("#edit_generic_image_for_comment_reply").val('');
                    });

            },
            onSuccess:function(files,data,xhr,pd)
            {
                var data_modified = base_url+"upload/image/"+user_id+"/"+data;
                $("#edit_generic_image_for_comment_reply").val(data_modified);
            }
        });


        $("#edit_nofilter_video_upload").uploadFile({
            url:base_url+"comment_automation/upload_live_video",
            fileName:"myfile",
            maxFileSize:video_upload_limit*1024*1024,
            uploadButtonClass: 'btn btn-sm btn-primary mr-10',
            showPreview:false,
            returnType: "json",
            dragDrop: true,
            showDelete: true,
            multiple:false,
            maxFileCount:1,
            acceptFiles:".flv,.mp4,.wmv,.WMV,.MP4,.FLV",
            deleteCallback: function (data, pd) {
                var delete_url="<?php echo site_url('comment_automation/delete_uploaded_live_file');?>";
                $.post(delete_url, {op: "delete",name: data},
                    function (resp,textStatus, jqXHR) {
                        $("#edit_nofilter_video_upload_reply").val('');
                    });

            },
            onSuccess:function(files,data,xhr,pd)
            {
                var file_path = base_url+"upload/video/"+data;
                $("#edit_nofilter_video_upload_reply").val(file_path);
            }
        });


        $("#edit_nofilter_image_upload").uploadFile({
            url:base_url+"comment_automation/upload_image_only",
            fileName:"myfile",
            maxFileSize:image_upload_limit*1024*1024,
            uploadButtonClass: 'btn btn-sm btn-primary mr-10',
            showPreview:false,
            returnType: "json",
            dragDrop: true,
            showDelete: true,
            multiple:false,
            maxFileCount:1,
            acceptFiles:".png,.jpg,.jpeg,.JPEG,.JPG,.PNG,.gif,.GIF",
            deleteCallback: function (data, pd) {
                var delete_url="<?php echo site_url('comment_automation/delete_uploaded_file');?>";
                $.post(delete_url, {op: "delete",name: data},
                    function (resp,textStatus, jqXHR) {
                        $("#edit_nofilter_image_upload_reply").val('');
                    });

            },
            onSuccess:function(files,data,xhr,pd)
            {
                var data_modified = base_url+"upload/image/"+user_id+"/"+data;
                $("#edit_nofilter_image_upload_reply").val(data_modified);
            }
        });

        // ============ pageresponse file upload section ==============//

        // ============ end of pageresponse file upload section ==============//

    });
</script>


<script type="text/javascript">
    $("document").ready(function(){
        var base_url = "<?php echo base_url(); ?>";

        $('.modal').on("hidden.bs.modal", function (e) {
            if ($('.modal:visible').length) {
                $('body').addClass('modal-open');
            }
        });

        $(document).on('click','.add_template',function(e){
            e.preventDefault();
            var current_id=$(this).prev().prev().attr('id');
            var current_val=$(this).prev().prev().val();
            var page_id = get_page_id();
            if(page_id=="")
            {
                swal.fire('<?php echo $this->lang->line("Error"); ?>', "<?php echo $this->lang->line('Please select a page first')?>", 'error');
                return false;
            }
            $("#add_template_modal").attr("current_id",current_id);
            $("#add_template_modal").attr("current_val",current_val);
            $("#add_template_modal").modal();
        });

        $(document).on('click','.ref_template',function(e){
            e.preventDefault();
            var current_val=$(this).prev().prev().prev().val();
            var current_id=$(this).prev().prev().prev().attr('id');
            var page_id = get_page_id();
            if(page_id=="")
            {
                swal.fire('<?php echo $this->lang->line("Error"); ?>', "<?php echo $this->lang->line('Please select a page first')?>", 'error');
                return false;
            }
            $.ajax({
                type:'POST',
                url: base_url+"comment_automation/get_private_reply_postbacks",
                data: {page_table_ids:page_id},
                dataType: 'JSON',
                success:function(response){
                    $("#"+current_id).html(response.options).val(current_val);
                }
            });
        });

        $('#add_template_modal').on('hidden.bs.modal', function (e) {
            var current_id=$("#add_template_modal").attr("current_id");
            var current_val=$("#add_template_modal").attr("current_val");
            var page_id = get_page_id();
            if(page_id=="")
            {
                swal.fire('<?php echo $this->lang->line("Error"); ?>', "<?php echo $this->lang->line('Please select a page first')?>", 'error');
                return false;
            }
            $.ajax({
                type:'POST' ,
                url: base_url+"comment_automation/get_private_reply_postbacks",
                data: {page_table_ids:page_id,is_from_add_button:'1'},
                dataType: 'JSON',
                success:function(response){
                    $("#"+current_id).html(response.options);
                }
            });
        });

        // getting postback list and making iframe
        $('#add_template_modal').on('shown.bs.modal',function(){
            var page_id = get_page_id();
            var iframe_link="<?php echo base_url('messenger_bot/create_new_template/1/');?>"+page_id;
            $(this).find('iframe').attr('src',iframe_link);
        });
        // getting postback list and making iframe

    });

    function get_page_id()
    {
        var page_id = $("#dynamic_page_id").val();
        return page_id;
    }


</script>




<!-- =========== pageresponse modal section ============= -->
<?php include(FCPATH.'application/n_views/comment_automation/pageresponse_modal_section.php'); ?>
<!-- =========== end of pageresponse modal section ============= -->


<!-- =========== pageresponse likeshare section ============= -->
<?php include(FCPATH.'application/n_views/comment_automation/pageresponse_likeshare_section.php'); ?>
<!-- =========== end of pageresponse likeshare section ============= -->

<!-- start of auto comment modal section -->
<?php include(FCPATH.'application/n_views/comment_automation/autocomment_modal_section.php'); ?>
<!-- end of auto comment modal section -->

<!-- comment hide and delete section -->
<!-- =========== pageresponse javascript section =============  -->
<?php include(FCPATH.'application/n_views/comment_automation/pageresponse_javascript_section.php'); ?>
<!-- =========== end of pageresponse javascript section =============  -->
<!-- start of auto comment javascript section -->
<?php include(FCPATH.'application/n_views/comment_automation/autocomment_javascript_section.php'); ?>
<!-- end of auto comment javascript section -->


<!-- postback add/refresh button section -->
<div class="modal fade" id="add_template_modal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title"><i class="bx bx-plus-circle"></i> <?php echo $this->lang->line('Add Template'); ?></h3>
                <button type="button" class="close rounded-pill" data-dismiss="modal" aria-label="Close"><i class="bx bx-x"></i></button>
            </div>
            <div class="modal-body">
                <iframe src="" frameborder="0" width="100%" ></iframe>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" type="button" class="btn-lg btn btn-dark"><i class="bx bx-sync"></i> <span class="align-middle ml-25"><?php echo $this->lang->line("Close & Refresh List"); ?></span></button>
            </div>
        </div>
    </div>
</div>
