<?php
$include_upload=1;  //upload_js
$include_datatable=0; //datatables
$include_datetimepicker=1; //datetimepicker, daterange, moment
$include_emoji=1;
$include_summernote=0;
$include_colorpicker=0;
$include_select2=0;
$include_jqueryui=0;
$include_mCustomScrollBar=1;
$include_dropzone=0;
$include_tagsinput=0;
$include_alertify=0;
$include_morris=0;
$include_chartjs=0;
$include_owlcar=0;
$include_prism=0;
?>


<?php
//todo: 0000000 before release

	if(ultraresponse_addon_module_exist())	$commnet_hide_delete_addon = 1;
	else $commnet_hide_delete_addon = 0;

	if(addon_exist(201,"comment_reply_enhancers")) $comment_tag_machine_addon = 1;
	else $comment_tag_machine_addon = 0;

	$image_upload_limit = 1; 
	if($this->config->item('autoreply_image_upload_limit') != '')
	$image_upload_limit = $this->config->item('autoreply_image_upload_limit'); 

	$video_upload_limit = 3; 
	if($this->config->item('autoreply_video_upload_limit') != '')
	$video_upload_limit = $this->config->item('autoreply_video_upload_limit');			
?>

<style type="text/css">
	.button-outline
	{
	  background: #fff;
	  border: .5px dashed #ccc;
	}
	.button-outline:hover
	{
	  border: 1px dashed #6777EF !important;
	  cursor: pointer;
	}
	.multi_layout{margin:0;background: #fff}
	.multi_layout .card{margin-bottom:0;border-radius: 0;}
	.multi_layout p, .multi_layout ul:not(.list-unstyled), .multi_layout ol{line-height: 15px;}
	.multi_layout .list-group li{padding: 15px 10px 12px 25px;}
	.multi_layout{border:.5px solid #dee2e6;}
	.multi_layout .collef,.multi_layout .colmid,.multi_layout .colrig{padding-left: 0px; padding-right: 0px;}
	.multi_layout .collef,.multi_layout .colmid{border-right: .5px solid #dee2e6;}
	.multi_layout .main_card{min-height: 500px;box-shadow: none;}
	.multi_layout .collef .makeScroll{max-height: 640px;overflow:auto;}
	.multi_layout .colrig .makeScroll{max-height: 605px;overflow:auto;}
	.multi_layout .list-group .list-group-item{border-radius: 0;border:.5px solid #dee2e6;border-left:none;border-right:none;cursor: pointer;z-index: 0;}
	.multi_layout .list-group .list-group-item:first-child{border-top:none;}
	.multi_layout .list-group .list-group-item:last-child{border-bottom:none;}
	.multi_layout .list-group .list-group-item.active{border:.5px solid #6777EF;}
	.multi_layout .mCSB_inside > .mCSB_container{margin-right: 0;}
	.multi_layout .card-statistic-1{border-radius: 0;}
	.multi_layout h6.page_name{font-size: 14px;}
	.multi_layout .card .card-header input{max-width: 100% !important;}
	.multi_layout .media-title{font-size: 13px;}
	.multi_layout .media-body{padding-left: 15px;}
	.multi_layout .media-body .small{font-size: 10px;color:#000;margin-top:12px;}
	.multi_layout .summary .summary-item{margin-top: 0;}
	.multi_layout .card-primary{margin-top: 35px;margin-bottom: 15px;}
	.multi_layout .product-details .product-name{font-size: 12px;}
	.multi_layout .set_cam_by_post:after {content: none !important;}
	.multi_layout .colrig .media {padding-bottom: 0;}
	.multi_layout .list-unstyled-border li {border-bottom: none;}
	.multi_layout .colmid .card-body {padding: 12px 10px;}
	.multi_layout .colrig .card-body {padding: 12px 20px;}

	.multi_layout .waiting,.modal_waiting {height: 100%;width:100%;display: table;}
    .multi_layout .waiting i,.modal_waiting i{font-size:60px;display: table-cell; vertical-align: middle;padding:30px 0;}

    .multi_layout .card .card-header h4 a{font-weight: 700 !important;}

    .smallspace{padding: 10px 0;}
    .lead_first_name,.lead_last_name,.lead_tag_name{background: #fff !important;}
    .ajax-file-upload-statusbar{width: 100% !important;}
    hr{
       margin-top: 10px;
    }

    .custom-top-margin{
      margin-top: 20px;
    }

    .sync_page_style{
       margin-top: 8px;
    }
    /* .wrapper,.content-wrapper{background: #fafafa !important;} */
    .well{background: #fff;}
    
    .emojionearea, .emojionearea.form-control
    {
    	height: 140px !important;
    }


    .emojionearea.small-height
    {
    	height: 140px !important;
    }

    /*ntheme*/
    .media{margin-bottom:20px;}
    .avatar-item{position: relative;}
    .avatar-item img{border-radius: 50%;}
    .avatar-item i{margin-top: 4px;}
    .avatar-item .dropdown{cursor:pointer;
        position: absolute;
        bottom: -5px;
        right: 0;
        background-color: #fff;
        color: #000;
        box-shadow: 0 4px 8px rgb(0 0 0 / 3%);
        border-radius: 50%;
        text-align: center;
        line-height: 25px;
        width: 25px;
        height: 25px;}
    .product-item{text-align: center;}
    #middle_column .btn{padding: 7px 23px !important;}
    #right_column .dropdown-menu{}
    #right_column .mCSB_container{height:100%!important;min-height: 500px!important;}
    #store_list_field .select2-container{width:80%!important;}
    .product-image img{max-width:80px;}

    #right_column ul{min-height: 350px!important;}


    #middle_column .media-body .badge {
        float: right;
        height: 50px;
        width: 50px;
        border-radius: 50%;
        line-height: 39px;
        font-size: 1rem;
        background-color: rgba(90,141,238,.17);
        color: #5A8DEE!important;
        margin-top: 0px;
    }

    #middle_column .media-title {font-size:1.1rem; font-weight: 500;}


</style>


<div class="content-header row">
    <div class="content-header-left col-12 mb-2 mt-1">
        <div class="breadcrumbs-top">
            <h5 class="content-header-title float-left pr-1 mb-0"><?php echo $page_title; ?></h5>
            <div class="breadcrumb-wrapper d-none d-sm-block rounded">
                <ol class="breadcrumb p-0 mb-0 pl-1">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard"><i class="bx bx-home-alt"></i></a></li>
                    <li class="breadcrumb-item active"><?php echo $this->lang->line("Comment Automation");?></li>
                </ol>
            </div>
        </div>
    </div>
</div>

<?php if(!empty($page_info)){ ?>
    <fieldset class="form-group width-450" id="store_list_field">
        <div class="input-group">
            <div class="input-group-prepend">
                <label class="input-group-text" for="bot_list_select"><?php echo $this->lang->line("PAGES");?></label>
            </div>
            <select class="form-control select2" id="bot_list_select">

                <?php $i=0;
                $current_store_data =  array();
                foreach($page_info as $value)
                {
                    if($value['id']==$this->session->userdata("ecommerce_selected_store")) $current_store_data = $value;

                    ?>
                    <option value="<?php echo $value['id']; ?>" <?php if($i==0) echo 'selected'; ?>><?php echo $value['page_name']; ?></option>

                    <?php $i++;
                } ?>
            </select>
        </div>
    </fieldset>
<?php } ?>



<?php if(empty($page_info))
{ ?>
	 
<div class="card" id="nodata">
  <div class="card-body">
    <div class="empty-state">
      <img class="img-fluid" style="height: 200px" src="<?php echo base_url('assets/img/drawkit/drawkit-nature-man-colour.svg'); ?>" alt="image">
      <h2 class="mt-0"><?php echo $this->lang->line("We could not find any page.");?></h2>
      <p class="lead"><?php echo $this->lang->line("Please import account if you have not imported yet.")."<br>".$this->lang->line("If you have already imported account then enable bot connection for one or more page to continue.") ?></p>
      <a href="<?php echo base_url('social_accounts'); ?>" class="btn btn-outline-primary mt-4"><i class="bx b"></i> <?php echo $this->lang->line("Continue");?></a>
    </div>
  </div>
</div>

<?php 
}
else
{ ?>
	

	<?php if(file_exists(APPPATH.'show_comment_automation_message.txt') && $this->session->userdata('user_type') == 'Admin') : ?>
	<div class="row">
		<div class="col-12">
			<div class="alert alert-light alert-has-icon  alert-dismissible show fade" style="margin-bottom:30px"  id="hide_comment_automation_message" >
	          <div class="alert-icon"><i class="bx bx-paperclip"></i></div>
	          <div class="alert-body">
	          	 <button class="close" data-dismiss="alert" data-toggle="tooltip" title="<?php echo $this->lang->line("Close this mesage forever"); ?>">
	              	<span>×</span>
	             </button>
	            <div class="alert-title"><?php echo $this->lang->line("Hello admin, please read me first"); ?></div>
	            <?php echo $this->lang->line("Comment automation features will not work until your Facebook app is fully approved and is in live mode."); ?>
	          </div>
	        </div>
		</div>
	</div>
	<?php endif; ?>

	<div class="row multi_layout">



		<div class="col-12 col-md-7 col-lg-4 colmid shadow-none" id="middle_column">
			
		</div>

		<div class="col-12 col-md-12 col-lg-8 colrig shadow-none" id="right_column">

	    </div>
		
	</div>

<?php } ?>


<?php 
	$Youdidntprovideallinformation = $this->lang->line("you didn\'t provide all information.");
	$Pleaseprovidepostid = $this->lang->line("please provide post id.");
	$Youdidntselectanytemplate = $this->lang->line("you have not select any template.");
	$Youdidntselectanyoptionyet = $this->lang->line("you have not select any option yet.");
	$Youdidntselectanyoption = $this->lang->line("you have not select any option.");
	
	$AlreadyEnabled = $this->lang->line("already enabled");
	$ThispostIDisnotfoundindatabaseorthispostIDisnotassociatedwiththepageyouareworking = $this->lang->line("This post ID is not found in database or this post ID is not associated with the page you are working.");
	$EnableAutoReply = $this->lang->line("enable auto reply");
	$TypeAutoCampaignname = $this->lang->line("You have not Type auto campaign name");
	$YouDidnotchosescheduleType = $this->lang->line("You have not choose any schedule type");
	$YouDidnotchosescheduletime = $this->lang->line("You have not select any schedule time");
	$YouDidnotchosescheduletimezone = $this->lang->line("You have not select any time zone");
	$YoudidnotSelectPerodicTime = $this->lang->line("You have not select any periodic time");
	$YoudidnotSelectCampaignStartTime = $this->lang->line("You have not choose campaign start time");
	$YoudidnotSelectCampaignEndTime = $this->lang->line("You have not choose campaign end time");

 ?>



<div class="modal fade" id="post_synch_modal" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg" style="width:100%">
		<div class="modal-content">
			<div class="modal-header">
			  <h3 class="modal-title"><i class="bx bx-poll"></i> <?php echo $this->lang->line("latest post for page") ?> - <span id="page_name_div"></span></h3>
			  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<i class="bx bx-x"></i>
			  </button>
			</div>

			<div class="modal-body" id="post_synch_modal_body">

			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="auto_reply_message_modal" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg"  style="min-width: 70%;">
		<div class="modal-content">

			<div class="modal-header">
			  <h3 class="modal-title" style="padding: 10px 20px 10px 20px;" ><?php echo $this->lang->line("Please give the following information for post auto reply") ?></h3>
			  <button type="button" class="close" id='modal_close'  aria-label="Close">
				<i class="bx bx-x"></i>
			  </button>
			</div>

			<form action="#" id="auto_reply_info_form" method="post">
				<input type="hidden" name="auto_reply_page_id" id="auto_reply_page_id" value="">
				<input type="hidden" name="auto_reply_post_id" id="auto_reply_post_id" value="">
				<input type="hidden" name="auto_reply_post_permalink" id="auto_reply_post_permalink" value="">
				<input type="hidden" name="manual_enable" id="manual_enable" value="">

				<div class="modal-body" id="auto_reply_message_modal_body">
					<!-- use saved template yes or no(new)  -->
					<br/>
					<div class="row" style="padding-left: 20px; padding-right: 20px;">
						<div class="col-12 col-md-6">
							<label><i class="bx bx-list-ul"></i> <?php echo $this->lang->line("do you want to use saved template?") ?>
								<a href="#" data-placement="bottom"  data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("message") ?>" data-content="<?php echo $this->lang->line("If you want to set campaign from previously saved template, then keep 'Yes' & select from below select option. If you want to add new settings, then select 'NO' , then auto reply settings form will come."); ?>"><i class='bx bx-info-circle'></i> </a>
							</label>
						</div>
						<div class="col-12 col-md-6">
						  <div class="form-group">
							<div class="custom-control custom-switch custom-control-inline">
							  <input type="checkbox" name="auto_template_selection" value="yes" id="template_select" class="custom-control-input" checked>
							  <label class="custom-control-label mr-1" for="template_select"></label>
							  <span><?php echo $this->lang->line('Yes');?></span>
							</div>
						  </div>
						</div>
					</div>

					<?php
					  $is_broadcaster_exist=false;
					  if($this->is_broadcaster_exist)
					  {
					      $is_broadcaster_exist=true;
					  }
				      $popover='<a href="#" data-placement="bottom"  data-toggle="popover" data-trigger="focus" title="'.$this->lang->line("Choose Labels").'" data-content="'.$this->lang->line("If you choose labels, then when user comment on the post & get private reply in their inbox , they will be added in those labels, that will help you to segment your leads & broadcasting from Messenger Broadcaster. If you don`t want to add labels for this post comment , then just keep it blank as it is.
Add label will only work once private reply is setup.  And you will need to sync subscribers later to update subscriber information. In this way the subscriber will not eligible for BOT subscriber until they reply back in messenger.").'"><i class="bx bx-info-circle"></i> </a>';
				      echo '<div class="row" style="padding-left: 20px; padding-right: 20px;">
				        <div class="col-3 col-md-3 hidden dropdown_con"> 
				            <div class="form-group">
				              <label style="width:100%"><i class="bx bx-purchase-tag-alt"></i> 
				              '.$this->lang->line("Choose Labels").' '.$popover.'
				              </label>                                 
				              <label>
				              	<a class="blue float-right pointer" page_id_for_label="" id="create_label_auto_reply"><i class="bx bx-plus-circle"></i> '.$this->lang->line("Create Label").'</a>
				              </label>
				            </div>       
				        </div>
				        <div class="col-9 col-md-9 hidden dropdown_con"> 
				            <div class="form-group">
				              <span id="first_dropdown"></span>                                  
				            </div>       
				        </div>
				      </div>';
					?>

					<div id="auto_reply_templates_section" style="padding: 10px 20px 10px 20px;">
						<!-- <hr> -->
						<div id="all_save_templates">
							<div id="saved_templates">
								<div class="row">
									<div class="form-group col-12 col-md-3">
										<label><i class="bx bx-reply"></i> <?php echo $this->lang->line('Auto Reply Template'); ?>
											<a href="#" data-placement="bottom"  data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("message") ?>" data-content="<?php echo $this->lang->line("Select any saved template of Auto Reply Campaign. If you want to modify any settings of this post campaign later, then edit this campaign & modify. Be notified that editing the saved template will not affect the campaign settings. To edit campaign, you need to edit post reply settings.") ?>"><i class='bx bx-info-circle'></i> </a>
										</label>
									</div>

									<div class="col-12 col-md-9">
										<select  class="select2 form-control" id="auto_reply_template" name="auto_reply_template">
										<?php
											echo "<option value='0'>{$this->lang->line('Please select a template')}</option>";
											foreach($auto_reply_template as $key => $val)
											{
												$template_id = $val['id'];
												$template_campaign_name = $val['ultrapost_campaign_name'];
												echo "<option value='{$template_id}'>{$template_campaign_name}</option>";
											}
										 ?>
										</select>
									</div>
								</div> <!-- end of row  -->
							</div>
						</div>
					</div>
					<!-- end of use saved template section -->

					<div id="new_template_section">
						<!-- <hr> -->
						<!-- comment hide and delete section -->
						<div class="row" style="padding: 10px 20px 10px 20px; <?php if(!$commnet_hide_delete_addon) echo "display: none;"; ?> ">
							<div class="col-12" style="margin-bottom: 20px;">
								<div class="row">
									<div class="col-12 col-md-6">
										<label><i class="bx bx-block"></i> <?php echo $this->lang->line("what do you want about offensive comments?") ?></label>
									</div>
									<div class="row">
									  <div class="col-12 col-md-6">
										<div class="custom-control custom-switch custom-control-inline">
										  <input type="radio" name="delete_offensive_comment" value="hide" id="delete_offensive_comment_hide" class="custom-control-input" checked>
										  <label class="custom-control-label mr-1" for="delete_offensive_comment_hide"></label>
										  <span><?php echo $this->lang->line('hide'); ?></span>
										</div>
									  </div>
									  <div class="col-12 col-md-6">
										<div class="custom-control custom-switch custom-control-inline">
										  <input type="radio" name="delete_offensive_comment" value="delete" id="delete_offensive_comment_delete" class="custom-control-input">
										  <label class="custom-control-label mr-1" for="delete_offensive_comment_delete"></label>
										  <span><?php echo $this->lang->line('delete'); ?>
										</div>
									  </div>
									</div>
								</div>
							</div>
							<br/><br/>

							<div class="col-12">
								<div class="row">
									<div class="col-12 col-md-6" id="delete_offensive_comment_keyword_div">
										<div class="form-group" style="border: 1px dashed #e4e6fc; padding: 10px;">
											<label><i class="bx bx-tag"></i> <small><?php echo $this->lang->line("write down the offensive keywords in comma separated") ?></small>
											</label>
											<textarea class="form-control message" name="delete_offensive_comment_keyword" id="delete_offensive_comment_keyword" placeholder="<?php echo $this->lang->line("Type keywords here in comma separated (keyword1,keyword2)...Keep it blank for no actions") ?>" style="height:59px !important;"></textarea>
										</div>
									</div>

									<div class="col-12 col-md-6">
										<div class="form-group clearfix" style="border: 1px dashed #e4e6fc; padding: 10px;">
											<label><small>
												<i class="bx bx-envelope"></i> <?php echo $this->lang->line("Select a message template for private reply after deleting offensive comment") ?></small>
											</label>
											<div>
												<select class="form-group private_reply_postback" id="private_message_offensive_words" name="private_message_offensive_words">
													<option><?php echo $this->lang->line('Please select a page first to see the message templates.'); ?></option>
												</select>

												<a href="" class="add_template float-left"><i class="bx bx-plus-circle"></i>     <?php echo $this->lang->line("Add Message Template");?></a>
												<a href="" class="ref_template float-right"><i class="bx bx-sync"></i> <?php echo $this->lang->line("Refresh List");?></a>

											</div>
										</div>
									</div>

								</div>
							</div>
						</div>
						<!-- end of comment hide and delete section -->

						<div class="row" style="padding: 10px 20px 10px 20px;">
							<!-- added by mostofa on 26-04-2017 -->
							<div class="col-12">
								<div class="row">
									<div class="col-12 col-md-6"><label><i class="bx bx-sort-down"></i> <?php echo $this->lang->line("do you want to send reply message to a user multiple times?") ?></label></div>
									<div class="col-12 col-md-6">
									  <div class="form-group">
										<div class="custom-control custom-switch custom-control-inline">
										  <input type="checkbox" name="multiple_reply" value="yes" id="multiple_reply" class="custom-control-input">
										  <label class="custom-control-label mr-1" for="multiple_reply"></label>
										  <span><?php echo $this->lang->line('Yes');?></span>
										</div>
									  </div>
									</div>
								</div>
							</div>
							<div class="smallspace clearfix"></div>
							<div class="col-12">
								<div class="row">
									<div class="col-12 col-md-6">
										<label><i class="bx bx-comment-dots"></i> <?php echo $this->lang->line("do you want to enable comment reply?") ?></label>
									</div>
									<div class="col-12 col-md-6">
									  <div class="form-group">
										<div class="custom-control custom-switch custom-control-inline">
										  <input type="checkbox" name="comment_reply_enabled" value="yes" id="comment_reply_enabled" class="custom-control-input" checked>
										  <label class="custom-control-label mr-1" for="comment_reply_enabled"></label>
										  <span><?php echo $this->lang->line('Yes');?></span>
										</div>
									  </div>
									</div>
								</div>
							</div>
							<div class="smallspace clearfix"></div>
							<div class="col-12">
								<div class="row">
									<div class="col-12 col-md-6">
										<label><i class="bx bx-comment"></i> <?php echo $this->lang->line("do you want to like on comment by page?") ?></label>
									</div>
									<div class="col-12 col-md-6">
									  <div class="form-group">
										<div class="custom-control custom-switch custom-control-inline">
										  <input type="checkbox" name="auto_like_comment" value="yes" id="auto_like_comment" class="custom-control-input">
										  <label class="custom-control-label mr-1" for="auto_like_comment"></label>
										  <span><?php echo $this->lang->line('Yes');?></span>
										</div>
									  </div>
									</div>
								</div>
							</div>
							<div class="smallspace clearfix"></div>
							<!-- comment hide and delete section -->
							<div class="col-12" <?php if(!$commnet_hide_delete_addon) echo "style='display: none;'"; ?> >
								<div class="row">
									<div class="col-12 col-md-6">
										<label><i class="bx bx-show-slash"></i>  <?php echo $this->lang->line("do you want to hide comments after comment reply?") ?></label>
									</div>
									<div class="col-12 col-md-6">
										<div class="form-group">
										  <div class="custom-control custom-switch custom-control-inline">
											<input type="checkbox" name="hide_comment_after_comment_reply" value="yes" id="hide_comment_after_comment_reply" class="custom-control-input">
											<label class="custom-control-label mr-1" for="hide_comment_after_comment_reply"></label>
											<span><?php echo $this->lang->line('Yes');?></span>
										  </div>
										</div>
									</div>
								</div>
							</div>
							<!-- comment hide and delete section -->

							<div class="smallspace clearfix"></div>

							<div class="col-12">
							  <div class="custom-control custom-radio">
								<input type="radio" name="message_type" value="generic" id="generic" class="custom-control-input radio_button">
								<label class="custom-control-label" for="generic"><?php echo $this->lang->line("generic message for all") ?></label>
							  </div>
							  <div class="custom-control custom-radio">
								<input type="radio" name="message_type" value="filter" id="filter" class="custom-control-input radio_button">
								<label class="custom-control-label" for="filter"><?php echo $this->lang->line("send message by filtering word/sentence") ?></label>
							  </div>
							</div>

							<div class="col-12" style="margin-top: 15px;">
								<div class="form-group">
									<label>
										<i class="bx bx-rocket"></i> <?php echo $this->lang->line("auto reply campaign name") ?> <span class="text-danger">*</span>
									</label>
									<input class="form-control" type="text" name="auto_campaign_name" id="auto_campaign_name" placeholder="<?php echo $this->lang->line("write your auto reply campaign name here") ?>">
								</div>
							</div>


							<div class="col-12" id="generic_message_div" style="display: none;">
								<div class="form-group clearfix" style="border: 1px dashed #e4e6fc; padding: 20px;">
									<label>
										<i class="bx bx-envelope"></i> <?php echo $this->lang->line("Message for comment reply") ?> <span class="text-danger">*</span>
										<a href="#" data-placement="bottom"  data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("message") ?>" data-content="<?php echo $this->lang->line("write your message which you want to send. You can customize the message by individual commenter name."); ?>  Spintax example : {Hello|Howdy|Hola} to you, {Mr.|Mrs.|Ms.} {{Jason|Malina|Sara}|Williams|Davis}"><i class='bx bx-info-circle'></i> </a>
									</label>
									<?php if($comment_tag_machine_addon) {?>
									<span class='float-right'>
										<a title="<?php echo $this->lang->line("You can tag user in your comment reply. Facebook will notify them about mention whenever you tag.") ?>" data-toggle="tooltip" data-placement="top" class='btn-sm lead_tag_name button-outline'><i class='bx bx-tag'></i>  <?php echo $this->lang->line("tag user") ?></a>
									</span>
									<?php } ?>
									<span class='float-right'>
									  <a title="<?php echo $this->lang->line("You can include #LEAD_USER_LAST_NAME# variable inside your message. The variable will be replaced by real names when we will send it.") ?>" data-toggle="tooltip" data-placement="top" class='btn-sm lead_last_name button-outline'><i class='bx bx-user'></i> <?php echo $this->lang->line("last name") ?></a>
									</span>
									<span class='float-right'>
									  <a title="<?php echo $this->lang->line("You can include #LEAD_USER_FIRST_NAME# variable inside your message. The variable will be replaced by real names when we will send it.") ?>" data-toggle="tooltip" data-placement="top" class='btn-sm lead_first_name button-outline'><i class='bx bx-user'></i> <?php echo $this->lang->line("first name") ?></a>
									</span>

									<div class="clearfix"></div>
									<textarea class="form-control message" name="generic_message" id="generic_message" placeholder="<?php echo $this->lang->line("type your message here...") ?>" style="height:170px !important;"></textarea>

									<!-- comment hide and delete section -->
									<br/>
									<div class="row clearfix" <?php if(!$commnet_hide_delete_addon) echo "style='display: none;'"; ?> >
										<div class="col-12 col-md-6">
											<label class="control-label" ><i class="bx bx-image"></i> <?php echo $this->lang->line("image for comment reply") ?></label>
											<div class="form-group">
												<div id="generic_comment_image"><?php echo $this->lang->line("upload") ?></div>
											</div>
											<div id="generic_image_preview_id"></div>
											<span class="text-danger" id="generic_image_for_comment_reply_error"></span>
											<input type="text" name="generic_image_for_comment_reply" class="form-control" id="generic_image_for_comment_reply" placeholder="<?php echo $this->lang->line("put your image url here or click the above upload button") ?>" style="margin-top: -14px;" />
										</div>

										<div class="col-12 col-md-6">
											<label class="control-label" ><i class="bx bxl-youtube"></i>  <?php echo $this->lang->line("video for comment reply") ?> [mp4 <?php echo $this->lang->line("Prefered");?>]
												<a href="#" data-placement="bottom" data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("video upload") ?>" data-content="<?php echo $this->lang->line("image and video will not work together. Please choose either image or video.") ?> [mp4,wmv,flv]"><i class='bx bx-info-circle'></i></a>
											</label>
											<div class="form-group">
												<div id="generic_video_upload"><?php echo $this->lang->line("upload") ?></div>
											</div>
											<div id="generic_video_preview_id"></div>
											<span class="text-danger" id="generic_video_comment_reply_error"></span>
											<input type="hidden" name="generic_video_comment_reply" class="form-control" id="generic_video_comment_reply" placeholder="<?php echo $this->lang->line("Put your image url here or click upload") ?>"  />
										</div>
									</div>
									<br/><br/>
									<!-- comment hide and delete section -->


									<label>
									  <i class="bx bx-envelope"></i> <?php echo $this->lang->line("Select a message template for private reply") ?> [<?php echo $this->lang->line('Maximum two reply message is supported.'); ?>]
									</label>
									<div>
									  <select class="form-group private_reply_postback" id="generic_message_private" name="generic_message_private">
									    <option><?php echo $this->lang->line('Please select a page first to see the message templates.'); ?></option>
									  </select>

									  <a href="" class="add_template float-left"><i class="bx bx-plus-circle"></i>     <?php echo $this->lang->line("Add Message Template");?></a>
									  <a href="" class="ref_template float-right"><i class="bx bx-sync"></i> <?php echo $this->lang->line("Refresh List");?></a>

									</div>

								</div>
							</div>


							<div class="col-12" id="filter_message_div" style="display: none;">
								<?php for ($i=1; $i <= 20 ; $i++) : ?>
										<div class="form-group clearfix" id="filter_div_<?php echo $i; ?>" style="border: 1px dashed #e4e6fc; padding: 20px; margin-bottom: 50px;">
											<label>
												<i class="bx bx-tag"></i> <?php echo $this->lang->line("filter word/sentence") ?> <span class="text-danger">*</span>
												<a href="#" data-placement="bottom"  data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("message") ?>" data-content="<?php echo $this->lang->line("Write the word or sentence for which you want to filter comment. For multiple filter keyword write comma separated. Example -   why, wanto to know, when") ?>"><i class='bx bx-info-circle'></i> </a>
											</label>
											<input class="form-control filter_word" type="text" name="filter_word_<?php echo $i; ?>" id="filter_word_<?php echo $i; ?>" placeholder="<?php echo $this->lang->line("write your filter word here") ?>">


											<!-- new feature comment reply section -->
											<br/>
											<label>
												<i class="bx bx-envelope"></i> <?php echo $this->lang->line("msg for comment reply") ?><span class="text-danger">*</span>
												<a href="#" data-placement="bottom"  data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("message") ?>" data-content="<?php echo $this->lang->line("write your message which you want to send based on filter words. You can customize the message by individual commenter name."); ?>  Spintax example : {Hello|Howdy|Hola} to you, {Mr.|Mrs.|Ms.} {{Jason|Malina|Sara}|Williams|Davis}"><i class='bx bx-info-circle'></i> </a>
											</label>
											<?php if($comment_tag_machine_addon) {?>
											<span class='float-right'>
												<a title="<?php echo $this->lang->line("You can tag user in your comment reply. Facebook will notify them about mention whenever you tag.") ?>" data-toggle="tooltip" data-placement="top" class='btn-sm lead_tag_name button-outline'><i class='bx bx-tag'></i>  <?php echo $this->lang->line("tag user") ?></a>
											</span>
											<?php } ?>
											<span class='float-right'>
											  <a title="<?php echo $this->lang->line("You can include #LEAD_USER_LAST_NAME# variable inside your message. The variable will be replaced by real names when we will send it.") ?>" data-toggle="tooltip" data-placement="top" class='btn-sm lead_last_name button-outline'><i class='bx bx-user'></i> <?php echo $this->lang->line("last name") ?></a>
											</span>
											<span class='float-right'>
											  <a title="<?php echo $this->lang->line("You can include #LEAD_USER_FIRST_NAME# variable inside your message. The variable will be replaced by real names when we will send it.") ?>" data-toggle="tooltip" data-placement="top" class='btn-sm lead_first_name button-outline'><i class='bx bx-user'></i> <?php echo $this->lang->line("first name") ?></a>
											</span>
											<div class="clearfix"></div>
											<textarea class="form-control message" name="comment_reply_msg_<?php echo $i; ?>" id="comment_reply_msg_<?php echo $i; ?>"  placeholder="<?php echo $this->lang->line("type your message here...") ?>" style="height:170px !important;"></textarea>

											<!-- comment hide and delete section -->
											<br/>
											<div class="clearfix" <?php if(!$commnet_hide_delete_addon) echo "style='display: none;'"; ?> >
												<div class="row">
													<div class="col-12 col-md-6">
														<label class="control-label" ><i class="bx bx-image"></i> <?php echo $this->lang->line("image for comment reply") ?>
														</label>
														<div class="form-group">
															<div id="filter_image_upload_<?php echo $i; ?>"><?php echo $this->lang->line("upload") ?></div>
														</div>
														<div id="generic_image_preview_id_<?php echo $i; ?>"></div>
														<span class="text-danger" id="generic_image_for_comment_reply_error_<?php echo $i; ?>"></span>
														<input type="text" name="filter_image_upload_reply_<?php echo $i; ?>" class="form-control" id="filter_image_upload_reply_<?php echo $i; ?>" placeholder="<?php echo $this->lang->line("Put your image url here or click the above upload button") ?>" style="margin-top: -14px;" />
													</div>

													<div class="col-12 col-md-6">
														<label class="control-label" ><i class="bx bxl-youtube"></i> <?php echo $this->lang->line("video for comment reply") ?> [mp4 <?php echo $this->lang->line("Prefered");?>]
															<a href="#" data-placement="bottom" data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("video upload") ?>" data-content="<?php echo $this->lang->line("image and video will not work together. Please choose either image or video.") ?> [mp4,wmv,flv]"><i class='bx bx-info-circle'></i></a>
														</label>
														<div class="form-group">
															<div id="filter_video_upload_<?php echo $i; ?>"><?php echo $this->lang->line("upload") ?></div>
														</div>
														<div id="generic_video_preview_id_<?php echo $i; ?>"></div>
														<span class="text-danger" id="edit_generic_video_comment_reply_error_<?php echo $i; ?>"></span>
														<input type="hidden" name="filter_video_upload_reply_<?php echo $i; ?>" class="form-control" id="filter_video_upload_reply_<?php echo $i; ?>" placeholder="<?php echo $this->lang->line("Put your image url here or click upload") ?>"  />
													</div>
												</div>
											</div>
											<!-- comment hide and delete section -->

											<br/>

											<label>
											  <i class="bx bx-envelope"></i> <?php echo $this->lang->line("Select a message template for private reply") ?> [<?php echo $this->lang->line('Maximum two reply message is supported.'); ?>]
											</label>
											<div>
											  <select class="form-group private_reply_postback" id="filter_message_<?php echo $i; ?>" name="filter_message_<?php echo $i; ?>">
											    <option><?php echo $this->lang->line('Please select a page first to see the message templates.'); ?></option>
											  </select>

											  <a href="" class="add_template float-left"><i class="bx bx-plus-circle"></i>     <?php echo $this->lang->line("Add Message Template");?></a>
											  <a href="" class="ref_template float-right"><i class="bx bx-sync"></i> <?php echo $this->lang->line("Refresh List");?></a>

											</div>


										</div>
								<?php endfor; ?>

								<div class="clearfix">
									<input type="hidden" name="content_counter" id="content_counter" />
									<button type="button" class="btn btn-sm btn-outline-primary float-right" id="add_more_button"><i class="bx bx-plus"></i> <?php echo $this->lang->line("add more filtering") ?></button>
								</div>

								<div class="form-group clearfix" id="nofilter_word_found_div" style="margin-top: 10px; border: 1px dashed #e4e6fc; padding: 20px;">
									<label>
										<i class="bx bx-envelope"></i> <?php echo $this->lang->line("comment reply if no matching found") ?>
										<a href="#" data-placement="bottom"  data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("message") ?>" data-content="<?php echo $this->lang->line("Write the message,  if no filter word found. If you don't want to send message them, just keep it blank ."); ?>  Spintax example : {Hello|Howdy|Hola} to you, {Mr.|Mrs.|Ms.} {{Jason|Malina|Sara}|Williams|Davis}"><i class='bx bx-info-circle'></i> </a>
									</label>
									<?php if($comment_tag_machine_addon) {?>
									<span class='float-right'>
										<a title="<?php echo $this->lang->line("You can tag user in your comment reply. Facebook will notify them about mention whenever you tag.") ?>" data-toggle="tooltip" data-placement="top" class='btn-sm lead_tag_name button-outline'><i class='bx bx-tag'></i>  <?php echo $this->lang->line("tag user") ?></a>
									</span>
									<?php } ?>
									<span class='float-right'>
									  <a title="<?php echo $this->lang->line("You can include #LEAD_USER_LAST_NAME# variable inside your message. The variable will be replaced by real names when we will send it.") ?>" data-toggle="tooltip" data-placement="top" class='btn-sm lead_last_name button-outline'><i class='bx bx-user'></i> <?php echo $this->lang->line("last name") ?></a>
									</span>
									<span class='float-right'>
									  <a title="<?php echo $this->lang->line("You can include #LEAD_USER_FIRST_NAME# variable inside your message. The variable will be replaced by real names when we will send it.") ?>" data-toggle="tooltip" data-placement="top" class='btn-sm lead_first_name button-outline'><i class='bx bx-user'></i> <?php echo $this->lang->line("first name") ?></a>
									</span>
									<div class="clearfix"></div>
									<textarea class="form-control message" name="nofilter_word_found_text" id="nofilter_word_found_text"  placeholder="<?php echo $this->lang->line("type your message here...") ?>" style="height:170px !important;"></textarea>

									<!-- comment hide and delete section -->
									<br/>
									<div class="clearfix" <?php if(!$commnet_hide_delete_addon) echo "style='display: none;'"; ?> >
										<div class="row">
											<div class="col-12 col-md-6">
												<label class="control-label" ><i class="bx bx-image"></i> <?php echo $this->lang->line("image for comment reply") ?>
												</label>
												<div class="form-group">
													<div id="nofilter_image_upload"><?php echo $this->lang->line("upload") ?></div>
												</div>
												<div id="nofilter_generic_image_preview_id"></div>
												<span class="text-danger" id="nofilter_image_upload_reply_error"></span>
												<input type="text" name="nofilter_image_upload_reply" class="form-control" id="nofilter_image_upload_reply" placeholder="<?php echo $this->lang->line("put your image url here or click the above upload button") ?>" style="margin-top: -14px;" />
											</div>

											<div class="col-12 col-md-6">
												<label class="control-label" ><i class="bx bxl-youtube"></i> <?php echo $this->lang->line("video for comment reply") ?> [mp4 <?php echo $this->lang->line("Prefered");?>]
													<a href="#" data-placement="bottom" data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("video upload") ?>" data-content="<?php echo $this->lang->line("image and video will not work together. Please choose either image or video.") ?> [mp4,wmv,flv]"><i class='bx bx-info-circle'></i></a>
												</label>
												<div class="form-group">
													<div id="nofilter_video_upload"><?php echo $this->lang->line("upload") ?></div>
												</div>
												<div id="nofilter_video_preview_id"></div>
												<span class="text-danger" id="nofilter_video_upload_reply_error"></span>
												<input type="hidden" name="nofilter_video_upload_reply" class="form-control" id="nofilter_video_upload_reply" placeholder="<?php echo $this->lang->line("put your image url here or click upload") ?>"  />
											</div>
										</div>
									</div>
									<br/><br/>
									<!-- comment hide and delete section -->

									<label>
									  <i class="bx bx-envelope"></i> <?php echo $this->lang->line("Select a message template for private reply if no matching found") ?> [<?php echo $this->lang->line('Maximum two reply message is supported.'); ?>]
									</label>
									<div>
									  <select class="form-group private_reply_postback" id="nofilter_word_found_text_private" name="nofilter_word_found_text_private">
									    <option><?php echo $this->lang->line('Please select a page first to see the message templates.'); ?></option>
									  </select>

									  <a href="" class="add_template float-left"><i class="bx bx-plus-circle"></i>     <?php echo $this->lang->line("Add Message Template");?></a>
									  <a href="" class="ref_template float-right"><i class="bx bx-sync"></i> <?php echo $this->lang->line("Refresh List");?></a>

									</div>

								</div>
							</div>
						</div>
					</div>
					<div class="col-12 text-center" id="response_status"></div>
				</div>

				<!-- This hidden field is for detecting the clicked button type -->
				<input type="hidden" name="btn_type" value="" id="submit_btn_values">
			</form>
			<div class="clearfix"></div>

			<div class="modal-footer" style="padding-left: 45px; padding-right: 45px; ">
				<div class="row">
					<div class="col-6">
						<button class="btn btn-info save_button float-left" id="save_and_create" button_name="submit_create_button"><i class='bx bx-save'></i> <?php echo $this->lang->line("submit & save as template") ?></button>
					</div>
					<div class="col-6">
						<button class="btn btn-primary save_button float-right" id="save_only" button_name="only_submit"><i class='bx bx-save'></i> <span class="align-middle ml-25"><?php echo $this->lang->line("submit"); ?></span></button>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>


<div class="modal fade" id="edit_auto_reply_message_modal" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg" style="min-width: 70%;">
		<div class="modal-content">

			<div class="modal-header">
			  <h3 class="modal-title" style="padding: 10px 20px 10px 20px;" ><?php echo $this->lang->line("Please give the following information for post auto reply") ?></h3>
			  <button type="button" class="close" id='edit_modal_close'  aria-label="Close">
				<i class="bx bx-x"></i>
			  </button>
			</div>
			
			<form action="#" id="edit_auto_reply_info_form" method="post">
				<input type="hidden" name="edit_auto_reply_page_id" id="edit_auto_reply_page_id" value="">
				<input type="hidden" name="edit_auto_reply_post_id" id="edit_auto_reply_post_id" value="">
				<input type="hidden" name="edit_auto_reply_post_permalink" id="edit_auto_reply_post_permalink" value="">
			<div class="modal-body" id="edit_auto_reply_message_modal_body">   
			
			<div class="text-center waiting previewLoader"><i class="bx bx-loader-alt bx-spin blue text-center" style="font-size: 40px;"></i></div>
			
			<br/>
				<?php
				  $is_broadcaster_exist=false;
				  if($this->is_broadcaster_exist)
				  {
				      $is_broadcaster_exist=true;
				  }
			      $popover='<a href="#" data-placement="bottom"  data-toggle="popover" data-trigger="focus" title="'.$this->lang->line("Choose Labels").'" data-content="'.$this->lang->line("If you choose labels, then when user comment on the post & get private reply in their inbox , they will be added in those labels, that will help you to segment your leads & broadcasting from Messenger Broadcaster. If you don`t want to add labels for this post comment , then just keep it blank as it is.
Add label will only work once private reply is setup.  And you will need to sync subscribers later to update subscriber information. In this way the subscriber will not eligible for BOT subscriber until they reply back in messenger.").'"><i class="bx bx-info-circle"></i> </a>';
			      echo '<div class="row" style="padding-left: 20px; padding-right: 20px;">
			        <div class="col-3 col-md-3 hidden dropdown_con"> 
			            <div class="form-group">
			              <label style="width:100%"><i class="bx bx-purchase-tag-alt"></i> 
			              '.$this->lang->line("Choose Labels").' '.$popover.'
			              </label>
			              <label>
			              	<a class="blue float-right pointer" page_id_for_label="" id="create_label_edit_auto_reply"><i class="bx bx-plus-circle"></i> '.$this->lang->line("Create Label").'</a>
			              </label>                              
			            </div>       
			        </div>
			        <div class="col-9 col-md-9 hidden dropdown_con"> 
			            <div class="form-group">
			              <span id="edit_first_dropdown"></span>                                  
			            </div>       
			        </div>
			      </div>';
				?>

				<!-- comment hide and delete section -->
				<div class="row" style="padding: 20px;<?php if(!$commnet_hide_delete_addon) echo "display: none;"; ?> ">
					<div class="col-12" style="margin-bottom: 20px;">
						<div class="row">							
							<div class="col-12 col-md-6" >
								<label><i class="bx bx-block"></i> <?php echo $this->lang->line("what do you want about offensive comments?") ?></label>
							</div>
							<div class="row">
							  <div class="col-12 col-md-6">
							    <div class="custom-control custom-switch custom-control-inline">
							      <input type="radio" name="edit_delete_offensive_comment" value="hide" id="edit_delete_offensive_comment_hide" class="custom-control-input" checked>
							      <label class="custom-control-label mr-1" for="edit_delete_offensive_comment_hide"></label>
							      <span><?php echo $this->lang->line('hide'); ?></span>
							    </div>
							  </div>
							  <div class="col-12 col-md-6">
							    <div class="custom-control custom-switch custom-control-inline">
							      <input type="radio" name="edit_delete_offensive_comment" value="delete"  id="edit_delete_offensive_comment_delete" class="custom-control-input">
							      <label class="custom-control-label mr-1" for="edit_delete_offensive_comment_delete"></label>
							      <span><?php echo $this->lang->line('delete'); ?>
							    </div>
							  </div>
							</div>
						</div>
					</div>
					<br/><br/>
					<div class="col-12 col-md-6" id="edit_delete_offensive_comment_keyword_div">
						<div class="form-group clearfix" style="border: 1px dashed #e4e6fc; padding: 10px;">
							<label><i class="bx bx-tag"></i> <small><?php echo $this->lang->line("write down the offensive keywords in comma separated") ?></small>
							</label>
							<textarea class="form-control message" name="edit_delete_offensive_comment_keyword" id="edit_delete_offensive_comment_keyword" placeholder="<?php echo $this->lang->line("Type keywords here in comma separated (keyword1,keyword2)...Keep it blank for no actions") ?>" style="height:59px !important;"></textarea>
						</div>
					</div>
					
					<div class="col-12 col-md-6">
						<div class="form-group clearfix" style="border: 1px dashed #e4e6fc; padding: 10px;">
							<label><small>
								<i class="bx bx-envelope"></i> <?php echo $this->lang->line("Select a message template for private reply after deleting offensive comment") ?></small>
							</label>
							<div>                      
								<select class="form-group private_reply_postback" id="edit_private_message_offensive_words" name="edit_private_message_offensive_words">
									<option><?php echo $this->lang->line('Please select a page first to see the message templates.'); ?></option>
								</select>

								<a href="" class="add_template float-left"><i class="bx bx-plus-circle"></i>     <?php echo $this->lang->line("Add Message Template");?></a>
								<a href="" class="ref_template float-right"><i class="bx bx-sync"></i> <?php echo $this->lang->line("Refresh List");?></a>

							</div>
						</div>
					</div>

				</div> 
				<!-- end of comment hide and delete section -->
				<div class="row" style="padding: 10px 20px 10px 20px;">
					<!-- added by mostofa on 26-04-2017 -->
					<div class="col-12">
						<div class="row">							
							<div class="col-12 col-md-6" ><label><i class="bx bx-sort-down"></i> <?php echo $this->lang->line("do you want to send reply message to a user multiple times?") ?></label></div>
							<div class="col-12 col-md-6">
							  <div class="form-group">
							    <div class="custom-control custom-switch custom-control-inline">
							      <input type="checkbox" name="edit_multiple_reply" value="yes" id="edit_multiple_reply" class="custom-control-input">
							      <label class="custom-control-label mr-1" for="edit_multiple_reply"></label>
							      <span><?php echo $this->lang->line('Yes');?></span>
							    </div>
							  </div>
							</div>
						</div>
					</div>
					<div class="smallspace clearfix"></div>
					<div class="col-12">
						<div class="row">							
							<div class="col-12 col-md-6" >
								<label><i class="bx bx-comment-dots"></i> <?php echo $this->lang->line("do you want to enable comment reply?") ?></label>
							</div>
							<div class="col-12 col-md-6">
							  <div class="form-group">
							    <div class="custom-control custom-switch custom-control-inline">
							      <input type="checkbox" name="edit_comment_reply_enabled" value="yes" id="edit_comment_reply_enabled" class="custom-control-input">
							      <label class="custom-control-label mr-1" for="edit_comment_reply_enabled"></label>
							      <span><?php echo $this->lang->line('Yes');?></span>
							    </div>
							  </div>
							</div>						
						</div>					
					</div>
					<div class="smallspace clearfix"></div>
					<div class="col-12">
						<div class="row">							
							<div class="col-12 col-md-6" >
								<label><i class="bx bx-comment"></i> <?php echo $this->lang->line("do you want to like on comment by page?") ?></label>
							</div>
							<div class="col-12 col-md-6">
							  <div class="form-group">
							    <div class="custom-control custom-switch custom-control-inline">
							      <input type="checkbox" name="edit_auto_like_comment" value="yes" id="edit_auto_like_comment" class="custom-control-input">
							      <label class="custom-control-label mr-1" for="edit_auto_like_comment"></label>
							      <span><?php echo $this->lang->line('Yes');?></span>
							    </div>
							  </div>
							</div>
						</div>
					</div>
					<div class="smallspace clearfix"></div>
					<!-- comment hide and delete section -->
					<div class="col-12" <?php if(!$commnet_hide_delete_addon) echo "style='display: none;'"; ?> >
						<div class="row">							
							<div class="col-12 col-md-6" >
								<label><i class="bx bx-show-slash"></i> <?php echo $this->lang->line("do you want to hide comments after comment reply?") ?></label>
							</div>
							<div class="col-12 col-md-6">
							  <div class="form-group">
							    <div class="custom-control custom-switch custom-control-inline">
							      <input type="checkbox" name="edit_hide_comment_after_comment_reply" value="yes" id="edit_hide_comment_after_comment_reply" class="custom-control-input">
							      <label class="custom-control-label mr-1" for="edit_hide_comment_after_comment_reply"></label>
							      <span><?php echo $this->lang->line('Yes');?></span>
							    </div>
							  </div>
							</div>
						</div>
					</div>
					<!-- comment hide and delete section -->

					<div class="smallspace clearfix"></div>
					<div class="col-12">
					  <div class="custom-control custom-radio">
					  	<input type="radio" name="edit_message_type" value="generic" id="edit_generic" class="custom-control-input radio_button">
					  	<label class="custom-control-label" for="edit_generic"><?php echo $this->lang->line("generic message for all") ?></label>
					  </div>
					  <div class="custom-control custom-radio">
					  	<input type="radio" name="edit_message_type" value="filter" id="edit_filter" class="custom-control-input radio_button">
					  	<label class="custom-control-label" for="edit_filter"><?php echo $this->lang->line("send message by filtering word/sentence") ?></label>
					  </div>
					</div>

					<div class="col-12" style="margin-top: 15px;">
						<div class="form-group">
							<label>
								<i class="bx bx-rocket"></i> <?php echo $this->lang->line("auto reply campaign name") ?> <span class="text-danger">*</span>
							</label>
							<input class="form-control" type="text" name="edit_auto_campaign_name" id="edit_auto_campaign_name" placeholder="<?php echo $this->lang->line("write your auto reply campaign name here") ?>">
						</div>
					</div>

					<div class="col-12" id="edit_generic_message_div" style="display: none;">
						<div class="form-group clearfix" style="border: 1px dashed #e4e6fc; padding: 20px;">
							<label>
								<i class="bx bx-envelope"></i> <?php echo $this->lang->line("message for comment reply") ?> <span class="text-danger">*</span>
								<a href="#" data-placement="bottom"  data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("message") ?>" data-content="<?php echo $this->lang->line("write your message which you want to send. You can customize the message by individual commenter name."); ?>  Spintax example : {Hello|Howdy|Hola} to you, {Mr.|Mrs.|Ms.} {{Jason|Malina|Sara}|Williams|Davis}"><i class='bx bx-info-circle'></i> </a>
							</label>
							<?php if($comment_tag_machine_addon) {?>
							<span class='float-right'> 
								<a title="<?php echo $this->lang->line("You can tag user in your comment reply. Facebook will notify them about mention whenever you tag.") ?>" data-toggle="tooltip" data-placement="top" class='btn-sm lead_tag_name button-outline'><i class='bx bx-tag'></i>  <?php echo $this->lang->line("tag user") ?></a>
							</span>
							<?php } ?>
							<span class='float-right'> 
							  <a title="<?php echo $this->lang->line("You can include #LEAD_USER_LAST_NAME# variable inside your message. The variable will be replaced by real names when we will send it.") ?>" data-toggle="tooltip" data-placement="top" class='btn-sm lead_last_name button-outline'><i class='bx bx-user'></i> <?php echo $this->lang->line("last name") ?></a>
							</span>
							<span class='float-right'> 
							  <a title="<?php echo $this->lang->line("You can include #LEAD_USER_FIRST_NAME# variable inside your message. The variable will be replaced by real names when we will send it.") ?>" data-toggle="tooltip" data-placement="top" class='btn-sm lead_first_name button-outline'><i class='bx bx-user'></i> <?php echo $this->lang->line("first name") ?></a>
							</span> 
							<div class="clearfix"></div>
							<textarea class="form-control message" name="edit_generic_message" id="edit_generic_message" placeholder="<?php echo $this->lang->line("type your message here...") ?>" style="height:170px !important;"></textarea>
							

							<!-- comment hide and delete scetion -->
							<br/>
							<div class="clearfix" <?php if(!$commnet_hide_delete_addon) echo "style='display: none;'"; ?> >
								<div class="row">									
									<div class="col-12 col-md-6">
										<label class="control-label" ><i class="bx bx-image"></i> <?php echo $this->lang->line("image for comment reply") ?>
										</label>									
										<div class="form-group">      
											<div id="edit_generic_comment_image"><?php echo $this->lang->line("upload") ?></div>	     
										</div>
										<div id="edit_generic_image_preview_id"></div>
										<span class="text-danger" id="generic_image_for_comment_reply_error"></span>
										<input type="text" name="edit_generic_image_for_comment_reply" class="form-control" id="edit_generic_image_for_comment_reply" placeholder="<?php echo $this->lang->line("put your image url here or click the above upload button") ?>" style="margin-top: -14px;" />

										<div class="overlay_wrapper">
											<span></span>
											<img src="" alt="image" id="edit_generic_image_for_comment_reply_display" height="240" width="100%" style="display:none;" />
										</div>
									</div>

									<div class="col-12 col-md-6">
										<label class="control-label" ><i class="bx bxl-youtube"></i> <?php echo $this->lang->line("video for comment reply") ?> [mp4 <?php echo $this->lang->line("Prefered");?>]
											<a href="#" data-placement="bottom" data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("video upload") ?>" data-content="<?php echo $this->lang->line("image and video will not work together. Please choose either image or video.") ?> [mp4,wmv,flv]"><i class='bx bx-info-circle'></i></a>
										</label>
										<div class="form-group">      
											<div id="edit_generic_video_upload"><?php echo $this->lang->line("upload") ?></div>	     
										</div>
										<div id="edit_generic_video_preview_id"></div>
										<span class="text-danger" id="edit_generic_video_comment_reply_error"></span>
										<input type="hidden" name="edit_generic_video_comment_reply" class="form-control" id="edit_generic_video_comment_reply" placeholder="<?php echo $this->lang->line("put your image url here or click upload") ?>" />

										<div class="overlay_wrapper">
											<span></span>
											<video width="100%" height="240" controls style="border:1px solid #ccc;display:none;">
												<source src="" id="edit_generic_video_comment_reply_display" type="video/mp4">
											<?php echo $this->lang->line("your browser does not support the video tag.") ?>
											</video>
										</div>
									</div>
								</div>
							</div>
							<br/><br/>
							<!-- comment hide and delete scetion -->

							<label>
							  <i class="bx bx-envelope"></i> <?php echo $this->lang->line("Select a message template for private reply") ?> [<?php echo $this->lang->line('Maximum two reply message is supported.'); ?>]
							</label>
							<div>                      
							  <select class="form-group private_reply_postback" id="edit_generic_message_private" name="edit_generic_message_private">
							    <option><?php echo $this->lang->line('Please select a page first to see the message templates.'); ?></option>
							  </select>

							  <a href="" class="add_template float-left"><i class="bx bx-plus-circle"></i>     <?php echo $this->lang->line("Add Message Template");?></a>
							  <a href="" class="ref_template float-right"><i class="bx bx-sync"></i> <?php echo $this->lang->line("Refresh List");?></a>

							</div>
							
						</div>
					</div>

					<div class="col-12" id="edit_filter_message_div" style="display: none;">
					<?php for($i=1;$i<=20;$i++) :?>
						<div class="form-group clearfix" id="edit_filter_div_<?php echo $i; ?>" style="border: 1px dashed #e4e6fc; padding: 20px; margin-bottom: 50px;">
							<label>
								<i class="bx bx-tag"></i> <?php echo $this->lang->line("filter word/sentence") ?> <span class="text-danger">*</span>
								<a href="#" data-placement="bottom"  data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("message") ?>" data-content="<?php echo $this->lang->line("Write the word or sentence for which you want to filter comment. For multiple filter keyword write comma separated. Example -   why, want to know, when") ?>"><i class='bx bx-info-circle'></i> </a>
							</label>
							<input class="form-control filter_word" type="text" name="edit_filter_word_<?php echo $i; ?>" id="edit_filter_word_<?php echo $i; ?>" placeholder="<?php echo $this->lang->line("write your filter word here") ?>">
							
							
							<br/>
							<label>
								<i class="bx bx-envelope"></i> <?php echo $this->lang->line("msg for comment reply") ?><span class="text-danger">*</span>
								<a href="#" data-placement="bottom"  data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("message") ?>" data-content="<?php echo $this->lang->line("write your message which you want to send based on filter words. You can customize the message by individual commenter name."); ?>  Spintax example : {Hello|Howdy|Hola} to you, {Mr.|Mrs.|Ms.} {{Jason|Malina|Sara}|Williams|Davis}"><i class='bx bx-info-circle'></i> </a>
							</label>
							<?php if($comment_tag_machine_addon) {?>
							<span class='float-right'> 
								<a title="<?php echo $this->lang->line("You can tag user in your comment reply. Facebook will notify them about mention whenever you tag.") ?>" data-toggle="tooltip" data-placement="top" class='btn-sm lead_tag_name button-outline'><i class='bx bx-tag'></i>  <?php echo $this->lang->line("tag user") ?></a>
							</span>
							<?php } ?>
							<span class='float-right'> 
							  <a title="<?php echo $this->lang->line("You can include #LEAD_USER_LAST_NAME# variable inside your message. The variable will be replaced by real names when we will send it.") ?>" data-toggle="tooltip" data-placement="top" class='btn-sm lead_last_name button-outline'><i class='bx bx-user'></i> <?php echo $this->lang->line("last name") ?></a>
							</span>
							<span class='float-right'> 
							  <a title="<?php echo $this->lang->line("You can include #LEAD_USER_FIRST_NAME# variable inside your message. The variable will be replaced by real names when we will send it.") ?>" data-toggle="tooltip" data-placement="top" class='btn-sm lead_first_name button-outline'><i class='bx bx-user'></i> <?php echo $this->lang->line("first name") ?></a>
							</span> 
							<div class="clearfix"></div>
							<textarea class="form-control message" name="edit_comment_reply_msg_<?php echo $i; ?>" id="edit_comment_reply_msg_<?php echo $i; ?>"  placeholder="<?php echo $this->lang->line("type your message here...") ?>" style="height:170px !important;"></textarea>
							

							<!-- comment hide and delete section -->
							<br/>
							<div class="clearfix" <?php if(!$commnet_hide_delete_addon) echo "style='display: none;'"; ?> >
								<div class="row">									
									<div class="col-12 col-md-6">
										<label class="control-label" ><i class="bx bx-image"></i> <?php echo $this->lang->line("image for comment reply") ?>
										</label>									
										<div class="form-group">      
											<div id="edit_filter_image_upload_<?php echo $i; ?>"><?php echo $this->lang->line("upload") ?></div>	     
										</div>
										<div id="edit_generic_image_preview_id_<?php echo $i; ?>"></div>
										<span class="text-danger" id="edit_generic_image_for_comment_reply_error_<?php echo $i; ?>"></span>
										<input type="text" name="edit_filter_image_upload_reply_<?php echo $i; ?>" class="form-control" id="edit_filter_image_upload_reply_<?php echo $i; ?>" placeholder="<?php echo $this->lang->line("put your image url here or click the above upload button") ?>" style="margin-top: -14px;" />

										<div class="overlay_wrapper">
											<span></span>
											<img src="" alt="image" id="edit_filter_image_upload_reply_display_<?php echo $i; ?>" height="240" width="100%" style="display:none"/>
										</div>
									</div>

									<div class="col-12 col-md-6">
										<label class="control-label" ><i class="bx bxl-youtube"></i> <?php echo $this->lang->line("video for comment reply") ?> [mp4 <?php echo $this->lang->line("Prefered");?>]
											<a href="#" data-placement="bottom" data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("video upload") ?>" data-content="<?php echo $this->lang->line("image and video will not work together. Please choose either image or video.") ?> [mp4,wmv,flv]"><i class='bx bx-info-circle'></i></a>
										</label>
										<div class="form-group">      
											<div id="edit_filter_video_upload_<?php echo $i; ?>"><?php echo $this->lang->line("upload") ?></div>	     
										</div>
										<div id="edit_generic_video_preview_id_<?php echo $i; ?>"></div>
										<span class="text-danger" id="edit_generic_video_comment_reply_error_<?php echo $i; ?>"></span>
										<input type="hidden" name="edit_filter_video_upload_reply_<?php echo $i; ?>" class="form-control" id="edit_filter_video_upload_reply_<?php echo $i; ?>" placeholder="<?php echo $this->lang->line("put your image url here or click upload") ?>"  />

										<div class="overlay_wrapper">
											<span></span>
											<video width="100%" height="240" controls style="border:1px solid #ccc;display:none;">
												<source src="" id="edit_filter_video_upload_reply_display<?php echo $i; ?>" type="video/mp4">
											<?php echo $this->lang->line("your browser does not support the video tag.") ?>
											</video>
										</div>
									</div>
								</div>
							</div>
							<!-- comment hide and delete section -->

							<br/>
							
							<label>
							  <i class="bx bx-envelope"></i> <?php echo $this->lang->line("Select a message template for private reply") ?> [<?php echo $this->lang->line('Maximum two reply message is supported.'); ?>]
							</label>
							<div>                      
							  <select class="form-group private_reply_postback" id="edit_filter_message_<?php echo $i; ?>" name="edit_filter_message_<?php echo $i; ?>">
							    <option><?php echo $this->lang->line('Please select a page first to see the message templates.'); ?></option>
							  </select>

							  <a href="" class="add_template float-left"><i class="bx bx-plus-circle"></i>     <?php echo $this->lang->line("Add Message Template");?></a>
							  <a href="" class="ref_template float-right"><i class="bx bx-sync"></i> <?php echo $this->lang->line("Refresh List");?></a>

							</div>

						</div>
					<?php endfor; ?>
						

						<div class="clearfix">
							<input type="hidden" name="edit_content_counter" id="edit_content_counter" />
							<button type="button" class="btn btn-sm btn-outline-primary float-right" id="edit_add_more_button"><i class="bx bx-plus"></i> <?php echo $this->lang->line("add more filtering") ?></button>
						</div>

						<div class="form-group clearfix" id="edit_nofilter_word_found_div" style="margin-top: 10px; border: 1px dashed #e4e6fc; padding: 20px;">
							<label>
								<i class="bx bx-envelope"></i> <?php echo $this->lang->line("comment reply if no matching found") ?>
								<a href="#" data-placement="bottom"  data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("message") ?>" data-content="<?php echo $this->lang->line("Write the message,  if no filter word found. If you don't want to send message them, just keep it blank ."); ?>  Spintax example : {Hello|Howdy|Hola} to you, {Mr.|Mrs.|Ms.} {{Jason|Malina|Sara}|Williams|Davis}"><i class='bx bx-info-circle'></i> </a>
							</label>
							<?php if($comment_tag_machine_addon) {?>
							<span class='float-right'> 
								<a title="<?php echo $this->lang->line("You can tag user in your comment reply. Facebook will notify them about mention whenever you tag.") ?>" data-toggle="tooltip" data-placement="top" class='btn-sm lead_tag_name button-outline'><i class='bx bx-tag'></i>  <?php echo $this->lang->line("tag user") ?></a>
							</span>
							<?php } ?>
							<span class='float-right'> 
							  <a title="<?php echo $this->lang->line("You can include #LEAD_USER_LAST_NAME# variable inside your message. The variable will be replaced by real names when we will send it.") ?>" data-toggle="tooltip" data-placement="top" class='btn-sm lead_last_name button-outline'><i class='bx bx-user'></i> <?php echo $this->lang->line("last name") ?></a>
							</span>
							<span class='float-right'> 
							  <a title="<?php echo $this->lang->line("You can include #LEAD_USER_FIRST_NAME# variable inside your message. The variable will be replaced by real names when we will send it.") ?>" data-toggle="tooltip" data-placement="top" class='btn-sm lead_first_name button-outline'><i class='bx bx-user'></i> <?php echo $this->lang->line("first name") ?></a>
							</span> 
							<div class="clearfix"></div>
							<textarea class="form-control message" name="edit_nofilter_word_found_text" id="edit_nofilter_word_found_text"  placeholder="<?php echo $this->lang->line("type your message here...") ?>" style="height:170px !important;"></textarea>
							
							
							<!-- comment hide and delete section -->
							<br/>
							<div class="clearfix" <?php if(!$commnet_hide_delete_addon) echo "style='display: none;'"; ?> >
								<div class="row">									
									<div class="col-12 col-md-6">
										<label class="control-label" ><i class="bx bx-image"></i> <?php echo $this->lang->line("image for comment reply") ?>
										</label>									
										<div class="form-group">      
											<div id="edit_nofilter_image_upload"><?php echo $this->lang->line("upload") ?></div>	     
										</div>
										<div id="edit_nofilter_generic_image_preview_id"></div>
										<span class="text-danger" id="edit_nofilter_image_upload_reply_error"></span>
										<input type="text" name="edit_nofilter_image_upload_reply" class="form-control" id="edit_nofilter_image_upload_reply" placeholder="<?php echo $this->lang->line("put your image url here or click the above upload button") ?>" style="margin-top: -14px;" />

										<div class="overlay_wrapper">
											<span></span>
											<img src="" alt="image" id="edit_nofilter_image_upload_reply_display"  height="240" width="100%" style="display:none;" />
										</div>
									</div>

									<div class="col-12 col-md-6">
										<label class="control-label" ><i class="bx bxl-youtube"></i> <?php echo $this->lang->line("video for comment reply") ?> [mp4 <?php echo $this->lang->line("Prefered");?>]
											<a href="#" data-placement="bottom" data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("video upload") ?>" data-content="<?php echo $this->lang->line("image and video will not work together. Please choose either image or video.") ?> [mp4,wmv,flv]"><i class='bx bx-info-circle'></i></a>
										</label>
										<div class="form-group">      
											<div id="edit_nofilter_video_upload"><?php echo $this->lang->line("upload") ?></div>	     
										</div>
										<div id="edit_nofilter_video_preview_id"></div>
										<span class="text-danger" id="edit_nofilter_video_upload_reply_error"></span>
										<input type="hidden" name="edit_nofilter_video_upload_reply" class="form-control" id="edit_nofilter_video_upload_reply" placeholder="<?php echo $this->lang->line("put your image url here or click upload") ?>" />

										<div class="overlay_wrapper">
											<span></span>
											<video width="100%" height="240" controls style="border:1px solid #ccc;display:none;">
												<source src="" id="edit_nofilter_video_upload_reply_display" type="video/mp4">
											<?php echo $this->lang->line("your browser does not support the video tag.") ?>
											</video>
										</div>
									</div>
								</div>
							</div>
							<br/><br/>
							<!-- comment hide and delete section -->

							<label>
							  <i class="bx bx-envelope"></i> <?php echo $this->lang->line("Select a message template for private reply if no matching found") ?> [<?php echo $this->lang->line('Maximum two reply message is supported.'); ?>]
							</label>
							<div>                      
							  <select class="form-group private_reply_postback" id="edit_nofilter_word_found_text_private" name="edit_nofilter_word_found_text_private">
							    <option><?php echo $this->lang->line('Please select a page first to see the message templates.'); ?></option>
							  </select>

							  <a href="" class="add_template float-left"><i class="bx bx-plus-circle"></i>     <?php echo $this->lang->line("Add Message Template");?></a>
							  <a href="" class="ref_template float-right"><i class="bx bx-sync"></i> <?php echo $this->lang->line("Refresh List");?></a>

							</div>
							
						</div>


					</div>
				</div>
				<div class="col-12 text-center" id="edit_response_status"></div>
			</div>
			</form>
			<div class="clearfix"></div>
			<div class="modal-footer" style="padding-left: 45px; padding-right: 45px; ">
				<div class="row">
					<div class="col-6">
						<button class="btn btn-primary float-left" id="edit_save_button"><i class='bx bx-save'></i> <span class="align-middle ml-25"><?php echo $this->lang->line("save"); ?></span></button>
					</div>  
					<div class="col-6">
						<button class="btn btn-secondary float-right cancel_button"><i class='bx bx-time'></i> <span class="align-middle ml-25"><?php echo $this->lang->line("cancel"); ?></span></button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="manual_reply_by_post" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
			  <h3 class="modal-title"><?php echo $this->lang->line("Enable reply by Post ID") ?> (<span id="manual_page_name"></span>)</h3>
			  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<i class="bx bx-x"></i>
			  </button>
			</div>

			<div class="modal-body ">
				<div class="row">
					<div class="col-12">						
						<div class="alert alert-light alert-has-icon alert-dismissible show fade">
							<div class="alert-icon"><i class="bx bx-paperclip"></i></div>
							<div class="alert-body">
								<button class="close" data-dismiss="alert">
									<span>×</span>
								</button>
								<div class="alert-title"><?php echo $this->lang->line('Disclaimer'); ?></div>
								<?php 
									echo $this->lang->line("Facebook ads post that you have created from Facebook Ads Manager, if you have ever edited/modified your ads after creating, then this technique of 'Set Campaign by ID' may not work as Facebook creates different variation of your post for each time you edit. Preview post for that ads may have different ID, which leads to a wrong ID actually sometimes."); 
									if(addon_exist(204,"comment_reply_enhancers"))
										echo $this->lang->line("In this case we suggest to use 'Full page campaigns' feature.");
								?>	
							</div>
						</div>
					</div>
					<br/><br/>
					<div class="col-12" id="waiting_div"></div>
					<div class="col-12">
						<form>

						    <input type="hidden" id="manual_table_id">
							<div class="input-group">
		                        <div class="input-group-prepend">
		                          <div class="input-group-text">
		                            <?php echo $this->lang->line("post id") ?>
		                          </div>
		                        </div>
		                        <input type="text" class="form-control" id="manual_post_id" placeholder="<?php echo $this->lang->line("please give a post id") ?>">
		                    </div>

							<div class="text-center" id="manual_reply_error"></div>
						  	<br/>
							<div class="form-group text-center" id="manual_check_button_div">
							</div>
						 </form>
						 <div class="form-group text-center">
							<button type="button" class="btn btn-info" id="check_post_id"><i class="bx bx-check"></i> <?php echo $this->lang->line("check existance") ?></button>
						 </div>
						
					</div>                    
				</div>               
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="manual_edit_reply_by_post" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close rounded-pill" data-dismiss="modal" aria-label="Close"><i class="bx bx-x"></i></button>
				<h3 class="modal-title"><?php echo $this->lang->line("please provide a post id of page") ?> (<span id="manual_edit_page_name"></span>)</h3>
			</div>
			<div class="modal-body ">
				<div class="row">
					<div class="col-12" id="waiting_div"></div>
					<div class="col-12 col-md-8 col-md-offset-2">
						<form>
							<div class="form-group">
							  <label for="manual_post_id"><?php echo $this->lang->line("post id") ?> :</label>
							  <input type="text" class="form-control" id="manual_edit_post_id" placeholder="<?php echo $this->lang->line("please give a post id") ?>" value="">
							  <input type="hidden" id="manual_edit_table_id">
							</div>
							<div class="text-center" id="manual_edit_error"></div>                           
						</form>
						<div class="form-group text-center" style="margin-top: 15px;">
						   <button type="button" class="btn btn-outline-warning edit_reply_info" id="manual_edit_auto_reply"><i class="bx bx-edit"></i> <?php echo $this->lang->line("edit auto reply") ?></button>
						</div>
						
					</div>                    
				</div>               
			</div>
		</div>
	</div>
</div>






<!-- comment hide and delete section -->
<div class="modal fade" id="modal-live-video-library"  data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
	  <div class="modal-header clearfix">
		<a class="pull-right" id="filemanager_close" style="font-size: 14px; color: white; cursor: pointer;" >&times;</a>
		<h3 class="modal-title"><i class="bx bx-videos"></i> <?php echo $this->lang->line("filemanager Library") ?></h3>
	  </div>
	  <div class="modal-body">
		
	  </div>
	</div>
  </div>
</div>

<!-- instand comment checker -->
<div class="modal fade" id="instant_comment_modal" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			  <h3 class="modal-title"><i class="bx bx-comment-dots"></i> <?php echo $this->lang->line("Instant Comment") ?></h3>
			  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<i class="bx bx-x"></i>
			  </button>
			</div>

			<div class="modal-body">     
				<input type="hidden" name="instant_comment_page_id" id="instant_comment_page_id">           
				<input type="hidden" name="instant_comment_post_id" id="instant_comment_post_id">           
				<div class="row">
					<div class="col-12">
						<div class="form-group">
							<label><i class="bx bx-keyboard"></i> <?php echo $this->lang->line("Please provide a message as comment") ?></label>
							<textarea class="form-control" name="instant_comment_message" id="instant_comment_message" placeholder="<?php echo $this->lang->line("Type your comment here.") ?>"></textarea>
						</div>
					</div>
					<div class="col-12">
						<button class="btn btn-primary submit_instant_comment"><i class="bx bx-paper-plane"></i> <?php echo $this->lang->line('Create Comment'); ?></button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<style type="text/css">
	.ajax-upload-dragdrop{width:100% !important;}

	.overlay_wrapper {
	  position: relative;
	  max-height: 240px;
	  width: 100%;
	  overflow: hidden;
	}

	.overlay_wrapper span.remove_media {
	  position: absolute;
	  right: 5px;
	  top: 5px;
	  background-color: black;
	  color: white;
	  padding: 4px 15px;
	  font-size: 12px;
	  border-radius: 15px;
	  transition: 0.4s;
	  -o-transition: 0.4s;
	  -webkit-transition: 0.4s;
	  -moz-transition: 0.4s;
	  -ms-transition: 0.4s;
	  opacity: 0;
	  cursor: pointer;
	  visibility: hidden;
	}

	.overlay_wrapper:hover span.remove_media{
	  display: block;
	  opacity: 1;
	  visibility: visible;
	  z-index: 1000;
	}
	.add_template,.ref_template{font-size: 10px;}

    #add_template_modal{z-index: 1053!important;}
</style>



<input type="hidden" name="dynamic_page_id" id="dynamic_page_id">
