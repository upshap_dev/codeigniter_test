<?php
if(!empty($current_store_data) && !empty($store_data))
{
    $max = (!empty($earning_chart_values)) ? max($earning_chart_values) : 0;
    $steps = $max/5;
    if($steps==0) $steps = 1;
    ?>

    <script type="text/javascript">
        var statistics_chart = document.getElementById("myChart").getContext('2d');
        var myChart = new Chart(statistics_chart, {
            type: 'line',
            data: {
                labels: <?php echo json_encode($earning_chart_labels); ?>,
                datasets: [{
                    label: '<?php echo $this->lang->line("Earning"); ?>',
                    data: <?php echo json_encode(array_values($earning_chart_values)); ?>,
                    borderWidth: 3,
                    borderColor: '#6777ef',
                    backgroundColor: 'transparent',
                    pointBackgroundColor: '#6777ef',
                    pointBorderColor: '#6777ef',
                    pointRadius: 3
                }]
            },
            options: {
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        gridLines: {
                            display: false,
                            drawBorder: false,
                        },
                        ticks: {
                            stepSize: <?php echo $steps; ?>
                        }
                    }],
                    xAxes: [{
                        gridLines: {
                            display: false,
                            drawBorder: false,
                            color: '#dee2e6',
                            lineWidth: 1
                        }
                    }]
                },
            }
        });
    </script>
    <?php
} ?>

<script>
    $(document).ready(function($) {
        var base_url = '<?php echo base_url(); ?>';


        $(document).on('click', '.delete_campaign', function(event) {
            event.preventDefault();

            swal.fire({
                title: '<?php echo $this->lang->line("Delete Store"); ?>',
                text: '<?php echo $this->lang->line("Do you really want to delete this store? Deleting store will also delete all related data like cart,purchase,settings etc."); ?>',
                icon: 'warning',
                    confirmButtonText: "<?php echo $this->lang->line('Yes'); ?>",
    cancelButtonText: "<?php echo $this->lang->line('No'); ?>",
    showCancelButton: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete.isConfirmed)
                    {
                        var base_url = '<?php echo site_url();?>';
                        $(this).addClass('btn-danger btn-progress');
                        $(this).removeClass('btn-outline-danger');
                        var that = $(this);
                        var campaign_id = $(this).attr('campaign_id');

                        $.ajax({
                            context: this,
                            type:'POST' ,
                            url:"<?php echo site_url();?>ecommerce/delete_store",
                            dataType: 'json',
                            data:{campaign_id : campaign_id},
                            success:function(response){

                                $(that).removeClass('btn-danger btn-progress');
                                $(this).addClass('btn-outline-danger');

                                if(response.status == '1')
                                {
                                    iziToast.success({title: '<?php echo $this->lang->line("Deleted Successfully"); ?>', message: response.message,position: 'bottomRight'});
                                    $("#search_submit").click();
                                }
                                else
                                    iziToast.error({title: '<?php echo $this->lang->line("Error"); ?>',message: response.message ,position: 'bottomRight'});
                            }
                        });
                    }
                });
        });

        $(document).on('click', '#copy_urls', function(event) {
            event.preventDefault();
            $("#copy_data_modal").modal();
        });

        $('#store_list_select').on('change', function (e) {
            var store_id = $('#store_list_select').val();
            $("#store_id").val(store_id);
            $("#search_submit").click();
        });

        var table2="";
        $(document).on('click', '.reminder_report', function(event) {
            event.preventDefault();
            $("#reminder_data").modal();

            setTimeout(function(){
                if (table2 == '')
                {
                    var perscroll2;
                    table2 = $("#mytable2").DataTable({
                        serverSide: true,
                        processing:true,
                        bFilter: true,
                        order: [[ 7, "desc" ]],
                        pageLength: 10,
                        ajax: {
                            url: '<?php echo base_url("ecommerce/reminder_send_status_data"); ?>',
                            type: 'POST',
                            data: function ( d )
                            {
                                d.page_id = $('#hidden_page_id').val();
                            }
                        },
                        language:
                            {
                                url: '<?php echo base_url('assets/modules/datatables/language/'.$this->language.'.json');?>'
                            },
                        dom: '<"top"f>rt<"bottom"lip><"clear">',
                        columnDefs: [
                            {
                                targets: [1],
                                visible: false
                            },
                            {
                                targets: [0,5,6,7,8],
                                className: 'text-center'
                            }
                        ],
                        fnInitComplete:function(){  // when initialization is completed then apply scroll plugin
                            if(areWeUsingScroll)
                            {
                                if (perscroll2) perscroll2.destroy();
                                perscroll2 = new PerfectScrollbar('#mytable2_wrapper .dataTables_scrollBody');
                            }
                        },
                        scrollX: 'auto',
                        fnDrawCallback: function( oSettings ) { //on paginition page 2,3.. often scroll shown, so reset it and assign it again
                            if(areWeUsingScroll)
                            {
                                if (perscroll2) perscroll2.destroy();
                                perscroll2 = new PerfectScrollbar('#mytable2_wrapper .dataTables_scrollBody');
                            }
                        }
                    });
                }
                else table2.draw();
            }, 1000);
        });


        $(document).on('click','.woo_error_log',function(e){
            e.preventDefault();
            $(this).removeClass('btn-outline-primary').addClass("btn-primary").addClass('btn-progress');
            var id = $(this).attr('data-id');

            $.ajax
            ({
                type:'POST',
                url:base_url+'ecommerce/reminder_response',
                data:{id:id},
                context: this,
                success:function(response)
                {
                    $(this).addClass('btn-outline-primary').removeClass("btn-primary").removeClass('btn-progress');

                    var success_message= response;
                    var span = document.createElement("span");
                    span.innerHTML = success_message;
                    swal.fire({ title:'<?php echo $this->lang->line("API Response"); ?>', html:span,icon:'info'});
                }
            });
        });

        $(document).on('click','.iframed',function(e){
            e.preventDefault();
            var iframe_url = $(this).attr('href');
            var iframe_height = $(this).attr('data-height');
            $("iframe").attr('src',iframe_url).show();
            $(".hide_in_iframe").hide();
            $('.hide_row_iframed').hide();
            var title=" : "+$(this).attr("data-original-title");
            $("#iframe_title").html(title);
        });

        $("#products-carousel").owlCarousel({
            <?php if($rtl_on){ echo 'rtl:true,';}  ?>
            items: 4,
            margin: 10,
            autoplay: true,
            autoplayTimeout: 5000,
            loop: true,
            responsive: {
                0: {
                    items: 2
                },
                768: {
                    items: 2
                },
                1200: {
                    items: 4
                }
            }
        });

        $('.datepicker_x').datetimepicker({
            theme:'light',
            format:'Y-m-d H:i:s',
            formatDate:'Y-m-d H:i:s',
            // minDate: today
        });

        $(".settings_menu a").click(function(){
            $(".settings_menu a").removeClass("active");
            $(this).addClass("active");
        });

        // Toggle menu

        $(document).ready(function(){
            $('body').addClass('menu-collapsed');
            $('.brand-logo').removeClass('d-none');

            $(".select2-icons").select2({
                dropdownAutoWidth: true,
                width: '100%',
                minimumResultsForSearch: Infinity,
                templateResult: iconFormat,
                templateSelection: iconFormat,
                escapeMarkup: function(es) { return es; }
            });

            function iconFormat(icon) {
                var originalOption = icon.element;
                if (!icon.id) { return icon.text; }
                var $icon = "<i class='" + $(icon.element).data('icon') + "'></i>" + icon.text;

                return $icon;
            }
        });


    });


</script>