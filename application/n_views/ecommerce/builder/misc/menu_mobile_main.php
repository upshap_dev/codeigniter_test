<!-- Start of Mobile Menu -->
<div class="mobile-menu-wrapper">
    <div class="mobile-menu-overlay"></div>
    <!-- End of .mobile-menu-overlay -->

    <a href="#" class="mobile-menu-close"><i class="close-icon"></i></a>
    <!-- End of .mobile-menu-close -->

    <div class="mobile-menu-container scrollable">
        <form action="#" method="get" class="input-wrapper d-none">
            <input type="text" class="form-control" name="search" autocomplete="off"
                   placeholder="Search" required />
            <button class="btn btn-search" type="submit">
                <i class="w-icon-search"></i>
            </button>
        </form>
        <!-- End of Search Form -->
        <div class="tab">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a href="#main-menu" class="nav-link active"><?php echo $l->line('Main Menu'); ?></a>
                </li>
                <li class="nav-item">
                    <a href="#categories" class="nav-link"><?php echo $l->line('Categories'); ?></a>
                </li>
            </ul>
        </div>
        <div class="tab-content">
            <div class="tab-pane active" id="main-menu">
                <ul class="mobile-menu">
                    <li>
                       <a href="<?php e_link('ecommerce/store/'.$js_store_unique_id); ?>"><?php echo $l->line('Home'); ?></a>
                    </li>
                    <li>
                        <a href="<?php e_link($my_account_link); ?>"><?php echo $l->line('My account'); ?></a>
                    </li>
                    <li>
                        <a href="<?php e_link('ecommerce/terms/'.$js_store_unique_id); ?>"><?php echo $l->line('Terms'); ?></a>
                    </li>
                    <li>
                        <a href="<?php e_link('ecommerce/refund_policy/'.$js_store_unique_id); ?>"><?php echo $l->line('Refund policy'); ?></a>
                    </li>
                    <li>
                        <a href="<?php e_link('ecommerce/contact/'.$js_store_unique_id); ?>"><?php echo $l->line('Contact'); ?></a>
                    </li>

                    <?php if(!$purchase_off AND $subscriberId=="" ){ ?>
                            <li>
                        <a style="display:inline-block" href="<?php e_link('ecommerce/login_signup/'.$js_store_unique_id); ?>" class="login sign-in"><?php echo $l->line('Sign in'); ?></a>
                        <span class="delimiter">/</span>
                        <a style="display:inline-block" href="<?php e_link('ecommerce/login_signup/'.$js_store_unique_id); ?>" class="ml-0 login register"><?php echo $l->line('Register'); ?></a>
                            </li>

                    <?php }else{ ?>
                        <li>
                        <a href="<?php e_link('ecommerce/login_signup/'.$js_store_unique_id); ?>" class="login sign-out logout_btn" ><?php echo $l->line('Logout'); ?></a>
                        </li>
                    <?php  } ?>




                </ul>
            </div>
            <div class="tab-pane" id="categories">
                <ul class="mobile-menu">
                    <?php

                    $active_class = '';
                    $menu_active_set = 'active';
                    unset($category_list['']);

                    $menu_build = '';
                    if(empty($category_id)){$category_id = -1;}
                    foreach ($category_list_raw as $key => $value){
                        if($category_id==$value['id'].'_'.url_title($value["category_name"])){
                            $active_class = 'active';
                            $menu_active_set = '';
                        }

                        $menu_build .= '<li class="'.$active_class.'">
                                <a href="'._link('ecommerce/category/'.$value['id'].'_'.url_title($value["category_name"])).'">'.$value["category_name"].'</a>
                            </li>';

                        $active_class = '';

                    }
                    echo '<li class="'.$menu_active_set.'">
                                <a href="'._link('ecommerce/store/'.$js_store_unique_id).'">'.$l->line('Home').'</a>
                            </li>';

                    echo $menu_build;


                    ?>


                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End of Mobile Menu -->