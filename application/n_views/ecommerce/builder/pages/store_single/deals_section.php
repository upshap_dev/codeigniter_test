<?php //start deals
if($n_eco_builder_config['front_deals_products_show'] =='true'){
    if(!empty($deals_list)) {
        ?>

        <div class="title-link-wrapper pb-1 mb-4">
            <h2 class="title ls-normal mb-0"><?php echo $n_eco_builder_config['front_deals_products_text']; ?></h2>
        </div>
    <div class="<?php columns_width($n_eco_builder_config['front_deals_products_rows']); ?>">

        <?php
        $hide_reviews_listing = 'true';
        $always_show_reviews = 'false';
        if($n_eco_builder_config['front_deals_reviews_show']=='show' OR $n_eco_builder_config['front_deals_reviews_show'] == 'always_show'){
            $hide_reviews_listing = 'false';
        }
        if($n_eco_builder_config['front_deals_reviews_show'] == 'always_show'){
            $always_show_reviews = 'true';
        }

        foreach ($deals_list as $key => $value){
            $imgSrc = ($value['thumbnail']!='') ? base_url('upload/ecommerce/'.$value['thumbnail']) : base_url('assets/img/products/product-1.jpg');

            if(isset($value["woocommerce_product_id"]) && !is_null($value["woocommerce_product_id"]) && $value['thumbnail']!=''){$imgSrc = $value['thumbnail'];}

            $display_price = mec_display_price($value['original_price'],$value['sell_price'],' '.$currency_icon.' ','1',$currency_position,$decimal_point,$thousand_comma);

            include(APPPATH.'n_views/ecommerce/builder/product/product_v1.php');
        };
        echo '</div>';
    };
};
//end deals
?>