<?php
if(isset($_GET['builder']) AND $_GET['builder']==1){
    if(function_exists('opcache_reset')){
        opcache_reset();
    }
}
$js_store_id = isset($social_analytics_codes['store_id']) ? $social_analytics_codes['store_id'] : $social_analytics_codes['id'];
$js_store_unique_id = isset($social_analytics_codes['store_unique_id']) ? $social_analytics_codes['store_unique_id'] : "";

    include(FCPATH.'application/n_views/config.php');
    include(APPPATH.'n_views/default_ecommerce_builder.php');

    if(file_exists(APPPATH . '/n_eco_user/builder/ecommerce_builder_' . $js_store_unique_id . '.php')){
        include(APPPATH . '/n_eco_user/builder/ecommerce_builder_' . $js_store_unique_id . '.php');
    }

    if(isset($_GET['builder']) AND $_GET['builder']==1){
        if(isset($_GET['new_var'])){
            $new_var = json_decode($_GET['new_var'], TRUE);
            foreach ($new_var AS $k => $v){
                $v = str_replace(array('apos', 'quot'),array('&apos;','&quot;'),$v);
                $n_eco_builder_config[$k] = $v;
            }
        }
    }

include(APPPATH.'n_views/ecommerce/builder/variables.php');
include(FCPATH.'application/n_views/include/function_helper_theme.php');


$body = str_replace('ecommerce/', '', $body);
    ?>

<!DOCTYPE html>
<html lang="<?php echo LangToCode($this->language, $n_config); ?>" <?php if(strpos(strtolower($n_config['rtl_langs']), strtolower($this->language)) !== false){echo 'dir="rtl" class="rtl_on"';} ?>>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">

    <title><?php echo isset($page_title) ? $page_title : $this->config->item('product_name');?></title>

    <meta name="keywords" content="<?php echo $n_eco_builder_config['site_keywords']; ?>" />
    <meta name="description" content="<?php echo $n_eco_builder_config['site_description']; ?>">

    <!-- Favicon -->
    <link rel="icon" type="image/png" href="<?php echo base_url($n_eco_builder_config['store_favicon']);?>">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/modules/flag-icon-css/css/flag-icon.min.css?ver=<?php echo $n_config['theme_version']; ?>">
    <!-- WebFont.js -->
    <script>
        WebFontConfig = {
            google: { families: ['<?php echo $n_eco_builder_config['font_family']; ?>:400,500,600,700,800',] }
        };
        (function (d) {
            var wf = d.createElement('script'), s = d.scripts[0];
            wf.src = '<?php echo base_url();?>n_assets/ecommerce/js/webfont.js';
            wf.async = true;
            s.parentNode.insertBefore(wf, s);
        })(document);
    </script>

    <link rel="preload" href="<?php echo base_url();?>n_assets/ecommerce/vendor/fontawesome-free/webfonts/fa-regular-400.woff2" as="font" type="font/woff2"
          crossorigin="anonymous">
    <link rel="preload" href="<?php echo base_url();?>n_assets/ecommerce/vendor/fontawesome-free/webfonts/fa-solid-900.woff2" as="font" type="font/woff2"
          crossorigin="anonymous">
    <link rel="preload" href="<?php echo base_url();?>n_assets/ecommerce/vendor/fontawesome-free/webfonts/fa-brands-400.woff2" as="font" type="font/woff2"
          crossorigin="anonymous">
    <link rel="preload" href="<?php echo base_url();?>n_assets/ecommerce/fonts/wolmart.ttf?png09e" as="font" type="font/ttf" crossorigin="anonymous">

    <!-- Vendor CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>n_assets/ecommerce/vendor/fontawesome-free/css/all.min.css?ver=<?php echo $n_config['theme_version']; ?>">

    <!-- Plugins CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>n_assets/ecommerce/vendor/owl-carousel/owl.carousel.min.css?ver=<?php echo $n_config['theme_version']; ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>n_assets/ecommerce/vendor/animate/animate.min.css?ver=<?php echo $n_config['theme_version']; ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>n_assets/ecommerce/vendor/magnific-popup/magnific-popup.min.css?ver=<?php echo $n_config['theme_version']; ?>">


    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>n_assets/ecommerce/vendor/photoswipe/photoswipe.min.css?ver=<?php echo $n_config['theme_version']; ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>n_assets/ecommerce/vendor/photoswipe/default-skin/default-skin.min.css?ver=<?php echo $n_config['theme_version']; ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>n_assets/ecommerce/vendor/threesixty-degree/360-degree-riewer.min.css?ver=<?php echo $n_config['theme_version']; ?>">


    <!-- Default CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>n_assets/ecommerce/css/style.min.css?ver=<?php echo $n_config['theme_version']; ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>n_assets/ecommerce/css/demo8.min.css?ver=<?php echo $n_config['theme_version']; ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>n_assets/ecommerce/vendor/sweetalert2/sweetalert2.min.css?ver=<?php echo $n_config['theme_version']; ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>n_assets/ecommerce/css/style_custom.css?ver=<?php echo $n_config['theme_version']; ?>">


    <?php
    $home_section = array();
    $home_section['gjs-html'] = '';
    $home_section['gjs-components'] = '';
    $home_section['gjs-assets'] = '';
    $home_section['gjs-css'] = '';
    $home_section['gjs-styles'] = '';

    $default_lang_inc = '';
    if($this->language != $social_analytics_codes['store_locale']){
        $default_lang_inc = strtolower($this->language);
    }


    if($n_eco_builder_config['home_page_editor_on']=='true' AND file_exists(APPPATH . 'n_eco_user/home_page_' . $js_store_id . '_p'.$default_lang_inc.'.php') AND $body=='store_single'){
        $import_home_section = APPPATH . 'n_eco_user/home_page_' . $js_store_id . $default_lang_inc .'_p.php';
        $n_editor_data = file_get_contents($import_home_section);
        $home_section = json_decode($n_editor_data, true);
    }
    ?>

    <style>
        <?php
            include(APPPATH.'n_views/ecommerce/builder/ecommerce_custom_styles.php');
            echo "\r\n\r\n";
             if (file_exists(APPPATH . '/n_eco_user/codes_custom_css_store_id_' . $js_store_id . '.php')) {
                include(APPPATH . '/n_eco_user/codes_custom_css_store_id_' . $js_store_id . '.php');
            }

             echo $home_section['gjs-css'];
             //echo $home_section['gjs-styles'];
        ?>
    </style>

    <script>
    <?php
        echo $home_section['gjs-components'];
    ?>
    </script>
</head>

<body class="<?php if($body=='store_single'){echo ' home';} if($body=='category'){echo ' category_page';}else{echo $body;} ?>">
    <div class="page-wrapper">
        <?php //header

        if(file_exists(APPPATH.'n_views/ecommerce/builder/header/'.$n_eco_builder_config['section_header'].'.php')){
            include(APPPATH.'n_views/ecommerce/builder/header/'.$n_eco_builder_config['section_header'].'.php');
        }

        echo '<div class="row">'.$home_section['gjs-html'].'</div>';

        $body = $n_eco_builder_config[$body];
        if(file_exists(APPPATH.'n_views/ecommerce/builder/pages/'.$body.'.php')){
            include(APPPATH.'n_views/ecommerce/builder/pages/'.$body.'.php');
        }

        ?>
    </div>





    <!-- Start of Scroll Top -->
    <a id="scroll-top" href="#top" title="Top" role="button" class="scroll-top"><i class="fas fa-chevron-up"></i></a>
    <!-- End of Scroll Top -->
    
    <!-- Plugin JS File -->
    <script src="<?php echo base_url();?>n_assets/ecommerce/vendor/jquery/jquery.min.js?ver=<?php echo $n_config['theme_version']; ?>"></script>
    <script src="<?php echo base_url();?>n_assets/ecommerce/vendor/parallax/parallax.min.js?ver=<?php echo $n_config['theme_version']; ?>"></script>
    <script src="<?php echo base_url();?>n_assets/ecommerce/vendor/jquery.plugin/jquery.plugin.min.js?ver=<?php echo $n_config['theme_version']; ?>"></script>
    <script src="<?php echo base_url();?>n_assets/ecommerce/vendor/owl-carousel/owl.carousel.min.js?ver=<?php echo $n_config['theme_version']; ?>"></script>
    <script src="<?php echo base_url();?>n_assets/ecommerce/vendor/imagesloaded/imagesloaded.pkgd.min.js?ver=<?php echo $n_config['theme_version']; ?>"></script>
    <script src="<?php echo base_url();?>n_assets/ecommerce/vendor/skrollr/skrollr.min.js?ver=<?php echo $n_config['theme_version']; ?>"></script>
    <script src="<?php echo base_url();?>n_assets/ecommerce/vendor/magnific-popup/jquery.magnific-popup.min.js?ver=<?php echo $n_config['theme_version']; ?>"></script>
    <script src="<?php echo base_url();?>n_assets/ecommerce/vendor/zoom/jquery.zoom.js?ver=<?php echo $n_config['theme_version']; ?>"></script>
    <script src="<?php echo base_url();?>n_assets/ecommerce/vendor/jquery.countdown/jquery.countdown.min.js?ver=<?php echo $n_config['theme_version']; ?>"></script>
    <script src="<?php echo base_url();?>n_assets/ecommerce/vendor/sweetalert2/sweetalert2.min.js?ver=<?php echo $n_config['theme_version']; ?>"></script>

    <script src="<?php echo base_url();?>n_assets/ecommerce/vendor/sticky/sticky.min.js?ver=<?php echo $n_config['theme_version']; ?>"></script>
    <script src="<?php echo base_url();?>n_assets/ecommerce/vendor/imagesloaded/imagesloaded.pkgd.min.js?ver=<?php echo $n_config['theme_version']; ?>"></script>
    <script src="<?php echo base_url();?>n_assets/ecommerce/vendor/photoswipe/photoswipe.min.js?ver=<?php echo $n_config['theme_version']; ?>"></script>
    <script src="<?php echo base_url();?>n_assets/ecommerce/vendor/photoswipe/photoswipe-ui-default.min.js?ver=<?php echo $n_config['theme_version']; ?>"></script>
    <script src="<?php echo base_url();?>n_assets/ecommerce/vendor/threesixty-degree/threesixty.min.js?ver=<?php echo $n_config['theme_version']; ?>"></script>
    <script src="<?php echo base_url();?>n_assets/ecommerce/js/popper.js?ver=<?php echo $n_config['theme_version']; ?>"></script>


    <!-- Main JS -->
    <script src="<?php echo base_url();?>n_assets/ecommerce/js/main.js?ver=<?php echo $n_config['theme_version']; ?>"></script>
    <?php include(APPPATH.'n_views/ecommerce/builder/variables_js.php'); ?>
    <script src="<?php echo base_url();?>n_assets/ecommerce/js/app_ecommerce.js?ver=<?php echo $n_config['theme_version']; ?>"></script>

    <?php
    if(file_exists(APPPATH.'n_views/ecommerce/builder/pages/'.$body.'_js.php')){
        include(APPPATH.'n_views/ecommerce/builder/pages/'.$body.'_js.php');
    }
        if (file_exists(APPPATH . '/n_eco_user/before_body_store_id_' . $js_store_id . '.php')) {
            include(APPPATH . '/n_eco_user/before_body_store_id_' . $js_store_id . '.php');
        }

        if(isset($output_js)){
            echo $output_js;
        }
    ?>

    <?php
    if($n_eco_builder_config['show_mobile_menu']=='true'){
        include(APPPATH.'n_views/ecommerce/builder/misc/sticky_footer.php');
    }

    include(APPPATH.'n_views/ecommerce/builder/footer/footer_v1.php');
    include(APPPATH.'n_views/ecommerce/builder/misc/menu_mobile_main.php');
    ?>

</body>

</html>