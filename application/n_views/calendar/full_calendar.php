<style type="text/css">
	
		.fc-button-group button:focus{
			outline: none;
			
		}

    .fc-icon-left-single-arrow:after {
        font-size: 32px !important; 
        top: -15% !important; 
    }

    .fc-icon-right-single-arrow:after {
        font-size: 32px !important; 
        top: -15% !important; 
    }

</style>


<!-- fullCalendar -->
<link rel="stylesheet" href="<?php echo base_url();?>/plugins/fullcalendar/fullcalendar.min.css?ver=<?php echo $n_config['theme_version']; ?>">
<!-- full calender -->

<div class="content-header row">
    <div class="content-header-left col-12 mb-2 mt-1">
        <div class="breadcrumbs-top">
            <h5 class="content-header-title float-left pr-1 mb-0"><?php echo $page_title; ?></h5>
            <div class="breadcrumb-wrapper d-none d-sm-block">
                <ol class="breadcrumb p-0 mb-0 pl-1">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard"><i class="bx bx-home-alt"></i></a></li>
                    <li class="breadcrumb-item active"><?php echo $page_title; ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>



  <div class="section-body">
    <div class="card">
      <div class="card-body">
        <div id="su"></div>
      </div>
    </div>
  </div>



