<script src="<?php echo base_url(); ?>n_assets/app-assets/vendors/js/extensions/moment.min.js?ver=<?php echo $n_config['theme_version']; ?>"></script>
<script src="<?php echo base_url();?>plugins/fullcalendar/fullcalendar.min.js?ver=<?php echo $n_config['theme_version']; ?>"></script>
<script>

    $(function () {



        /* initialize the calendar
         -----------------------------------------------------------------*/
        //Date for the calendar events (dummy data)

        var events = <?php echo json_encode($data) ?>;

        var date = new Date()
        var d    = date.getDate(),
            m    = date.getMonth(),
            y    = date.getFullYear()


        $('#su').fullCalendar({
            header    : {
                left  : 'prev,next today',
                center: 'title',
                right : 'month,agendaWeek,agendaDay'
            },
            buttonText: {
                today: 'today',
                month: 'month',
                week : 'week',
                day  : 'day'
            },
            eventRender: function(eventObj, $el) {
                $el.popover({
                    title: eventObj.title,
                    content: eventObj.description,
                    trigger: 'hover',
                    placement: 'top',
                    container: 'body',
                    html:true
                });
            },
            //Random default events
            events    : events,
            editable  : true,
            droppable : true, // this allows things to be dropped onto the calendar !!!

        })


    })
</script>