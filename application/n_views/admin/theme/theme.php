<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
	<title><?php echo $this->config->item('product_name')." | ".$page_title;?></title>
	<link rel="shortcut icon" href="<?php echo base_url();?>assets/img/favicon.png">

    <?php
    include(FCPATH.'application/n_views/config.php');
    $current_language = isset($language_info[$this->language]) ? $language_info[$this->language] : $this->lang->line("Language");
    if(strpos(strtolower($n_config['rtl_langs']), strtolower($current_language)) !== false){
        $bfont_default = 'IBM+Plex+Sans';
        $nfont_default = 'Rubik';
        if($bfont_default!=$n_config['body_font_rtl']) {
            $bfont_default = str_replace(' ', '+', $n_config['body_font_rtl']);
        }
        if($nfont_default!=$n_config['nav_font_rtl']) {
            $nfont_default = str_replace(' ', '+', $n_config['nav_font_rtl']);
        }

        ?>
        <link href="https://fonts.googleapis.com/css?family=<?php echo $bfont_default; ?>:300,400,500,600%7C<?php echo $nfont_default; ?>:300,400,500,600,700" rel="stylesheet">
    <?php }else{
        $bfont_default = 'IBM+Plex+Sans';
        $nfont_default = 'Rubik';
        if($bfont_default!=$n_config['body_font']) {
            $bfont_default = str_replace(' ', '+', $n_config['body_font']);
        }
        if($nfont_default!=$n_config['nav_font']) {
            $nfont_default = str_replace(' ', '+', $n_config['nav_font']);
        }

        ?>
        <link href="https://fonts.googleapis.com/css?family=<?php echo $bfont_default; ?>:300,400,500,600%7C<?php echo $nfont_default; ?>:300,400,500,600,700" rel="stylesheet">
  <?php } ?>


	  <?php

            include(FCPATH.'application/n_views/include/function_helper_theme.php');

      if(file_exists(FCPATH.'manifest.json') AND $n_config['pwa_on']=='true'){
          echo '<meta name="theme-color" content="'.$n_config['pwa_theme_color'].'">';
          echo '<link rel="manifest" href="'.base_url().'manifest.json">';
          echo '<meta name="apple-mobile-web-app-status-bar-style" content="'.$n_config['pwa_apple_status_bar'].'">';

          if(!empty($n_config['iphone5_splash']) AND file_exists(FCPATH.'assets/img/iphone5_splash.png')){
              echo '<link href="'.base_url().'assets/img/iphone5_splash.png" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />';
          }

          if(!empty($n_config['iphone6_splash']) AND file_exists(FCPATH.'assets/img/iphone6_splash.png')){
              echo '<link href="'.base_url().'assets/img/iphone6_splash.png" media="(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />';
          }

          if(!empty($n_config['iphoneplus_splash']) AND file_exists(FCPATH.'assets/img/iphoneplus_splash.png')){
              echo '<link href="'.base_url().'assets/img/iphoneplus_splash.png" media="(device-width: 621px) and (device-height: 1104px) and (-webkit-device-pixel-ratio: 3)" rel="apple-touch-startup-image" />';
          }

          if(!empty($n_config['iphonex_splash']) AND file_exists(FCPATH.'assets/img/iphonex_splash.png')){
              echo '<link href="'.base_url().'assets/img/iphonex_splash.png" media="(device-width: 375px) and (device-height: 812px) and (-webkit-device-pixel-ratio: 3)" rel="apple-touch-startup-image" />';
          }

          if(!empty($n_config['iphonexr_splash']) AND file_exists(FCPATH.'assets/img/iphonexr_splash.png')){
              echo '<link href="'.base_url().'assets/img/iphonexr_splash.png" media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />';
          }

          if(!empty($n_config['iphonexsmax_splash']) AND file_exists(FCPATH.'assets/img/iphonexsmax_splash.png')){
              echo '<link href="'.base_url().'assets/img/iphonexsmax_splash.png" media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 3)" rel="apple-touch-startup-image" />';
          }

          if(!empty($n_config['ipad_splash']) AND file_exists(FCPATH.'assets/img/ipad_splash.png')){
              echo '<link href="'.base_url().'assets/img/ipad_splash.png" media="(device-width: 768px) and (device-height: 1024px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />';
          }

          if(!empty($n_config['ipadpro1_splash']) AND file_exists(FCPATH.'assets/img/ipadpro1_splash.png')){
              echo '<link href="'.base_url().'assets/img/ipadpro1_splash.png" media="(device-width: 834px) and (device-height: 1112px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />';
          }

          if(!empty($n_config['ipadpro3_splash']) AND file_exists(FCPATH.'assets/img/ipadpro3_splash.png')){
              echo '<link href="'.base_url().'assets/img/ipadpro3_splash.png" media="(device-width: 834px) and (device-height: 1194px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />';
          }

          if(!empty($n_config['ipadpro2_splash']) AND file_exists(FCPATH.'assets/img/ipadpro2_splash.png')){
              echo '<link href="'.base_url().'assets/img/ipadpro2_splash.png" media="(device-width: 1024px) and (device-height: 1366px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />';
          }

          ?>
          <meta name="apple-mobile-web-app-capable" content="yes" />

          <link href="<?php echo base_url();?>assets/img/pwa_icon_72.png" rel="apple-touch-icon">
          <link href="<?php echo base_url();?>assets/img/pwa_icon_76.png" rel="apple-touch-icon" sizes="76x76">
          <link href="<?php echo base_url();?>assets/img/pwa_icon_120.png" rel="apple-touch-icon" sizes="120x120">
          <link href="<?php echo base_url();?>assets/img/pwa_icon_152.png" rel="apple-touch-icon" sizes="152x152">



          <?php
      }

      if(strpos(strtolower($n_config['rtl_langs']), strtolower($current_language)) !== false){
          include(FCPATH.'application/n_views/include/css_include_back_rtl.php');
          $rtl_on = true;
      } else{
          $rtl_on = false;
          include(FCPATH.'application/n_views/include/css_include_back.php');
      }
          $upload_js = false;
	  ?>

<script>
    window.resizeIframe = function (obj) {
        return;
    }
</script>
    <style>
        @media all and (display-mode: standalone) {

        }


        <?php if($n_config['theme_mobile_full_width']=='true'){ ?>
             @media (max-width: 575.98px) {
                html .content .content-wrapper {
                    padding-left: 0 !important;
                    padding-right: 0 !important;
                }
            }
        <?php } ?>

    </style>

    <?php if($n_config['theme_appeareance_on']=='true'){
        include(APPPATH.'n_views/include/custom_style.php');
    } ?>

	</head>


<?php
    $n_theme_on_load = get_cookie('layout-name');
    if(!empty($n_theme_on_load)){
        $layout_on_load = $n_theme_on_load;
    }else{
        $layout_on_load = $n_config['current_theme'];
    }
?>
	<body class="vertical-layout vertical-menu-modern <?php echo $layout_on_load; ?> 2-columns  navbar-sticky footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">
    <?php
        include(FCPATH.'application/n_views/admin/theme/header.php');
        include(FCPATH.'application/n_views/admin/theme/sidebar.php');
	?>

	<div class="app-content content">
      <div class="content-overlay"></div>
      <div class="content-wrapper">
			<?php

            if($this->_module!=null){
                $body = 'modules/'.$this->_module.'/'.$body;
            }

				//$this->load->view($body);
           $body = str_replace('.php','',$body);
            if($n_config['dev_mode'] == true){print('File:<pre>application/n_views/'.$body.'.php</pre>');}

            if($n_config['import_account_fb_alert']=='alert_all'){
                $where_alert['where'] = array('user_id'=>$this->user_id);
                $existing_accounts_alert = $this->basic->get_data('facebook_rx_fb_user_info',$where_alert);

                if(empty($existing_accounts_alert)){
                    echo '<div class="alert alert-warning mb-1" role="alert"><a href="'.base_url('/social_accounts/index').'" class="text-white">';
                    echo $this->lang->line('You haven not connected any account yet.');
                    echo '</a></div>';
                }
            }

        if(file_exists(FCPATH.'application/n_views/'.$body.'.php')){
	        include(FCPATH.'application/n_views/'.$body.'.php');
	    }else{
            var_dump('application/n_views/'.$body.'.php');
        }
			//include(FCPATH.'application/n_views/admin/theme/footer.php');

			?>
      </div>
    </div>
    <!-- END: Content-->
    
	<div class="sidenav-overlay"></div>
    <div class="drag-target"></div>
    
	<footer class="footer footer-static footer-light">
        <p class="clearfix mb-0">
            <span class="float-left d-inline-block"><?php echo date('Y');?> &copy; <?php  echo $this->config->item("product_short_name")." ";?></span>
<!--            <span class="float-right d-sm-inline-block d-none">-->
<!--                Created with<i class="bx bxs-heart pink mx-50 font-small-3"></i>by MD-->
<!--            </span>-->
            <button class="btn btn-primary btn-icon scroll-top" type="button"><i class="bx bx-up-arrow-alt"></i></button>
        </p>
    </footer>


    <?php
        include(FCPATH.'application/n_views/include/js_include_back.php');
        include(FCPATH.'application/n_views/include/js_include_back_function.php');
    ?>



    <?php


    if(file_exists(FCPATH.'application/n_views/'.$body.'_js.php')){
        include(FCPATH.'application/n_views/'.$body.'_js.php');
    }

      if(file_exists(FCPATH.'n_assets/js/system/'.$body.'.js?ver='.$n_config['theme_version'])){
          echo '<script src="'.base_url("n_assets/js/system/".$body.".js?ver=".$n_config['theme_version']).'"></script>';
      }else{
          if($n_config['dev_mode'] == true){
              var_dump(FCPATH.'n_assets/js/system/'.$body.'.js?ver='.$n_config['theme_version']);
          }
      }

	    if(!empty($include_js_uni)){
	        include $include_js_uni;
        }

      if(!empty($include_js_uni_2)){
          include $include_js_uni_2;
      }

	  ?>

    <?php if(isset($include_select2) AND $include_select2==1){ ?>
        <script>
            $(document).ready(function() {
                'use strict';

                $(".select2").select2({
                    dropdownAutoWidth: true,
                });
            });
        </script>
    <?php } ?>

    <script>
        function send_command(command){
            var right_column_content_js = true;
            if(document.getElementById("right_column_content")==null || document.getElementById("right_column_content").getElementsByTagName('iframe')[0]==null){right_column_content_js = false;}
            if(right_column_content_js){
                document.getElementById("right_column_content").getElementsByTagName('iframe')[0].contentWindow.postMessage(command);
            }
            var right_column_js = true;
            if(document.getElementById("right_column")==null || document.getElementById("right_column").getElementsByTagName('iframe')[0]==null){right_column_js = false;}
            if(right_column_js){
                document.getElementById("right_column").getElementsByTagName('iframe')[0].contentWindow.postMessage(command);
            }
        }

        $(".layout-name").on("click", function () {

            if (body.hasClass("light-layout")) {
                send_command("light-layout");
                body.removeClass("light-layout").addClass("dark-layout");
                mainMenu.removeClass("menu-light").addClass("menu-dark");
                navbar.removeClass("navbar-light").addClass("navbar-dark");
                Cookies.set('layout-name', 'dark-layout');
                return;
            }

            if (body.hasClass("semi-dark-layout")) {
                send_command("light-layout");
                body.removeClass("semi-dark-layout").addClass("dark-layout");
                mainMenu.removeClass("menu-light").addClass("menu-dark");
                navbar.removeClass("navbar-light").addClass("navbar-dark");
                Cookies.set('layout-name', 'dark-layout');
                return;
            }
            <?php if($n_config['current_theme']=='semi-dark-layout'){ ?>

            if (body.hasClass("dark-layout")) {
                send_command("dark-layout");
                body.removeClass("dark-layout").addClass("semi-dark-layout");
                mainMenu.removeClass("menu-light").addClass("menu-dark");
                navbar.removeClass("navbar-dark").addClass("navbar-dark");
                Cookies.set('layout-name', 'semi-dark-layout');
                return;
            }

            <?php }elseif($n_config['current_theme']=='light-layout' OR $n_config['current_theme']=='dark-layout'){ ?>

            if (body.hasClass("dark-layout")) {
                send_command("dark-layout");
                body.removeClass("dark-layout").addClass("light-layout");
                mainMenu.removeClass("menu-dark").addClass("menu-light");
                navbar.removeClass("navbar-dark").addClass("navbar-light");
                Cookies.set('layout-name', 'light-layout');
                return;
            }

            <?php } ?>


        });

        if(Cookies.get('layout-name')=='dark-layout'){
            body.removeClass("semi-dark-layout").addClass("dark-layout");
            mainMenu.removeClass("menu-light").addClass("menu-dark");
            navbar.removeClass("navbar-dark").addClass("navbar-dark");
        }


        $(document).ready(function() {
                iFrameResize({
                    log: false,
                    minHeight: 700,
                })
        });


    </script>

    <?php if($n_config['pwa_on']=='true'){ ?>
        <script>
            function registerServiceWorker() {
                if ('serviceWorker' in navigator) {
                    window.addEventListener('load', function() {
                        navigator.serviceWorker.register('<?php echo base_url();?>serviceworker.js')
                            .then(function(registration) {
                            }, function(err) {
                            });
                    });
                }
            }
            registerServiceWorker();
        </script>
    <?php } ?>

    <?php include(FCPATH.'application/n_views/include/helper_pages.php'); ?>
    <?php include(FCPATH.'application/views/include/google_code.php'); ?>
    <?php include(FCPATH.'application/views/include/fb_px.php'); ?>

	</body>
</html>


