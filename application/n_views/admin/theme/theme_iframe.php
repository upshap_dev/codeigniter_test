<!DOCTYPE html>
<html lang="en">
	<head>
	  <meta charset="UTF-8">
	  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
        <link rel="shortcut icon" href="<?php echo base_url();?>assets/img/favicon.png">

        <?php
        $uri_last = md5(current_url());

        include(FCPATH.'application/n_views/config.php');
        $current_language = isset($language_info[$this->language]) ? $language_info[$this->language] : $this->lang->line("Language");
        if(strpos(strtolower($n_config['rtl_langs']), strtolower($current_language)) !== false){
            $bfont_default = 'IBM+Plex+Sans';
            $nfont_default = 'Rubik';
            if($n_config['theme_appeareance_on']=='true'){
                $bfont_default = str_replace(' ', '+', $n_config['body_font_rtl']);
                $nfont_default = str_replace(' ', '+', $n_config['nav_font_rtl']);
            }
            ?>
            <link href="https://fonts.googleapis.com/css?family=<?php echo $bfont_default; ?>:300,400,500,600%7C<?php echo $nfont_default; ?>:300,400,500,600,700" rel="stylesheet">
        <?php } else{
            $bfont_default = 'IBM+Plex+Sans';
            $nfont_default = 'Rubik';
            if($n_config['theme_appeareance_on']=='true'){
                $bfont_default = str_replace(' ', '+', $n_config['body_font']);
                $nfont_default = str_replace(' ', '+', $n_config['nav_font']);
            }
            ?>
            <link href="https://fonts.googleapis.com/css?family=<?php echo $bfont_default; ?>:300,400,500,600%7C<?php echo $nfont_default; ?>:300,400,500,600,700" rel="stylesheet">
        <?php } ?>
        <?php

        include(FCPATH.'application/n_views/include/function_helper_theme.php');

        if(strpos(strtolower($n_config['rtl_langs']), strtolower($current_language)) !== false){
            include(FCPATH.'application/n_views/include/css_include_back_rtl.php');
            $rtl_on = true;
        } else{
            $rtl_on = false;
            include(FCPATH.'application/n_views/include/css_include_back.php');
        }
        $upload_js = false;
        ?>

		<script type="text/javascript">
		  window.addEventListener('load', function () {
            $(".preloading_body").fadeOut("slow");
            }, false);
		</script>

		<style type="text/css">
		body {
            height: initial!important;
            background: transparent!important;
		}

        textarea, .multi_layout
        {
            background: transparent!important;
        }

        body.dark-layout #main_iframe > div .modal-content{
            background: transparent!important;
        }

        body.light-layout #main_iframe.ecommerce .modal-content,
        body.semi-dark-layout #main_iframe.ecommerce .modal-content {
            background: #ffffff!important;
        }


		.preloading_body i{font-size:40px;display: table-cell; vertical-align: middle;padding:30px 0;}
		
		.preloading_body {
			height: 100%;width:100%;display: table;
		}

        body.dark-layout .card{
            background: #1A233A;
        }


		/* loader */
		</style>



        <?php if($n_config['theme_appeareance_on']=='true'){
            include(APPPATH.'n_views/include/custom_style.php');
        }
        $n_str = explode('/',$body);
        if(!empty($n_str[0])){
            $add_n_class = $n_str[0];
        }else{
            $add_n_class = '';
        }
        ?>
	</head>
    <?php
    $n_theme_on_load = get_cookie('layout-name');
    if(!empty($n_theme_on_load)){
        $layout_on_load = $n_theme_on_load;
    }else{
        $layout_on_load = $n_config['current_theme'];
    }
    ?>
	<body class="<?php echo $layout_on_load; echo ' '.$uri_last; ?>" id="main_iframe">
		<div class="text-center preloading_body">
		  <i class="bx bx-loader-alt bx-spin blue text-center"></i>
		</div>

		<div class="card shadow-none <?php echo $add_n_class; ?>" style="border:0!important;">

            <?php
            if($this->_module!=null){
                $body = 'modules/'.$this->_module.'/'.$body;
            }

            //$this->load->view($body);
            $body = str_replace('.php','',$body);
            if($n_config['dev_mode'] == true){print('File:<pre>application/n_views/'.$body.'.php</pre>');}
            if(file_exists(FCPATH.'application/n_views/'.$body.'.php')){
                include(FCPATH.'application/n_views/'.$body.'.php');
            }else{
                var_dump('application/n_views/'.$body.'.php');
            }
            //include(FCPATH.'application/n_views/admin/theme/footer.php');

            ?>


        </div>


        <?php
        include(FCPATH.'application/n_views/include/js_include_back.php');
        include(FCPATH.'application/n_views/include/js_include_back_function.php');
        ?>


        <?php

        if(file_exists(FCPATH.'application/n_views/'.$body.'_js.php')){
            include(FCPATH.'application/n_views/'.$body.'_js.php');
        }

        if(file_exists(FCPATH.'n_assets/js/system/'.$body.'.js?ver='. $n_config['theme_version'])){
            echo '<script src="'.base_url("n_assets/js/system/".$body.".js?ver=". $n_config['theme_version']).'"></script>';
        }else{
            if($n_config['dev_mode'] == true){
                var_dump(FCPATH.'n_assets/js/system/'.$body.'.js?ver='. $n_config['theme_version']);
            }
        }

        if(!empty($include_js_uni)){
            include $include_js_uni;
        }

        if(!empty($include_js_uni_2)){
            include $include_js_uni_2;
        }

        ?>

        <?php if(isset($include_select2) AND $include_select2==1){ ?>
            <script>
                $(document).ready(function() {
                    'use strict';

                    $(".select2").select2({
                        dropdownAutoWidth: true,
                    });
                });
            </script>
        <?php } ?>

    <script>
        window.addEventListener("message", receiveMessage, false);

        function receiveMessage(event)
        {
            if(event.data=="light-layout") {
                body.removeClass("light-layout").addClass("dark-layout");
                mainMenu.removeClass("menu-light").addClass("menu-dark");
                navbar.removeClass("navbar-light").addClass("navbar-dark");
            }
            if(event.data=="dark-layout") {
                body.removeClass("dark-layout").addClass("light-layout");
                mainMenu.removeClass("menu-dark").addClass("menu-light");
                navbar.removeClass("navbar-dark").addClass("navbar-light");
            }
        }

        if(Cookies.get('layout-name')=='dark-layout'){
            body.removeClass("semi-dark-layout").addClass("dark-layout");
            mainMenu.removeClass("menu-light").addClass("menu-dark");
            navbar.removeClass("navbar-dark").addClass("navbar-dark");
        }




    </script>





	</body>
</html>
<!--Todo: check this file-->
<!--<link rel="stylesheet" href="--><?php //echo base_url('assets/css/system/inline.css?ver='. $n_config['theme_version']);?><!--">-->