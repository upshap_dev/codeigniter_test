<style>
    .menu-collapsed .brand-text, .expanded .brand-logo{display:none;}
    .menu-expanded .brand-logo{display:none;}
    .menu-collapsed .menu-light.expanded .dark_icon_br, .menu-collapsed .menu-dark.expanded .light_icon_br{display:none!important;}
    .menu-light .dark_icon, .menu-dark .light_icon,.menu-light .dark_icon_br, .menu-dark .light_icon_br{display:none;}
    .main-menu.menu-dark .navigation > li.nav-item.open.has-sub.open, .main-menu.menu-dark .navigation > li.nav-item.open.has-sub.sidebar-group-active, .main-menu.menu-dark .navigation > li.nav-item.sidebar-group-active.has-sub.open, .main-menu.menu-dark .navigation > li.nav-item.sidebar-group-active.has-sub.sidebar-group-active {
    border-radius: 0.267rem;
    border: 1px solid #eeeeee;
    background-color: #f3f6f4;
    transition: none;
}


</style>

<?php
if($n_config['current_theme']=='light-layout'){
    $n_theme_scheme =  'menu-light';
}else{
    $n_theme_scheme =  'menu-dark';
}

?>

 <!-- BEGIN: Main Menu-->
 <div class="main-menu menu-fixed <?php echo $n_theme_scheme; ?> menu-accordion menu-shadow expanded" data-scroll-to-active="true">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mr-auto">
                    <a class="navbar-brand" href="<?php echo base_url(); ?>">
                        <?php

                        if($rtl_on){
                            if(!empty($n_config['dark_icon_rtl'])){
                                $n_config['dark_icon'] = $n_config['dark_icon_rtl'];
                            }
                            if(!empty($n_config['dark_logo_rtl'])){
                                $n_config['dark_logo'] = $n_config['dark_logo_rtl'];
                            }
                            if(!empty($n_config['light_icon_rtl'])){
                                $n_config['light_icon'] = $n_config['light_icon_rtl'];
                            }
                        }

                        if(!empty($n_config['dark_icon'])){  ?>
                        <div class="brand-logo dark_icon d-none">
                            <img class="img-fluid" src="<?php echo base_url(); echo $n_config['dark_icon']; ?>" alt='<?php echo $this->config->item("product_short_name"); ?>' />
                        </div>
                      <?php  }
                        if(!empty($n_config['dark_logo'])){
                            $logo_uri = base_url().$n_config['dark_logo'];
                        }else{
                            $logo_uri =  base_url().'assets/img/logo.png';
                            if($rtl_on) {
                                if (!empty($n_config['light_logo_rtl'])) {
                                    $logo_uri =  base_url().$n_config['light_logo_rtl'];

                        
                                }
                            }
                        }
                      ?>
                        <img class="brand-text dark_icon_br" src="<?php echo $logo_uri; ?>" alt='<?php echo $this->config->item("product_short_name"); ?>' style="max-width:170px;"/>
                        <img class="brand-text light_icon_br" src="<?php echo base_url().'assets/img/logo.png'; ?>" alt='<?php echo $this->config->item("product_short_name"); ?>' style="max-width:170px;"/>
                    </a>
                </li>
                <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="bx bx-x d-block d-xl-none font-medium-4 primary toggle-icon"></i><i class="toggle-icon bx bx-disc font-medium-4 d-none d-xl-block primary" data-ticon="bx-disc"></i></a></li>
            </ul>
        </div>
        <div class="shadow-bottom"></div>
        <div class="main-menu-content ">
                        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation" data-icon-style="<?php echo $n_config['livicon_icon_style']; ?>">
                <li class="nav-item"><a class="" href="<?php echo base_url("dashboard");?>"><i class="bx bx-home" style="visibility: visible; width: 60px;"></i><span class="menu-title" data-i18n="Dashboard"><?php echo $this->lang->line("Dashboard"); ?></span></a>
                </li>
                 <li class="nav-item"><a class="" href="<?php echo base_url("affiliate_system/affiliate_login_page");?>"><i class="bx bx-dollar-circle" style="visibility: visible; width: 60px;"></i><span class="menu-title" data-i18n="Dashboard"><?php echo $this->lang->line("Affiliate"); ?></span></a>
                </li>
                  <?php if($this->basic->is_exist("add_ons",array("unique_name"=>"hidden_interest_explorer"))) : ?>
                 <li class="nav-item"><a href="<?php echo base_url("marketing/get_interest");?>" ><i class="feather icon-target"></i> <span class="menu-title" data-i18n="Interest Finder"> <?php echo $this->lang->line("InterestExplorer"); ?></span></a>
                </li>
                <?php endif; ?>
				 <?php if($this->basic->is_exist("add_ons",array("unique_name"=>"nvx_pixie_xero"))) : ?>
                <li class="nav-item"><a href="<?php echo base_url("n_image_editor");?>"><i class="feather icon-camera"></i> <span class="menu-title" data-i18n="image"> <?php echo $this->lang->line("Image Editor"); ?></span>
                 <span class="badge badge-pill badge-glow badge-success mb-1 float-right mr-2"> <?php echo $this->lang->line("POP"); ?> </span></a>
                </li>
				<?php endif; ?>
                 <li class=" navigation-header"><span> <?php echo $this->lang->line("Account Manager"); ?> </span>
                </li>
                <li class=" nav-item"><a href="<?php echo base_url("social_accounts");?>" ><i class="bx bx-like" style="visibility: visible; width: 60px;"></i><span class="menu-title" data-i18n="user"> <?php echo $this->lang->line("Facebook Account"); ?></span></a>
                </li>
                <li class=" nav-item"><a href="<?php echo base_url("comboposter/social_accounts");?>" ><i class="bx bx-user-plus" style="visibility: visible; width: 60px;"></i><span class="menu-title" data-i18n="user"><?php echo $this->lang->line("Social Accounts"); ?></span></a>
                </li>
              
                <?php if($this->session->userdata('user_type') == 'Admin'): ?>
	             <li class=" navigation-header"><span> <?php echo $this->lang->line("Administration"); ?> </span>
                </li>
                 <li class=" nav-item has-sub "><a href="#" class="nav-link" ><i class="feather icon-settings"></i><span class="menu-title" data-i18n="Ecommerce"> <?php echo $this->lang->line("System"); ?> </span></a>
                    <ul class="menu-content">
                        <li><a href="<?php echo base_url("admin/settings");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Shop"> <?php echo $this->lang->line("Settings"); ?></span></a>
                        </li>
                        <li><a href="<?php echo base_url("social_apps/index");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Details"> <?php echo $this->lang->line("Social Apps"); ?></span></a>
                        </li>
                        <li><a href="<?php echo base_url("cron_job/index");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Wish List"> <?php echo $this->lang->line("Cron Job"); ?></span></a>
                        </li>
                        <li><a href="<?php echo base_url("multi_language/index");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Details"> <?php echo $this->lang->line("Language Editor"); ?></span></a>
                        </li>
                        <li><a href="<?php echo base_url("addons/lists");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Wish List"> <?php echo $this->lang->line("Add-on Manager"); ?></span></a>
                        </li>
						<?php if($this->basic->is_exist("add_ons",array("unique_name"=>"nvx_addon_manager"))) : ?>
                         <li><a href="<?php echo base_url("nvx_addon_manager");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Wish List"> <?php echo $this->lang->line("nvx_addon_manager"); ?></span></a>
                        </li>
						 <?php endif; ?>
                        <li><a href="<?php echo base_url("themes/lists");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Details"> <?php echo $this->lang->line("Theme manager"); ?></span></a>
                        </li>
                        <li><a href="<?php echo base_url("menu_manager/index");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Wish List"> <?php echo $this->lang->line("Menu manager"); ?></span></a>
                        </li>
                          <li><a href="<?php echo base_url("update_system/index");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Details"> <?php echo $this->lang->line("Check Update"); ?></span></a>
                        </li>
                          <li><a href="<?php echo base_url("/update_system/update_list_v2");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Details"> <?php echo $this->lang->line("Check Update v2"); ?></span></a>
                        </li>
                    </ul>
                </li>
	             <li class=" nav-item has-sub"><a href="#" class="nav-link" ><i class="ficon feather icon-users"></i><span class="menu-title" data-i18n="Ecommerce"> <?php echo $this->lang->line("Subscription"); ?> </span></a>
                    <ul class="menu-content ">
                        <li><a href="<?php echo base_url("payment/package_manager");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Shop"> <?php echo $this->lang->line("Package Manager"); ?> </span></a>
                        </li>
                        <li><a href="<?php echo base_url("admin/user_manager");?>" class="d-flex align-items-center"  ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Details"> <?php echo $this->lang->line("User Manager"); ?> </span></a>
                        </li>
                        <li><a href="<?php echo base_url("announcement/full_list");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Wish List"> <?php echo $this->lang->line("Announcement"); ?> </span></a>
                        </li>
                         <?php if($this->session->userdata('license_type') == 'double' && $this->session->userdata('user_type')=='Admin'): ?>
                        <li><a href="<?php echo base_url("payment/accounts");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Details"> <?php echo $this->lang->line("Payment Accounts"); ?> </span></a>
                        </li>
                        <li><a href="<?php echo base_url("payment/earning_summary");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Wish List"> <?php echo $this->lang->line("Earning Summary"); ?> </span></a>
                        </li>
                        <li><a href="<?php echo base_url("payment/transaction_log");?>"><i class="feather icon-circle" class="bx bx-right-arrow-alt" ></i><span class="menu-item" data-i18n="Details"> <?php echo $this->lang->line("Transaction Log"); ?> </span></a>
                        </li>
                        <?php endif; ?>
                        
                    </ul>
                </li>
	          	
				<?php if($this->session->userdata('license_type') == 'double' && $this->session->userdata('user_type')=='Admin'): ?>
				<li class=" nav-item has-sub"><a href="#" data-toggle="tooltip" data-placement="top" title="Blog"><i class="feather icon-file-text"></i><span class="menu-title" data-i18n="Ecommerce"> <?php echo $this->lang->line("Blog manager"); ?></span></a>
                    <ul class="menu-content">
                        <li><a href="/blog/posts" data-toggle="tooltip" data-placement="top" title="Blog manager" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Shop"> <?php echo $this->lang->line("Blog manager"); ?></span></a>
                        </li>
                        <li><a href="/blog/add_post" data-toggle="tooltip" data-placement="top" title="New Blog" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Details"> <?php echo $this->lang->line("New Blog"); ?></span></a>
                        </li>
                        <li><a href="/blog/category" data-toggle="tooltip" data-placement="top" title="Category Manager" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Wish List"> <?php echo $this->lang->line("Category Manager"); ?></span></a>
                        </li>
                        <li><a href="/blog/tag" data-toggle="tooltip" data-placement="top" title="Tag manager" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Details"> <?php echo $this->lang->line("Tag manager"); ?></span></a>
                        </li>
                        
                    </ul>
                </li>
				 	 <?php endif?>
				
				 <?php if($this->basic->is_exist("add_ons",array("unique_name"=>"affiliate_system"))) : ?>
				 <li class=" nav-item has-sub"><a href="#"><i class="ficon lni lni-network"></i> <span class="menu-title" > <?php echo $this->lang->line("Affiliate System"); ?>  </span></a>
                    <ul class="menu-content">
                      <li class=" nav-item "><a href="<?php echo base_url("/affiliate_system/affiliate_users");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i> <span class="menu-title" data-i18n="pen"> <?php echo $this->lang->line("Affiliate Users"); ?></span></a> </li>
                     <li class=" nav-item "><a href="<?php echo base_url("/affiliate_system/affiliate_payment_settings");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i> <span class="menu-title" data-i18n="pen"> <?php echo $this->lang->line("Commission Settings"); ?></span></a> </li>
                     
                     <li class="nav-item"><a href="<?php echo base_url("/affiliate_system/all_withdrawal_requests");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-title" data-i18n="Documentation"> <?php echo $this->lang->line("Withdrawal Requests"); ?></span></a></li>
                      </ul>
                </li>
	            <?php endif; ?>
			    <?php endif;//admin ?> 
				
				<?php if($this->session->userdata('license_type') == 'double' && $this->session->userdata('user_type')=='Member')
        {
          echo'
          <li class="navigation-header"><span>'.$this->lang->line("Subscription").'</span></li>
              <li class="nav-item "><a href="'.base_url("payment/buy_package").'" ><i class="bx bx-refresh" style="visibility: visible; width: 60px;"></i> <span class="menu-title" data-i18n="Documentation"> '.$this->lang->line("Renew Plan").'</span></a></li>
              <li class="nav-item "><a href="'.base_url("payment/transaction_log").'" ><i class="bx bx-menu" style="visibility: visible; width: 60px;"></i> <span class="menu-title" data-i18n="Documentation"> '.$this->lang->line("Transaction Log").'</span></a></li>
              <li class="nav-item "><a href="'.base_url("payment/usage_history").'" ><i class="bx bx-history" style="visibility: visible; width: 60px;"></i> <span class="menu-title" data-i18n="Documentation"> '.$this->lang->line("Usage Log").'</span></a></li>
           
          ';
        }
      ?>
      
      <li class=" navigation-header"><span><?php echo $this->lang->line("Ecommerce Tools"); ?></span>
                </li>
                <li class="nav-item "><a href="<?php echo base_url("/ecommerce");?>"><i class="bx bx-store" style="visibility: visible; width: 60px;"></i><span class="menu-title" > <?php echo $this->lang->line("Ecommerce Store"); ?> </span><!--<span class="badge badge badge-pill badge-success float-right mr-2"> <?php echo $this->lang->line("New"); ?></span>--></a>
                </li>
				 <?php if($this->basic->is_exist("add_ons",array("unique_name"=>"woocommerce_integration"))) : ?>
                <li class="nav-item "><a href="<?php echo base_url("/woocommerce_integration");?>"><i class="menu-livicon livicon-evo-holder" data-icon="shoppingcart-in" style="visibility: visible; width: 60px;"></i><span class="menu-title" > <?php echo $this->lang->line("WC Integration"); ?> </span></a>
                </li>
				 <?php endif ?>
				 <?php if($this->basic->is_exist("add_ons",array("unique_name"=>"woocommerce_abandoned_cart"))) : ?>
                <li class="nav-item "><a href="<?php echo base_url("/woocommerce_abandoned_cart");?>"><i class="bx bx-cart-alt" style="visibility: visible; width: 60px;"></i> <span class="menu-title" > <?php echo $this->lang->line("WC Abandoned Cart"); ?> </span></a>
                </li>
				 <?php endif ?>
      
      <li class=" navigation-header"><span><?php echo $this->lang->line("Messenger Chatbot"); ?></span>
                </li>
                <li class="nav-item"><a href="<?php echo base_url();?>messenger_bot/saved_templates"><i class="bx bx-copy-alt" style="visibility: visible; width: 60px;"></i><span class="menu-item" data-i18n="Wish List"> <?php echo $this->lang->line("Bot Templates"); ?> </span></a></li>
                 <li class="nav-item "><a href="<?php echo base_url("/subscriber_manager/contact_group");?>"><i class="bx bx-purchase-tag" style="visibility: visible; width: 60px;"></i> <span class="menu-title"><?php echo $this->lang->line("Labels/Tags"); ?></span></a>
                </li>
                <li class="nav-item"><a href="<?php echo base_url();?>subscriber_manager/livechat"><i class="bx bx-chat" style="visibility: visible; width: 60px;"></i><span class="menu-item" data-i18n="Shop"> <?php echo $this->lang->line("Live Chat"); ?></span></a></li>
				<?php if($this->basic->is_exist("add_ons",array("unique_name"=>"flow_builder"))) : ?>
                <li class="nav-item "><a href="<?php echo base_url("flow_builder");?>"><i class="menu-livicon livicon-evo-holder" data-icon="share" style="visibility: visible; width: 60px;"></i> <span class="menu-title"><?php echo $this->lang->line("Chatbot Flow Builder"); ?></span></a>
                </li>
				<?php endif ?>
				<?php if($this->basic->is_exist("add_ons",array("unique_name"=>"visual_flow_builder"))) : ?>
                 <li class="nav-item "><a href="<?php echo base_url("/visual_flow_builder/flowbuilder_manager");?>"><i class="bx bx-share-alt"  style="visibility: visible; width: 60px;"></i> <span class="menu-title"><?php echo $this->lang->line("Bot Flow Builder"); ?></span></a>
                </li>
				<?php endif ?>
				<?php if($this->basic->is_exist("add_ons",array("unique_name"=>"custom_field_manager"))) : ?>

                <li class="nav-item"><a href="<?php echo base_url();?>bot_instagram"><i class="bx bx-git-merge" style="visibility: visible; width: 60px;"></i><span class="menu-item" data-i18n="Shop"> <?php echo $this->lang->line("Instagram Bot"); ?></span></a></li>
				
			<li class=" nav-item has-sub"><a href="#"><i class="bx bx-list-plus" style="visibility: visible; width: 60px;"></i><span class="menu-title" data-i18n="Ecommerce"> <?php echo $this->lang->line("User Input"); ?></span></a>
                <ul class="menu-content">
                 <li class="nav-item "><a href="<?php echo base_url("/custom_field_manager/custom_field_list");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Shop"><?php echo $this->lang->line("User Input Fields"); ?></span></a>
                </li>
                <li class="nav-item "><a href="<?php echo base_url("/custom_field_manager/campaign_list");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Shop"><?php echo $this->lang->line("User Input Flow"); ?></span></a>
                </li>
                </ul>
                </li>
				<?php endif ?>
                <li class="nav-item"><a href="<?php echo base_url();?>messenger_bot/bot_list"><i class="bx bx-bot" style="visibility: visible; width: 60px;"></i><span class="menu-item" data-i18n="Shop"> <?php echo $this->lang->line("Bot settings"); ?></span></a></li>
                
                <li class=" nav-item has-sub"><a href="#"><i class="bx bx-cabinet" style="visibility: visible; width: 60px;"></i><span class="menu-item" data-i18n="Shop"> <?php echo $this->lang->line("Postback Manager"); ?></span></a>
                <ul class="menu-content">
                 <li class="nav-item "><a href="<?php echo base_url("messenger_bot/template_manager");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Shop"><?php echo $this->lang->line("Regular Postback"); ?></span></a>
                </li>
                <li class="nav-item "><a href="<?php echo base_url("messenger_bot/otn_template_manager");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Shop"><?php echo $this->lang->line("OTN Postback"); ?></span></a>
                </li>
                </ul>
                </li>
                
                <li class="nav-item"><a href="<?php echo base_url("messenger_bot/domain_whitelist");?>"><i class="bx bx-check-shield" style="visibility: visible; width: 60px;"></i> <span class="menu-title" data-i18n="Raise Support"> <?php echo $this->lang->line("Whitelist Domain"); ?></span></a>
                </li>
                
                <li class=" navigation-header"><span><?php echo $this->lang->line("Messenger Marketing"); ?></span>
                </li>
                
                <li class=" nav-item has-sub"><a href="#"><i class="bx bx-user-pin" style="visibility: visible; width: 60px;"></i><span class="menu-title" data-i18n="Ecommerce"> <?php echo $this->lang->line("Subscribers"); ?></span></a>
                <ul class="menu-content">
                    <li class="nav-item"><a href="<?php echo base_url("subscriber_manager/sync_subscribers");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Shop"> <?php echo $this->lang->line("Sync FB subscribers"); ?></span></a>
                </li>
                <li class="nav-item"><a href="<?php echo base_url("subscriber_manager/sync_subscribers/1");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Shop"> <?php echo $this->lang->line("Sync IG subscribers"); ?></span></a>
                </li>
                <li class=" nav-item"><a href="<?php echo base_url();?>subscriber_manager/bot_subscribers" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i> <span class="menu-item" data-i18n="user"> <?php echo $this->lang->line("Bot Subscribers"); ?></span></a>
                </li>
                <li class=" nav-item"><a href="<?php echo base_url();?>messenger_bot/otn_subscribers" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i> <span class="menu-item" data-i18n="user"> <?php echo $this->lang->line("OTN Subscribers"); ?></span></a>
                </li>
                </ul>
                </li>
                
                <li class=" nav-item"><a href="<?php echo base_url();?>messenger_bot_enhancers/create_subscriber_broadcast_campaign"><i class="bx bx-send" style="visibility: visible; width: 60px;"></i><span class="menu-title" data-i18n="Chat"> <?php echo $this->lang->line("Messenger Broadcast"); ?></span></a>
                </li>
                <li class=" nav-item"><a href="<?php echo base_url();?>messenger_bot_broadcast/otn_create_subscriber_broadcast_campaign"><i class="bx bx-notification" style="visibility: visible; width: 60px;"></i><span class="menu-title" data-i18n="Chat"> <?php echo $this->lang->line("OTN Broadcast"); ?></span></a>
                </li>
                 <li class=" nav-item"><a href="<?php echo base_url();?>messenger_bot_connectivity/webview_builder_manager"><i class="bx bx-detail" style="visibility: visible; width: 60px;"></i> <span class="menu-title" data-i18n="Chat"> <?php echo $this->lang->line("Form Builder"); ?></span></a>
                </li>
                
                <li class=" nav-item has-sub"><a href="#"><i class="bx bx-envelope-open" style="visibility: visible; width: 60px;"></i><span class="menu-title" data-i18n="Ecommerce"> <?php echo $this->lang->line("Autoresponder API"); ?></span></a>
                <ul class="menu-content">
                <li class=" nav-item"><a href="<?php echo base_url();?>email_auto_responder_integration/mailchimp_list" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i> <span class="menu-title" data-i18n="user"> <?php echo $this->lang->line("Mailchimp API"); ?></span></a>
                </li>
                <li class="nav-item"><a href="<?php echo base_url("email_auto_responder_integration/sendinblue_list");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-title" data-i18n="Documentation"> <?php echo $this->lang->line("Sendinblue API"); ?></span></a>
                </li>
                <li class="nav-item"><a href="<?php echo base_url("email_auto_responder_integration/activecampaign_list");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-title" data-i18n="Documentation"> <?php echo $this->lang->line("ActiveCampaign API"); ?></span></a>
                </li>
                <li class="nav-item"><a href="<?php echo base_url("email_auto_responder_integration/mautic_list");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-title" data-i18n="Documentation"> <?php echo $this->lang->line("Mautic API"); ?></span></a>
                </li>
                <li class="nav-item"><a href="<?php echo base_url("email_auto_responder_integration/acelle_list");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-title" data-i18n="Documentation"> <?php echo $this->lang->line("Acelle API"); ?></span></a>
                </li>
                </ul>
                </li>
                
                
                <li class="navigation-header"><span>  <?php echo $this->lang->line("Growth Tool"); ?></span>
                </li>
                   <li class=" nav-item has-sub"><a href="#"><i class="bx bx-wrench" style="visibility: visible; width: 60px;"></i><span class="menu-title" data-i18n="Ecommerce"> <?php echo $this->lang->line("Growth Tools"); ?></span></a>
                <ul class="menu-content">
               
                
				<?php if($this->basic->is_exist("add_ons",array("unique_name"=>"messenger_bot_enhancers"))) : ?>
                <li class="nav-item"><a href="<?php echo base_url("messenger_bot_enhancers/customer_chat_add");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Shop"> <?php echo $this->lang->line("Customer Chat Plugin"); ?></span></a>
                </li>
                <li class="nav-item"><a href="<?php echo base_url("/messenger_bot_enhancers/mme_link_list");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Shop"><?php echo $this->lang->line("M.Me Link/QR"); ?></span></a>
                </li>
                 <li class="nav-item"><a href="<?php echo base_url("messenger_bot_enhancers/checkbox_plugin_add");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Shop"> <?php echo $this->lang->line("Checkbox messenger"); ?></span></a>
                </li>
                
                <li class="nav-item"><a href="<?php echo base_url("messenger_bot_enhancers/send_to_messenger_add");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Shop"> <?php echo $this->lang->line("Send To Messenger"); ?></span></a>
                </li>
				<?php endif ?>
				<?php if($this->basic->is_exist("add_ons",array("unique_name"=>"messenger_bot_connectivity"))) : ?>
                <li class="nav-item"><a href="<?php echo base_url("messenger_bot_connectivity/json_api_connector");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-title" data-i18n="json"> <?php echo $this->lang->line("JSON API Connector"); ?></span></a>
                </li>
				 
				<?php endif ?>
                </ul>
                </li>
	  
                <li class=" navigation-header"><span> <?php echo $this->lang->line("Social Media Marketing"); ?></span>
                </li>
                <li class="nav-item"><a href="<?php echo base_url("instagram_reply/hashTag_search");?>"><i class="bx bx-hash" style="visibility: visible; width: 60px;"></i> <span class="menu-title" data-i18n="Raise Support"> <?php echo $this->lang->line("Hashtag Analysis"); ?></span></a>
                </li>
                <li class=" nav-item has-sub"><a href="#"><i class="bx bx-photo-album"  style="visibility: visible; width: 60px;"></i> <span class="menu-title" > <?php echo $this->lang->line("Facebook Posting"); ?>  </span></a>
                    <ul class="menu-content">
                      <li class=" nav-item "><a href="<?php echo base_url();?>ultrapost/text_image_link_video" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i> <span class="menu-title" data-i18n="pen"> <?php echo $this->lang->line("Basic FB Posts"); ?></span></a> </li>
                     <li class=" nav-item "><a href="<?php echo base_url();?>ultrapost/cta_post" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i> <span class="menu-title" data-i18n="pen"> <?php echo $this->lang->line("Call-to-Action Posts"); ?></span></a> </li>
                     <li class=" nav-item "><a href="<?php echo base_url("ultrapost/carousel_slider_post");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i> <span class="menu-title" data-i18n="internet"> <?php echo $this->lang->line("Carousel Slider Posts"); ?></span></a> </li>
                     <li class=" nav-item "><a href="<?php echo base_url("vidcasterlive/live_scheduler_list");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i> <span class="menu-title" data-i18n="internet"> <?php echo $this->lang->line("Pre-recorded FB Live"); ?></span></a> </li>

                </ul>
                </li>
                
                <li class=" nav-item "><a href="<?php echo base_url();?>instagram_poster"><i class="bx bx-image-add" style="visibility: visible; width: 60px;"></i> <span class="menu-title" data-i18n="pen"> <?php echo $this->lang->line("Instagram Posting"); ?></span></a> </li>
                
                 <li class=" nav-item has-sub"><a href="#"><i class="bx bx-images" style="visibility: visible; width: 60px;"></i> <span class="menu-title" > <?php echo $this->lang->line("Bulk Posting"); ?>  </span></a>
                    <ul class="menu-content">
                      <li class=" nav-item "><a href="<?php echo base_url();?>comboposter/text_post/campaigns" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Shop"></i> <span class="menu-title" data-i18n="pen"> <?php echo $this->lang->line("Text Posts"); ?></span></a> </li>
                     <li class=" nav-item "><a href="<?php echo base_url();?>comboposter/image_post/campaigns" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Shop"></i> <span class="menu-title" data-i18n="pen"> <?php echo $this->lang->line("Image Posts"); ?></span></a> </li>
                     <li class=" nav-item "><a href="<?php echo base_url("comboposter/video_post/campaigns");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Shop"></i> <span class="menu-title" data-i18n="internet"> <?php echo $this->lang->line("Video Posts"); ?></span></a> </li>
                      <li class="nav-item"><a href="<?php echo base_url("comboposter/link_post/campaigns");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Shop"></i><span class="menu-title" data-i18n="Documentation"> <?php echo $this->lang->line("Link Posts"); ?></span></a></li>
                      <li class="nav-item"><a href="<?php echo base_url("comboposter/html_post/campaigns");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Shop"></i><span class="menu-title" data-i18n="Documentation"> <?php echo $this->lang->line("HTML Posts"); ?></span></a></li>
                      <li class="nav-item"><a href="<?php echo base_url("post_planner");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Shop"></i><span class="menu-title" data-i18n="Documentation"> <?php echo $this->lang->line("CSV Bulk Posting"); ?></span></a></li>
                   
                    </ul>
                </li>
                
                <li class=" nav-item has-sub"><a href="#"><i class="bx bx-comment-detail" style="visibility: visible; width: 60px;"></i> <span class="menu-title" > <?php echo $this->lang->line("FB Comment Tools"); ?>  </span></a>
                    <ul class="menu-content">
                     <li class=" nav-item"><a href="<?php echo base_url();?>comment_automation/comment_template_manager" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i> <span class="menu-title" data-i18n="Chat"> <?php echo $this->lang->line("Comment Template"); ?></span></a>
                     </li>
                     <li class=" nav-item"><a href="<?php echo base_url();?>comment_automation/template_manager" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i> <span class="menu-title" data-i18n="Chat"> <?php echo $this->lang->line("Reply Template"); ?></span></a>
                     </li>
                     <li class=" nav-item"><a href="<?php echo base_url();?>comment_automation/index" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i> <span class="menu-title" data-i18n="Chat"> <?php echo $this->lang->line("Automation Campaign"); ?></span></a>
                     </li>
                     <li class=" nav-item"><a href="<?php echo base_url();?>comment_reply_enhancers/post_list" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i> <span class="menu-title" data-i18n="Chat"> <?php echo $this->lang->line("Tag Campaign"); ?></span></a>
                     </li>
                     <li class=" nav-item"><a href="<?php echo base_url();?>comment_automation/comment_section_report" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i> <span class="menu-title" data-i18n="Chat"> <?php echo $this->lang->line("Reports"); ?></span></a>
                     </li>
                </ul>
                </li>

                <?php if($this->basic->is_exist("add_ons",array("unique_name"=>"instagram_reply_enhancers"))) : ?>
                <li class=" nav-item has-sub"><a href="#"><i class="bx bx-comment-add" style="visibility: visible; width: 60px;"></i> <span class="menu-title" > <?php echo $this->lang->line("IG Comment Tools"); ?>  </span></a>
                    <ul class="menu-content">
                     <li class=" nav-item"><a href="<?php echo base_url();?>comment_automation/comment_template_manager" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i> <span class="menu-title" data-i18n="Chat"> <?php echo $this->lang->line("Comment Template"); ?></span></a>
                     </li>
                     <li class=" nav-item"><a href="<?php echo base_url();?>instagram_reply/template_manager" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i> <span class="menu-title" data-i18n="Chat"> <?php echo $this->lang->line("Reply Template"); ?></span></a>
                     </li>
                     <li class=" nav-item"><a href="<?php echo base_url();?>instagram_reply/get_account_lists" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i> <span class="menu-title" data-i18n="Chat"> <?php echo $this->lang->line("Automation Campaign"); ?></span></a>
                     </li>
                     <li class=" nav-item"><a href="<?php echo base_url();?>instagram_reply/reports" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i> <span class="menu-title" data-i18n="Chat"> <?php echo $this->lang->line("Reports"); ?></span></a>
                     </li>
                </ul>
                </li>
                <?php endif ?>

                <li class=" nav-item has-sub"><a href="#"><i class="bx bx-rss" style="visibility: visible; width: 60px;"></i> <span class="menu-title" > <?php echo $this->lang->line("RSS Auto Posting"); ?>  </span></a>
                 <ul class="menu-content">
                <li class=" nav-item"><a href="<?php echo base_url();?>autoposting/settings" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i> <span class="menu-title" > <?php echo $this->lang->line("RSS Sharing"); ?></span></a>
                </li>
				<?php if($this->basic->is_exist("add_ons",array("unique_name"=>"auto_feed_post"))) : ?>
                 <li class=" nav-item"><a href="<?php echo base_url();?>auto_feed_post/youtube_settings" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i> <span class="menu-title"> <?php echo $this->lang->line("Youtube Sharing"); ?></span></a>
                </li>
                 <li class=" nav-item"><a href="<?php echo base_url();?>auto_feed_post/wordpress_settings" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i> <span class="menu-title" > <?php echo $this->lang->line("Wordpress Sharing"); ?></span></a>
                </li>
				 <?php endif; ?>
				 <?php if($this->basic->is_exist("add_ons",array("unique_name"=>"gmb"))) : ?>
                 <li class=" nav-item"><a href="<?php echo base_url();?>gmb/rss" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i> <span class="menu-title" > <?php echo $this->lang->line("RSS to GMB"); ?></span></a>
                </li>
				  <?php endif; ?>
                </ul>
                </li>
        
                <li class=" navigation-header"><span> <?php echo $this->lang->line("Email SMS Marketing"); ?></span>
                </li>
                <li class=" nav-item has-sub"><a href="#"><i class="bx bx-book-content" style="visibility: visible; width: 60px;"></i><span class="menu-title" data-i18n="Ecommerce"> <?php echo $this->lang->line("Contacts"); ?></span></a>
                    <ul class="menu-content">
                        <li><a href="<?php echo base_url();?>sms_email_manager/contact_group_list" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Shop"></i><span class="menu-item"> <?php echo $this->lang->line("Contact Groups"); ?></span></a>
                        </li>
                        <li><a href="<?php echo base_url();?>sms_email_manager/contact_list" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Shop"></i><span class="menu-item"> <?php echo $this->lang->line("Contact Lists"); ?></span></a>
                        </li>
                         </ul>
				<?php if($this->basic->is_exist("add_ons",array("unique_name"=>"sms_email_sequence"))) : ?>
                   <li class=" nav-item"><a href="<?php echo base_url('sms_email_sequence/create_sequnce_for_external'); ?>"><i class="bx bx-mail-send" style="visibility: visible; width: 60px;"></i> <span class="menu-item"> <?php echo $this->lang->line("Email/SMS Sequence"); ?></span></a>
                    </li>
					<?php endif ?>
					<?php if($this->basic->is_exist("add_ons",array("unique_name"=>"email_optin_form_builder"))) : ?>
                   <li class=" nav-item"><a href="<?php echo base_url('email_optin_form_builder'); ?>"><i class="bx bx-spreadsheet" style="visibility: visible; width: 60px;"></i> <span class="menu-item"> <?php echo $this->lang->line("Opt-in Form Builder"); ?></span></a>
                    </li>
						
				<?php endif ?>
                 <li class=" nav-item has-sub"><a href="#"><i class="bx bx-envelope" style="visibility: visible; width: 60px;"></i><span class="menu-title"> <?php echo $this->lang->line("Email campaign"); ?> </span></a>
                    <ul class="menu-content">
                        <li><a href="<?php echo base_url('sms_email_manager/create_email_campaign'); ?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Shop"> <?php echo $this->lang->line("Create Campaign"); ?></span></a>
                        </li>
                        <li><a href="<?php echo base_url('sms_email_manager/email_campaign_lists'); ?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Shop"> <?php echo $this->lang->line("My Campaigns"); ?></span></a>
                        </li>
                        <li><a href="<?php echo base_url();?>sms_email_manager/template_lists/email" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Details"> <?php echo $this->lang->line("Email Templates"); ?></span></a>
                        </li>
                        <li><a href="<?php echo base_url("sms_email_manager/smtp_config");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Wish List"> <?php echo $this->lang->line("SMTP API Settings"); ?></span></a>
                        </li>
                        <li><a href="<?php echo base_url("sms_email_manager/mandrill_api_config");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Wish List"> <?php echo $this->lang->line("Mandrill API Settings"); ?></span></a>
                        </li>
                        <li><a href="<?php echo base_url("sms_email_manager/sendgrid_api_config");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Wish List"> <?php echo $this->lang->line("Sendgrid API Settings"); ?></span></a>
                        </li>
                        <li><a href="<?php echo base_url("sms_email_manager/mailgun_api_config");?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Wish List"> <?php echo $this->lang->line("Mailgun API Settings"); ?></span></a>
                        </li>
                    </ul>
                </li>
                 <li class=" nav-item has-sub"><a href="#"><i class="bx bx-paper-plane" style="visibility: visible; width: 60px;"></i><span class="menu-title" data-i18n="sms"> <?php echo $this->lang->line("SMS campaign"); ?></span></a>
                    <ul class="menu-content">
                        <li><a href="<?php echo base_url('sms_email_manager/create_sms_campaign'); ?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Shop"> <?php echo $this->lang->line("Create Campaign"); ?></span></a>
                        </li>
                        <li><a href="<?php echo base_url('sms_email_manager/sms_campaign_lists'); ?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Shop"> <?php echo $this->lang->line("My Campaigns"); ?></span></a>
                        </li>
                        <li><a href="<?php echo base_url('sms_email_manager/template_lists/sms'); ?>" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Shop"> <?php echo $this->lang->line("SMS Templates"); ?></span></a>
                        </li>
                        <li><a href="<?php echo base_url();?>sms_email_manager/sms_api_lists" class="d-flex align-items-center" ><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Wish List"> <?php echo $this->lang->line("SMS API Settings"); ?></span></a>
                        </li>
                    </ul>
                </li>
                 
                   
                </li>
                
                
                <li class="navigation-header"><span> <?php echo $this->lang->line("Request"); ?></span>
                </li>
                 
                <li class=" nav-item"><a href="https://chathero.co/feature-request/"><i class="bx bx-edit-alt" style="visibility: visible; width: 60px;"></i> <span class="menu-title" > <?php echo $this->lang->line("Feature Request"); ?></span></a>
                </li>
                
                 <li class="navigation-header"><span> <?php echo $this->lang->line("Community"); ?></span>
                </li>
                 
                <li class=" nav-item"><a href="https://www.facebook.com/groups/664047870982074"><i class="bx bx-heart-circle" style="visibility: visible; width: 60px;"></i> <span class="menu-title" > <?php echo $this->lang->line("Join Community"); ?></span></a>
                </li>
				
                 <?php  if($this->session->userdata('license_type') == 'double') if($this->config->item('enable_support') == '1')
                   {
                   $support_menu = $this->lang->line("Contact Support");
                   $support_url = base_url('/simplesupport/tickets');
          
                echo '
				<li class="navigation-header"><span> '.$support_menu.'</span>
                <li class="nav-item"><a href="'.$support_url.'"><i class="bx bx-support" style="visibility: visible; width: 60px;"></i><span class="menu-title" data-i18n="Raise Support">'.$support_menu.'</span></a>
                </li>';
                  }
                 ?>
				
                </br>
            </ul>
        </div>
    </div>
  
 
 <script>
 $('#main-menu-navigation li.nav-item').on('click', function() {
$('#main-menu-navigation li.nav-item').removeClass('active');
  $(this).addClass('active');
});

</script>