<?php
    $page_name = str_replace('inatagram.com', 'instagram', $page_name);
?>


<div class="content-header row">
    <div class="content-header-left col-12 mb-2 mt-1">
        <div class="breadcrumbs-top">
            <h5 class="content-header-title float-left pr-1 mb-0"><?php  echo $this->lang->line('Live Chat')." : ".$page_name; ?></h5>
            <div class="breadcrumb-wrapper d-none d-sm-block">
                <ol class="breadcrumb p-0 mb-0 pl-1">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard"><i class="bx bx-home-alt"></i></a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url("subscriber_manager"); ?>"><?php echo $this->lang->line("subscriber manager"); ?></a></li>
                    <li class="breadcrumb-item active"><?php echo $this->lang->line('IG Conversations'); ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">

    </div>
</div>


  <div class="section-body">
      <div class="row justify-content-center">

          <div class="col-12 col-sm-6 col-lg-3 m-0 p-0">
                <div class="card shadow-none">
                  <div class="card-body">
                      <a name="refresh_data" id="refresh_data" href="#" page_table_id="<?php echo $page_table_id; ?>" class="btn btn-primary mb-1">
                          <i class="bx bx-sync"></i> <?php echo $this->lang->line("Refresh"); ?>
                      </a>
                      <div class="makeScroll">
                          <ul class="list-unstyled list-unstyled-border" id="put_content">
                          </ul>
                      </div>
                  </div>
                </div>
          </div>
      <div class="col-12 col-sm-6 col-lg-6 m-0 p-0">
              <div class="card chat-box card-success shadow-none" style="min-height: 500px" id="mychatbox2">
                  <div class="card-header">
                      <h4 class="w-100 pr-0">   <span class="float-left pr-2 width-95-per"><i class="bx bxs-circle text-success mr-2" title="" data-toggle="tooltip" data-original-title="Online"></i> <?php echo $this->lang->line("Chat with") ?> <span id="chat_with"></span></span>
                          <input type="text" class="form-control float-left search_list" onkeyup="search_in_div(this,'conversation_modal_body')" autofocus="" placeholder="<?php echo $this->lang->line("Search...") ?>">
                          <select name="refresh_seconds" id="refresh_interval" class="form-control d-inline float-right py-0">
                              <option value="30000"> <?php echo $this->lang->line('Refresh');?></option>
                              <!-- <option value="5000"> 5 <?php echo $this->lang->line('Sec');?></option> -->
                              <option value="10000"> 10 <?php echo $this->lang->line('Sec');?></option>
                              <option value="15000"> 15 <?php echo $this->lang->line('Sec');?></option>
                              <option value="20000"> 20 <?php echo $this->lang->line('Sec');?></option>
                              <option value="30000"> 30 <?php echo $this->lang->line('Sec');?></option>
                              <option value="60000"> 60 <?php echo $this->lang->line('Sec');?></option>
                          </select>
                      </h4>
                  </div>
                  <div class="card-body chat-content2" style="overflow-y: auto;" id="conversation_modal_body">
                  </div>
                  <div class="card-footer chat-form">
                      <form id="chat-form2">

                <div class="row">
                  <div class="col-12 col-md-3 pr-0">
                    <?php echo form_dropdown('message_tag', $tag_list, 'HUMAN_AGENT','class="form-control select2" id="message_tag" style="width: 100% !important;"'); ?>
                  </div>
                  <div class="col-12 col-md-9 no_padding_col_left">
                      <fieldset>
                          <div class="input-group">
                              <input  type="text" id="reply_message" class="form-control border no_radius" placeholder="<?php echo $this->lang->line('Type a message..');?>" autofocus="">
                              <div class="input-group-append" id="button-addon2">
                                  <button class="btn btn-primary" id="final_reply_button" type="button"><i class="bx bx-paper-plane"></i></button>
                              </div>
                          </div>
                      </fieldset>


                  </div>
                </div>


                      </form>
                  </div>
              </div>
          </div>
      <div class="col-12 col-sm-12 col-lg-3">
        <div class="card card-primary shadow-none" style="min-height: 500px">
          <div class="card-header">
            <h4 class="w-100">
              <?php echo $this->lang->line('Actions'); ?>
            </h4>
          </div>
          <div class="card-body p-0">
            <div id="subscriber_action">
            </div>
  </div>
        </div>
      </div>
    </div>
  </div>



