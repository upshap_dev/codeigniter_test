<?php
$include_upload=0;  //upload_js
$include_datatable=1; //datatables
$include_datetimepicker=1; //datetimepicker, daterange, moment
$include_emoji=0;
$include_summernote=0;
$include_colorpicker=0;
$include_select2=1;
$include_jqueryui=0;
$include_mCustomScrollBar=0;
$include_dropzone=0;
$include_tagsinput=0;
$include_alertify=0;
$include_morris=0;
$include_chartjs=0;
$include_owlcar=0;
$include_prism=0;
?>
<style>
    #page_id{width: 150px;}
    #searching{max-width: 40%;}
    .swal-text{text-align: left !important;}
    @media (max-width: 575.98px) {
      #page_id{width: 90px;}
      #searching{max-width: 50%;}
      #add_custom_field { max-width: 100% !important; }
    }
    div.tooltip_pd{top:0px!important;}
    #get_subscriber_formdata{z-index: 1060!important;}
</style>
<input type="hidden" name="csrf_token" id="csrf_token" value="<?php echo $this->session->userdata('csrf_token_session'); ?>">

<div class="content-header row">
    <div class="content-header-left col-12 mb-2 mt-1">
        <div class="breadcrumbs-top">
            <h5 class="content-header-title float-left pr-1 mb-0"><?php echo $page_title; ?></h5>
            <div class="breadcrumb-wrapper d-none d-sm-block">
                <ol class="breadcrumb p-0 mb-0 pl-1">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard"><i class="bx bx-home-alt"></i></a></li>
                    <?php if($media_type =='ig'){
                        ?>
                        <li class="breadcrumb-item"><a href="<?php echo base_url("bot_instagram"); ?>"><?php echo $this->lang->line("Instagram Bot"); ?></a></li>
                        <?php
                    }else{
                        ?>
                        <li class="breadcrumb-item"><a href="<?php echo base_url("messenger_bot/bot_menu_section"); ?>"><?php echo $this->lang->line("Messenger Bot"); ?></a></li>
                        <?php
                    } ?>
                    <li class="breadcrumb-item active"><?php echo $page_title; ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <a href="<?php echo base_url('custom_field_manager/input_flow_builder/'.$media_type); ?>" class="btn btn-primary mb-1 add_custom_field">
            <i class="bx bx-plus-circle"></i> <?php echo $this->lang->line("New Flow"); ?>
        </a>
    </div>
</div>




<?php $this->load->view('admin/theme/message'); ?>


	<div class="section-body">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body data-card">
                        <div class="row">
                            <div class="col-md-8 col-12">
                                <div class="input-group mb-3 float-left" id="searchbox">
                                    <input type="text" class="form-control" id="searching" name="searching" autofocus placeholder="<?php echo $this->lang->line('Search...'); ?>" aria-label="" aria-describedby="basic-addon2">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" id="search_submit" title="<?php echo $this->lang->line('Search'); ?>" type="button"><i class="bx bx-search"></i> <span class="d-none d-sm-inline"><?php echo $this->lang->line('Search'); ?></span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
						<div class="table-responsive2">
							<table class="table table-bordered" id="mytable">
								<thead>
									<tr>
										<th>#</th>
										<th><?php echo $this->lang->line("ID"); ?></th>
										<th><?php echo $this->lang->line("Flow Name"); ?></th>
										<th><?php echo $this->lang->line("Page Name"); ?></th>
										<th><?php echo $this->lang->line("Action"); ?></th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>



<div class="modal fade" id="detail-flow-input" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title"><i class="bx bx-users"></i> <?php echo $this->lang->line("Flow Subscribers"); ?></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="bx bx-x"></i>
                </button>
            </div>

            <div class="modal-body" id="subscriber_actions_modal_body" data-backdrop="static" data-keyboard="false">
                <div class="card shadow-none">
                    <div class="row">
                        <div class="col-12">
                            <div class="card-body pb-0">
                                <input type="text" id="searching2" name="searching2" class="form-control" placeholder="<?php echo $this->lang->line("Search..."); ?>" style='width:200px;'>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="card-body data-card">
                                <div class="table-responsive2">
                                    <input type="hidden" id="put_table_id">
                                    <input type="hidden" id="media_type" value="<?php echo $media_type; ?>">
                                    <table class="table table-bordered" id="mytable1">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><?php echo $this->lang->line("Avatar"); ?></th>
                                                <th><?php echo $this->lang->line("First Name"); ?></th>
                                                <th><?php echo $this->lang->line("Last Name"); ?></th>
                                                <th><?php echo $this->lang->line("Subscriber ID"); ?></th>
                                                <th><?php echo $this->lang->line("Submitted At"); ?></th>
                                                <th><?php echo $this->lang->line("Actions"); ?></th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <!-- <div id="detail-first-view">
                            <div class="first-view-spinner">
                                <i class="bx bx-sync bx-spin bx-2x blue"></i>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>

            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $this->lang->line('Close'); ?></button>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="get_subscriber_formdata" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title"><i class="bx bx-info-circle"></i> <?php echo $this->lang->line("All Submitted Data"); ?></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="bx bx-x"></i>
                </button>
            </div>

            <div class="modal-body" data-backdrop="static" data-keyboard="false">
                <div class="row">
                    <div class="col-12">
                        <div class="row formdata_div"></div>
                    </div>

                    <div class="text-center waiting" id="waiting-div">
                        <i class="bx bx-sync bx-spin blue text-center" style="font-size:40px"></i>
                    </div>
                </div>
            </div>

            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $this->lang->line('Close'); ?></button>
            </div>

        </div>
    </div>
</div>