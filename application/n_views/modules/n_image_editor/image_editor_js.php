<script src="<?= base_url() ?>plugins/pixie/scripts.min.js?ver=<?php echo $n_config['theme_version']; ?>"></script>
<script>
    var pixie = new Pixie({
        languages: {
            active: '<?php echo strtolower($current_language); ?>',
            custom: {
                english: <?php echo file_get_contents(APPPATH.'modules/n_image_editor/language/pixie/translations_en.json'); ?>,
                arabic: <?php echo file_get_contents(APPPATH.'modules/n_image_editor/language/pixie/translations_ar.json'); ?>,
                bengali: <?php echo file_get_contents(APPPATH.'modules/n_image_editor/language/pixie/translations_bn.json'); ?>,
                dutch: <?php echo file_get_contents(APPPATH.'modules/n_image_editor/language/pixie/translations_nl.json'); ?>,
                french: <?php echo file_get_contents(APPPATH.'modules/n_image_editor/language/pixie/translations_fr.json'); ?>,
                german: <?php echo file_get_contents(APPPATH.'modules/n_image_editor/language/pixie/translations_de.json'); ?>,
                greek: <?php echo file_get_contents(APPPATH.'modules/n_image_editor/language/pixie/translations_el.json'); ?>,
                italian: <?php echo file_get_contents(APPPATH.'modules/n_image_editor/language/pixie/translations_it.json'); ?>,
                polish: <?php echo file_get_contents(APPPATH.'modules/n_image_editor/language/pixie/translations_pl.json'); ?>,
                portuguese: <?php echo file_get_contents(APPPATH.'modules/n_image_editor/language/pixie/translations_pt.json'); ?>,
                russian: <?php echo file_get_contents(APPPATH.'modules/n_image_editor/language/pixie/translations_ru.json'); ?>,
                spanish: <?php echo file_get_contents(APPPATH.'modules/n_image_editor/language/pixie/translations_es.json'); ?>,
                turkish: <?php echo file_get_contents(APPPATH.'modules/n_image_editor/language/pixie/translations_tr.json'); ?>,
                vietnamese: <?php echo file_get_contents(APPPATH.'modules/n_image_editor/language/pixie/translations_vi.json'); ?>,
            }
        },
        baseUrl: '/plugins/pixie',
        urls: {
            base: '/plugins/pixie',
            assets: '/plugins/pixie/assets/',
        },
        onLoad: function() {
            //console.log('Pixie is ready');
        }
    });



         var fullscreen = 0;

            $("#fullscreen_switch").click(function(e){
                e.preventDefault();
                $("#editormain").toggleClass("fullscreen_ie");

                // Toggle menu
                $.app.menu.toggle()

                setTimeout(function () {
                    $(window).trigger("resize")
                }, 200)

                if ($("#collapsed-sidebar").length > 0) {
                    setTimeout(function () {
                        if ($body.hasClass("menu-expanded") || $body.hasClass("menu-open")) {
                            $("#collapsed-sidebar").prop("checked", false)
                        } else {
                            $("#collapsed-sidebar").prop("checked", true)
                        }
                    }, 1000)
                }

                // Hides dropdown on click of menu toggle
                // $('[data-toggle="dropdown"]').dropdown('hide');

                // Hides collapse dropdown on click of menu toggle
                if (
                    $(
                        ".vertical-overlay-menu .navbar-with-menu .navbar-container .navbar-collapse"
                    ).hasClass("show")
                ) {
                    $(
                        ".vertical-overlay-menu .navbar-with-menu .navbar-container .navbar-collapse"
                    ).removeClass("show")
                }

                return false

        });

        </script>

