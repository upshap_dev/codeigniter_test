<?php
    include(FCPATH.'application/n_views/config.php');
    if($n_config['default_flowbuilder']=='true'){
        include(FCPATH.'application/modules/visual_flow_builder/views/index.php');
        exit;
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon.png">
    <title><?php echo $page_title; ?></title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.1/css/all.min.css?ver=<?php echo $n_config['theme_version']; ?>">

    <script src="<?php echo base_url(); ?>n_assets/app-assets/vendors/js/extensions/i18next.min.js?ver=<?php echo $n_config['theme_version']; ?>"></script>
    <script src="<?php echo base_url(); ?>n_assets/app-assets/vendors/js/extensions/i18nextXHRBackend.min.js?ver=<?php echo $n_config['theme_version']; ?>"></script>
    <script>
        i18next
            .init({
                    debug: false,
                lng: 'dev',
                fallbackLng: false,
                keySeparator: '>',
                nsSeparator: '|',
                resources: {
                    dev: {
                        translation:
                        <?php
                        $current_language = $this->language;
                        if(!file_exists(FCPATH . 'n_assets/js/flow_builder/'.$current_language.'.json')){
                            $current_language = $n_config['default_lang_flowbuilder'];
                        }
                        if(file_exists(APPPATH.'n_lang'.$current_language.'.json')){
                            include(APPPATH.'n_lang'.$current_language.'.json');
                        }else{
                            include(FCPATH . 'n_assets/js/flow_builder/'.$current_language.'.json');
                        }
                        ?>
                    },
                },

                    returnObjects: true,
                keySeparator: false,
                saveMissing: true
                }
            );
        function docReady(fn) {
            // see if DOM is already available
            if (document.readyState === "complete" || document.readyState === "interactive") {
                // call on next available tick
                setTimeout(fn, 1);
            } else {
                document.addEventListener("DOMContentLoaded", fn);
            }
        }
        i18next.on('missingKey', function(lngs, namespace, key, res) {
            console.log('"'+key+'"');
        })
        function docReady(fn) {
            // see if DOM is already available
            if (document.readyState === "complete" || document.readyState === "interactive") {
                // call on next available tick
                setTimeout(fn, 1);
            } else {
                document.addEventListener("DOMContentLoaded", fn);
            }
        }
    </script>




    <link href="<?php echo base_url(); ?>plugins/flow_builder/css/app.8cccb580.css?ver=<?php echo $n_config['theme_version']; ?>" rel="stylesheet">
    <link href="<?php echo base_url(); ?>plugins/flow_builder/css/chunk-vendors.580971a8.css?ver=<?php echo $n_config['theme_version']; ?>" rel="stylesheet">





    <?php if($json_data) { ?>
        <script>
            var data = '<?php echo sanitize_json_string($json_data); ?>';
        </script>
    <?php } else { ?>
        <script>
            var data = null;
        </script>
    <?php } ?>
    <script>


            var builder_id = "xitFB@0.0.1"
            var page_table_id = '<?php echo $page_table_id; ?>'
            var builder_table_id = '<?php echo $builder_table_id; ?>'
            var sequence_addon = '<?php echo $sequence_addon; ?>'
            var user_input_flow_addon = '<?php echo $user_input_flow_addon; ?>'
            var go_back_link = '<?php echo $go_back_link; ?>';
            window.xitFlowBuilderData = {
                "page_table_id": parseInt(page_table_id, 10),
                "base_url": '<?php echo base_url(); ?>',
                "data": JSON.parse(data),
                "builder_id": builder_id,
                "builder_table_id": parseInt(builder_table_id, 10),
                "sequence_addon": parseInt(sequence_addon, 10),
                "user_input_flow_addon": parseInt(user_input_flow_addon, 10),
                "go_back_link": go_back_link
            }
    </script>
</head>
<body>
<noscript><strong>We're sorry but flow-builder doesn't work properly without JavaScript enabled. Please enable it to continue.</strong></noscript>
<div id="xit-flow-builder"></div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.1/js/all.min.js?ver=<?php echo $n_config['theme_version']; ?>"></script>
<script defer src="<?php echo base_url(); ?>plugins/flow_builder/js/chunk-vendors.fac0cd55.js?ver=<?php echo $n_config['theme_version']; ?>"></script>
<script defer  src="<?php echo base_url(); ?>n_assets/js/app.217576e6.js?ver=<?php echo $n_config['theme_version']; ?>"></script>
<script defer>
    docReady(function() {
        var arr_n = document.getElementsByClassName('tabs-content-component-title');
        for (var i = 0; i < arr_n.length; i++) {
            arr_n[i].innerHTML = i18next.t(arr_n.item(i).innerText);
        }
        var arr_n = document.getElementsByClassName('title-content');
        for (var i = 0; i < arr_n.length; i++) {
            text_rp = arr_n.item(i).innerText.trim();
            arr_n[i].innerText = i18next.t(text_rp);
        }

    });
</script>
</body>
</html>
