<script>
    $(document).ready(function($) {

        var base_url = '<?php echo base_url(); ?>';

        setTimeout(function() {
            $('#post_date_range').daterangepicker({
                ranges: {
                    '<?php echo $this->lang->line("Last 30 Days");?>': [moment().subtract(29, 'days'), moment()],
                    '<?php echo $this->lang->line("This Month");?>'  : [moment().startOf('month'), moment().endOf('month')],
                    '<?php echo $this->lang->line("Last Month");?>'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                startDate: moment().subtract(29, 'days'),
                endDate  : moment()
            }, function (start, end) {
                $('#post_date_range_val').val(start.format('YYYY-M-D') + '|' + end.format('YYYY-M-D')).change();
            });
        }, 2000);

        var perscroll;
        var table = $("#mytable").DataTable({
            serverSide: true,
            processing:true,
            bFilter: false,
            order: [[ 1, "desc" ]],
            pageLength: 10,
            ajax: {
                "url": base_url + 'gmb/posts_data',
                "type": 'POST',
                data: function (d) {
                    d.location_name = $('#location_name').val();
                    d.post_type = $('#post_type').val();
                    d.searching = $('#searching').val();
                    d.post_date_range = $('#post_date_range_val').val();
                }
            },
            language: {
                url: "<?php echo base_url('assets/modules/datatables/language/' . $this->language . '.json'); ?>"
            },
            dom: '<"top"f>rt<"bottom"lip><"clear">',
            columnDefs: [
                {
                    targets: [1,8],
                    visible: false
                },
                {
                    targets: [0,2,3,4,6],
                    className: 'text-center'
                },
                {
                    targets:[0,5,7,8],
                    sortable: false
                }
            ],
            fnInitComplete:function() {  // when initialization is completed then apply scroll plugin
                if(areWeUsingScroll) {
                    if (perscroll) perscroll.destroy();
                    perscroll = new PerfectScrollbar('#mytable_wrapper .dataTables_scrollBody');
                }
            },
            scrollX: 'auto',
            fnDrawCallback: function( oSettings ) { //on paginition page 2,3.. often scroll shown, so reset it and assign it again
                if(areWeUsingScroll) {
                    if (perscroll) perscroll.destroy();
                    perscroll = new PerfectScrollbar('#mytable_wrapper .dataTables_scrollBody');
                }
            }
        });

        $(document).on('change', '#location_name', function(event) {
            event.preventDefault();
            table.draw();
        });

        $(document).on('change', '#post_type', function(event) {
            event.preventDefault();
            table.draw();
        });

        $(document).on('change', '#post_date_range_val', function(event) {
            event.preventDefault();
            table.draw();
        });

        $(document).on('click', '#search_submit', function(event) {
            event.preventDefault();
            table.draw();
        });


        $(document).on('click', '.campaign-report', function(event) {
            event.preventDefault();

            var post_id = $(this).data('post-id');
            var campaign_name = $(this).data('campaign-name');

            $("#report_data").html('<div class="text-center p-5"><i class="bx bx-spin bx-loader fa-3x text-primary"></i></div>');
            $("#campaign-report-modal").modal();

            $("#campaign-report-modal .modal-footer").attr('style', 'display:none !important');

            $.ajax({
                type: 'POST',
                data: { post_id },
                url: base_url + 'gmb/campaign_report',
                success:function(response){
                    $("#campaign-report-modal .modal-footer").attr('style', 'display:block !important');
                    $("#report_data").html(response)
                }
            })

        });

        $(document).on('click','.delete',function(e){
            e.preventDefault();
            swal.fire({
                title: '<?php echo $this->lang->line("Are you sure?"); ?>',
                text: "<?php echo $this->lang->line('Do you really want to delete this post from the database?'); ?>",
                icon: 'warning',
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    var id = $(this).attr('id');
                    $.ajax({
                        context: this,
                        type: 'POST',
                        url: "<?php echo base_url('gmb/delete_post')?>",
                        data:{ id },
                        success:function(response) {
                            var res = JSON.parse(response);

                            if (false === res.status) {
                                iziToast.error({
                                    title: '',
                                    message: '<?php echo $this->lang->line("You do not have permission delete this campaign."); ?>',
                                    position: 'bottomRight'
                                });
                            } else if (true === res.status) {
                                iziToast.success({
                                    title: '',
                                    message: '<?php echo $this->lang->line("Campaign has been deleted successfully."); ?>',
                                    position: 'bottomRight'
                                });
                                table.draw();
                            }
                        }
                    });
                }
            });
        });
    });
</script>