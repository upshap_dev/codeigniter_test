<script type="text/javascript">
    $("document").ready(function(){
        var user_id = "<?php echo $this->session->userdata('user_id'); ?>";
        var base_url="<?php echo site_url(); ?>";

        var table = $("#mytable").DataTable({
            language:
                {
                    url: "<?php echo base_url('assets/modules/datatables/language/'.$this->language.'.json'); ?>"
                },
            dom: '<"top"f>rt<"bottom"lip><"clear">',
            columnDefs: [
                {
                    targets: [3,5],
                    sortable: false
                }
            ]
        });

        (function ($, undefined) {
            $.fn.getCursorPosition = function() {
                var el = $(this).get(0);
                var pos = 0;
                if('selectionStart' in el) {
                    pos = el.selectionStart;
                } else if('selection' in document) {
                    el.focus();
                    var Sel = document.selection.createRange();
                    var SelLength = document.selection.createRange().text.length;
                    Sel.moveStart('character', -el.value.length);
                    pos = Sel.text.length - SelLength;
                }

                return pos;
            }
        })(jQuery);

        $(document).on('click', '#title_variable', function(event) {
            // event.preventDefault();

            let textAreaTxt = $(".emojionearea-editor").html();
            var lastIndex = textAreaTxt.lastIndexOf("<br>");
            var lastTag = textAreaTxt.substr(textAreaTxt.length - 4);
            lastTag=lastTag.trim(lastTag);

            if(lastTag=="<br>") {
                textAreaTxt = textAreaTxt.substring(0, lastIndex);
            }

            var txtToAdd = " #TITLE# ";
            var new_text = textAreaTxt + txtToAdd;
            $(".emojionearea-editor").html(new_text);
            $(".emojionearea-editor").click();
        });

        $(document).on('click', '.campaign_settings', function(e) {
            e.preventDefault();

            $('.xit-spinner').show();
            var id = $(this).attr('data-id');

            $.ajax({
                type:'POST' ,
                url: base_url + "gmb/rss_campaign_settings",
                data: {id},
                dataType: 'JSON',
                success:function(response) {
                    if(response.status =='0' ) {
                        $("#settings_modal .modal-footer").hide();
                    } else {
                        $("#settings_modal .modal-footer").show();
                    }

                    $("#feed_setting_container").html(response.html);
                    $("#put_feed_name").html(" : "+response.feed_name);

                    $("#submit_status").hide();
                    $('.xit-spinner').hide();
                    $("#settings_modal").modal();

                }
            });
        });

        $(document).on('click','#save_settings',function(e) {
            e.preventDefault();

            var location_name = $("#location_name").val(),
                posting_start_time=$("#posting_start_time").val(),
                posting_end_time=$("#posting_end_time").val();

            if (! Array.isArray(location_name) || ! location_name.length) {
                swal.fire(
                    '<?php echo $this->lang->line("Error"); ?>',
                    '<?php echo $this->lang->line('Please select location name(s)');?>',
                    'error'
                );

                return;
            }


            if(location_name.length)
            {
                if(posting_start_time == '' ||  posting_end_time == '') {
                    swal.fire(
                        '<?php echo $this->lang->line("Error"); ?>',
                        '<?php echo $this->lang->line('Please select post between times'); ?>',
                        'error'
                    );

                    return;
                }

                var rep1 = parseFloat(posting_start_time.replace(":", "."));
                var rep2 = parseFloat(posting_end_time.replace(":", "."));
                var rep_diff = rep2 - rep1;

                if(rep1 >= rep2 || rep_diff < 1.0)
                {
                    swal.fire(
                        '<?php echo $this->lang->line("Error"); ?>',
                        '<?php echo $this->lang->line("Post time was invalid. (The time difference should be 1 hour at least)");?>',
                        'error'
                    );

                    return;
                }
            }

            $("#save_settings").addClass("btn-progress");
            var queryString = new FormData($("#campaign_settings_form")[0]);

            $.ajax({
                type:'POST' ,
                url: base_url + "gmb/create_rss_campaign",
                dataType: 'JSON',
                data: queryString,
                contentType: false,
                processData: false,
                success:function(response)
                {
                    $("#save_settings").removeClass("btn-progress");

                    if ("0" === response.status) {
                        swal.fire('<?php echo $this->lang->line("Error"); ?>', response.message , 'error');
                        return;
                    }

                    if(response.status == '1') {
                        swal.fire('<?php echo $this->lang->line("Success"); ?>', response.message , 'success');
                    }

                    $("#settings_modal").modal('hide');
                    $('.modal-backdrop').hide();
                }
            });
        });

        $(document).on('click','.enable_settings',function(e){
            e.preventDefault();

            $(this).addClass('disabled');
            var id=$(this).attr('data-id');

            swal.fire({
                title: '<?php echo $this->lang->line("Delete Campaign"); ?>',
                text: '<?php echo $this->lang->line("Do you really want to enable this campaign?"); ?>',
                icon: 'warning',
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete)
                {
                    $.ajax({
                        type:'POST' ,
                        url: base_url + "gmb/enable_rss_settings",
                        data: {id:id},
                        dataType:'JSON',
                        success:function(response)
                        {
                            if(response.status=='0')
                            {
                                $("#enable"+id).removeClass('disabled');
                                iziToast.error({title: '<?php echo $this->lang->line("Error"); ?>',message: response.message,position: 'bottomRight'});
                            } else {
                                iziToast.success({title: '<?php echo $this->lang->line("Success"); ?>',message: '<?php echo $this->lang->line("Campaign has been enabled successfully."); ?>',position: 'bottomRight'});
                                setTimeout(function(){ location.reload(); }, 1000);
                            }
                        }
                    });
                }
            });
        });

        $(document).on('click','.disable_settings',function(e){
            e.preventDefault();

            var id=$(this).attr('data-id');

            swal.fire({
                title: '<?php echo $this->lang->line("Delete Campaign"); ?>',
                text: '<?php echo $this->lang->line("Do you really want to disable this campaign?"); ?>',
                icon: 'warning',
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete)
                {
                    $.ajax({
                        type:'POST' ,
                        url: base_url + "gmb/disable_rss_settings",
                        data: {id},
                        success:function(response)
                        {
                            iziToast.success({title: '<?php echo $this->lang->line("Success"); ?>',message: '<?php echo $this->lang->line("Campaign has been disabled successfully."); ?>',position: 'bottomRight'});
                            setTimeout(function(){ location.reload(); }, 1000);
                        }
                    });
                }
            });
        });

        $(document).on('click','.delete_settings',function(e){
            e.preventDefault();

            var id=$(this).attr('data-id');

            swal.fire({
                title: '<?php echo $this->lang->line("Delete Campaign"); ?>',
                text: '<?php echo $this->lang->line("Do you really want to delete this campaign?"); ?>',
                icon: 'warning',
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete)
                {
                    $.ajax({
                        type:'POST' ,
                        url: base_url + "gmb/delete_rss_settings",
                        data: {id:id},
                        success:function(response)
                        {
                            iziToast.success({title: '<?php echo $this->lang->line("Success"); ?>',message: '<?php echo $this->lang->line("Campaign has been deleted successfully."); ?>',position: 'bottomRight'});
                            setTimeout(function(){ location.reload(); }, 1000);
                        }
                    });
                }
            });
        });

        $(document).on('click','#add_feed_submit',function() {

            var feed_name = $("#feed_name").val();
            var feed_url = $("#feed_url").val();

            if(feed_name=='') {
                swal.fire('<?php echo $this->lang->line("Error"); ?>', "<?php echo $this->lang->line('Please select feed type name');?>" , 'error');
                return;
            }

            if(feed_url=='') {
                swal.fire('<?php echo $this->lang->line("Error"); ?>', "<?php echo $this->lang->line('Feed URL can not be empty');?>" , 'error');
                return;
            }

            $("#add_feed_submit").addClass('btn-progress');

            var queryString = new FormData($("#add_feed_form")[0]);

            $.ajax({
                type:'POST' ,
                url: base_url + "gmb/add_feed_action",
                data: queryString,
                dataType : 'JSON',
                processData: false,
                contentType: false,
                success: function(response) {
                    $("#add_feed_submit").removeClass('btn-progress');

                    if(response.status=='1') {
                        swal.fire('<?php echo $this->lang->line("Success"); ?>', response.message , 'success');
                    } else {
                        swal.fire('<?php echo $this->lang->line("Error"); ?>', response.message , 'error');
                    }

                }
            });
        });

        $('#add_feed_modal').on('hidden.bs.modal', function () {
            location.reload();
        });

        $('#settings_modal').on('hidden.bs.modal', function () {
            location.reload();
        });

        $(document).on('click','.error_log',function(e){
            e.preventDefault();
            $(".xit-spinner").show();
            $("#error_modal_container").html("");
            $("#error_modal").modal();
            var id=$(this).attr('data-id');
            $.ajax({
                type:'POST' ,
                url: base_url + "gmb/show_rss_error_log",
                data: {id:id},
                success:function(response)
                {
                    $("#error_modal_container").html(response);
                    $(".xit-spinner").hide();
                }
            });
        });

        $(document).on('click','.clear_log',function(e){
            e.preventDefault();
            var id=$(this).attr('data-id');
            swal.fire({
                title: '<?php echo $this->lang->line("Clear Log"); ?>',
                text: '<?php echo $this->lang->line("Do you really want to clear log?"); ?>',
                icon: 'warning',
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete)
                {
                    $.ajax({
                        type:'POST' ,
                        url: base_url + "gmb/clear_rss_error_log",
                        data: {id:id},
                        success:function(response)
                        {
                            $("#error_modal").modal('toggle');
                            swal.fire('<?php echo $this->lang->line("Clear Log"); ?>', "<?php echo $this->lang->line('Log has been cleared successfully.');?>" , 'success');
                        }
                    });
                }
            });
        });

        // Upload status
        $(document).on('change', '#media_status', function(e) {
            e.preventDefault();

            if (true === $('#media_status').prop('checked')) {
                $('#upload-wrapper').removeClass('d-none');
            } else {
                $('#upload-wrapper').addClass('d-none');
            }
        });
    });
</script>