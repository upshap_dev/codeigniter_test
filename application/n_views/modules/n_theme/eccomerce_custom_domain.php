<div class="content-header row d-none">
    <div class="content-header-left col-12 mb-2 mt-1">
        <div class="breadcrumbs-top">
            <h5 class="content-header-title float-left pr-1 mb-0"><?php echo $page_title; ?></h5>
            <div class="breadcrumb-wrapper d-none d-sm-block">
                <ol class="breadcrumb p-0 mb-0 pl-1">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard"><i class="bx bx-home-alt"></i></a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url("ecommerce"); ?>"><?php echo $this->lang->line("E-commerce"); ?></a></li>
                    <li class="breadcrumb-item active"><?php echo $page_title; ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('admin/theme/message'); ?>

<div class="section-body">
    <div class="row">
        <div class="col-12">
            <div class="card shadow-none">


                <?php if($no_access){ ?>

                <div class="card-body p-0">
                    <h4 class="card-title"><?php echo $this->lang->line('Permission denied');?></h4>
                </div>

               <?php }else{ ?>

                <div class="card-body p-0">

                    <form class="form-horizontal text-c" action="<?php echo site_url().'n_theme/custom_domain_save';?>" method="POST">
                        <input type="hidden" name="csrf_token" id="csrf_token" value="<?php echo $this->session->userdata('csrf_token_session'); ?>">
                        <input type="hidden" name="custom_id" id="custom_id" value="<?php echo $custom_id; ?>">

                                <p><?php echo $this->lang->line("You can connect your own domain to an ecommerce store. Enter the domain name below without http, www."); ?></p>
                                <fieldset class="form-group">
                                    <input class="form-control" id="eco_custom_domain" name="eco_custom_domain"  value="<?php echo $host_url; ?>" <?php if(!empty($host_url)){ echo 'disabled'; }; ?>>
                                </fieldset>
                        <span class="text-danger"><?php echo form_error('eco_custom_domain'); ?></span>

                        <div class="card-footer p-0">
                            <button class="btn btn-primary" id="save-btn" type="submit"><i class="bx bx-save"></i> <span class="align-middle ml-25"><?php echo $this->lang->line("save"); ?></span></button>
                        </div>
                    </form>
                </div>

                <?php  } ?>

            </div>
        </div>
    </div>
</div>