<div class="card shadow-none bg-transparent panel_dis" data-panel="category_but">
    <div class="card-header rounded-0 shadow-none bg-primary bg-darken-2 p-1">
        <h4 class="card-title text-white">
            <?php echo $this->lang->line("Category view") ?>
        </h4>
    </div>
    <div class="card-body bg-transparent pt-1 pb-0 pl-0 pr-0 scrolling">
        <div class="accordion" id="accordionCategory1">

            <div class="card bg-secondary bg-darken-3 collapse-header rounded-0  ">
                <div id="Category1" class="card-header border-0 pl-1 pr-1" role="tablist" data-toggle="collapse" data-target="#accordionCategory1" aria-expanded="false" aria-controls="accordionCategory1">
                    <span class="collapse-title text-white"><?php echo $l->line('Product view'); ?></span>

                </div>

                <div id="accordionCategory1" role="tabpanel" data-parent="#accordionCategory1" aria-labelledby="Category1" class="collapse bg-secondary border-top-darken-2">

                    <div class="card-body bg-secondary text-white pl-1 pr-1">
                        <p class="text-center text-bold-700"><?php echo $l->line('Columns'); ?></p>
                        <div class="form-group">
                            <div id="pips-range" class="mt-1 mb-3"></div>
                        </div>
                    </div>

                    <div class="card-body bg-secondary bg-darken-1 text-white pl-1 pr-1">
                        <h6 class="text-white"><?php echo $this->lang->line("Show reviews");?></h6>
                        <div class="form-group">
                            <?php
                            $select_lan=false;
                            if( isset($n_eco_builder_config['category_hide_reviews'])){
                                $select_lan=$n_eco_builder_config['category_hide_reviews'];
                            }
                            $options = array();
                            $options['none'] = 'Hide reviews';
                            $options['show'] = 'Show';
                            $options['always_show'] = 'Always show';

                            echo form_dropdown('category_hide_reviews',$options,$select_lan,'class="make_change form-control" id="category_hide_reviews"');  ?>
                        </div>
                        <span class="text-danger"><?php echo form_error('category_hide_reviews'); ?></span>
                    </div>






                </div>
            </div>




        </div>
    </div>
</div>