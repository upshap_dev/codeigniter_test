<div class="card shadow-none bg-transparent panel_dis" style="display:none;" data-panel="special_but">
    <div class="card-header rounded-0 shadow-none bg-primary bg-darken-2 p-1">
        <h4 class="card-title text-white">
            <?php echo $this->lang->line("Special pages") ?>
        </h4>
    </div>

    <?php
        $id_header = 'accordionSpecial1';
        $controls_header = 'Special';
        $i = 0;
    ?>
    <div class="card-body bg-transparent pt-1 pb-0 pl-0 pr-0 scrolling">
        <div class="accordion" id="<?php echo $id_header; ?>">

            <div class="card bg-secondary bg-darken-3 collapse-header rounded-0  ">
                <div id="<?php echo $controls_header.$i; ?>" class="card-header border-0 pl-1 pr-1" role="tablist" data-toggle="collapse" data-target="#<?php echo $controls_header.$i; ?>" aria-expanded="false" aria-controls="<?php echo $controls_header; ?>">
                    <span class="collapse-title text-white"><?php echo $l->line('Contact page'); ?></span>
                </div>

                <div id="<?php echo $controls_header.$i; ?>" role="tabpanel" data-parent="#<?php echo $id_header; ?>" aria-labelledby="heading1" class="collapse bg-secondary border-top-darken-2">
                    <div class="card-body bg-secondary text-white pl-1 pr-1">
                        <div class="card-body bg-secondary text-white pl-1 pr-1">
                            <h6 class="text-white"><?php echo $this->lang->line("Turn on contact page");?></h6>
                            <div class="form-group">
                                <?php
                                $select_lan=false;
                                if( isset($n_eco_builder_config['contact_page_on'])){
                                    $select_lan=$n_eco_builder_config['contact_page_on'];
                                }
                                $options = array();
                                $options['true'] = 'On';
                                $options['false'] = 'Off';

                                echo form_dropdown('contact_page_on',$options,$select_lan,'class="make_change form-control" id="contact_page_on"');  ?>
                            </div>
                            <span class="text-danger"><?php echo form_error('contact_page_on'); ?></span>
                        </div>
                    </div>
                    <div class="card-body bg-secondary bg-darken-1 text-white pl-1 pr-1">
                        <div class="col-12 p-0 text-center mb-1">
                        <a class="btn btn-primary action_iframe" href="#" data-action="<?php echo base_url(); ?>n_theme/user_editor_page/<?php echo $shop_id_short; ?>" target="_blank"><i class="bx bx-edit"></i> <span class="align-middle ml-25"><?php echo $this->lang->line("Edit contact page").' (default language)'; ?></span></a>
                        </div>

                        <?php
                        $language_info = _e_language_list();
                        ksort($language_info);
                        foreach ($language_info as $key_lang => $value_lang) {?>
                            <div class="col-12 p-0 text-center mb-1">

                                <a class="btn btn-primary action_iframe" href="#" data-action="<?php echo base_url(); ?>n_theme/user_editor_page/<?php echo $shop_id_short.'/'.strtolower($key_lang); ?>" target="_blank"><i class="bx bx-edit"></i> <span class="align-middle ml-25"><?php echo $this->lang->line("Edit homepage").' ('.$key_lang.')'; ?></span></a>

                            </div>

                        <?php  } ?>

                    </div>
                </div>
            </div>

            <?php $i++; ?>

            <div class="card bg-secondary bg-darken-3 collapse-header rounded-0  ">
                <div id="<?php echo $controls_header.$i; ?>" class="card-header border-0 pl-1 pr-1" role="tablist" data-toggle="collapse" data-target="#<?php echo $controls_header.$i; ?>" aria-expanded="false" aria-controls="<?php echo $controls_header; ?>">
                    <span class="collapse-title text-white"><?php echo $l->line('Home page editor'); ?></span>
                </div>

                <div id="<?php echo $controls_header.$i; ?>" role="tabpanel" data-parent="#<?php echo $id_header; ?>" aria-labelledby="heading1" class="collapse bg-secondary border-top-darken-2">
                    <div class="card-body bg-secondary text-white pl-1 pr-1">
                        <div class="card-body bg-secondary text-white pl-1 pr-1">
                            <h6 class="text-white"><?php echo $this->lang->line("Turn on custom home page");?></h6>
                            <div class="form-group">
                                <?php
                                $select_lan=false;
                                if( isset($n_eco_builder_config['home_page_editor_on'])){
                                    $select_lan=$n_eco_builder_config['home_page_editor_on'];
                                }
                                $options = array();
                                $options['true'] = 'On';
                                $options['false'] = 'Off';

                                echo form_dropdown('home_page_editor_on',$options,$select_lan,'class="make_change form-control" id="home_page_editor_on"');  ?>
                            </div>
                            <span class="text-danger"><?php echo form_error('home_page_editor_on'); ?></span>
                        </div>
                    </div>
                    <div class="card-body bg-secondary bg-darken-1 text-white pl-1 pr-1">
                        <div class="col-12 p-0 text-center mb-1">
                        <a class="btn btn-primary action_iframe" href="#" data-action="<?php echo base_url(); ?>n_theme/ecommerce_home_editor_page/<?php echo $shop_id_short; ?>" target="_blank"><i class="bx bx-edit"></i> <span class="align-middle ml-25"><?php echo $this->lang->line("Edit homepage").' (default language)'; ?></span></a>
                        </div>
                        <?php
                        ksort($language_info);
                        foreach ($language_info as $key_lang => $value_lang) {?>
                            <div class="col-12 p-0 text-center mb-1">

                                <a class="btn btn-primary action_iframe" href="#" data-action="<?php echo base_url(); ?>n_theme/ecommerce_home_editor_page/<?php echo $shop_id_short.'/'.strtolower($key_lang); ?>" target="_blank"><i class="bx bx-edit"></i> <span class="align-middle ml-25"><?php echo $this->lang->line("Edit homepage").' ('.$key_lang.')'; ?></span></a>

                            </div>

                        <?php  } ?>
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>