<script>
    var base_url="<?php echo site_url(); ?>";
    var table_id="<?php echo $table_id; ?>";


    $(document).ready(function() {

        var table = $("#mytable").DataTable({
            serverSide: true,
            processing:true,
            bFilter: false,
            order: [[ 2, "desc" ]],
            pageLength: 10,
            ajax: {
                url: base_url+'comment_reply_enhancers/page_response_report_data/'+table_id,
                type: 'POST',
                dataSrc: function ( json )
                {
                    //$(".table-responsive").niceScroll();
                    return json.data;
                },
                data: function ( d )
                {
                    d.post_id = $('#post_id').val();
                }
            },
            language:
                {
                    url: "<?php echo base_url('assets/modules/datatables/language/'.$this->language.'.json'); ?>"
                },
            dom: '<"top"f>rt<"bottom"lip><"clear">',
            columnDefs: [
                {
                    targets: [1,2,5,6,7,8],
                    visible: false
                },
                {
                    targets: '',
                    className: 'text-center'
                },
                {
                    targets: [0,4,9,11],
                    sortable: false
                }
            ]
        });


        $(document).on('click', '#search_submit', function(event) {
            event.preventDefault();
            table.draw();
        });


        // report table started
        var table1 = '';
        var perscroll1;
        $(document).on('click','.view_report',function(e){
            e.preventDefault();
            var table_id = $(this).attr('table_id');

            if(table_id !='')
            {
                $("#put_row_id").val(table_id);
                $("#download").attr("href",base_url+"comment_reply_enhancers/download_get_reply_info/"+table_id);
            }


            $("#view_report_modal").modal();

            var commnet_hide_delete_addon = "<?php echo $commnet_hide_delete_addon; ?>";
            if(commnet_hide_delete_addon == 1)
                var visible_section = "";
            else
                var visible_section = [9];

            if (table1 == '')
            {

                table1 = $("#mytable1").DataTable({
                    serverSide: true,
                    processing:true,
                    bFilter: false,
                    order: [[ 3, "desc" ]],
                    pageLength: 10,
                    ajax: {
                        url: base_url+'comment_reply_enhancers/ajax_get_reply_info',
                        type: 'POST',
                        data: function ( d )
                        {
                            d.table_id = $("#put_row_id").val();
                            d.searching = $("#searching").val();
                        }
                    },
                    language:
                        {
                            url: "<?php echo base_url('assets/modules/datatables/language/'.$this->language.'.json'); ?>"
                        },
                    dom: '<"top"f>rt<"bottom"lip><"clear">',
                    columnDefs: [
                        {
                            targets: visible_section,
                            visible: false
                        },
                        {
                            targets: '',
                            className: 'text-center'
                        },
                        {
                            targets: [0,1,2,5,6,7,8,9],
                            sortable: false
                        }
                    ],
                    fnInitComplete:function(){ // when initialization is completed then apply scroll plugin
                        if(areWeUsingScroll)
                        {
                            if (perscroll1) perscroll1.destroy();
                            perscroll1 = new PerfectScrollbar('#mytable1_wrapper .dataTables_scrollBody');
                        }
                    },
                    scrollX: 'auto',
                    fnDrawCallback: function( oSettings ) { //on paginition page 2,3.. often scroll shown, so reset it and assign it again
                        if(areWeUsingScroll)
                        {
                            if (perscroll1) perscroll1.destroy();
                            perscroll1 = new PerfectScrollbar('#mytable1_wrapper .dataTables_scrollBody');
                        }
                    },
                    "drawCallback": function (settings) {
                        $('table [data-toggle="tooltip"]').tooltip('dispose');
                        $('table [data-toggle="tooltip"]').tooltip(
                            {
                                placement: 'left',
                                container: 'body',
                                html: true,
                                template: '<div class="tooltip tooltip_pd" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
                            }
                        );
                    }
                });
            }
            else table1.draw();

            $("#outside_filter").html('');
            setTimeout(function(){
                $.ajax({
                    type:'POST' ,
                    url: "<?php echo site_url(); ?>comment_reply_enhancers/get_count_info",
                    data:{table_id:table_id},
                    dataType:'JSON',
                    success:function(response)
                    {
                        if(response.status == '1')
                            $("#outside_filter").html(response.str);
                    }
                });
            }, 2000);

        });

        $(document).on('keyup', '#searching', function(event) {
            event.preventDefault();
            table1.draw();
        });





    });


</script>