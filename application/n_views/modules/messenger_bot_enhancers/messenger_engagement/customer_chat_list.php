<?php $this->load->view('admin/theme/message'); ?>
<style>
    .dropdown-toggle::after{content:none !important;}
  .dropdown-toggle::before{content:none !important;}
  #search_ref_name{max-width: 30% !important;}
  #search_page_id{width: 150px !important;}
  @media (max-width: 575.98px) {
    #search_page_id{width: 130px !important;}
    #search_ref_name{max-width: 77% !important;}
  }
</style>

<div class="content-header row">
    <div class="content-header-left col-12 mb-2 mt-1">
        <div class="breadcrumbs-top">
            <h5 class="content-header-title float-left pr-1 mb-0"><?php echo $page_title; ?></h5>
            <div class="breadcrumb-wrapper d-none d-sm-block">
                <ol class="breadcrumb p-0 mb-0 pl-1">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard"><i class="bx bx-home-alt"></i></a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url("messenger_bot"); ?>"><?php echo $this->lang->line("Messenger Bot"); ?></a></li>
                    <li class="breadcrumb-item active"><?php echo $page_title; ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <a href="<?php echo base_url("messenger_bot_enhancers/customer_chat_add");?>" class="btn btn-primary mb-1">
            <i class="bx bx-plus-circle"></i> <?php echo $this->lang->line("Create Plugin"); ?>
        </a>
    </div>
</div>

  <div class="section-body">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-body data-card">
            <div class="row">
              <div class="col-md-9 col-12">
                <div class="input-group mb-3 float-left" id="searchbox">


                  <!-- search by page name -->
                  <div class="input-group-prepend">
                    <?php $page_info['']=$this->lang->line("All Pages"); ?>
                    <?php echo form_dropdown('search_page_id', $page_info,'','id="search_page_id"  class="select2 form-control"'); ?>

                  </div>
                  <input type="text" class="form-control" id="search_ref_name" name="search_ref_name" autofocus placeholder="<?php echo $this->lang->line('Search...'); ?>" aria-label="" aria-describedby="basic-addon2">
                  <div class="input-group-append">
                    <button class="btn btn-primary" id="search_submit" title="<?php echo $this->lang->line('Search'); ?>" type="button"><i class="bx bx-search"></i> <span class="d-none d-sm-inline"><?php echo $this->lang->line('Search'); ?></span></button>
                  </div>
                </div>
              </div>
            </div>
            <div class="table-responsive2">
              <table class="table table-bordered" id="mytable">
                <thead>
                  <tr>
                    <th>#</th>      
                    <th><?php echo $this->lang->line("Campaign ID"); ?></th>      
                    <th><?php echo $this->lang->line("Domain"); ?></th>      
                    <th><?php echo $this->lang->line("Page"); ?></th>
                    <th><?php echo $this->lang->line("Embed Code"); ?></th>
                    <th><?php echo $this->lang->line("Domain Code"); ?></th>
                    <th><?php echo $this->lang->line("Actions"); ?></th>
                    <th><?php echo $this->lang->line('Created at'); ?></th>
                    <th><?php echo $this->lang->line('Language Code'); ?></th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>             
          </div>
        </div>
      </div>
    </div>

  </div>



<div class="modal fade" role="dialog" id="get_embed_modal">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title"><i class="bx bx-code"></i> <?php echo $this->lang->line('Chat plugin embed code'); ?></h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="bx bx-x"></i>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-12">
            <div class="form-group">
              <label for="description"> <?php echo $this->lang->line("Copy the code below and paste inside body tag or at the very last of your webpage.")?> </label>

              <pre class="language-javascript" ><code id="test" class="dlanguage-javascript description" ></code></pre>

            </div>
          </div>

        </div>
      </div>
      <div class="modal-footer">
        <!-- <button id="copy_js_code" type="button" class="btn btn-primary"><i class="bx bx-cut"></i> <?php echo $this->lang->line('Copy'); ?> -->
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="bx bx-time"></i> <?php echo $this->lang->line('Close'); ?></button>
    </div>
  </div>
</div>
</div>


