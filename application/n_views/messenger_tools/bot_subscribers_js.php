<?php
$doyouwanttodeletethiscontact = $this->lang->line("Do you want to delete this subscriber?");
$youhavenotselected = $this->lang->line("You have not selected any subscriber to assign label. You can choose upto");
$youhavenotselectanysubscribertoassignsequence = $this->lang->line("You have not selected any subscriber to assign sms/email sequence campaign. You can choose upto");
$youhavenotselected2 = $this->lang->line("You have not selected any subscriber to delete.");
$leadsatatime = $this->lang->line("subscribers at a time.");
$youcanselectupto = $this->lang->line("You can select upto");
$leadsyouhaveselected = $this->lang->line(",you have selected");
$leads = $this->lang->line("subscribers.");
$youhavenotselectedany = $this->lang->line("You have not selected any subscriber to delete. you can choose upto");
$youhavenotselectedanyleadtoassigngroup = $this->lang->line("You have not selected any subscriber to assign label.");
$youhavenotselectedanyleadtoassigndripcampaign = $this->lang->line("You have not selected any subscriber to assign sequence campaign.");
$youhavenotselectedanyleadgroup = $this->lang->line("You have not selected any label.");
$youhavenotselectedanysequence = $this->lang->line("You have not selected any sequence campaign.");
$pleasewait = $this->lang->line("Please wait...");
$groupshavebeenassignedsuccessfully = $this->lang->line("Labels have been assigned successfully");
$sequencehavebeenassignedsuccessfully = $this->lang->line("Sequence campaign have been assigned successfully");
$contactshavebeendeletedsuccessfully = $this->lang->line("Subscribers have been deleted successfully");
$somethingwentwrongpleasetryagain = $this->lang->line("Something went wrong, please try again.");
    $ig_bot_exists = addon_exist($module_id=320,$addon_unique_name="instagram_bot") ? '1' : '0';
?>
<link rel="stylesheet" href="<?php echo base_url();?>assets/modules/chocolat/dist/css/chocolat.css?ver=<?php echo $n_config['theme_version']; ?>">
<script src="<?php echo base_url();?>assets/modules/chocolat/dist/js/jquery.chocolat.min.js?ver=<?php echo $n_config['theme_version']; ?>"></script>
<script src="<?php echo base_url(); ?>n_assets/app-assets/vendors/js/forms/select/select2.full.min.js?ver=<?php echo $n_config['theme_version']; ?>"></script>
<script src="<?php echo base_url(); ?>n_assets/app-assets/vendors/js/extensions/moment.min.js?ver=<?php echo $n_config['theme_version']; ?>"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>plugins/datetimepickerjquery/jquery.datetimepicker.js?ver=<?php echo $n_config['theme_version']; ?>"></script>
<script src="<?php echo base_url(); ?>n_assets/app-assets/vendors/js/pickers/daterange/daterangepicker.js?ver=<?php echo $n_config['theme_version']; ?>"></script>
<script src="<?php echo base_url(); ?>n_assets/app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js?ver=<?php echo $n_config['theme_version']; ?>"></script>
<script src="<?php echo base_url(); ?>n_assets/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js?ver=<?php echo $n_config['theme_version']; ?>"></script>
<script src="<?php echo base_url(); ?>n_assets/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js?ver=<?php echo $n_config['theme_version']; ?>"></script>
<script src="<?php echo base_url(); ?>n_assets/app-assets/vendors/js/tables/datatable/buttons.html5.min.js?ver=<?php echo $n_config['theme_version']; ?>"></script>
<script src="<?php echo base_url(); ?>n_assets/app-assets/vendors/js/tables/datatable/buttons.print.min.js?ver=<?php echo $n_config['theme_version']; ?>"></script>
<script src="<?php echo base_url(); ?>n_assets/app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js?ver=<?php echo $n_config['theme_version']; ?>"></script>
<script src="<?php echo base_url(); ?>n_assets/app-assets/vendors/js/tables/datatable/pdfmake.min.js?ver=<?php echo $n_config['theme_version']; ?>"></script>
<script src="<?php echo base_url(); ?>n_assets/app-assets/vendors/js/tables/datatable/vfs_fonts.js?ver=<?php echo $n_config['theme_version']; ?>"></script>
<script src="<?php echo base_url(); ?>n_assets/plugins/summernote/summernote-bs4.js?ver=<?php echo $n_config['theme_version']; ?>"></script>
<script type="text/javascript">
    var areWeUsingScroll = false;
    //TODO: areWeUsingScroll

    $(document).ready(function() {
        'use strict';

        $(".select2").select2({
            dropdownAutoWidth: true,

        });
    });

    var is_webview_exist = "<?php echo $this->is_webview_exist; ?>"
    var base_url="<?php echo base_url();?>";
    var youhavenotselected = "<?php echo $youhavenotselected;?>";
    var youhavenotselectanysubscribertoassignsequence = "<?php echo $youhavenotselectanysubscribertoassignsequence; ?>";
    var youhavenotselected2 = "<?php echo $youhavenotselected2;?>";
    var leadsatatime = "<?php echo $leadsatatime;?>";
    var youcanselectupto = "<?php echo $youcanselectupto;?>";
    var leadsyouhaveselected = "<?php echo $leadsyouhaveselected;?>";
    var leads = "<?php echo $leads;?>";
    var youhavenotselectedanyleadtoassigngroup = "<?php echo $youhavenotselectedanyleadtoassigngroup; ?>";
    var youhavenotselectedanyleadtoassigndripcampaign = "<?php echo $youhavenotselectedanyleadtoassigndripcampaign; ?>";
    var youhavenotselectedanyleadgroup = "<?php echo $youhavenotselectedanyleadgroup; ?>";
    var pleasewait = "<?php echo $pleasewait; ?>";
    var groupshavebeenassignedsuccessfully = "<?php echo $groupshavebeenassignedsuccessfully; ?>";
    var sequencehavebeenassignedsuccessfully = "<?php echo $sequencehavebeenassignedsuccessfully; ?>";
    var contactshavebeendeletedsuccessfully = "<?php echo $contactshavebeendeletedsuccessfully; ?>";
    var auto_selected_page = "<?php echo $auto_selected_page; ?>";
    var auto_selected_subscriber = "<?php echo $auto_selected_subscriber; ?>";
    var youhavenotselectedanysequence = "<?php echo $youhavenotselectedanysequence; ?>";
    var ig_bot_exists = "<?php echo $ig_bot_exists; ?>";

    setTimeout(function(){
        $('#search_date_range').daterangepicker({
            ranges: {
                '<?php echo $this->lang->line("Last 30 Days");?>': [moment().subtract(29, 'days'), moment()],
                '<?php echo $this->lang->line("This Month");?>'  : [moment().startOf('month'), moment().endOf('month')],
                '<?php echo $this->lang->line("Last Month");?>'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            startDate: moment().subtract(29, 'days'),
            endDate  : moment()
        }, function (start, end) {
            $('#search_date_range_val').val(start.format('YYYY-M-D') + '|' + end.format('YYYY-M-D')).change();
        });
    }, 3000);


    $("document").ready(function(){
        var perscroll;
        var table1 = '';
        if(auto_selected_page!='' && auto_selected_page!='0' ) $('#page_id').val(auto_selected_page).trigger('change');

      var hideCol = [4,10,11];
        table1 = $("#mytable").DataTable({
            serverSide: true,
            processing:true,
            bFilter: false,
        order: [[ 12, "desc" ]],
            pageLength: 10,
            ajax: {
                url: base_url+'subscriber_manager/bot_subscribers_data',
                type: 'POST',
                data: function ( d )
                {
                    d.page_id = $('#page_id').val();
                    d.search_value = $('#search_value').val();
                    d.label_id = $('#label_id').val();
                    d.email_phone_birth = $('#email_phone_birth').val();
                    d.gender = $('#gender').val();
                }
            },
            language:
                {
                    url: "<?php echo base_url('assets/modules/datatables/language/'.$this->language.'.json'); ?>"
                },
            dom: '<"top"f>rt<"bottom"lip><"clear">',
            columnDefs: [
                {
              targets: hideCol,
                    visible: false
                },
                {
              targets: [0,2,4,8,9,10,11,12],
                    className: 'text-center'
                },
                {
              targets: [0,1,2,8,10],
                    sortable: false
                },
                {
                    targets: [5,6,7,8],
                    "render": function ( data, type, row ) {
                        data = data.replaceAll('fas fa-user-slash', 'bx bxs-user-x');
                        data = data.replaceAll('fas fa-comment-slash', 'bx bx-comment-x');
                        data = data.replaceAll('fas fa-map', 'bx bx-map');
                        data = data.replaceAll('fas fa-birthday-cake', 'bx bx-cake');
                        data = data.replaceAll('fas fa-headset', 'bx bx-headphone');
                        data = data.replaceAll('fas fa-phone', 'bx bx-phone');
                        data = data.replaceAll('fas fa-robot', 'bx bx-bot');
                        data = data.replaceAll('fas fa-envelope', 'bx bx-envelope');
                        data = data.replaceAll('fas fa-code', 'bx bx-code');
                        data = data.replaceAll('fas fa-edit', 'bx bx-edit');
                        data = data.replaceAll('fa fa-edit', 'bx bx-edit');
                        data = data.replaceAll('fa  fa-edit', 'bx bx-edit');
                        data = data.replaceAll('far fa-copy', 'bx bx-copy');
                        data = data.replaceAll('fa fa-trash', 'bx bx-trash');
                        data = data.replaceAll('fas fa-trash', 'bx bx-trash');
                        data = data.replaceAll('fa fa-eye', 'bx bxs-show');
                        data = data.replaceAll('fas fa-eye', 'bx bxs-show');
                        data = data.replaceAll('fas fa-trash-alt', 'bx bx-trash');
                        data = data.replaceAll('fa fa-wordpress', 'bx bxl-wordpress');
                        data = data.replaceAll('fa fa-briefcase', 'bx bx-briefcase');
                        data = data.replaceAll('fas fa-briefcase', 'bx bx-briefcase');
                        data = data.replaceAll('fab fa-wpforms', 'bx bx-news');
                        data = data.replaceAll('fas fa-file-export', 'bx bx-export');
                        data = data.replaceAll('fa fa-comment', 'bx bx-comment');
                        data = data.replaceAll('fa fa-user', 'bx bx-user');
                        data = data.replaceAll('fa fa-refresh', 'bx bx-refresh');
                        data = data.replaceAll('fa fa-plus-circle', 'bx bx-plus-circle');
                        data = data.replaceAll('fas fa-comments', 'bx bx-comment');
                        data = data.replaceAll('fa fa-hand-o-right', 'bx bx-link-external');
                        data = data.replaceAll('fab fa-facebook-square', 'bx bxl-facebook-square');
                        data = data.replaceAll('fas fa-exchange-alt', 'bx bx-repost');
                        data = data.replaceAll('fa fa-sync-alt', 'bx bx-sync');
                        data = data.replaceAll('fas fa-key', 'bx bx-key');
                        data = data.replaceAll('fas fa-bolt', 'bx bxs-bolt');
                        data = data.replaceAll('fas fa-male', 'bx bx-male')
                        data = data.replaceAll('fas fa-female', 'bx bx-female')
                        data = data.replaceAll('fas fa-clone', 'bx bxs-copy-alt');
                        data = data.replaceAll('fas fa-receipt', 'bx bx-receipt');
                        data = data.replaceAll('fa fa-paper-plane', 'bx bx-paper-plane');
                        data = data.replaceAll('fa fa-send', 'bx bx-send');
                        data = data.replaceAll('fas fa-hand-point-right', 'bx bx-news');
                        data = data.replaceAll('fa fa-code', 'bx bx-code');
                        data = data.replaceAll('fa fa-clone', 'bx bx-duplicate');
                        data = data.replaceAll('fas fa-pause', 'bx bx-pause');
                        data = data.replaceAll('fa fa-cog', 'bx bx-cog');
                        data = data.replaceAll('fa fa-check-circle', 'bx bx-check-circle');
                        data = data.replaceAll('fas fa-comment', 'bx bx-comment');
                        data = data.replaceAll('swal(', 'swal.fire(');
                        data = data.replaceAll('rounded-circle', 'rounded-circle');
                        data = data.replaceAll('fas fa-check-circle', 'bx bx-check-circle');
                        data = data.replaceAll('fas fa-plug', 'bx bx-plug');
                        data = data.replaceAll('fas fa-times-circle', 'bx bx-time');
                        data = data.replaceAll('fas fa-chart-bar', 'bx bx-chart');
                        data = data.replaceAll('fas fa-cloud-download-alt', 'bx bxs-cloud-download');
                        data = data.replaceAll('padding-10', 'p-10');
                        data = data.replaceAll('padding-left-10', 'pl-10');
                        data = data.replaceAll('h4', 'h5 class="card-title font-medium-1"');
                        data = data.replaceAll('fas fa-heart', 'bx bx-heart');
                        data = data.replaceAll('fas fa-road', 'bx bx-location-plus');
                        data = data.replaceAll('fas fa-city', 'bx bxs-city');
                        data = data.replaceAll('fas fa-globe-americas', 'bx bx-globe');
                        data = data.replaceAll('fas fa-at', 'bx bx-at');
                        data = data.replaceAll('fas fa-mobile-alt', 'bx bx-mobile-alt');
                        data = data.replaceAll('<div class="dropdown-title">Options</div>', '<h6 class="dropdown-header">Options</h6>');
                        data = data.replaceAll('fas fa-file-signature', 'bx bxs-user-badge');
                        data = data.replaceAll('fas fa-user-circle', 'bx bxs-user');
                        data = data.replaceAll('fas fa-toggle-on', 'bx bx-toggle-right');
                        data = data.replaceAll('fas fa-toggle-off', 'bx bx-toggle-left');
                        data = data.replaceAll('fas fa-info-circle', 'bx bx-info-circle');
                        data = data.replaceAll('208px', '308px');
                        data = data.replaceAll("data-toggle='tooltip'", " data-html='true' data-toggle='tooltip'");

                        return data;
                    }
                },
                {
                    targets: [2],
                    "render": function (data, type, row) {
                        data = data.replaceAll('<img ', '<img onerror="this.onerror=null;" ');
                        data = data.replaceAll('null;', "null;this.src='<?php echo base_url('assets/img/avatar/avatar-1.png'); ?>'");
                        return data;
                    }
                }
            ],
            fnInitComplete:function(){  // when initialization is completed then apply scroll plugin
                if(areWeUsingScroll)
                {
                    if (perscroll) perscroll.destroy();
                    perscroll = new PerfectScrollbar('#mytable_wrapper .dataTables_scrollBody');
                }
            },
            scrollX: 'auto',
            fnDrawCallback: function( oSettings ) { //on paginition page 2,3.. often scroll shown, so reset it and assign it again
                if(areWeUsingScroll)
                {
                    if (perscroll) perscroll.destroy();
                    perscroll = new PerfectScrollbar('#mytable_wrapper .dataTables_scrollBody');
                }
            }
        });

        $(document).on('change', '#page_id', function(e) {
            var page_id =$(this).val();
            $.ajax({
                context: this,
                type:'POST',
                url:"<?php echo site_url();?>subscriber_manager/get_label_dropdown",
                data:{page_id:page_id},
                success:function(response){
                    $("#label_dropdown").html(response);
                    table1.draw(false);
                }
            });
        });

        if(auto_selected_subscriber!='' && auto_selected_subscriber!='0')
        {
            $("#search_value").val(auto_selected_subscriber);
            $("#search_subscriber").click();
        }

        $(document).on('click', '#search_subscriber', function(e) {
            e.preventDefault();
            table1.draw(false);
        });

        $(document).on('change', '#label_id', function(e) {
            table1.draw(false);
        });

        $(document).on('change', '#gender', function(e) {
            table1.draw(false);
        });

        $(document).on('change', '#email_phone_birth', function(e) {
            table1.draw(false);
        });


        $(document).on('click','#assign_group',function(e){
            e.preventDefault();
            var upto = 500;
            var selected_page=$("#page_id").val(); // database id
            var ids = [];
            $(".datatableCheckboxRow:checked").each(function ()
            {
                ids.push(parseInt($(this).val()));
            });
            var selected = ids.length;

            if(selected_page=="")
            {
                swal.fire('<?php echo $this->lang->line("Warning") ?>',"<?php echo $this->lang->line('To assign labels in bulk you have to search by any page first.');?>", 'warning');
                return;
            }

            if(ids=="")
            {
                swal.fire('<?php echo $this->lang->line("Warning") ?>', youhavenotselected+" "+upto+" "+leadsatatime, 'warning');
                return;
            }
            if(selected>upto)
            {
                swal.fire('<?php echo $this->lang->line("Warning") ?>',youcanselectupto+" "+upto+" "+leadsyouhaveselected+" "+selected+" "+leads, 'warning');
                return;
            }

            $.ajax({
                type:'POST' ,
                url: "<?php echo site_url(); ?>subscriber_manager/get_label_dropdown_multiple",
                data:{selected_page:selected_page},
                success:function(response)
                {
                    $("#get_labels").html(response);
                }
            });

            $("#assign_group_modal").modal();
        });

        $(document).on('click', '#assign_sms_email_sequence', function(event) {
            event.preventDefault();
            var upto = 500;
            var selected_page=$("#page_id").val();
            var ids = [];

            $(".datatableCheckboxRow:checked").each(function ()
            {
                ids.push(parseInt($(this).val()));
            });
            var selected = ids.length;

            if(selected_page=="")
            {
                swal.fire('<?php echo $this->lang->line("Warning") ?>',"<?php echo $this->lang->line('To assign sequence in bulk you have to search by any page first.');?>", 'warning');
                return;
            }

            if(ids=="")
            {
                swal.fire('<?php echo $this->lang->line("Warning") ?>', youhavenotselectanysubscribertoassignsequence+" "+upto+" "+leadsatatime, 'warning');
                return;
            }
            if(selected>upto)
            {
                swal.fire('<?php echo $this->lang->line("Warning") ?>',youcanselectupto+" "+upto+" "+leadsyouhaveselected+" "+selected+" "+leads, 'warning');
                return;
            }

            $.ajax({
                type:'POST' ,
                url: "<?php echo site_url(); ?>subscriber_manager/get_sequence_campaigns",
                data:{selected_page:selected_page},
                success:function(response)
                {
                    $("#sequence_campaigns").html(response);
                }
            });

            $("#assign_sqeuence_campaign_modal").modal();

        });

        $(document).on('click','#assign_group_submit',function(e){
            e.preventDefault();
            swal.fire({
                title: '<?php echo $this->lang->line("Assign Label"); ?>',
                text: '<?php echo $this->lang->line("Do you really want to assign selected labels to your selected subscribers? Please be noted that bulk assigning labels will replace subscribers previous labels if any."); ?>',
                icon: 'warning',
                    confirmButtonText: "<?php echo $this->lang->line('Yes'); ?>",
    cancelButtonText: "<?php echo $this->lang->line('No'); ?>",
    showCancelButton: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete.isConfirmed)
                    {
                        var ids = [];
                        $(".datatableCheckboxRow:checked").each(function ()
                        {
                            ids.push(parseInt($(this).val()));
                        });
                        var selected = ids.length;


                        if(ids=="")
                        {
                            swal.fire('<?php echo $this->lang->line("Warning") ?>', youhavenotselected+" "+upto+" "+leadsatatime, 'warning');
                            return;
                        }

                        var group_id=$("#label_ids").val();
                        var page_id=$("#page_id").val();
                        var count=group_id.length;

                        if(count==0)
                        {
                            swal.fire('<?php echo $this->lang->line("Error") ?>', youhavenotselectedanyleadgroup, 'error');
                            return;
                        }

                        $("#assign_group_submit").addClass("btn-progress");

                        $.ajax({
                            type:'POST' ,
                            url: "<?php echo site_url(); ?>subscriber_manager/bulk_group_assign",
                            data:{ids:ids,group_id:group_id,page_id:page_id},
                            success:function(response)
                            {
                                $("#assign_group_submit").removeClass("btn-progress");
                                swal.fire('<?php echo $this->lang->line("Label Assign") ?>', groupshavebeenassignedsuccessfully+" ("+selected+")", 'success')
                                    .then((value) => {
                                        $("#assign_group_modal").modal('hide');
                                        table1.draw(false);
                                    });

                            }
                        });
                    }
                });
        });

        $(document).on('click','#assign_sequence_submit',function(e){
            e.preventDefault();

            var ids = [];
            $(".datatableCheckboxRow:checked").each(function ()
            {
                ids.push(parseInt($(this).val()));
            });
            var selected = ids.length;

            if(ids=="")
            {
                swal.fire('<?php echo $this->lang->line("Warning") ?>', youhavenotselectanysubscribertoassignsequence+" "+upto+" "+leadsatatime, 'warning');
                return;
            }

            var sequence_id = $("#sequence_ids").val();
            var page_id = $("#page_id").val();
            var count = sequence_id.length;

            if(count==0)
            {
                swal.fire('<?php echo $this->lang->line("Error") ?>', youhavenotselectedanysequence, 'error');
                return;
            }

            $("#assign_sequence_submit").addClass("btn-progress");

            $.ajax({
                type:'POST' ,
                url: "<?php echo site_url(); ?>subscriber_manager/bulk_sequence_campaign_assign",
                data:{ids:ids,sequence_id:sequence_id,page_id:page_id},
                success:function(response)
                {
                    $("#assign_sequence_submit").removeClass("btn-progress");
                    swal.fire('<?php echo $this->lang->line("Sequence Campaign Assign") ?>', sequencehavebeenassignedsuccessfully+" ("+selected+")", 'success')
                        .then((value) => {
                            $("#assign_sqeuence_campaign_modal").modal('hide');
                            table1.draw(false);
                        });

                }
            });

        });

        $(document).on('click','#bulk_delete_contact',function(e){
            e.preventDefault();
            var ids = [];
            var page_id=$("#page_id").val();

            $(".datatableCheckboxRow:checked").each(function ()
            {
                ids.push(parseInt($(this).val()));
            });
            var selected = ids.length;

            if(page_id=="")
            {
                swal.fire('<?php echo $this->lang->line("Warning") ?>',"<?php echo $this->lang->line('To delete subscribers in bulk you have to search by any page first.');?>", 'warning');
                return;
            }
            if(ids=="")
            {
                swal.fire('<?php echo $this->lang->line("Warning") ?>', youhavenotselected2, 'warning');
                return;
            }

            swal.fire({
                title: '<?php echo $this->lang->line("Delete Subscribers"); ?>',
                text: '<?php echo $this->lang->line("Do you really want to delete selected subscribers?"); ?>',
                icon: 'error',
                    confirmButtonText: "<?php echo $this->lang->line('Yes'); ?>",
    cancelButtonText: "<?php echo $this->lang->line('No'); ?>",
    showCancelButton: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete.isConfirmed)
                    {
                        $.ajax({
                            type:'POST' ,
                            url: "<?php echo site_url(); ?>subscriber_manager/delete_bulk_subscriber",
                            data:{ids:ids,page_id:page_id},
                            success:function(response)
                            {
                                swal.fire('<?php echo $this->lang->line("Delete Subscribers") ?>', contactshavebeendeletedsuccessfully+" ("+selected+")", 'success')
                                    .then((value) => {
                                        table1.draw(false);
                                    });

                            }
                        });
                    }
                });
        });


        $(document).on('click','.subscriber_actions_modal',function(e){
            e.preventDefault();

            var id=$(this).attr('data-id');
            var subscribe_id=$(this).attr('data-subscribe-id');
            var page_id=$(this).attr('data-page-id'); // auto id
            $("#search_subscriber_id").val(subscribe_id);

          var social_media = 'ig';
          if (page_id.indexOf('fb') > -1) social_media = 'fb';

            $("#subscriber_actions_modal").modal();
            get_subscriber_action_content(id,subscribe_id,page_id);
            var user_input_flow_exist = "<?php echo $user_input_flow_exist; ?>";
          if(user_input_flow_exist == 'yes' && social_media=='fb')
            {
                get_subscriber_flowdata(id,subscribe_id,page_id);
                get_subscriber_customfields(id,subscribe_id,page_id);
            }
          else
          {
            $("#flowanswers-tab,#customfields-tab,#formdata-tab").hide();
          }

          if(is_webview_exist && social_media=='fb') get_subscriber_formdata(id,subscribe_id,page_id);

            $("#default-tab").click();
        });


        $(document).on('click','.update_user_details',function(e){
            e.preventDefault();
            $(this).addClass('btn-progress');
            $("#save_changes").addClass("btn-progress");
            var post_value = $(this).attr('button_id');
        var social_media = $(this).attr('social_media');

            var exp=[];
            exp=post_value.split("-");
            var id=exp[0];
            var subscribe_id=exp[1];
            var page_id=exp[2];

        var page_id_media = page_id+"-"+social_media;

            $.ajax({
                context: this,
                type:'POST',
                dataType:'JSON',
                url:"<?php echo site_url();?>subscriber_manager/sync_subscriber_data",
          data:{post_value:post_value,social_media:social_media},
                success:function(response){
                    $(this).removeClass('btn-progress');
                    $("#save_changes").removeClass('btn-progress');
                    if(response.status=='1')   iziToast.success({title: '',message: response.message,position: 'bottomRight'});
                    else  iziToast.error({title: '',message: response.message,position: 'bottomRight'});
             get_subscriber_action_content(id,subscribe_id,page_id_media);
                }
            });

        });

        $(document).on('click','.reset_user_input_flow',function(e){
            e.preventDefault();
            $(this).addClass('btn-progress');
            $("#save_changes").addClass("btn-progress");
            var post_value = $(this).attr('button_id');
        var social_media = $(this).attr('social_media');

            var exp=[];
            exp=post_value.split("-");
            var id=exp[0];
            var subscribe_id=exp[1];
            var page_id=exp[2];

        var page_id_media = page_id+"-"+social_media;

            $.ajax({
                context: this,
                type:'POST',
                dataType:'JSON',
                url:"<?php echo site_url();?>subscriber_manager/reset_user_input_flow",
          data:{post_value:post_value,social_media:social_media},
                success:function(response){
                    $(this).removeClass('btn-progress');
                    $("#save_changes").removeClass('btn-progress');
                    if(response.status=='1')   iziToast.success({title: '',message: response.message,position: 'bottomRight'});
                    else  iziToast.error({title: '',message: response.message,position: 'bottomRight'});
             get_subscriber_action_content(id,subscribe_id,page_id_media);
                }
            });

        });

        $(document).on('click','.delete_user_details',function(e){
            e.preventDefault();

            swal.fire({
                title: '<?php echo $this->lang->line("Delete Subscriber"); ?>',
                text: '<?php echo $this->lang->line("Do you really want to delete this subscriber?"); ?>',
                icon: 'warning',
                    confirmButtonText: "<?php echo $this->lang->line('Yes'); ?>",
    cancelButtonText: "<?php echo $this->lang->line('No'); ?>",
    showCancelButton: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete.isConfirmed)
                    {
                        $(this).addClass('btn-progress');
                        $("#save_changes").addClass("btn-progress");
                        var post_value = $(this).attr('button_id');
               var social_media = $(this).attr('social_media');
                        $.ajax({
                            context: this,
                            type:'POST',
                            dataType:'JSON',
                            url:"<?php echo site_url();?>subscriber_manager/delete_subsriber",
                 data:{post_value:post_value,social_media:social_media},
                            success:function(response){
                                $(this).removeClass('btn-progress');
                                $("#save_changes").removeClass('btn-progress');
                                $("#subscriber_actions_modal").modal('hide');
                                iziToast.success({title: '',message: '<?php echo $this->lang->line("Subscriber has been deleted successfully."); ?>',position: 'bottomRight'});
                            }
                        });
                    }
                });

        });

        function get_subscriber_flowdata(id,subscribe_id,page_id)
        {
            $(".flowanswers_div").html('<div class="text-center waiting"><i class="bx bx-loader-alt bx-spin blue text-center"></i></div>');

            $.ajax({
                type:'POST' ,
                url: "<?php echo site_url(); ?>subscriber_manager/get_subscriber_inputflow_data",
                data:{id:id,page_id:page_id,subscribe_id:subscribe_id},
                success:function(response)
                {
                    $(".flowanswers_div").html(response);
                }
            });
        }

        function get_subscriber_customfields(id,subscribe_id,page_id)
        {
            $(".customfields_div").html('<div class="text-center waiting"><i class="bx bx-loader-alt bx-spin blue text-center"></i></div>');

            $.ajax({
                type:'POST' ,
                url: "<?php echo site_url(); ?>subscriber_manager/get_subscriber_customfields_data",
                data:{id:id,page_id:page_id,subscribe_id:subscribe_id},
                success:function(response)
                {
                    $(".customfields_div").html(response);
                }
            });
        }

        function get_subscriber_formdata(id,subscribe_id,page_id)
        {
            $(".formdata_div").html('<div class="text-center waiting"><i class="bx bx-loader-alt bx-spin blue text-center"></i></div>');

            $.ajax({
                type:'POST' ,
                url: "<?php echo site_url(); ?>subscriber_manager/get_subscriber_formdata",
                data:{id:id,page_id:page_id,subscribe_id:subscribe_id},
                success:function(response)
                {
                    $(".formdata_div").html(response);
                }
            });
        }

        var table2 = '';

        $(document).on('change', '#search_status', function(e) {
            table2.draw();
        });

        $(document).on('change', '#search_date_range_val', function(e) {
            e.preventDefault();
            table2.draw();
        });

        $(document).on('keypress', '#search_value2', function(e) {
            if(e.which == 13) $("#search_action").click();
        });

        $(document).on('click', '#search_action', function(event) {
            event.preventDefault();
            table2.draw();
        });

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var id = $(this).attr('id');
            if(id=='purchase-tab' ) setTimeout(function(){ get_purchase_data(); }, 1000);
        });


        function get_purchase_data()
        {
            var perscroll2;
            if (table2 == '')
            {
                table2 = $("#mytable2").DataTable({
                    serverSide: true,
                    processing:true,
                    bFilter: false,
                    order: [[ 10, "desc" ]],
                    pageLength: 10,
                    ajax: {
                        url: base_url+'subscriber_manager/my_orders_data',
                        type: 'POST',
                        data: function ( d )
                        {
                            d.search_subscriber_id = $('#search_subscriber_id').val();
                            d.search_status = $('#search_status').val();
                            d.search_value = $('#search_value2').val();
                            d.search_date_range = $('#search_date_range_val').val();
                        }
                    },
                    language:
                        {
                            url: "<?php echo base_url('assets/modules/datatables/language/'.$this->language.'.json'); ?>"
                        },
                    dom: '<"top"f>rt<"bottom"lip><"clear">',
                    columnDefs: [
                        {
                            targets: [1,3,6,7,11],
                            visible: false
                        },
                        {
                            targets: [5,7,8,9,10,11],
                            className: 'text-center'
                        },
                        {
                            targets: [3,4],
                            className: 'text-right'
                        },
                        {
                            targets: [2,8,9],
                            sortable: false
                        },
                        {
                            targets: [5,6,7,8,9],
                            "render": function ( data, type, row ) {
                                data = data.replaceAll('fas fa-user-slash', 'bx bxs-user-x');
                                data = data.replaceAll('fas fa-comment-slash', 'bx bx-comment-x');
                                data = data.replaceAll('fas fa-map', 'bx bx-map');
                                data = data.replaceAll('fas fa-birthday-cake', 'bx bx-cake');
                                data = data.replaceAll('fas fa-headset', 'bx bx-headphone');
                                data = data.replaceAll('fas fa-phone', 'bx bx-phone');
                                data = data.replaceAll('fas fa-robot', 'bx bx-bot');
                                data = data.replaceAll('fas fa-envelope', 'bx bx-envelope');
                                data = data.replaceAll('fas fa-code', 'bx bx-code');
                                data = data.replaceAll('fas fa-edit', 'bx bx-edit');
                                data = data.replaceAll('fa fa-edit', 'bx bx-edit');
                                data = data.replaceAll('fa  fa-edit', 'bx bx-edit');
                                data = data.replaceAll('far fa-copy', 'bx bx-copy');
                                data = data.replaceAll('fa fa-trash', 'bx bx-trash');
                                data = data.replaceAll('fas fa-trash', 'bx bx-trash');
                                data = data.replaceAll('fa fa-eye', 'bx bxs-show');
                                data = data.replaceAll('fas fa-eye', 'bx bxs-show');
                                data = data.replaceAll('fas fa-trash-alt', 'bx bx-trash');
                                data = data.replaceAll('fa fa-wordpress', 'bx bxl-wordpress');
                                data = data.replaceAll('fa fa-briefcase', 'bx bx-briefcase');
                                data = data.replaceAll('fas fa-briefcase', 'bx bx-briefcase');
                                data = data.replaceAll('fab fa-wpforms', 'bx bx-news');
                                data = data.replaceAll('fas fa-file-export', 'bx bx-export');
                                data = data.replaceAll('fa fa-comment', 'bx bx-comment');
                                data = data.replaceAll('fa fa-user', 'bx bx-user');
                                data = data.replaceAll('fa fa-refresh', 'bx bx-refresh');
                                data = data.replaceAll('fa fa-plus-circle', 'bx bx-plus-circle');
                                data = data.replaceAll('fas fa-comments', 'bx bx-comment');
                                data = data.replaceAll('fa fa-hand-o-right', 'bx bx-link-external');
                                data = data.replaceAll('fab fa-facebook-square', 'bx bxl-facebook-square');
                                data = data.replaceAll('fas fa-exchange-alt', 'bx bx-repost');
                                data = data.replaceAll('fa fa-sync-alt', 'bx bx-sync');
                                data = data.replaceAll('fas fa-key', 'bx bx-key');
                                data = data.replaceAll('fas fa-bolt', 'bx bxs-bolt');
                                data = data.replaceAll('fas fa-male', 'bx bx-male')
                                data = data.replaceAll('fas fa-female', 'bx bx-female')
                                data = data.replaceAll('fas fa-clone', 'bx bxs-copy-alt');
                                data = data.replaceAll('fas fa-receipt', 'bx bx-receipt');
                                data = data.replaceAll('fa fa-paper-plane', 'bx bx-paper-plane');
                                data = data.replaceAll('fa fa-send', 'bx bx-send');
                                data = data.replaceAll('fas fa-hand-point-right', 'bx bx-news');
                                data = data.replaceAll('fa fa-code', 'bx bx-code');
                                data = data.replaceAll('fa fa-clone', 'bx bx-duplicate');
                                data = data.replaceAll('fas fa-pause', 'bx bx-pause');
                                data = data.replaceAll('fa fa-cog', 'bx bx-cog');
                                data = data.replaceAll('fa fa-check-circle', 'bx bx-check-circle');
                                data = data.replaceAll('fas fa-comment', 'bx bx-comment');
                                data = data.replaceAll('swal(', 'swal.fire(');
                                data = data.replaceAll('rounded-circle', 'rounded-circle');
                                data = data.replaceAll('fas fa-check-circle', 'bx bx-check-circle');
                                data = data.replaceAll('fas fa-plug', 'bx bx-plug');
                                data = data.replaceAll('fas fa-times-circle', 'bx bx-time');
                                data = data.replaceAll('fas fa-chart-bar', 'bx bx-chart');
                                data = data.replaceAll('fas fa-cloud-download-alt', 'bx bxs-cloud-download');
                                data = data.replaceAll('padding-10', 'p-10');
                                data = data.replaceAll('padding-left-10', 'pl-10');
                                data = data.replaceAll('h4', 'h5 class="card-title font-medium-1"');
                                data = data.replaceAll('fas fa-heart', 'bx bx-heart');
                                data = data.replaceAll('fas fa-road', 'bx bx-location-plus');
                                data = data.replaceAll('fas fa-city', 'bx bxs-city');
                                data = data.replaceAll('fas fa-globe-americas', 'bx bx-globe');
                                data = data.replaceAll('fas fa-at', 'bx bx-at');
                                data = data.replaceAll('fas fa-mobile-alt', 'bx bx-mobile-alt');
                                data = data.replaceAll('<div class="dropdown-title">Options</div>', '<h6 class="dropdown-header">Options</h6>');
                                data = data.replaceAll('fas fa-file-signature', 'bx bxs-user-badge');
                                data = data.replaceAll('fas fa-user-circle', 'bx bxs-user');
                                data = data.replaceAll('fas fa-toggle-on', 'bx bx-toggle-right');
                                data = data.replaceAll('fas fa-toggle-off', 'bx bx-toggle-left');
                                data = data.replaceAll('fas fa-info-circle', 'bx bx-info-circle');
                                data = data.replaceAll('fa fa-image', 'bx bx-image');
                                data = data.replaceAll('208px', '308px');
                                data = data.replaceAll("data-toggle='tooltip'", " data-html='true' data-toggle='tooltip'");

                                return data;
                            }
                        }
                    ],
                    fnInitComplete:function(){  // when initialization is completed then apply scroll plugin
                        if(areWeUsingScroll)
                        {
                            if (perscroll2) perscroll2.destroy();
                            perscroll2 = new PerfectScrollbar('#mytable2_wrapper .dataTables_scrollBody');
                        }
                    },
                    scrollX: 'auto',
                    fnDrawCallback: function( oSettings ) { //on paginition page 2,3.. often scroll shown, so reset it and assign it again
                        if(areWeUsingScroll)
                        {
                            if (perscroll2) perscroll2.destroy();
                            perscroll2 = new PerfectScrollbar('#mytable2_wrapper .dataTables_scrollBody');
                        }
                    },
                    "drawCallback": function (settings) {
                        $('table [data-toggle="tooltip"]').tooltip('dispose');
                        $('table [data-toggle="tooltip"]').tooltip(
                            {
                                placement: 'left',
                                container: 'body',
                                html: true,
                                template: '<div class="tooltip tooltip_pd" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
                            }
                        );
                    }
                });
            }
            else table2.draw();
        }

        $('#subscriber_actions_modal').on('hidden.bs.modal', function () {
            table1.draw(false);
        });
        $('#subscriber_actions_modal').on('shown.bs.modal', function() {
            $(document).off('focusin.modal');
        });



    });

</script>

<?php include(FCPATH.'application/n_views/messenger_tools/subscriber_actions_common_js.php');?>