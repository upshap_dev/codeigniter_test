<div class="content-header row">
    <div class="content-header-left col-12 mb-2 mt-1">
        <div class="breadcrumbs-top">
            <h5 class="content-header-title float-left pr-1 mb-0"><?php echo $page_title; ?></h5>
            <div class="breadcrumb-wrapper d-none d-sm-block">
                <ol class="breadcrumb p-0 mb-0 pl-1">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard"><i class="bx bx-home-alt"></i></a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url("subscriber_manager"); ?>"><?php echo $this->lang->line("Subscriber Manager"); ?></a></li>
                    <li class="breadcrumb-item active"><?php echo $page_title; ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>

<?php
//todo: modals
//todo: fix datatable
?>
  <?php $this->load->view('admin/theme/message'); ?>

  <style type="text/css">
    #page_id{width: 120px;}
    #label_id{width: 100px;}
    .bbw{border-bottom-width: thin !important;border-bottom:solid .5px #f9f9f9 !important;padding-bottom:20px;}
    @media (max-width: 575.98px) {
      #page_id{width: 80px;}
      #label_id{width: 80px;}
    }
    .flex-column .nav-item .nav-link.active
    {
      background: #fff !important;
      color: #3516df !important;
      border: 1px solid #988be1 !important;
    }

    .flex-column .nav-item .nav-link .form_id, .flex-column .nav-item .nav-link .insert_date
    {
      color: #608683 !important;
      font-size: 12px !important;
      padding: 0 !important;
      margin: 0 !important;
    }
    .waiting {height: 100%;width:100%;display: table;}
   .waiting i{font-size:60px;display: table-cell; vertical-align: middle;padding:30px 0;}
  </style>

  <div class="section-body">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-body data-card">
            <div class="row">
              <div class="col-12 col-md-10">
                <?php 

                echo 
                '<div class="input-group mb-3" id="searchbox">
                  <div class="input-group-prepend">
                   '.$page_dropdown.'
                  </div>
                  <div class="input-group-prepend" id="label_dropdown">
                  </div>

                  <div class="input-group-prepend">
                    <select class="select2 form-control" id="gender" name="gender">
                      <option value="">'.$this->lang->line("Gender").'</option>
                      <option value="male">'.$this->lang->line("Male").'</option>
                      <option value="female">'.$this->lang->line("Female").'</option>
                    </select>
                  </div>

                  <select class="select2 form-control" id="email_phone_birth" name="email_phone_birth[]" multiple="multiple" style="max-width:40%;">
                    <option value="has_phone">'.$this->lang->line("Has Phone").'</option>
                    <option value="has_email">'.$this->lang->line("Has Email").'</option>
                    <option value="has_birthdate">'.$this->lang->line("Has Birth Date").'</option>
                  </select>

                  <input type="text" class="form-control" autofocus id="search_value" name="search_value" placeholder="'.$this->lang->line("Search...").'" style="max-width:30%;">
                  <div class="input-group-append">
                    <button class="btn btn-primary" type="button" id="search_subscriber"><i class="bx bx-search"></i> <span class="d-none d-sm-inline">'.$this->lang->line("Search").'</span></button>
                  </div>
                </div>'; ?>                                          
              </div>

              <div class="col-12 col-md-2">

                <div class="btn-group dropleft float-right">
                  <button type="button" class="btn btn-primary float-right has-icon-left btn-icon dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <?php echo $this->lang->line("Options"); ?>
                  </button>
                  <div class="dropdown-menu dropleft">
                    <a class="dropdown-item" href="<?php echo base_url('subscriber_manager/download_result'); ?>"><i class="bx bx-cloud-download pr-1"></i> <?php echo $this->lang->line("Download Result"); ?></a>
                    <a class="dropdown-item" id="assign_group" href=""><i class="bx bx-tag pr-1"></i> <?php echo $this->lang->line("Assign Label"); ?></a>

                    <?php if($this->sms_email_drip_exist) : ?>
                      <?php if($this->session->userdata('user_type') == 'Admin' || count(array_intersect($this->module_access, array(270,271))) > 0 ) :  ?>
                        <a class="dropdown-item" id="assign_sms_email_sequence" href=""><i class="bx bx-plug pr-1"></i> <?php echo $this->lang->line("Assign Sequence"); ?></a>
                      <?php endif; ?>
                    <?php endif; ?>

                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item red" id="bulk_delete_contact" href=""><i class="bx bx-trash pr-1"></i> <?php echo $this->lang->line("Delete Subscriber"); ?></a>
                  </div>
                </div>                          
              </div>
            </div>

            <div class="table-responsive2">
                <input type="hidden" id="put_page_id">
                <table class="table table-bordered" id="mytable">
                  <thead>
                    <tr>
                      <th>#</th>      
                      <th style="vertical-align:middle;width:20px">
                          <input class="regular-checkbox" id="datatableSelectAllRows" type="checkbox"/><label for="datatableSelectAllRows"></label>        
                      </th>
                      <th><?php echo $this->lang->line("Avatar"); ?></th>      
                      <th><?php echo addon_exist($module_id=320,$addon_unique_name="instagram_bot") ? $this->lang->line("Page/Account") : $this->lang->line("Page Name"); ?></th>
                      <th><?php echo $this->lang->line("Subscriber ID"); ?></th>      
                      <th><?php echo $this->lang->line("First Name"); ?></th>      
                      <th><?php echo $this->lang->line("Last Name"); ?></th>      
                      <th><?php echo $this->lang->line("Full Name"); ?></th>
                      <th><?php echo $this->lang->line("Actions"); ?></th>
                      <th><?php echo $this->lang->line("Quick Info"); ?></th>      
                      <th><?php echo $this->lang->line("Label/Tag"); ?></th>      
                      <th><?php echo $this->lang->line("Thread ID"); ?></th>      
                      <th><?php echo $this->lang->line("Synced at"); ?></th>
                      <th><?php echo $this->lang->line("Social Media"); ?></th>
                    </tr>
                  </thead>
                </table>
            </div>
          </div>
        </div>
      </div>       
        
    </div>
  </div>          







<style type="text/css">
  .multi_layout{margin:0;background: #fff}
  .multi_layout .card{margin-bottom:0;border-radius: 0;}
  .multi_layout{border:1px solid #dee2e6;border-top-width: 0;}
  .multi_layout .collef{padding-left: 0px; padding-right: 0px;border-right: 1px solid #dee2e6;}
  .multi_layout .colmid{padding-left: 0px; padding-right: 0px;}
  .multi_layout .card-statistic-1{border:1px solid #dee2e6;border-radius: 4px;}
  .multi_layout .main_card{min-height: 100%;}
  .multi_layout h6.page_name{font-size: 14px;}
  .multi_layout .card .card-header input{max-width: 100% !important;}
  .multi_layout .card .card-header h4 a{font-weight: 700 !important;}
  .multi_layout .card-primary{margin-top: 35px;margin-bottom: 15px;}
  .multi_layout .product-details .product-name{font-size: 12px;}
  .multi_layout .margin-top-50 {margin-top: 70px;}
  .multi_layout .waiting {height: 100%;width:100%;display: table;}
  .multi_layout .waiting i{font-size:60px;display: table-cell; vertical-align: middle;padding:30px 0;}
  .multi_layout .collef .bgimage{border-radius:5px;height: 250px;background-position: 50% 50%; background-size: cover;min-width: 140px;background-repeat:no-repeat;display: block;}
  .multi_layout .collef .subscriber_details{padding-right: 20px;}
  .multi_layout .colmid .section-title{padding-bottom: 10px;}
  .tab-content > .tab-pane{padding:0;}
   @media (max-width: 575.98px) {
      .multi_layout .collef{border-right: none !important;}
    }
</style>

<div class="modal fade" id="assign_group_modal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <h3 class="modal-title"><i class="bx bx-tag"></i> <?php echo $this->lang->line("Assign Label");?></h3>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i class="bx bx-x"></i>
              </button>
            </div>

            <div class="modal-body">    
                <div id="get_labels">              
                </div>
            </div>
            <div class="modal-footer">
              <a class="btn btn-primary float-left" href="" id="assign_group_submit"><i class="bx bx-tag"></i> <?php echo $this->lang->line("Assign Label") ?></a>
              <a class="btn btn-outline-secondary float-right" data-dismiss="modal"><i class="bx bx-time"></i> <?php echo $this->lang->line("Close") ?></a>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="assign_sqeuence_campaign_modal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" style='min-width:40%;'>
        <div class="modal-content">
            <div class="modal-header bbw">
              <h3 class="modal-title"><i class="bx bx-sort-numeric-up"></i> <?php echo $this->lang->line("Assign sms/email Sequence");?></h3>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i class="bx bx-x"></i>
              </button>
            </div>
            
            <div class="modal-body">   
              <div class="text-center" style="padding:20px;margin-bottom:20px;border:.5px solid #dee2e6; color:#6777ef;background: #fff;"><?php echo $this->lang->line("Bulk sequence assign is available for Email & SMS cmapaign. For Messenger, bulk campaign isn't available due to safety & avoiding breaking 24 Hours policy. "); ?></div>
              <div id="sequence_campaigns"></div>
            </div>
            <div class="modal-footer bg-whitesmoke">
              <a class="btn btn-primary float-left" href="" id="assign_sequence_submit"><i class="bx bxs-save"></i> <?php echo $this->lang->line("Assign Sequence") ?></a>
              <a class="btn btn-light float-right" data-dismiss="modal"><i class="bx bx-time"></i> <?php echo $this->lang->line("Close") ?></a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="subscriber_actions_modal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header" style="padding:15px;">
              <h3 class="modal-title"><?php echo $this->lang->line("Subscriber Actions");?></h3>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i class="bx bx-x"></i>
              </button>
            </div>

            <div class="modal-body" id="subscriber_actions_modal_body" style="padding:0 15px 15px 15px;" data-backdrop="static" data-keyboard="false">
              
              <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="default-tab" data-toggle="tab" href="#default" role="tab" aria-controls="default" aria-selected="true"><?php echo $this->lang->line("Subscriber Data"); ?></a>
                </li>

                <?php if($user_input_flow_exist == 'yes') : ?>
                <li class="nav-item">
                  <a class="nav-link" id="flowanswers-tab" data-toggle="tab" href="#flowanswers" role="tab" aria-controls="flowanswers" aria-selected="false"><?php echo $this->lang->line("User Input Flow Answer"); ?></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="customfields-tab" data-toggle="tab" href="#customfields" role="tab" aria-controls="customfields" aria-selected="false"><?php echo $this->lang->line("Custom Fields"); ?></a>
                </li>
                <?php endif; ?>

                <?php if($webview_access == 'yes') : ?>
                <li class="nav-item">
                  <a class="nav-link" id="formdata-tab" data-toggle="tab" href="#formdata" role="tab" aria-controls="formdata" aria-selected="false"><?php echo $this->lang->line("Custom Form Data"); ?></a>
                </li>
                <?php endif; ?>
                <?php if($ecommerce_exist == 'yes') : ?>
                <li class="nav-item">
                  <a class="nav-link" id="purchase-tab" data-toggle="tab" href="#purchase" role="tab" aria-controls="purchase" aria-selected="false"><?php echo $this->lang->line("Purchase History"); ?></a>
                </li>
                <?php endif; ?>

              </ul>

              <div class="tab-content" id="myTabContent">
                
                <div class="tab-pane fade active show" id="default" role="tabpanel" aria-labelledby="default-tab">
                  <div class="row multi_layout">
                  </div> 
                </div>

                <div class="tab-pane fade" id="formdata" role="tabpanel" aria-labelledby="formdata-tab">
                  <div class="card shadow-none" style="border:1px solid #dee2e6;border-top:none;border-radius:0">
                    <div class="card-body">
                      <div class="row formdata_div" style="padding-top:20px;"></div>                  
                    </div>
                  </div>
                </div>

                <div class="tab-pane fade" id="flowanswers" role="tabpanel" aria-labelledby="flowanswers-tab">
                  <div class="card shadow-none" style="border:1px solid #dee2e6;border-top:none;border-radius:0">
                    <div class="card-body">
                      <div class="row flowanswers_div" style="padding-top:20px;"></div>                  
                    </div>
                  </div>
                </div>

                <div class="tab-pane fade" id="customfields" role="tabpanel" aria-labelledby="customfields-tab">
                  <div class="card shadow-none" style="border:1px solid #dee2e6;border-top:none;border-radius:0">
                    <div class="card-body">
                      <div class="row customfields_div" style="padding-top:20px;"></div>                  
                    </div>
                  </div>
                </div>

                <div class="tab-pane fade" id="purchase" role="tabpanel" aria-labelledby="purchase-tab">
                  <div class="card shadow-none data-card" style="border:1px solid #dee2e6;border-top:none;border-radius:0">
                    <div class="card-body">
                      <div class="row purchase_div" style="padding-top:20px;"></div>
                      <div class="row">
                        <div class="col-12 col-md-9">
                          <?php
                          $status_list[''] = $this->lang->line("Status");                
                          echo 
                          '<div class="input-group mb-3" id="searchbox">
                            <div class="input-group-prepend d-none">
                              <input type="text" value="" name="search_subscriber_id" id="search_subscriber_id">
                            </div>
                            <div class="input-group-prepend d-none">
                              '.form_dropdown('search_status',$status_list,'','class="select2 form-control" id="search_status"').'
                            </div>
                            <input type="text" class="form-control" id="search_value2" autofocus name="search_value2" placeholder="'.$this->lang->line("Search...").'" style="max-width:25%;">
                            <div class="input-group-append">
                              <button class="btn btn-primary" type="button" id="search_action"><i class="bx bx-search"></i> <span class="d-none d-sm-inline">'.$this->lang->line("Search").'</span></button>
                            </div>
                          </div>'; ?>                                          
                        </div>

                        <div class="col-12 col-md-3">

                        <?php
                          echo $drop_menu ='<a href="javascript:;" id="search_date_range" class="btn btn-primary float-right has-icon-left btn-icon"><i class="bx bx-calendar"></i> '.$this->lang->line("Choose Date").'</a><input type="hidden" id="search_date_range_val">';
                        ?>

                                                   
                        </div>
                      </div>

                      <div class="table-responsive2">
                          <table class="table table-bordered" id="mytable2">
                            <thead>
                              <tr>
                                <th>#</th>      
                                <th style="vertical-align:middle;width:20px">
                                    <input class="regular-checkbox" id="datatableSelectAllRows" type="checkbox"/><label for="datatableSelectAllRows"></label>        
                                </th>         
                                <th style="max-width: 130px"><?php echo $this->lang->line("Status")?></th>              
                                <th><?php echo $this->lang->line("Coupon")?></th>                   
                                <th><?php echo $this->lang->line("Amount")?></th>                   
                                <th><?php echo $this->lang->line("Currency")?></th>                   
                                <th><?php echo $this->lang->line("Method")?></th>                   
                                <th><?php echo $this->lang->line("Transaction ID")?></th>                   
                                <th><?php echo $this->lang->line("Invoice")?></th>                              
                                <th><?php echo $this->lang->line("Docs")?></th>                              
                                <th><?php echo $this->lang->line("Ordered at")?></th>                   
                                <th><?php echo $this->lang->line("Paid at")?></th>                  
                              </tr>
                            </thead>
                          </table>
                      </div>

                    </div>
                  </div>
                </div>

              </div>

                         
            </div>
   
        </div>
    </div>
</div>

<style type="text/css">
  .chocolat-wrapper{z-index: 1060 !important;}
</style>