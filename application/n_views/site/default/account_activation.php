<div class="content-header row">
</div>
<div class="content-body"><!-- reset password start -->
    <section class="row flexbox-container">
        <div class="col-xl-7 col-10">
            <div class="card bg-authentication mb-0">
                <div class="row m-0">
                    <!-- left section-login -->
                    <div class="col-md-6 col-12 px-0">
                        <div class="card disable-rounded-right d-flex justify-content-center mb-0 p-2 h-100">
                            <div class="text-center mt-1">
                                <a href="<?php echo base_url();?>"><img src="<?php echo base_url(); ?>assets/img/logo.png" alt="<?php echo $this->config->item('product_name');?>" width="200"></a>
                            </div>
                            <div class="card-header pb-1">
                                <div class="card-title">
                                    <h4 class="text-center mb-2"><?php echo $this->lang->line("Account Activation");?></h4>
                                </div>
                            </div>
                            <div class="card-body" id="recovery_form">
                                <p class="text-muted"><?php echo $this->lang->line("Put your email and activation code that we sent to your email"); ?></p>
                                <form method="POST" action="<?php echo site_url();?>home/account_activation_action">
                                    <div class="form-group">
                                        <label class="text-bold-600" for="email"><?php echo $this->lang->line("email"); ?> *</label>
                                        <input type="email" class="form-control" id="email" name="email" required
                                               placeholder="<?php echo $this->lang->line("email"); ?>">
                                        <div class="invalid-feedback"><?php echo $this->lang->line("Please enter your email"); ?></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="text-bold-600" for="code"><?php echo $this->lang->line("Account Activation Code");?> *</label>
                                        <input type="text" class="form-control" id="code" name="email" required
                                               placeholder="<?php echo $this->lang->line("Account Activation Code");?>">
                                        <div class="invalid-feedback"><?php echo $this->lang->line("Please enter activation code"); ?></div>
                                    </div>
                                    <button type="submit" id="submit" class="btn btn-primary glow position-relative w-100"><?php echo $this->lang->line("Activate My Account");?><i id="icon-arrow" class="bx bx-right-arrow-alt"></i></button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- right section image -->
                    <div class="col-md-6 d-md-block d-none text-center align-self-center p-3">
                        <div class="d-flex"><img class="img-fluid" src="<?php echo base_url(); ?>n_assets/app-assets/images/pages/lock-screen.png" alt="branding logo"></div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view("include/fb_px"); ?>
        <?php $this->load->view("include/google_code"); ?>
    </section>
    <!-- reset password ends -->
