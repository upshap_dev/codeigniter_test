<?php 
$n_eco_builder_config['store_logo_light'] = 'upload/ecommerce/store_logo_light_535_1627889788169237.png';
$n_eco_builder_config['store_logo_dark'] = 'upload/ecommerce/store_logo_dark_535_1627890912205006.png';
$n_eco_builder_config['store_light_icon'] = 'n_assets/ecommerce/images/placeholder_icon_light.png';
$n_eco_builder_config['store_dark_icon'] = 'n_assets/ecommerce/images/placeholder_icon_dark.png';
$n_eco_builder_config['store_logo_show'] = 'logo';
$n_eco_builder_config['logo_height_header'] = '200';
$n_eco_builder_config['background_color_header_top'] = '#f44336';
$n_eco_builder_config['background_color_header_middle'] = 'white';
$n_eco_builder_config['show_store_name'] = 'true';
$n_eco_builder_config['font_family'] = 'Poppins';
$n_eco_builder_config['store_favicon'] = 'upload/ecommerce/store_favicon_535_1627891493250619.png';
$n_eco_builder_config['header_text'] = 'Welcome to our Store';
$n_eco_builder_config['category'] = 'category';
$n_eco_builder_config['header_color_logo'] = 'light';
$n_eco_builder_config['front_featured_products_show'] = 'true';
$n_eco_builder_config['front_featured_products_rows'] = '4';
$n_eco_builder_config['front_featured_products_text'] = 'Featured Products';
$n_eco_builder_config['front_featured_reviews_show'] = 'show';
$n_eco_builder_config['front_featured_products_limit'] = '4';
$n_eco_builder_config['front_deals_products_show'] = 'true';
$n_eco_builder_config['front_deals_products_rows'] = '4';
$n_eco_builder_config['front_deals_products_text'] = 'Sales Products';
$n_eco_builder_config['front_deals_reviews_show'] = 'show';
$n_eco_builder_config['front_deals_products_limit'] = '4';
$n_eco_builder_config['front_sales_products_show'] = 'true';
$n_eco_builder_config['front_sales_products_rows'] = '4';
$n_eco_builder_config['front_sales_products_text'] = 'Bestsellers';
$n_eco_builder_config['front_sales_reviews_show'] = 'show';
$n_eco_builder_config['front_sales_products_limit'] = '4';
$n_eco_builder_config['new_products_show'] = 'true';
$n_eco_builder_config['new_products_rows'] = '4';
$n_eco_builder_config['new_products_text'] = 'New Products';
$n_eco_builder_config['new_products_reviews_show'] = 'show';
$n_eco_builder_config['new_products_products_limit'] = '4';
$n_eco_builder_config['front_width_size'] = 'full_width';
$n_eco_builder_config['front_hide_reviews'] = 'false';
$n_eco_builder_config['contact_page_on'] = 'false';
$n_eco_builder_config['category_hide_reviews'] = 'show';
$n_eco_builder_config['front_order'] = 'front_sales_products,front_featured_products,new_products,front_deals_products,';
$n_eco_builder_config['header_search'] = 'true';
$n_eco_builder_config['product_single_gallery_type'] = 'product-gallery-horizontal';
$n_eco_builder_config['product_single_sticky'] = 'true';
$n_eco_builder_config['product_single_width_0_items_related'] = '3';
$n_eco_builder_config['product_single_width_576_items_related'] = '3';
$n_eco_builder_config['product_single_width_768_items_related'] = '5';
$n_eco_builder_config['product_single_width_992_items_related'] = '6';
$n_eco_builder_config['product_single_autoplay_items_related'] = 'true';
$n_eco_builder_config['product_single_max_load_items_related'] = '6';
$n_eco_builder_config['addthis_show'] = 'false';
$n_eco_builder_config['addthis_class'] = '';
$n_eco_builder_config['product_single_alert_add_to_cart'] = 'true';
$n_eco_builder_config['product_single_popup_add_to_cart'] = 'true';
$n_eco_builder_config['product_single_reviews_show'] = 'always_show';
$n_eco_builder_config['product_single_show_sales'] = 'false';
$n_eco_builder_config['product_single_autoplaytimeout_items_related'] = '4000';
$n_eco_builder_config['hide_add_to_cart'] = '0';
$n_eco_builder_config['hide_buy_now'] = '0';
$n_eco_builder_config['show_mobile_menu'] = 'true';
$n_eco_builder_config['show_mobile_menu_only_icons'] = 'false';
$n_eco_builder_config['show_mobile_menu_home'] = 'true';
$n_eco_builder_config['show_mobile_menu_cart'] = 'true';
$n_eco_builder_config['show_mobile_menu_account'] = 'true';
$n_eco_builder_config['show_mobile_menu_contact'] = 'false';
$n_eco_builder_config['show_mobile_menu_orders'] = 'true';
$n_eco_builder_config['front_hide_add_to_cart_slideup'] = 'true';
$n_eco_builder_config['buy_button_type'] = 'add_to_cart';
$n_eco_builder_config['add_to_cart_button_title'] = 'Add to cart';
$n_eco_builder_config['store_pickup_title'] = 'Store Pickup';
$n_eco_builder_config['buy_button_title'] = 'Buy Now';
$n_eco_builder_config['category_perpage'] = '4';
$n_eco_builder_config['empty_cart_image'] = 'assets/img/drawkit/drawkit-full-stack-man-colour.svg';
$n_eco_builder_config['show_mobile_menu_search'] = 'false';
$n_eco_builder_config['addthis_code'] = '';
$n_eco_builder_config['store_type'] = 'classic';
$n_eco_builder_config['section_header'] = 'colored_v1';
$n_eco_builder_config['store_single'] = 'store_single';
$n_eco_builder_config['contact'] = 'contact';
$n_eco_builder_config['product_single'] = 'product_single';
$n_eco_builder_config['login_signup'] = 'login_signup';
$n_eco_builder_config['cart'] = 'cart';
$n_eco_builder_config['terms'] = 'terms';
$n_eco_builder_config['refund_policy'] = 'refund_policy';
$n_eco_builder_config['order'] = 'order';
$n_eco_builder_config['my_orders'] = 'my_orders';
