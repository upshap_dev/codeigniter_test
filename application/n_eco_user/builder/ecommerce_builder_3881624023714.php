<?php 
$n_eco_builder_config['store_logo_light'] = 'upload/ecommerce/store_logo_light_388_1628384764635229.png';
$n_eco_builder_config['store_logo_dark'] = 'upload/ecommerce/store_logo_dark_388_1628384794127268.png';
$n_eco_builder_config['store_light_icon'] = 'upload/ecommerce/store_light_icon_388_1628384863165105.png';
$n_eco_builder_config['store_dark_icon'] = 'upload/ecommerce/store_dark_icon_388_1628384856943539.png';
$n_eco_builder_config['store_logo_show'] = 'logo';
$n_eco_builder_config['logo_height_header'] = '100';
$n_eco_builder_config['background_color_header_top'] = '#ffde59';
$n_eco_builder_config['background_color_header_middle'] = '#01263a';
$n_eco_builder_config['show_store_name'] = 'true';
$n_eco_builder_config['font_family'] = 'Balmy';
$n_eco_builder_config['store_favicon'] = 'upload/ecommerce/store_favicon_388_1628383518109481.jpg';
$n_eco_builder_config['header_text'] = 'Welcome to Donnas Kitchen';
$n_eco_builder_config['category'] = 'category';
$n_eco_builder_config['header_color_logo'] = 'light';
$n_eco_builder_config['front_featured_products_show'] = 'true';
$n_eco_builder_config['front_featured_products_rows'] = '4';
$n_eco_builder_config['front_featured_products_text'] = 'Featured Products';
$n_eco_builder_config['front_featured_reviews_show'] = 'show';
$n_eco_builder_config['front_featured_products_limit'] = '4';
$n_eco_builder_config['front_deals_products_show'] = 'true';
$n_eco_builder_config['front_deals_products_rows'] = '4';
$n_eco_builder_config['front_deals_products_text'] = 'Sales Products';
$n_eco_builder_config['front_deals_reviews_show'] = 'show';
$n_eco_builder_config['front_deals_products_limit'] = '4';
$n_eco_builder_config['front_sales_products_show'] = 'true';
$n_eco_builder_config['front_sales_products_rows'] = '4';
$n_eco_builder_config['front_sales_products_text'] = 'Bestsellers';
$n_eco_builder_config['front_sales_reviews_show'] = 'show';
$n_eco_builder_config['front_sales_products_limit'] = '4';
$n_eco_builder_config['new_products_show'] = 'true';
$n_eco_builder_config['new_products_rows'] = '4';
$n_eco_builder_config['new_products_text'] = 'New Products';
$n_eco_builder_config['new_products_reviews_show'] = 'show';
$n_eco_builder_config['new_products_products_limit'] = '4';
$n_eco_builder_config['front_width_size'] = 'boxed';
$n_eco_builder_config['front_hide_reviews'] = 'false';
$n_eco_builder_config['contact_page_on'] = 'false';
$n_eco_builder_config['category_hide_reviews'] = 'show';
$n_eco_builder_config['front_order'] = 'front_featured_products,new_products,front_deals_products,front_sales_products,';
$n_eco_builder_config['header_search'] = 'true';
$n_eco_builder_config['product_single_gallery_type'] = 'product-gallery-horizontal';
$n_eco_builder_config['product_single_sticky'] = 'true';
$n_eco_builder_config['product_single_width_0_items_related'] = '3';
$n_eco_builder_config['product_single_width_576_items_related'] = '3';
$n_eco_builder_config['product_single_width_768_items_related'] = '5';
$n_eco_builder_config['product_single_width_992_items_related'] = '6';
$n_eco_builder_config['product_single_autoplay_items_related'] = 'true';
$n_eco_builder_config['product_single_max_load_items_related'] = '6';
$n_eco_builder_config['addthis_show'] = 'false';
$n_eco_builder_config['addthis_class'] = '';
$n_eco_builder_config['product_single_alert_add_to_cart'] = 'true';
$n_eco_builder_config['product_single_popup_add_to_cart'] = 'true';
$n_eco_builder_config['product_single_reviews_show'] = 'always_show';
$n_eco_builder_config['product_single_show_sales'] = 'true';
$n_eco_builder_config['product_single_autoplaytimeout_items_related'] = '4000';
$n_eco_builder_config['hide_add_to_cart'] = '0';
$n_eco_builder_config['hide_buy_now'] = '0';
$n_eco_builder_config['show_mobile_menu'] = 'true';
$n_eco_builder_config['show_mobile_menu_only_icons'] = 'false';
$n_eco_builder_config['show_mobile_menu_home'] = 'true';
$n_eco_builder_config['show_mobile_menu_cart'] = 'true';
$n_eco_builder_config['show_mobile_menu_account'] = 'true';
$n_eco_builder_config['show_mobile_menu_contact'] = 'true';
$n_eco_builder_config['show_mobile_menu_orders'] = 'true';
$n_eco_builder_config['front_hide_add_to_cart_slideup'] = 'true';
$n_eco_builder_config['buy_button_type'] = 'buy_now';
$n_eco_builder_config['add_to_cart_button_title'] = 'Add to cart';
$n_eco_builder_config['store_pickup_title'] = 'Store Pickup';
$n_eco_builder_config['buy_button_title'] = 'Buy Now';
$n_eco_builder_config['addthis_code'] = '';
$n_eco_builder_config['logo_width_header'] = '180';
$n_eco_builder_config['add_to_cart_color_bg'] = '#08d';
$n_eco_builder_config['add_to_cart_color_bg_hover'] = '#029EFF';
$n_eco_builder_config['add_to_cart_color_font'] = '#ffffff';
$n_eco_builder_config['add_to_cart_color_font_hover'] = '#ffffff';
$n_eco_builder_config['home_add_to_cart_color_bg'] = '#454545';
$n_eco_builder_config['home_add_to_cart_color_bg_hover'] = '#029EFF';
$n_eco_builder_config['home_add_to_cart_color_font'] = '#ffffff';
$n_eco_builder_config['home_add_to_cart_color_font_hover'] = '#ffffff';
$n_eco_builder_config['footer_background'] = '#01263a';
$n_eco_builder_config['footer_background_text_color'] = '#ffffff';
$n_eco_builder_config['footer_background_link_color'] = '#ffde59';
$n_eco_builder_config['footer_background_link_color_hover'] = '#1914fe';
$n_eco_builder_config['whatsapp_send_order_button'] = '0';
$n_eco_builder_config['whatsapp_send_order_text'] = 'New Order #{{order_no}}

Customer: {{customer_info}}

{{product_info}}

Order Status: {{order_status}}
Order URL: {{order_url}}
Payment Method: {{payment_method}}

Tax: {{tax}}
Total Price: {{total_price}}
{{delivery_address}}';
$n_eco_builder_config['whatsapp_phone_number'] = '';
$n_eco_builder_config['codes_before_body'] = '';
$n_eco_builder_config['text_show_delivery_note'] = 'Show delivery note';
$n_eco_builder_config['show_show_delivery_note'] = 'show';
$n_eco_builder_config['font_color_text_dark'] = '#333';
$n_eco_builder_config['font_color_text_light'] = '#999';
$n_eco_builder_config['font_color_product_cat'] = '#999';
$n_eco_builder_config['font_color_headers'] = '#333';
$n_eco_builder_config['font_color_header_middle'] = '#fff';
$n_eco_builder_config['font_color_header_top'] = '#fff';
$n_eco_builder_config['product_single_items_related_title'] = 'Related Items';
$n_eco_builder_config['category_perpage'] = '4';
$n_eco_builder_config['empty_cart_image'] = 'assets/img/drawkit/drawkit-full-stack-man-colour.svg';
$n_eco_builder_config['show_mobile_menu_search'] = 'false';
$n_eco_builder_config['store_type'] = 'classic';
$n_eco_builder_config['section_header'] = 'colored_v1';
$n_eco_builder_config['store_single'] = 'store_single';
$n_eco_builder_config['contact'] = 'contact';
$n_eco_builder_config['product_single'] = 'product_single';
$n_eco_builder_config['login_signup'] = 'login_signup';
$n_eco_builder_config['cart'] = 'cart';
$n_eco_builder_config['terms'] = 'terms';
$n_eco_builder_config['refund_policy'] = 'refund_policy';
$n_eco_builder_config['order'] = 'order';
$n_eco_builder_config['my_orders'] = 'my_orders';
$n_eco_builder_config['empty_cart'] = 'empty_cart';
$n_eco_builder_config['comment_single'] = 'comment_single';
$n_eco_builder_config['review_single'] = 'review_single';
