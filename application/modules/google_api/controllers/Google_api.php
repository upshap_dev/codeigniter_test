<?php
/*
Addon Name: Google API
Unique Name: google_api
Project ID: 149
Addon URI: https://marketplace.bcheckin.com
Author: Mike TMD
Author URI: https://bcheckin.com
Version: 1.0.1
Description:
*/

require_once("application/controllers/Home.php");

class Google_api extends Home {

    public function index()
    {
       
       //$this->dashboard();
       redirect('/google_api/google_sheet_integrations', 'location');
    }
    
    
    public function google_sheet_integrations()
    {
        if ($this->session->userdata('logged_in') != 1)
        redirect('home/login_page', 'location');

        $this->member_validity();

        $data['body'] = 'google_sheet_integrations';
        
        $data['page_title'] = $this->lang->line("Google Sheet Integrations");
        
        $this->_viewcontroller($data);
        
    }
    
    
    public function activate()
    {
        
        $this->ajax_check();
        
        
        $menu_exists = $this->db->query(" SELECT id FROM `menu` where url LIKE '%google_api%' ")->row_array();
        if(!$menu_exists){
            try{
                $sql = "INSERT INTO `menu` (`name`, `icon`, `url`, `serial`, `module_access`, `have_child`, `only_admin`, `only_member`, `add_ons_id`, `is_external`, `header_text`)
                VALUES ('Google Api', 'fab fa-google', 'google_api', 50, '', '0', '0', '0', 0, '0', 'UTILITY TOOLS');" ;
                $this->db->query($sql);
            }catch(Exception $e){

            }
        }
        
        unlink(FCPATH."application/modules/google_api/install.txt");
        
        unlink(FCPATH."application/views/marketplace/autoload/add_google_sheet_menu_bar.php");
        
        echo json_encode(array('status'=>'1','message'=>$this->lang->line('Add-on has been activated successfully.')));exit();
        
    }
    
    public function deactivate()
    {
        
        $this->ajax_check();
        $addon_controller_name=ucfirst($this->router->fetch_class());
        $install_txt_path=APPPATH."modules/".strtolower($addon_controller_name)."/install.txt";
        if(!file_exists($install_txt_path)) // putting install.txt
        fopen($install_txt_path, "w");
        
        $menu = $this->db->query(" SELECT id FROM `menu` where url LIKE '%google_api%' ")->row_array();
        if($menu){
                $menu_id = $menu['id'];
                $this->db->query(" DELETE FROM `menu` WHERE `menu`.`id` = $menu_id ");   
        }
            
        unlink(FCPATH."application/views/marketplace/autoload/add_google_sheet_menu_bar.php");
        
        echo json_encode(array('status'=>'1','message'=>$this->lang->line('Add-on has been deactivated successfully.')));exit();
        
    }
    
    
    public function delete(){
        
        
        // Delete Menu in SQL
        $menu = $this->db->query(" SELECT id FROM `menu` where url LIKE '%google_api%' ")->row_array();
        if($menu){
                $menu_id = $menu['id'];
                $this->db->query(" DELETE FROM `menu` WHERE `menu`.`id` = $menu_id ");   
        }
            
        // Delete Add-on folder
        $addon_controller_name = 'google_api';
        $addon_path = APPPATH."modules/".$addon_controller_name;
        $this->delete_directory($addon_path);
        
        echo json_encode(array('status'=>'1','message'=>$this->lang->line('add-on has been deleted successfully.')));  exit();  
        
    }
    
    
    public function get_user_pages_ext(){

        //$this->ajax_check();
        $user_id  =  $this->session->userdata('user_id');
        
        echo json_encode($this->get_user_pages($user_id));
        
    }
    
    public function get_user_pages($user_id){
        $table_type = 'facebook_rx_fb_page_info';
        $where_type['where'] = array('user_id'=>$user_id,"bot_enabled"=>"1");
        $info_type = $this->basic->get_data($table_type,$where_type);
        return $info_type;
    }
    
    
    // Get All Orders by Store ID & User_ID
    public function get_all_order() {        
          
          $store_id = 700;
          $limit = 10;
          $select="ecommerce_cart.id as order_id,subscriber_id,buyer_first_name,buyer_last_name,buyer_email,buyer_mobile,buyer_country,buyer_city,buyer_state,buyer_address,buyer_zip,coupon_code,coupon_type,discount,payment_amount,currency,ordered_at,transaction_id,card_ending,payment_method,manual_additional_info,paid_at,ecommerce_cart.status as payment_status";
          $where['where']['store_id']= $store_id;
          $packages= $this->basic->get_data("ecommerce_cart",$where,$select,"",$limit,"","id asc");
            
          if( $packages == NULL ){
                $response['status']='error';
                $response['message']="You do not have any Orders";
                echo json_encode($response);
                exit;
          }
            
          $response=json_encode($packages);
          echo $response;
            
    }
  
    // Get All Subscriber by Pages ID & User_ID
    public function get_all_subscriber() {

        $page_id = $this->input->get('x');
        if(!$page_id){
            $response['status']='error';
            $response['message']="Missing Pages ID";
            echo json_encode($response);
            exit;
        }
        
        if (!$this->input->get('limit')) {$limit = "50";} else { $limit = $this->input->get('limit'); };
        
        $user_id = $this->input->get('y');
        if(!$user_id){
            $response['status']='error';
            $response['message']="Missing User ID";
            echo json_encode($response);
            exit;
        }

        
        $user_password = $this->db->select('password')
	        		->get_where('users', array('id' => $user_id))
	        		->row()->password;
	        		
     
        
        $key_id = $this->input->get('z');
        if(!$key_id){
            $response['status']='error';
            $response['message']="Missing Key Number";
            echo json_encode($response);
            exit;
        }

        $md5_key_input = $this->input->get('z');
        
        $md5_private_key=md5($page_id.$user_password);
        
        if( $md5_key_input == NULL OR $md5_key_input != $md5_private_key ){
            $response['status']='error';
            $response['message']="Wrong Key field";
            echo json_encode($response);
            exit;
        }
        
        $select="page_id,subscribe_id,first_name,last_name,gender,email,phone_number,user_location,subscribed_at,last_subscriber_interaction_time";
        $where['where']['page_id']=$page_id;
        $packages= $this->basic->get_data("messenger_bot_subscriber",$where,$select,"",$limit,"","subscribed_at DESC");

        if( $packages == NULL ){
            $response['status']='error';
            $response['message']="You do not have any subscribers";
            echo json_encode($response);
            exit;
        }
        
        $response=json_encode($packages);
        echo $response;
    }
    
    


}    









