<?php
/*
Addon Name: Marketplace
Unique Name: mkp_marketplace
Project ID: 149
Addon URI: https://marketplace.bcheckin.com
Author: Mike TMD
Author URI: https://bcheckin.com
Version: 1.0.10
Description:
*/

require_once("application/controllers/Home.php");

class Marketplace extends Home {

    public function index()
    {
       
       $this->dashboard();
       
    }


    public function dashboard()
    {
        if ($this->session->userdata('logged_in') != 1)
        redirect('home/login_page', 'location');

        if($this->session->userdata('user_type') != 'Admin')
        redirect('home/login_page', 'location');

        $this->member_validity();

        $data['body'] = 'dashboard';
        $data['page_title'] = $this->lang->line("Market Place");
        $this->_viewcontroller($data);
    }
    

    public function activate()
    {
        $postRequest = array(
            'module_name' => 'marketplace_main_app',
            'purchase_code' => '000000',
            'current_version' => '0000',
            'latest_version' => '000000'
            
        );
        
        $cURLConnection = curl_init( base_url('marketplace/install') );
        curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, $postRequest);
        curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
        $apiResponse = curl_exec($cURLConnection);
        curl_close($cURLConnection);
        
        unlink(FCPATH."application/modules/marketplace/install.txt");
        
        echo json_encode(array('status'=>'1','message'=>$this->lang->line('Add-on has been activated successfully.')));
        
    }
    
    public function deactivate()
    {
        
        $postRequest = array(
            'module_name' => 'marketplace_main_app_uninstall',
            'purchase_code' => '000000',
            'current_version' => '0000',
            'latest_version' => '000000'
            
        );
        
        $cURLConnection = curl_init( base_url('marketplace/install') );
        curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, $postRequest);
        curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
        $apiResponse = curl_exec($cURLConnection);
        curl_close($cURLConnection);
        
        
        fopen(FCPATH."application/modules/marketplace/install.txt", "w");
        echo json_encode(array('status'=>'1','message'=>$this->lang->line('Add-on has been deactivated successfully.')));
    }
    
    
    public function install()
    {    
        
        //if ($_SERVER['REQUEST_METHOD'] === 'GET') exit();
        
        $purchase_code = $this->input->post('purchase_code');
            if ($purchase_code == NULL) {echo "Empty purchase code";  exit;}
        
        $module_name = $this->input->post('module_name');
            if ($module_name == NULL) {echo "Empty file name";  exit;}
        
        $current_version = $this->input->post('current_version');
            if ($current_version == NULL) {echo "Missing current version";  exit;}
        
        $latest_version = $this->input->post('latest_version');
            if ($latest_version == NULL) {echo "Missing latest version";  exit;}
        
        function downloadZipFile($url, $filepath){
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_REFERER,$_SERVER['SERVER_NAME']);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3"); 
            $raw_file_data = curl_exec($ch);
            
             if(curl_errno($ch)){
                echo 'error:' . curl_error($ch);
             }
             curl_close($ch);
        
             file_put_contents($filepath, $raw_file_data);
             return (filesize($filepath) > 0)? true : false;
        }
        
        function extractZipFile($filePath, $extractPath){
            $zip = new ZipArchive;
            if ($zip->open($filePath) === TRUE) 
            {
                $zip->extractTo($extractPath);
                $zip->close();
                @unlink($filePath);
                //$this->session->set_flashdata('addon_uplod_success',$this->lang->line('add-on has been uploaded successfully. you can activate it from here.'));
            } 
        }        
        
        $server_url='https://marketplace.bcheckin.com';
        
        
        // Check any error ! 1
        //$json = file_get_contents($server_url."/update/?file=".$module_name."&current_version=".$current_version."&latest_version=".$latest_version."&key=".$purchase_code."&url=".$_SERVER['SERVER_NAME']);
        $curl_url = $server_url."/update/?file=".$module_name."&current_version=".$current_version."&latest_version=".$latest_version."&key=".$purchase_code."&url=".$_SERVER['SERVER_NAME'];
        $curl_handle=curl_init();
    	curl_setopt($curl_handle, CURLOPT_URL,$curl_url);
    	curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
    	curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
    	curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Your application name');
    	$server_query = curl_exec($curl_handle);
    	curl_close($curl_handle);
        
        $obj = json_decode($server_query );
            
        
        // If don't have any Errors -> Downlooad
        if ($obj == NULL) {
            
            downloadZipFile($server_url."/update/?file=".$module_name."&url=".$_SERVER['SERVER_NAME']."&current_version=".$current_version."&latest_version=".$latest_version."&key=".$purchase_code."", FCPATH."/download/".$module_name.".zip");
            
            extractZipFile(FCPATH."/download/".$module_name.".zip", FCPATH );
            
            $this->load->helper('file');
            $module_name = str_replace("_update","",$module_name);
            write_file(FCPATH."application/views/marketplace/".$module_name.".txt",$latest_version.",".$purchase_code);
            
            echo "Successful!";
            exit; 
        } else {
            echo $obj->content;
        }

        
    }    

}    
