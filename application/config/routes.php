<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$check_ef = false;
if(file_exists(APPPATH.'custom_domain.php')){
    include(APPPATH.'custom_domain.php');

    if($ncd_config['eco_custom_domain']=='false'){
        $check_ef = true;
    }else{
        switch ( $_SERVER['HTTP_HOST'] ) {

            case $ncd_config['custom_domain_host'];
                $check_ef = true;
                break;

            default;

                $route['default_controller'] = "custom_cname";
                $route['bot_instagram'] = 'n_theme/bot_instagram';
                $route['ecommerce_review_comment/new_review'] = 'custom_cname/new_review';

                $route['n_paymongo'] = 'n_paymongo';
                $route['n_paymongo/(:any)'] = 'n_paymongo/$1';
                $route['n_paymongo/(:any)/(:any)'] = 'n_paymongo/$1/$2';
                $route['n_paymongo/(:any)/(:any)/(:any)'] = 'n_paymongo/$1/$2/$3';
                $route['n_paymongo/(:any)/(:any)/(:any)/(:any)'] = 'n_paymongo/$1/$2/$3/$4';
                $route['n_paymongo/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'n_paymongo/$1/$2/$3/$4/$5';

                $route['(:any)'] = 'custom_cname/$1';
                $route['(:any)/(:any)'] = 'custom_cname/$1/$2';
                $route['(:any)/(:any)/(:any)'] = 'custom_cname/$1/$2/$3';
                $route['(:any)/(:any)/(:any)/(:any)'] = 'custom_cname/$1/$2/$3/$4';

                $route['404_override'] = 'home/error_404';
                break;

        }
    }
}else{
    $check_ef = true;
}

if($check_ef){
    $route['default_controller'] = "home";
    $route['404_override'] = 'home/error_404';

    $route['faq'] = 'n_theme/faq';
    $route['help'] = 'n_theme/help';
    $route['task'] = 'n_task/index';
    $route['bot_instagram'] = 'n_theme/bot_instagram';

    $route['ecommerce_builder/(:num)'] = 'n_theme/ecommerce_builder/$1';
    $route['ecommerce_builder/(:any)/(:any)'] = 'n_theme/$1/$2';
    $route['ecommerce_builder/(:any)/(:any)/(:any)'] = 'n_theme/$1/$2/$3';

    $route['ecommerce/(:any)'] = 'custom_cname/$1';
    $route['ecommerce/(:any)/(:any)'] = 'custom_cname/$1/$2';
    $route['ecommerce/(:any)/(:any)/(:any)'] = 'custom_cname/$1/$2/$3';
    $route['ecommerce/(:any)/(:any)/(:any)/(:any)'] = 'custom_cname/$1/$2/$3/$4';

    $route['task/(:any)'] = 'n_task/$1';
    $route['task/(:any)/(:any)'] = 'n_task/$1/$2';
    $route['task/(:any)/(:any)/(:any)'] = 'n_task/$1/$2/$3';
    $route['task/(:any)/(:any)/(:any)/(:any)'] = 'n_task/$1/$2/$3/$4';
    $route['task/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'n_task/$1/$2/$3/$4/$5';
    $route['task/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'n_task/$1/$2/$3/$4/$5/$6';
}

/* End of file routes.php */
/* Location: ./application/config/routes.php */