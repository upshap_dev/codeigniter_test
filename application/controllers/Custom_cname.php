<?php
require_once("application/controllers/Ecommerce.php"); // loading home controller

if ( ! function_exists('mec_number_format')) {
    function mec_number_format($number, $decimal_point = 0, $thousand_comma = '0')
    {
        $decimal_point_count = strlen(substr(strrchr($number, "."), 1));


        if ($decimal_point_count > 0 && $decimal_point == 0) $decimal_point = $decimal_point_count; // if setup no deciaml place but the number is naturally float, we can not just skip it

        $number = (float)$number;
        $comma = $thousand_comma == '1' ? ',' : '';
        return number_format($number, $decimal_point, '.', $comma);
    }
}


class custom_cname extends Ecommerce
{
    var $nstore_id = 0;
    var $check_cn = true;

    public function __construct()
    {
        //parent::__construct();
        Home::__construct();

        $this->load->helpers(array('ecommerce_helper'));

        $function_name = $this->uri->segment(2);
        if(file_exists(APPPATH.'custom_domain.php')) {
            include(APPPATH . 'custom_domain.php');
            if ($ncd_config['eco_custom_domain'] == 'true') {
                if ($_SERVER['HTTP_HOST'] != $ncd_config['custom_domain_host']) {
                    $function_name = $this->uri->segment(1);
                }
            }
        }


        if($function_name==null){
            $function_name = 'index';
        }


        $private_functions = array("","","qr_code","download_qr","qr_code_action","qr_code_live","notification_settings","notification_settings_action","reset_notification","reset_reminder","reminder_settings","reminder_settings_action","store_list","copy_url","order_list","change_payment_status","order_list_data","reminder_send_status_data","reminder_response","add_store","add_store_action","edit_store","edit_store_action","product_list","product_list_data","delete_store","add_product","add_product_action","edit_product","edit_product_action","delete_product","payment_accounts","payment_accounts_action","attribute_list","attribute_list_data","ajax_create_new_attribute","ajax_get_attribute_update_info","ajax_update_attribute","delete_attribute","category_list","category_list_data","ajax_create_new_category","ajax_get_category_update_info","ajax_update_category","delete_category","coupon_list","coupon_list_data","add_coupon","add_coupon_action","edit_coupon","edit_coupon_action","delete_coupon","upload_product_thumb","delete_product_thumb","upload_store_logo","delete_store_logo","upload_store_favicon","delete_store_favicon","download_csv","upload_featured_image","delete_featured_image","pickup_point_list","pickup_point_list_data","ajax_create_new_pickup_point","ajax_get_pickup_point_update_info","ajax_update_pickup_point","delete_pickup_point","appearance_settings","appearance_settings_action","business_hour_settings","business_hour_settings_action","customer_list","customer_list_data","change_user_password_action","download_result","sort_category");
        if(in_array($function_name, $private_functions))
        {
            if($this->session->userdata('logged_in')!= 1) redirect('home/login', 'location');
            if($this->session->userdata('user_type') != 'Admin' && !in_array(268,$this->module_access)) redirect('home/login', 'location');
            $this->member_validity();
        }
        $this->currency_icon = $this->currency_icon();
        $this->editor_allowed_tags = '<h1><h2><h3><h4><h5><h6><a><b><strong><p><i><div><span><ul><li><ol><blockquote><code><table><tr><td><th><iframe><img>';
        $this->login_to_continue = $this->lang->line("Please login to continue.");

        $check_cn = true;
        if(file_exists(APPPATH.'custom_domain.php')) {

            if ($ncd_config['eco_custom_domain'] == 'true') {
                if ($_SERVER['HTTP_HOST'] != $ncd_config['custom_domain_host']) {
                    $n_cd_data = $this->check_access();
                    $this->nstore_id = $n_cd_data[0]['store_unique_id'];
                    $check_cn = false;
                }elseif($this->uri->segment(2)=='store'){
                    $eco_host = $_SERVER['HTTP_HOST'];
                    $nstoreid = $this->get_store_ids($this->uri->segment(3));
                    if($nstoreid!=0){
                        $n_cd_data = $this->basic->get_data("n_custom_domain", array("where" => array(
                            "custom_id" => $nstoreid,
                        "active" => 1,
                    )));
                        if(!empty($n_cd_data[0])){
                            $subscriber_id = $this->session->userdata($nstoreid."ecom_session_subscriber_id");
                            if($subscriber_id=="") $subscriber_id = $this->input->get("subscriber_id",true);

                            $nruil = mec_add_get_param('https://'.$n_cd_data[0]['host_url'],array("subscriber_id"=>$subscriber_id));
                            redirect($nruil, 'location');
                        }
                    }

                }
            }
        }

        if($check_cn==true){
            $this->nstore_id = $this->uri->segment(3);
        }

        $this->check_cn = $check_cn;

        if(empty($this->is_ecommerce_related_product_exist)){
            $this->is_ecommerce_related_product_exist = false;
        }
    }

    public function index(){
        $store_id = $this->get_store_id(0);
        $this->store($store_id);
    }

    public function login_signup($store_unique_id = ''){
        $store_data = $this->initial_load_store($store_unique_id);

        $subscriber_id=$this->session->userdata($store_data[0]['id']."ecom_session_subscriber_id");
        if($subscriber_id=="") $subscriber_id = $this->input->get("subscriber_id",true);
        if($subscriber_id!=""){
            redirect($this->return_base_url_php('ecommerce/store/'.$store_data[0]['store_unique_id']), 'location');
            exit();
        }

        $fb_app_id = $this->get_app_id();
        $data = array(
            'body' => "login_signup",
            "page_title"=> $store_data[0]['store_name']." | ".$this->lang->line("Login / Sign Up"),
            "fb_app_id"=>$fb_app_id
        );
        $data["social_analytics_codes"] = $store_data[0];

        $ecommerce_config = $this->get_ecommerce_config($store_data[0]['id']);
        $data['ecommerce_config'] = $ecommerce_config;

        $category_list = $this->get_category_list($store_data[0]['id'],true);
        $cat_info=array();
        foreach($category_list as $value)
        {
            $cat_info[$value['id']] = $value['category_name'];
        }
        $data["category_list"] = $cat_info;
        $data["category_list_raw"] = $category_list;

//        $data["attribute_list"] = $this->get_attribute_list($store_id);
//        $data['currency_icons'] = $this->currency_icon();
//        $data['ecommerce_config'] = $ecommerce_config;
//        $data['current_cart'] = $this->get_current_cart($subscriber_id,$store_id);
//        $data["show_search"] = true;
//        $data["show_header"] = true;

        $this->load->view('ecommerce/bare-theme', $data);
    }

    public function my_orders($store_id=0){
        $this->my_account($store_id);
    }

    public function my_account($store_id=0, $subpage = 'my_orders'){
        if($store_id==0){
            $store_id = $this->get_store_id($store_id);
        }
        $n_subscriber_info = array();
        $where_subs = array();

        $subscriber_id=$this->session->userdata($store_id."ecom_session_subscriber_id");

        $login_needed = false;
        if($subscriber_id!="") $where_subs = array("subscriber_type"=>"system","subscribe_id"=>$subscriber_id,"store_id"=>$store_id);
        else {
            if($subscriber_id=="") $subscriber_id = $this->input->get("subscriber_id",true);
            if($subscriber_id!="") $where_subs = array("subscriber_type!="=>"system","subscribe_id"=>$subscriber_id);
            $n_subscriber_info = $this->basic->get_data("messenger_bot_subscriber",array("where"=>$where_subs));
        }

        if($subscriber_id=='') $login_needed = true;
        else
        {
            $subscriber_info = $this->basic->count_row("messenger_bot_subscriber",array("where"=>$where_subs),"id");
            $n_subscriber_info = $this->basic->get_data("messenger_bot_subscriber",array("where"=>$where_subs));
            if($subscriber_info[0]['total_rows']==0) $login_needed = true;
        }

        $store_data = $this->basic->get_data("ecommerce_store",array("where"=>array("id"=>$store_id)),"store_name,store_unique_id,store_logo,store_favicon,terms_use_link,refund_policy_link,store_locale,pixel_id,google_id,id,user_id");
        if($store_id==0 || !isset($store_data[0]))
        {
            $not_found = $this->lang->line("Order data not found.");
            echo '<br/><h1 style="text-align:center">'.$not_found.'</h1>';
            exit();
        }
        if($login_needed){
            redirect($this->return_base_url_php('ecommerce/login_signup/'.$store_data[0]['store_unique_id']), 'location');
            exit();
        }

        $this->_language_loader($store_data[0]['store_locale']);
        $data['store_data'] = $store_data[0];
        $data['store_id'] = $store_id;
        $data['subscriber_id'] = $subscriber_id;
        $data['body'] = 'ecommerce/my_orders';
        $data['page_title'] = $this->lang->line('My Orders');
        $data['status_list'] = $this->get_payment_status();
        $data["fb_app_id"] = $this->get_app_id();
        $data['current_cart'] = $this->get_current_cart($subscriber_id,$store_id);
        $data['ecommerce_config'] = $this->get_ecommerce_config($store_id);
        $data["social_analytics_codes"] = $store_data[0];
        if($login_needed){
            $data['body'] = 'ecommerce/login_to_continue';
            $data['page_title'] = $this->lang->line('Login to Continue');
        }

        $category_list = $this->get_category_list($store_data[0]['id'],true);
        $cat_info=array();
        foreach($category_list as $value)
        {
            $cat_info[$value['id']] = $value['category_name'];
        }
        $data["category_list"] = $cat_info;
        $data["category_list_raw"] = $category_list;

        $data["n_subscriber_info"] = $n_subscriber_info[0];
        $this->load->view('ecommerce/bare-theme', $data);
    }

    public function my_orders_data(){
        $this->ajax_check();
        $search_value = $this->input->post("search_value");
        $subscriber_id = $this->input->post("search_subscriber_id");
        $pickup = $this->input->post("search_pickup");
        $store_id = $this->input->post("search_store_id");
        $search_status = $this->input->post("search_status");
        $search_date_range = $this->input->post("search_date_range");

        $display_columns_sort =
            array(
                "#",
                "CHECKBOX",
                "id",
                'discount',
                'transaction_id'
            );

        $display_columns =
            array(
                "#",
                'order-id',
                'order-date',
                'order-status',
                'order-total',
                'order-action',

                "CHECKBOX",
                "id",
                'discount',
                'transaction_id'
            );
        $search_columns = array('coupon_code','transaction_id','ecommerce_cart.id');

        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $start = isset($_POST['start']) ? intval($_POST['start']) : 0;
        $limit = isset($_POST['length']) ? intval($_POST['length']) : 10;
        $sort_index = isset($_POST['order'][0]['column']) ? strval($_POST['order'][0]['column']) : 2;
        $sort = isset($display_columns_sort[$sort_index]) ? $display_columns_sort[$sort_index] : 'ecommerce_cart.id';
        $order = isset($_POST['order'][0]['dir']) ? strval($_POST['order'][0]['dir']) : 'desc';
        $order_by=$sort." ".$order;

        if($search_status!="") $this->db->where(array("ecommerce_cart.status"=>$search_status));
        $where_custom="action_type='checkout' AND ecommerce_cart.subscriber_id = '".$subscriber_id."' AND store_id=".$store_id;

        if($search_value != '')
        {
            foreach ($search_columns as $key => $value)
                $temp[] = $value." LIKE "."'%$search_value%'";
            $imp = implode(" OR ", $temp);
            $where_custom .=" AND (".$imp.") ";
        }
        if($search_date_range!="")
        {
            $exp = explode('|', $search_date_range);
            $from_date = isset($exp[0])?$exp[0]:"";
            $to_date = isset($exp[1])?$exp[1]:"";
            if($from_date!="Invalid date" && $to_date!="Invalid date")
                $where_custom .= " AND ecommerce_cart.updated_at >= '{$from_date}' AND ecommerce_cart.updated_at <='{$to_date}'";
        }
        $this->db->where($where_custom);

        $table="ecommerce_cart";
        $select = "ecommerce_cart.id,action_type,ecommerce_cart.user_id,store_id,subscriber_id,coupon_code,coupon_type,discount,payment_amount,currency,ordered_at,transaction_id,card_ending,payment_method,manual_additional_info,manual_filename,paid_at,ecommerce_cart.status,ecommerce_cart.updated_at,ecommerce_store.store_name,status_changed_note";
        $join = array('ecommerce_store'=>"ecommerce_store.id=ecommerce_cart.store_id,left");
        $info=$this->basic->get_data($table,$where='',$select,$join,$limit,$start,$order_by,$group_by='');

        if($search_status!="") $this->db->where(array("ecommerce_cart.status"=>$search_status));
        $this->db->where($where_custom);
        $total_rows_array=$this->basic->count_row($table,$where='',$count=$table.".id",$join,$group_by='');

        $total_result=$total_rows_array[0]['total_rows'];

        $payment_status = $this->get_payment_status();
        $time=0;
        $currency_position = "left";
        $decimal_point = 0;
        $thousand_comma = '0';
        foreach($info as $key => $value)
        {
            $time++;
            if($time==1)
            {
                $ecommerce_config = $this->get_ecommerce_config($value["store_id"]);
                $currency_position = isset($ecommerce_config['currency_position']) ? $ecommerce_config['currency_position'] : "left";
                $decimal_point = isset($ecommerce_config['decimal_point']) ? $ecommerce_config['decimal_point'] : 0;
                $thousand_comma = isset($ecommerce_config['thousand_comma']) ? $ecommerce_config['thousand_comma'] : '0';
            }

            $config_currency = isset($value['currency']) ? $value['currency'] : "USD";

            if($value['coupon_code']!='')
                $info[$key]['discount']= mec_number_format($info[$key]['discount'],$decimal_point,$thousand_comma);
            else $info[$key]['discount'] = "";

            $payment_amount = $value['currency']." ".mec_number_format($info[$key]['payment_amount'],$decimal_point,$thousand_comma);

            if($info[$key]['payment_method'] == 'Cash on Delivery') $pay = "Cash";
            else $pay = $info[$key]['payment_method'];

            $payment_method = $pay." ".$info[$key]['card_ending'];
            if(trim($payment_method)=="") $payment_method = "x";

            $transaction_id = ($info[$key]['transaction_id']!="") ? "<b class='text-primary'>".$info[$key]['transaction_id']."</b>" : "x";

            $updated_at = date("M j,y H:i",strtotime($info[$key]['updated_at']));

            if($value["paid_at"]!='0000-00-00 00:00:00')
            {
                $paid_at = date("M j, y H:i",strtotime($info[$key]['paid_at']));
            }
            else $paid_at = 'x';

            $st1=$st2="";
            $file = base_url('upload/ecommerce/'.$value['manual_filename']);
            $st1 = ($value['payment_method']=='Manual') ? $this->handle_attachment($value['id'], $file,true):"";

            if($value['payment_method']=='Manual')
                $st2 = '<a data-id="'.$value['id'].'" href="#" class="dropdown-item has-icon additional_info" itle="" data-original-title="'.$this->lang->line("Additional Info").'"><i class="fas fa-info-circle"></i> '.$this->lang->line("Payment Info").'</a>';

            if($value["action_type"]=="checkout") $invoice =  $this->return_base_url_php("ecommerce/order/".$value['id']);
            else $invoice =  $this->return_base_url_php("ecommerce/cart/".$value['id']);
            $invoice = mec_add_get_param($invoice,array("subscriber_id"=>$subscriber_id,"pickup"=>$pickup));

            $payment_status = $info[$key]['status'];

            if($payment_status=='pending') $payment_status_badge = "<span class='text-danger'>".$this->lang->line("Pending")."</span>";
            else if($payment_status=='approved') $payment_status_badge = "<span class='text-primary'>".$this->lang->line("Approved")."</span>";
            else if($payment_status=='rejected') $payment_status_badge = "<span class='text-danger'>".$this->lang->line("Rejected")."</span>";
            else if($payment_status=='shipped') $payment_status_badge = "<span class='text-info'>".$this->lang->line("Shipped")."</span>";
            else if($payment_status=='delivered') $payment_status_badge = "<span class='text-info'>".$this->lang->line("Delivered")."</span>";
            else if($payment_status=='completed') $payment_status_badge = "<span class='text-success'>".$this->lang->line("Completed")."</span>";

            $payment_status_note = ($info[$key]['status_changed_note']!='') ? htmlspecialchars($info[$key]['status_changed_note']) : "";

            $info[$key]['order-id'] = '<a class="text-job text-primary" href="'.$invoice.'">'." #".$value["id"]."</a>";
            $info[$key]['order-date'] = $updated_at;
            $info[$key]['order-status'] = $payment_status_badge;
            $info[$key]['order-total'] = $payment_amount;
            $info[$key]['order-action'] = '<div style="display:flex; min-width: 200px;flex-direction: column;"><a href="'.$invoice.'" class="dropdown-item has-icon"><i class="fas fa-receipt"></i> '.$this->lang->line("Invoice").'</a>'.$st1.$st2.'</div>';

//            var_dump($info);
//            $info[$key]['my_data'] = '
//    <div class="activities">
//    <div class="activity">
//    <div class="activity-detail w-100 mb-2">
//    <div class="mb-2">
//    <span class="text-job">'.$updated_at.'</span>
//    <span class="bullet"></span>
//    <a class="text-job text-primary" href="'.$invoice.'">'." #".$value["id"]." (".$payment_amount.')</a>
//    <div class="float-right dropdown ml-3">
//    <a href="#" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
//    <div class="dropdown-menu">
//    <div class="dropdown-title">'.$this->lang->line("Options").'</div>
//    <a href="'.$invoice.'" class="dropdown-item has-icon"><i class="fas fa-receipt"></i> '.$this->lang->line("Invoice").'</a>
//    '.$st1.'
//    '.$st2.'
//    </div>
//    </div>
//    <span class="float-right text-small">'.$payment_status_badge.'</span>
//    </div>
//    <p>'.$payment_status_note.'</p>
//    </div>
//    </div>
//    </div>';


        }
        $data['draw'] = (int)$_POST['draw'] + 1;
        $data['recordsTotal'] = $total_result;
        $data['recordsFiltered'] = $total_result;
        $data['data'] = convertDataTableResult($info, $display_columns ,$start,$primary_key="id");
        echo json_encode($data);
    }

    public function order($id=0) // if $id passed means not ajax, it's loading view
    {
        $pickup = isset($_GET['pickup']) ? $_GET['pickup'] : '';
        $this->ecommerce_review_comment_exist = $this->ecommerce_review_comment_exist();
        $store_data_temp = $this->basic->get_data("ecommerce_cart",array("where"=>array("id"=>$id)),"store_id");
        $store_id_temp = isset($store_data_temp[0]['store_id']) ? $store_data_temp[0]['store_id'] : "0";

        $is_ajax = $this->input->post('is_ajax',true);
        if($id==0) $id = $this->input->post('webhook_id',true);
        $subscriber_id = "";

        if($is_ajax=='1') // ajax call | means it's being loaded inside xerochat admin panel
        {
            $this->ajax_check();
            if($this->session->userdata('logged_in')!= 1){
                echo '<div class="alert alert-danger text-center">'.$this->lang->line("Access Forbidden").'</div>';
                exit();
            }
        }
        else // view load
        {
            $subscriber_id = $this->session->userdata($store_id_temp."ecom_session_subscriber_id");
            if($subscriber_id=="") $subscriber_id = $this->input->get("subscriber_id",true); // if loaded via webview then we will get this
        }

        if($subscriber_id=="" && $this->session->userdata('logged_in')!= 1 AND !empty($store_data_temp))
        {
            //echo $this->login_to_continue;
            redirect($this->return_base_url_php('ecommerce/login_signup/'.$this->get_store_uq($store_data_temp[0]['store_id'])),'location');
            exit();
        }

        $select2 = array("ecommerce_cart.*","first_name","last_name","full_name","profile_pic","user_location","email","image_path","phone_number","store_name","store_type","store_email","store_favicon","store_phone","store_logo","store_address","store_zip","store_city","store_country","store_state","store_unique_id","terms_use_link","refund_policy_link","store_locale","pixel_id","google_id");
        $join2 = array('messenger_bot_subscriber'=>"messenger_bot_subscriber.subscribe_id=ecommerce_cart.subscriber_id,left",'ecommerce_store'=>"ecommerce_store.id=ecommerce_cart.store_id,left");
        $where_simple2 = array("ecommerce_cart.id"=>$id);
        if($subscriber_id!="") $where_simple2['ecommerce_cart.subscriber_id'] = $subscriber_id;
        else $where_simple2['ecommerce_cart.user_id'] = $this->user_id;
        $where2 = array('where'=>$where_simple2);
        $webhook_data = $this->basic->get_data("ecommerce_cart",$where2,$select2,$join2);

        if(!isset($webhook_data[0]))
        {
            $not_found = $this->lang->line("Sorry, we could not find the order you are looking for.");
            if($is_ajax=='1') echo '<div class="alert alert-danger text-center">'.$not_found.'</div>';
            else echo '<br/><h2 style="border:1px solid red;padding:15px;color:red">'.$not_found.'</h2>';
            exit();
        }
        $webhook_data_final = $webhook_data[0];
        if($is_ajax!='1')$this->_language_loader($webhook_data_final['store_locale']);
        $country_names = $this->get_country_names();
        $currency_icons = $this->currency_icon();
        $order_title = $this->lang->line("Order");

        $ecommerce_config = $this->get_ecommerce_config($webhook_data_final['store_id']);
        $currency_position = isset($ecommerce_config['currency_position']) ? $ecommerce_config['currency_position'] : "left";
        $decimal_point = isset($ecommerce_config['decimal_point']) ? $ecommerce_config['decimal_point'] : 0;
        $thousand_comma = isset($ecommerce_config['thousand_comma']) ? $ecommerce_config['thousand_comma'] : '0';
        $whatsapp_send_order_button = isset($ecommerce_config['whatsapp_send_order_button']) ? $ecommerce_config['whatsapp_send_order_button'] : '0';
        $whatsapp_phone_number = isset($ecommerce_config['whatsapp_phone_number']) ? $ecommerce_config['whatsapp_phone_number'] : '';
        $whatsapp_send_order_text = isset($ecommerce_config['whatsapp_send_order_text']) ? $ecommerce_config['whatsapp_send_order_text']:"";
        // echo "<pre>"; print_r($ecommerce_config); exit;


        $join = array('ecommerce_product'=>"ecommerce_product.id=ecommerce_cart_item.product_id,left");
        $product_list = $this->basic->get_data("ecommerce_cart_item",array('where'=>array("cart_id"=>$id)),array("ecommerce_cart_item.*","product_name","thumbnail","taxable","woocommerce_product_id"),$join);

        $order_date = date("jS M,Y",strtotime($webhook_data_final['updated_at']));
        $order_date2 = date("d M,y H:i",strtotime($webhook_data_final['updated_at']));
        $wc_first_name = $webhook_data_final['first_name'];
        $wc_last_name = $webhook_data_final['last_name'];
        $wc_buyer_bill = ($webhook_data_final['bill_first_name']!='') ? $webhook_data_final['bill_first_name']." ".$webhook_data_final['bill_last_name'] : $wc_first_name." ".$wc_last_name;
        $confirmation_response = json_decode($webhook_data_final['confirmation_response'],true);
        $currency = $webhook_data_final['currency'];
        $currency_icon = isset($currency_icons[$currency])?$currency_icons[$currency]:'$';
        $wc_email_bill = $webhook_data_final['bill_email'];
        $wc_phone_bill = $webhook_data_final['bill_mobile'];
        $shipping_cost = $webhook_data_final["shipping"];
        $total_tax = $webhook_data_final["tax"];
        $checkout_amount  = $webhook_data_final['payment_amount'];
        $coupon_code = $webhook_data_final['coupon_code'];
        $coupon_type = $webhook_data_final['coupon_type'];
        $coupon_amount =  $webhook_data_final['discount'];
        $subtotal =  $webhook_data_final['subtotal'];
        $payment_status = $webhook_data_final['status'];
        $currency_left = $currency_right = "";
        if($currency_position=='left') $currency_left = $currency_icon;
        if($currency_position=='right') $currency_right = $currency_icon;

        $payment_method =  $webhook_data_final['payment_method'];
        if($payment_method!='') $payment_method =  $payment_method." ".$webhook_data_final['card_ending'];

        if($payment_status=='pending' && $webhook_data_final['action_type']=='checkout') $payment_status_badge = "<span class='text-danger'><i class='fas fa-spinner'></i> ".$this->lang->line("Pending")."</span>";
        else if($payment_status=='approved') $payment_status_badge = "<span class='text-primary'><i class='fas fa-thumbs-up'></i> ".$this->lang->line("Approved")."</span>";
        else if($payment_status=='rejected') $payment_status_badge = "<span class='text-warning'><i class='fas fa-thumbs-down'></i> ".$this->lang->line("Rejected")."</span>";
        else if($payment_status=='shipped') $payment_status_badge = "<span class='text-info'><i class='fas fa-truck'></i> ".$this->lang->line("Shipped")."</span>";
        else if($payment_status=='delivered') $payment_status_badge = "<span class=text-info'><i class='fas fa-truck-loading'></i> ".$this->lang->line("Delivered")."</span>";
        else if($payment_status=='completed') $payment_status_badge = "<span class='text-success'><i class='fas fa-check-circle'></i> ".$this->lang->line("Completed")."</span>";
        else $payment_status_badge = $payment_status_badge = "<span class='text-danger'><i class='fas fa-times'></i> ".$this->lang->line("Incomplete")."</span>";

        $order_no =  $webhook_data_final['id'];
        $order_url =  base_url("ecommerce/order/".$order_no);

        $buyer_country = isset($country_names[$webhook_data_final["buyer_country"]]) ? ucwords(strtolower($country_names[$webhook_data_final["buyer_country"]])) : $webhook_data_final["buyer_country"];
        $store_country = isset($country_names[$webhook_data_final["store_country"]]) ? ucwords(strtolower($country_names[$webhook_data_final["store_country"]])) : $webhook_data_final["store_country"];

        $tmp_buter_state = $webhook_data_final["buyer_state"];
        if(!empty($webhook_data_final["buyer_zip"])) $tmp_buter_state = $tmp_buter_state.' '.$webhook_data_final["buyer_zip"];
        $buyer_address_array = array($webhook_data_final["buyer_address"],$webhook_data_final["buyer_city"],$tmp_buter_state,$buyer_country);
        $buyer_address_array =array_filter($buyer_address_array);
        $buyer_address =implode('<br>', $buyer_address_array);
        $store_name = $webhook_data_final['store_name'];
        $store_type = $webhook_data_final['store_type'];
        $store_address = $webhook_data_final["store_address"]."<br>".$webhook_data_final["store_city"]."<br>".$webhook_data_final["store_state"]." ".$webhook_data_final["store_zip"]."<br>".$store_country;
        $store_phone = $webhook_data_final["store_phone"];
        $store_email = $webhook_data_final["store_email"];
        $subscriber_id_database = $webhook_data_final["subscriber_id"];
        $store_unique_id = $webhook_data_final["store_unique_id"];
        $store_address2 = $webhook_data_final["store_address"]."<br>".$webhook_data_final["store_city"].", ".$webhook_data_final["store_state"].", ".$store_country."<br>".$store_phone;

        $table_bordered = ($is_ajax=='1') ? '' : 'table-bordered';
        $table_data ='<table class="shop_table cart-totals mb-5">
										<tbody>											<tr>
												<td colspan="2" class="border-top-0">
													<strong class="text-color-dark">'.$this->lang->line('Product').'</strong>
												</td>
											</tr>';
        $i=0;
        $subtotal_count = 0;
        $table_data_print = '
    <table>
    <thead>
    <tr>
    <th class="description">'.$this->lang->line("Item").'</th>
    <th class="price">'.$this->lang->line("Price").'</th>
    </tr>
    </thead>
    <tbody>';

        $whatsapp_message_product_info = array();
        foreach ($product_list as $key => $value)
        {
            $title = isset($value['product_name']) ? $value['product_name'] : "";
            $quantity = isset($value['quantity']) ? $value['quantity'] : 1;
            $price = isset($value['unit_price']) ? $value['unit_price'] : 0;
            $item_total = $price*$quantity;
            $subtotal_count+=$item_total;
            $item_total = mec_number_format($item_total,$decimal_point,$thousand_comma);
            $price =  mec_number_format($price,$decimal_point,$thousand_comma);

            $image_url = (isset($value['thumbnail']) && !empty($value['thumbnail'])) ? base_url('upload/ecommerce/'.$value['thumbnail']) : base_url('assets/img/example-image.jpg');
            if(isset($value["woocommerce_product_id"]) && !is_null($value["woocommerce_product_id"]) && isset($value['thumbnail']) && !empty($value['thumbnail']))
                $image_url = $value["thumbnail"];

            $permalink = $this->return_base_url_php("ecommerce/product/".$value['product_id']);
            $attribute_info = (is_array(json_decode($value["attribute_info"],true))) ? json_decode($value["attribute_info"],true) : array();

            $attribute_query_string_array = array();
            $attribute_query_string = "";
            foreach ($attribute_info as $key2 => $value2)
            {
                $urlencode = is_array($value2) ? implode(',', $value2) : $value2;
                $attribute_query_string_array[]="option".$key2."=".urlencode($urlencode);
            }
            $attribute_query_string = implode("&", $attribute_query_string_array);
            if(!empty($attribute_query_string_array)) $attribute_query_string = "&quantity=".$quantity."&".$attribute_query_string;

            $attribute_print = "";
            $attribute_print_for_whatsapp = "";
            if(!empty($attribute_info))
            {
                $attribute_print_tmp = array();
                foreach ($attribute_info as $key2 => $value2)
                {
                    $attribute_print_tmp[] = is_array($value2) ? implode('+', array_values($value2)) : $value2;
                }
                $attribute_print = "<small class='text-muted'>".implode(', ', $attribute_print_tmp)."</small>";
                $attribute_print_for_whatsapp = implode(', ', $attribute_print_tmp);
            }

            if(!empty($attribute_print_for_whatsapp)) {
                $attribute_print_for_whatsapp = ' ('.$attribute_print_for_whatsapp.')';
            }
            // if($subscriber_id!='') $permalink.="?subscriber_id=".$subscriber_id.$attribute_query_string;

            if($subscriber_id!='' || $pickup!="")
                $permalink = mec_add_get_param($permalink,array("subscriber_id"=>$subscriber_id,"pickup"=>$pickup)).$attribute_query_string;

            // for whatsapp send order message
            $product_info = $title.$attribute_print_for_whatsapp.' - '.$quantity.' piece'.' - '.$currency_icon.$price;
            array_push($whatsapp_message_product_info, $product_info);
            // for whatsapp send order message
            if(!empty($currency_left)){$currency_left = $currency_left.' ';}

            $i++;
            $off = $value["coupon_info"];
            if($off!="") $off.=" ".$this->lang->line("OFF");
            $table_data .='											<tr>
												<td> <a href="'.$permalink.'" class="d-print-none-thermal d-print-none">
													<strong class="d-block text-color-dark line-height-1 font-weight-semibold">'.$title.'<span class="product-qty"> x'.$quantity.'</span></strong></a>
													<span class="text-1 text-uppercase">'.$attribute_print.' '.$currency_left.$price.$currency_right.'</span>
													
												</td>
												<td class="text-right align-top">
													<strong class="amount font-weight-medium text-uppercase">'.$currency_left.$item_total.$currency_right.'</strong>
												</td>
											</tr>';


            $table_data_print .= '
      <tr>
      <th class="description">'.$title.' ('.$quantity.')<br><small>'.$attribute_print.'</small></th>
      <th class="price">'.$currency_left.$item_total.$currency_right.'</th>
      </tr>';
        }

        $table_data_print .= '</tbody></table>';

        if($coupon_code=="") $coupon_info = "";
        else $coupon_info = '<div class="section-title">'.$this->lang->line("Coupon").' : '.$coupon_code.'</div>';

        $coupon_info2 = "";
        if($coupon_code!='' && $coupon_type=="fixed cart")
            $coupon_info2 =
                '<div class="invoice-detail-item">
    <div class="invoice-detail-name">'.$this->lang->line("Discount").'</div>
    <div class="invoice-detail-value text-uppercase">-'.$currency_left.mec_number_format($coupon_amount,$decimal_point,$thousand_comma).$currency_right.'</div>
    </div>';

        $tax_info = "";
        if($total_tax>0)
            $tax_info =
                '<div class="invoice-detail-item">
    <div class="invoice-detail-name">'.$this->lang->line("Tax").'</div>
    <div class="invoice-detail-value text-uppercase">+'.$currency_left.mec_number_format($total_tax,$decimal_point,$thousand_comma).$currency_right.'</div>
    </div>';

        $shipping_info = "";
        if($shipping_cost>0)
            $shipping_info =
                '<div class="invoice-detail-item">
    <div class="invoice-detail-name">'.$this->lang->line("Delivery Charge").'</div>
    <div class="invoice-detail-value text-uppercase">+'.$currency_left.mec_number_format($shipping_cost,$decimal_point,$thousand_comma).$currency_right.'</div>
    </div>';


        // $coupon_code." (".$currency_icon.$coupon_amount.")";

        if($webhook_data_final['action_type']!='checkout') $subtotal = $subtotal_count;
        $subtotal = mec_number_format($subtotal,$decimal_point,$thousand_comma);
        $checkout_amount = mec_number_format($checkout_amount,$decimal_point,$thousand_comma);
        $coupon_amount = mec_number_format($coupon_amount,$decimal_point,$thousand_comma);

        if($subscriber_id=='')
        {
            $wc_buyer_bill_formatted = '<a href="'.base_url('subscriber_manager/bot_subscribers/'.$subscriber_id_database).'">'.$wc_buyer_bill.'</a>'; //todo: bad url
            $store_name_formatted = '<a href="'.$this->return_base_url_php('ecommerce/store/'.$store_unique_id).'">'.$store_name.'</a>';
            $store_image = ($webhook_data_final['store_logo']!='' && $is_ajax!='1') ? '<div class="col-lg-12 text-center d-print-none-thermal"><a href="'.$this->return_base_url_php('ecommerce/store/'.$store_unique_id).'"><img style="max-height:50px" src="'.base_url("upload/ecommerce/".$webhook_data_final['store_logo']).'"></a><hr class="m-3 mb-4"></div>':'';
        }
        else
        {
            $wc_buyer_bill_formatted = $wc_buyer_bill;
            $tempu = $this->return_base_url_php('ecommerce/store/'.$store_unique_id);
            $tempu = mec_add_get_param($tempu,array("subscriber_id"=>$subscriber_id,"pickup"=>$pickup));
            $store_name_formatted = '<a href="'.$tempu.'">'.$store_name.'</a>';
            $store_image = ($webhook_data_final['store_logo']!='' && $is_ajax!='1') ? '<div class="col-lg-12 text-center d-print-none-thermal"><a href="'.$tempu.'"><img style="max-height:50px" src="'.base_url("upload/ecommerce/".$webhook_data_final['store_logo']).'"></a><hr class="m-3 mb-4"></div>':'';
        }

        if($is_ajax=='1') $order_details = '<h5>'.$order_title.' #<a href="'.$order_url.'">'.$order_no.'</a></h5>';
        else $order_details = '<h5>'.$order_title.' #'.$order_no.'</h5>';


        $order_details_print = '<h5>#'.$order_no.' ('.$order_date2.')</h5>';


        $output = "";
        $coupon_details_print = "";
        $after_checkout_details ="";
        $payment_method_deatils = '';

        $coupon_details = '<div class="col-7">'.$payment_method_deatils.'</div>';

        $hide_on_modal = '';
        $hide_on_modal_md = 'd-sm-block';
        if($is_ajax=='1'){
            $hide_on_modal = 'd-none';
            $hide_on_modal_md = 'd-none';
        }

        if($webhook_data_final['action_type']=='checkout')
        {
            $after_checkout_details =
                $coupon_info2.$shipping_info.$tax_info.'
      <hr class="mt-2 mb-2">
      <div class="invoice-detail-item">
      <div class="invoice-detail-name">'.$this->lang->line("Total").'</div>
      <div class="invoice-detail-value text-uppercase">('.$currency.') '.$currency_left.$checkout_amount.$currency_right.'</div>
      </div>';

            $delivery_note = !empty($webhook_data_final['delivery_note']) ? "<br>(".$webhook_data_final['delivery_note'].")":"";
            $receipt_name = !empty($webhook_data_final['buyer_first_name']) ? $webhook_data_final['buyer_first_name']." ".$webhook_data_final['buyer_last_name'] : $wc_buyer_bill;
            $recipt_address = $webhook_data_final['store_pickup']=='1' ? $webhook_data_final['pickup_point_details'] : $buyer_address;
            $contact_address = $webhook_data_final["buyer_email"];
            if(!empty($webhook_data_final["buyer_mobile"])) $contact_address.=' , '.$webhook_data_final["buyer_mobile"];
            $coupon_details =
                '<div class="col-7 d-print-none-thermal">
      '.$coupon_info.$payment_method_deatils.'
      <div class="title">'.$this->lang->line("Deliver to").'</div>
      <p class="section-lead ml-0">
      '.$receipt_name."<br>".$recipt_address.'<br>'.$contact_address.'<small>'.$delivery_note.'</small>
      </p>  
      </div>';

            $coupon_details_print =
                '<div class="d-print-thermal '.$hide_on_modal.'">'.$order_details_print.'</div>
      <div class="d-print-thermal '.$hide_on_modal.'">
      <p class="section-lead m-0 text-center small">
      '.$store_address2.'
      </p>
      <br>
      <p class="section-lead m-0 text-left">
      '.$this->lang->line("Customer")." : ".$receipt_name."<br>".$recipt_address.'
      </p>  
      </div>';
        }
        $padding = ($is_ajax=='1') ? "padding:40px" : "padding:25px;margin:20px 0;";

        $user_loc = "";
        if($webhook_data_final['bill_first_name']=='')
        {
            $tmp = json_decode($webhook_data_final['user_location'],true);
            if(is_array($tmp))
            {
                $user_country = isset($tmp['country']) ? $tmp['country'] : "";
                $country_name = isset($country_names[$user_country]) ? ucwords(strtolower($country_names[$user_country])) : $user_country;
                $tmp["country"] = $country_name;
                if(isset($tmp["state"]) && isset($tmp["zip"]))
                {
                    $tmp["state"] = $tmp["state"]." ".$tmp["zip"];
                    unset($tmp["zip"]);
                }
                $user_loc = implode('<br>', $tmp);
            }
        }
        else
        {
            $user_country = isset($webhook_data_final['bill_country']) ? $webhook_data_final['bill_country'] : "";
            $country_name = isset($country_names[$user_country]) ? ucwords(strtolower($country_names[$user_country])) : $user_country;

            $tmp =  array($webhook_data_final['bill_address'],$webhook_data_final['bill_city'],$webhook_data_final['bill_state'],$country_name);
            if(isset($tmp["bill_state"]) || isset($tmp["bill_zip"]))  $tmp["bill_state"] = $tmp["bill_state"]." ".$tmp["bill_zip"];
            unset($tmp["bill_zip"]);
            $tmp = array_filter($tmp);
            $user_loc = implode('<br>', $tmp);
        }

        $pay_message = "";
        if($subscriber_id!="")
        {
            $payment_action = $this->input->get("action",true);
            $payment_status_message=$payment_status='';
            if($payment_action!="")
            {
                if($payment_action=="success")
                {
                    $invoice_link = $this->return_base_url_php("ecommerce/my_orders/".$webhook_data_final['store_id']);
                    $invoice_link = mec_add_get_param($invoice_link,array("subscriber_id"=>$subscriber_id,"pickup"=>$pickup));
                    $message = "<i class='fas fa-check-circle'></i> ".$this->lang->line('Your payment has been received successfully and order will be processed soon. It may take few seconds to change your payment status depending on PayPal request.');
                    $payment_status='1';
                    $payment_status_message=$message;
                }
                else if($payment_action=="success3")
                {
                    $invoice_link = $this->return_base_url_php("ecommerce/my_orders/".$webhook_data_final['store_id']);
                    $invoice_link = mec_add_get_param($invoice_link,array("subscriber_id"=>$subscriber_id,"pickup"=>$pickup));
                    $message = "<i class='fas fa-check-circle'></i> ".$this->lang->line('Your payment has been received successfully and order will be processed soon.');
                    $payment_status='1';
                    $payment_status_message=$message;
                }
                else if($payment_action=="success2")
                {
                    $invoice_link = $this->return_base_url_php("ecommerce/my_orders/".$webhook_data_final['store_id']);
                    $invoice_link = mec_add_get_param($invoice_link,array("subscriber_id"=>$subscriber_id,"pickup"=>$pickup));
                    $message = "<i class='fas fa-check-circle'></i> ".$this->lang->line('Your order has been placed successfully and is now being reviewed.');
                    $payment_status='1';
                    $payment_status_message=$message;
                }
                else if($payment_action=="cancel")
                {
                    $message = "<i class='fas fa-times-circle'></i> ".$this->lang->line('Payment was failed to process.');
                    $payment_status='0';
                    $payment_status_message=$message;
                }
            }

            if($payment_status=='1')
                $pay_message = "<div class='alert alert-success d-print-none-thermal text-center mt-2 mb-0 ml-0 mr-0'>".$payment_status_message."</div>";
            else if($payment_status=='0')
                $pay_message = "<div class='alert alert-danger d-print-none-thermal text-center mt-2 mb-0 ml-0 mr-0'>".$payment_status_message."</div>";
        }

        $hide_order = '';
        $no_order = '';
        if(count($product_list)==0)
        {
            $hide_order='d-none';
            $no_order = '
      <div class="col-12">
      <div class="empty-state">
      <img class="img-fluid" style="height: 300px" src="'.base_url('assets/img/drawkit/drawkit-full-stack-man-colour.svg').'" alt="image">
      <h2 class="mt-0">'.$this->lang->line("Cart is empty").'</h2>
      <p class="lead">'.$this->lang->line("There is no product added to cart.").'</p>
      </div>
      </div>
      ';
        }

        $print_button_text = ($this->session->userdata('user_id') != '') ? $this->lang->line("Large").' <b>A4</b>' : '<i class="fas fa-print"></i> '. $this->lang->line("Print");
        $thermal_hide = ($this->session->userdata('user_id') != '') ? '' : 'd-none';
        $go_back_hide = ($this->session->userdata('user_id') != '') ? 'd-none' : '';

        $tempu2 = $this->return_base_url_php('ecommerce/store/'.$store_unique_id);
        $tempu2 = mec_add_get_param($tempu2,array("subscriber_id"=>$subscriber_id,"pickup"=>$pickup));

        $output .=
            '<div class="d-print-none pt-2 text-center d-block m-auto '.$hide_on_modal_md.'">
   
    <p class="mb-0 '.$thermal_hide.'"><b>'.$this->lang->line("Print Options").'</b></p>
    <div class="text-center" role="group" aria-label="">
    <button type="button" id="large-print" class="btn-sm btn btn-outline-primary print-options no_radius">'.$print_button_text.'</button>
    <button type="button" id="thermal-print" class="'.$thermal_hide.' btn-sm btn btn-outline-primary print-options no_radius">'.$this->lang->line("Thermal").' <b>80mm</b></button>
    <button type="button" id="mobile-print" class="'.$thermal_hide.' btn-sm btn btn-outline-primary print-options no_radius">'.$this->lang->line("Thermal").' <b>57mm</b></button>
    </div>
    </div>';

        $userid = $webhook_data_final['user_id'];
        $join222 = ['package'=>'users.package_id=package.id,left'];
        $select222 = ['users.id AS userid','users.user_type','users.package_id','package.*'];
        $get_user_info = $this->basic->get_data("users",['where'=>['users.id'=>$userid]],$select222,$join222);
        $store_user_module_ids = isset($get_user_info[0]['module_ids']) ? explode(",", $get_user_info[0]['module_ids']): [];

        $output .= '<div class="text-center mt-3 ext_buttons">';
        // this section is for send order to whatsapp (start)
        if($this->basic->is_exist("modules",array("id"=>310))) {

            if((isset($get_user_info[0]) && $get_user_info[0]['user_type'] == 'Admin') || in_array(310,$store_user_module_ids)) {

                $product_names_string = implode("\r\n",$whatsapp_message_product_info);

                if($whatsapp_send_order_text == '') {
                    $whatsapp_send_order_text  = 'New Order #{{order_no}}

            Customer: {{customer_info}}

            {{product_info}}

            Order Status: {{order_status}}
            Order URL: {{order_url}}
            Payment Method: {{payment_method}}

            Tax: {{tax}}
            Total Price: {{total_price}}
            {{delivery_address}}';
                }

                if(isset($recipt_address))
                    $recipt_address = str_replace("<br>", "\r\n", $recipt_address);
                else
                    $recipt_address = "";

                $order_urrls = $this->return_base_url_php("ecommerce/order/".$order_no.'?subscriber_id='.$subscriber_id."&action=success2");
                $taxes = $currency_left.mec_number_format($total_tax,$decimal_point,$thousand_comma);

                $whatsapp_send_order_text = str_replace(array('{{order_no}}','{{customer_info}}','{{product_info}}','{{order_status}}','{{order_url}}','{{payment_method}}','{{tax}}','{{total_price}}','{{delivery_address}}'), array($order_no,$wc_buyer_bill,$product_names_string,$webhook_data_final['status'],$order_urrls,$payment_method,$taxes,$currency_icon.$checkout_amount,$recipt_address), $whatsapp_send_order_text);

                $output_js = '';

                // echo "<pre>"; print_r($whatsapp_send_order_text); exit;
                if($whatsapp_send_order_button == '1' && $this->session->userdata('logged_in')!= 1) {
                    $output .= '<div class="send_order_whatsapp mr-1 d-inline"></div>';
                    $output_js .= "<script>
            var is_mobile = false;
            var whatsapp_button = '';
            $(document).ready(function() {
              if( $('#device-check').css('display')=='none') {
                is_mobile = true;
                whatsapp_button = '<a href=\"whatsapp://send/?phone=".$whatsapp_phone_number."&text=".urlencode($whatsapp_send_order_text)."\" class=\"btn btn-success\"><i class=\"fab fa-whatsapp\"></i> ".$this->lang->line('Send Order In WhatsApp')."</a>';
                } else {
                  whatsapp_button = '<a href=\"https://api.whatsapp.com/send?phone=phone=".$whatsapp_phone_number."&text=".urlencode($whatsapp_send_order_text)."\" class=\"btn btn-success\"><i class=\"fab fa-whatsapp\"></i> ".$this->lang->line('Send Order In WhatsApp')."</a>';
                }
                $('.send_order_whatsapp').html(whatsapp_button);
                });
                </script>";
                }
            }
        }
        // this section is for send order to whatsapp (end)

        if(isset($store_type) && $store_type == 'digital') {
            if($this->basic->is_exist("modules",array("id"=>316))) {
                if((isset($get_user_info[0]) && $get_user_info[0]['user_type'] == 'Admin') || in_array(316,$store_user_module_ids)) {
                    if($this->session->userdata('logged_in')!= 1) {
                        $output .= '<div class="ml-1 d-inline"><a target="_BLANK" href="'.base_url("ecommerce/digital_product_orders/").$store_id_temp.'/'.$order_no.'?subscriber_id='.$subscriber_id.'" class="btn btn-primary"><i class="fas fa-cloud-download-alt"></i> Download Orders</a></div>';
                    }
                }

            }
        }
        $output .= '</div>';

        $output .= '<section class="section" id="print-area">
        '.$pay_message.'
        <div class="section-body">
        <div class="invoice" style="border:1px solid #dee2e6;'.$padding.'">
        <div class="invoice-print">
        <div class="row">
        '.$store_image.'
        <h4 class="d-print-thermal '.$hide_on_modal.'">'.$store_name.'</h4>
        '.$no_order.'
        <div class="col-lg-12 '.$hide_order.'">
        <div class="invoice-title d-print-none-thermal">
        <div class="d-md-flex justify-content-between py-3 px-4 my-4  d-print-none-thermal">
								<div class="text-center">
									<span>
										'.$this->lang->line('Order Number').' <br>
										<strong class="text-color-dark">'.$order_details.'</strong>
									</span>
								</div>
								<div class="text-center mt-4 mt-md-0">
									<span>
										'.$this->lang->line('Date').' <br>
										<strong class="text-color-dark">'.$order_date.'</strong>
									</span>
								</div>
								<div class="text-center mt-4 mt-md-0">
									<span>
										'.$this->lang->line('Email').' <br>
										<strong class="text-color-dark">'.$wc_email_bill.'</strong>
									</span>
								</div>
								<div class="text-center mt-4 mt-md-0">
									<span>
										'.$this->lang->line('Total').' <br>
										<strong class="text-color-dark text-uppercase">('.$currency.') '.$currency_left.$checkout_amount.$currency_right.'</strong>
									</span>
								</div>
								<div class="text-center mt-4 mt-md-0">
									<span>
										'.$this->lang->line('Payment Method').' <br>
										<strong class="text-color-dark">'.$payment_status_badge.'<br>'.$payment_method.'</strong>
									</span>
								</div>
							</div>

        </div>
        <br class="d-print-none-thermal">
        <div class="row d-print-none-thermal">
        <div class="col-6">
        <address>
        <strong>'.$this->lang->line("Bill to").':</strong><br><br>
        '.$wc_buyer_bill_formatted.'
        <br>                        
        <span class="d-print-none-thermal">'.$user_loc.'<br>                     
        '.$wc_email_bill.'<br>                         
        '.$wc_phone_bill.'</span>
        </address>
        </div>
        <div class="col-6 text-right">
        <address>
        <strong>'.$this->lang->line("Seller").':</strong><br><br>
        '.$store_name_formatted.'<br>
        '.$store_address.'<br>
        </address>
        </div>
        </div>
        <div class="d-print-thermal">'.$coupon_details_print.'</div>
        </div>
        </div>

        <div class="row '.$hide_order.'">
        <div class="col-md-12">
        <span class="d-print-none-thermal">'.$table_data.'</tbody>
									</table></span>
        <span class="d-print-thermal '.$hide_on_modal.'">'.$table_data_print.'</span>
        <div class="row">
        '.$coupon_details.'
        <div class="col-5 text-right">
        <div class="invoice-detail-item"  style="margin-top: 20px;">
        <div class="invoice-detail-name">'.$this->lang->line("Subtotal").'</div>
        <div class="invoice-detail-value text-uppercase">'.$currency_left.$subtotal.$currency_right.'</div>
        </div>
        '.$after_checkout_details.'
        </div>
        </div>
        </div>
        </div>
        </div>              
        </div>
        </div>
        </section>';
        if(!$is_ajax) $output.="<div style='height:60px'></div>";

        if($webhook_data_final['action_type']=='checkout' && $is_ajax=='1')
        {
            $messenger_confirmation_badge = '<span class="badge badge-light badge-pill">'.$this->lang->line("Unknown").'</span>';
            if(isset($confirmation_response['messenger']))
            {
                if(isset($confirmation_response['messenger']['status']) && $confirmation_response['messenger']['status']=='1') $messenger_confirmation_badge = '<span data-toggle="tooltip" title="'.htmlspecialchars($confirmation_response['messenger']['response']).'" class="badge badge-success badge-pill">'.$this->lang->line("Sent").'</span>';
                else if(isset($confirmation_response['messenger']['status']) && $confirmation_response['messenger']['status']=='0') $messenger_confirmation_badge = '<span data-toggle="tooltip" title="'.htmlspecialchars($confirmation_response['messenger']['response']).'" class="badge badge-danger badge-pill">'.$this->lang->line("Error").'</span>';
                else $messenger_confirmation_badge = '<span class="badge badge-dark badge-pill">'.$this->lang->line("Not Set").'</span>';
            }
            $messenger_li = '<li class="list-group-item d-flex justify-content-between align-items-center">
          '.$this->lang->line("Messenger Confirmation").'
          '.$messenger_confirmation_badge.'
          </li>';

            $sms_li = $email_li = "";
            if($this->session->userdata('user_type') == 'Admin' || in_array(264,$this->module_access))
            {
                $sms_confirmation_badge = '<span class="badge badge-light badge-pill">'.$this->lang->line("Unknown").'</span>';
                if(isset($confirmation_response['sms']))
                {
                    if(isset($confirmation_response['sms']['status']) && $confirmation_response['sms']['status']=='1') $sms_confirmation_badge = '<span data-toggle="tooltip" title="'.htmlspecialchars($confirmation_response['sms']['response']).'" class="badge badge-success badge-pill">'.$this->lang->line("Sent").'</span>';
                    else if(isset($confirmation_response['sms']['status']) && $confirmation_response['sms']['status']=='0') $sms_confirmation_badge = '<span data-toggle="tooltip" title="'.htmlspecialchars($confirmation_response['sms']['response']).'" class="badge badge-danger badge-pill">'.$this->lang->line("Error").'</span>';
                    else $sms_confirmation_badge = '<span class="badge badge-dark badge-pill">'.$this->lang->line("Not Set").'</span>';
                }
                $sms_li = '<li class="list-group-item d-flex justify-content-between align-items-center">
            '.$this->lang->line("SMS Confirmation").'
            '.$sms_confirmation_badge.'
            </li>';
            }

            if($this->session->userdata('user_type') == 'Admin' || in_array(263,$this->module_access))
            {
                $email_confirmation_badge = '<span class="badge badge-light badge-pill">'.$this->lang->line("Unknown").'</span>';
                if(isset($confirmation_response['email']))
                {
                    if(isset($confirmation_response['email']['status']) && $confirmation_response['email']['status']=='1') $email_confirmation_badge = '<span data-toggle="tooltip" title="'.htmlspecialchars($confirmation_response['email']['response']).'" class="badge badge-success badge-pill">'.$this->lang->line("Sent").'</span>';
                    else if(isset($confirmation_response['email']['status']) && $confirmation_response['email']['status']=='0') $email_confirmation_badge ='<span data-toggle="tooltip" title="'.htmlspecialchars($confirmation_response['email']['response']).'" class="badge badge-danger badge-pill">'. $this->lang->line("Error").'</span>';
                    else $email_confirmation_badge = '<span class="badge badge-dark badge-pill">'.$this->lang->line("Not Set").'</span>';
                }
                $email_li = '<li class="list-group-item d-flex justify-content-between align-items-center">
            '.$this->lang->line("Email Confirmation").'
            '.$email_confirmation_badge.'
            </li>';
            }
            $output .=
                '
          <section class="section">
          <div class="section-body">
          <div class="invoice" style="border:1px solid #dee2e6;">
          <div class="invoice-print">
          <div class="row">
          <div class="col-12">
          <div class="invoice-title">
          <h6>'.$this->lang->line("Checkout Confirmation").'</h6>
          <div class="invoice-number"></div>
          </div>
          <hr>
          <ul class="list-group">
          '.$messenger_li.$sms_li.$email_li.'
          </ul>
          </div>
          </div>              
          </div>
          </div>
          </div>
          </section>
          ';
            $output .=  "<script>$('[data-toggle=\"tooltip\"]').tooltip();</script>";
        }

        $output.="<style>.section .section-title{margin:20px 0 20px 0;}</style>";

        if($is_ajax=='1')
        {
            if($webhook_data_final['action_type']=='checkout') $report_where = array("where"=>array("cart_id"=>$id,"is_sent"=>"1"));
            else $report_where = array("where"=>array("cart_id"=>$id));

            $reminder_report_data = $this->basic->get_data("ecommerce_reminder_report",$report_where,'','','','','sent_at DESC');

            $tableBody = '';
            $trsl = 0;
            foreach ($reminder_report_data as $keyReport => $valueReport)
            {
                $trsl++;

                if($valueReport["is_sent"]=='1' && $valueReport["sent_at"] != "0000-00-00 00:00:00")
                    $sent_time_tmp = date("M j, y H:i",strtotime($valueReport["sent_at"]));
                else $sent_time_tmp = '<span class="text-muted">X<span>';

                $subscriber_id_tmp =  "<a href='".base_url("subscriber_manager/bot_subscribers/".$valueReport['subscriber_id'])."' target='_BLANK'>".$valueReport['subscriber_id']."</a>";
                $last_updated_at_tmp = date("M j, y H:i",strtotime($valueReport['last_updated_at']));

                $response_tmp =  "<a class='btn btn-sm btn-outline-primary woo_error_log' href='' data-id='".$valueReport['id']."'><i class='fas fa-plug'></i> ".$this->lang->line('Response')."</a>";
                $cart_id_tmp =  "<a target='_BLANK' href='".base_url('ecommerce/order/'.$valueReport['cart_id'])."'>".$this->lang->line('Order').'#'.$valueReport['cart_id']."</a>";

                $tableBody .= '
            <tr>
            <td>'.$trsl.'</td>
            <td class="text-center">'.$valueReport["last_completed_hour"].'</td>
            <td>'.$response_tmp.'</td>
            <td class="text-center">'.$sent_time_tmp.'</td>
            <td>'.$cart_id_tmp.'</td>
            </tr>';
            }
            if(empty($reminder_report_data)) $tableBody.='<tr><td class="text-center" colspan="5">'.$this->lang->line("No data found").'</td></tr>';

            if(count($product_list)>0) $output .= '
          <section class="section">
          <div class="section-body">
          <div class="invoice" style="border:1px solid #dee2e6;">
          <div class="invoice-print">
          <div class="row">
          <div class="col-12">
          <div class="invoice-title">
          <h6>'.$this->lang->line("Abandoned Cart Reminder Report").'</h6>
          <div class="invoice-number"></div>
          </div>
          <hr>
          <div class="data-card">
          <div class="table-responsive2">
          <table class="table table-bordered" id="myTableReport">
          <thead>
          <tr>
          <th>#</th>
          <th>'.$this->lang->line("Reminder Hour").'</th>
          <th>'.$this->lang->line("API Response").'</th>
          <th>'.$this->lang->line("Sent at").'</th>
          <th>'.$this->lang->line("Order").'</th>
          </tr>
          </thead>
          <tbody>'.$tableBody.'</tbody>
          </table>
          </div>
          </div>
          </div>
          </div>
          </div>
          </div>
          </div>
          </section>';

            if($webhook_data_final['action_type']=='checkout' && $is_ajax=='1')
            {
                $resp = json_decode($webhook_data_final['checkout_source_json'], true);
                $resp = "<pre>".var_export($resp,true)."</pre>";
                $output .= '
            <section class="section">
            <div class="section-body">
            <div class="invoice" style="border:1px solid #dee2e6;">
            <div class="invoice-print">
            <div class="row">
            <div class="col-12">
            <div class="invoice-title">
            <h6>'.$this->lang->line("Payment API Response").'</h6>
            <div class="invoice-number"></div>
            </div>
            <hr>
            '.$resp.'
            </div>
            </div>
            </div>
            </div>
            </div>
            </section>';
            }

            echo $output;
        }
        else
        {
            $fb_app_id = $this->get_app_id();
            $data = array('output'=>$output,"page_title"=>$store_name." | Order# ".$order_no,"fb_app_id"=>$fb_app_id,"favicon"=>base_url('upload/ecommerce/'.$webhook_data_final['store_favicon']));
            $data['current_cart'] = $this->get_current_cart($subscriber_id,$webhook_data_final['store_id']);
            $data['body'] = 'order';
            $data['current_cart'] = $this->get_current_cart($subscriber_id,$webhook_data_final['store_id']);
            $data['ecommerce_config'] = $this->get_ecommerce_config($webhook_data_final["store_id"]);

            $category_list = $this->get_category_list($webhook_data_final["store_id"],true);
            $cat_info=array();
            foreach($category_list as $value)
            {
                $cat_info[$value['id']] = $value['category_name'];
            }
            $data["category_list"] = $cat_info;
            $data["category_list_raw"] = $category_list;

            if(empty($output_js)){$output_js='';}
            $data['output_js'] = $output_js;

            $data["social_analytics_codes"] = $webhook_data_final;
//            include(APPPATH."views/ecommerce/common_style.php");
            $this->load->view('ecommerce/bare-theme', $data);
        }

    }

    function empty_cart($store_unique_id = 0){
        $store_unique_id = $this->get_store_id($store_unique_id);
        if($store_unique_id==0) exit();

        $store_data = $this->initial_load_store($store_unique_id);
        $n_eco_builder_config = $this->get_config();

        $where_subs = array();
        $n_subscriber_info = array();
        $subscriber_id=$this->session->userdata($store_data[0]['id']."ecom_session_subscriber_id");

        if(empty($subscriber_id)) $subscriber_id = $this->input->get("subscriber_id",true);
        if(empty($subscriber_id)) $where_subs = array("subscriber_type"=>"system","subscribe_id"=>$subscriber_id,"store_id"=>$store_data[0]['id']);
        else {

            if(empty($subscriber_id)) $subscriber_id = $this->input->get("subscriber_id",true);
            if(empty($subscriber_id)) $where_subs = array("subscriber_type!="=>"system","subscribe_id"=>$subscriber_id);
            $n_subscriber_info = $this->basic->get_data("messenger_bot_subscriber",array("where"=>$where_subs));
        }

        if(empty($subscriber_id)) $login_needed = true;
        else
        {
            $subscriber_info = $this->basic->count_row("messenger_bot_subscriber",array("where"=>$where_subs),"id");
            $n_subscriber_info = $this->basic->get_data("messenger_bot_subscriber",array("where"=>$where_subs));
            if($subscriber_info[0]['total_rows']==0) $login_needed = true;
            $subscriber_id = $n_subscriber_info[0]['id'];
        }



        if(empty($subscriber_id)) {
            //echo $this->login_to_continue;
            redirect($this->return_base_url_php('ecommerce/login_signup/'.$store_unique_id),'location');
            exit();
        }

        $fb_app_id = $this->get_app_id();
        $data = array(
            'body' => "empty_cart",
            "page_title"=> $store_data[0]['store_name']." | ".$this->lang->line("Cart"),
            "fb_app_id"=>$fb_app_id
        );
        $data["social_analytics_codes"] = $store_data[0];

        $category_list = $this->get_category_list($store_data[0]['id'],true);
        $cat_info=array();
        foreach($category_list as $value)
        {
            $cat_info[$value['id']] = $value['category_name'];
        }
        $data["category_list"] = $cat_info;
        $data["category_list_raw"] = $category_list;
        $data['subscriber_id'] = $subscriber_id;

        $this->load->view('ecommerce/bare-theme', $data);
    }

    function contact($store_unique_id = 0){
        $store_unique_id = $this->get_store_id($store_unique_id);
        if($store_unique_id==0) exit();

        $store_data = $this->initial_load_store($store_unique_id);
        $n_eco_builder_config = $this->get_config();

        $fb_app_id = $this->get_app_id();
        $data = array(
            'body' => "contact",
            "page_title"=> $store_data[0]['store_name']." | ".$this->lang->line("Contact"),
            "fb_app_id"=>$fb_app_id
        );
        $data["social_analytics_codes"] = $store_data[0];

        $category_list = $this->get_category_list($store_data[0]['id'],true);
        $cat_info=array();
        foreach($category_list as $value)
        {
            $cat_info[$value['id']] = $value['category_name'];
        }
        $data["category_list"] = $cat_info;
        $data["category_list_raw"] = $category_list;

        $import_section = 'false';
            if ($n_eco_builder_config['contact_page_on'] == 'true') {
                if (file_exists(APPPATH . 'n_eco_user/contact_page_' . $store_data[0]['id'] . '_p.php')) {
                    $import_section = APPPATH . 'n_eco_user/contact_page_' . $store_data[0]['id'] . '_p.php';
                }
            }

        $data['gjs']['gjs-html'] = '';
        $data['gjs']['gjs-components'] = '';
        $data['gjs']['gjs-assets'] = '';
        $data['gjs']['gjs-css'] = '';
        $data['gjs']['gjs-styles'] = '';

        if ($import_section != 'false') {
            $n_editor_data = file_get_contents($import_section);
            $data['gjs'] = json_decode($n_editor_data, true);
        }


        $this->load->view('ecommerce/bare-theme', $data);
    }

    function terms($store_unique_id = 0){
        $store_unique_id = $this->get_store_id($store_unique_id);
        if($store_unique_id==0) exit();

        $store_data = $this->initial_load_store($store_unique_id);
        $n_eco_builder_config = $this->get_config();

        $fb_app_id = $this->get_app_id();
        $data = array(
            'body' => "terms",
            "page_title"=> $store_data[0]['store_name']." | ".$this->lang->line("Terms"),
            "fb_app_id"=>$fb_app_id
        );
        $data["social_analytics_codes"] = $store_data[0];

        $category_list = $this->get_category_list($store_data[0]['id'],true);
        $cat_info=array();
        foreach($category_list as $value)
        {
            $cat_info[$value['id']] = $value['category_name'];
        }
        $data["category_list"] = $cat_info;
        $data["category_list_raw"] = $category_list;


        $this->load->view('ecommerce/bare-theme', $data);
    }

    function refund_policy($store_unique_id = 0){
        $store_unique_id = $this->get_store_id($store_unique_id);
        if($store_unique_id==0) exit();

        $store_data = $this->initial_load_store($store_unique_id);
        $n_eco_builder_config = $this->get_config();

        $fb_app_id = $this->get_app_id();
        $data = array(
            'body' => "refund_policy",
            "page_title"=> $store_data[0]['store_name']." | ".$this->lang->line("Refund policy"),
            "fb_app_id"=>$fb_app_id
        );
        $data["social_analytics_codes"] = $store_data[0];

        $category_list = $this->get_category_list($store_data[0]['id'],true);
        $cat_info=array();
        foreach($category_list as $value)
        {
            $cat_info[$value['id']] = $value['category_name'];
        }
        $data["category_list"] = $cat_info;
        $data["category_list_raw"] = $category_list;


        $this->load->view('ecommerce/bare-theme', $data);
    }

    private function check_login_eco($store_unique_id){
        $store_id_temp = $this->get_store_id($store_unique_id);
        $subscriber_id=$this->session->userdata($store_id_temp."ecom_session_subscriber_id");

        if($subscriber_id==""){
            $subscriber_id = $this->input->get("subscriber_id",true);
        }

        if($subscriber_id==""){
            redirect($this->return_base_url_php('ecommerce/login_signup'), 'location');
            exit();
        }
    }

    private function build_body_data(){

    }

    private function base_url_php($uri, $skip = false){
        if(file_exists(APPPATH.'custom_domain.php')) {
            include(APPPATH . 'custom_domain.php');
        }
        if(isset($_GET['builder']) AND $_GET['builder']==1 AND $skip==false){
            $uri = $uri.'?builder=1';
        }
        $custom = false;
        if($ncd_config['eco_custom_domain']=='true'){
            if($_SERVER['HTTP_HOST'] != $ncd_config['custom_domain_host']){
                $custom = true;
            }
        }
        if($custom==true){
            echo base_url(str_replace('ecommerce/','',$uri));
        }else{
            echo base_url($uri);
        }
    }

    private function return_base_url_php($uri, $skip = false){
        if(file_exists(APPPATH.'custom_domain.php')) {
            include(APPPATH . 'custom_domain.php');
        }
        if(isset($_GET['builder']) AND $_GET['builder']==1 AND $skip==false){
            $uri = $uri.'?builder=1';
        }
        $custom = false;
        if(isset($ncd_config['eco_custom_domain']) AND $ncd_config['eco_custom_domain']=='true'){
            if($_SERVER['HTTP_HOST'] != $ncd_config['custom_domain_host']){
                $custom = true;
            }
        }
        if($custom==true){
            return base_url(str_replace('ecommerce/','',$uri));
        }else{
            return base_url($uri);
        }
    }

    private function manual_payment_display_attachment($file,$format=false)
    {
        $output = '<div class="mp-display-img d-inline">';
        if($format)$output .= '<a data-image="' . $file . '" href="' . $file . '" class="dropdown-item has-icon mp-img-item"><i class="fas fa-image"></i> '.$this->lang->line("View Attachment").'</a>';
        else
        {
            $output .= '<a class="mp-img-item btn btn-outline-info" data-image="' . $file . '" href="' . $file . '">';
            $output .= '<i class="fa fa-image"></i>';
            $output .= '</a>';
        }
        $output .= '</div>';
        $output .= '<script>$(".mp-display-img").Chocolat({className: "mp-display-img", imageSelector: ".mp-img-item"});</script>';

        return $output;
    }

    private function handle_attachment($id, $file,$format=false)
    {
        $info = pathinfo($file);
        if (isset($info['extension']) && ! empty($info['extension'])) {
            switch (strtolower($info['extension'])) {
                case 'jpg':
                case 'jpeg':
                case 'png':
                case 'gif':
                    return $this->manual_payment_display_attachment($file,$format);
                case 'zip':
                case 'pdf':
                case 'txt':
                    if(!$format)return '<div data-id="' . $id . '" id="mp-download-file" class="btn btn-outline-info" data-toggle="tooltip" title="'.$this->lang->line("Attachment").'"><i class="fas fa-download"></i></div>';
                    else return '<a data-id="' . $id . '" id="mp-download-file" href="#" class="dropdown-item has-icon"><i class="fas fa-download"></i> '.$this->lang->line("Download Attachment").'</a>';
            }
        }
    }

    public function category($category=0){
        if($category==0) exit();
        $this->ecommerce_review_comment_exist = $this->ecommerce_review_comment_exist();
        $where_simple = array("ecommerce_category.id"=>$category,"ecommerce_category.status"=>'1');
        $where = array('where'=>$where_simple);
        $store_data = $this->basic->get_data("ecommerce_category",$where);

        if(!isset($store_data[0]))
        {
            echo '<br/><h2 style="border:1px solid red;padding:15px;color:red">'.$this->lang->line("Store not found.").'</h2>';
            exit();
        }

        $store_unique_id = $store_data[0]['store_id'];
        $where_simple = array("ecommerce_store.id"=>$store_unique_id,"ecommerce_store.status"=>'1');
        $where = array('where'=>$where_simple);
        $store_data = $this->basic->get_data("ecommerce_store",$where);

        $this->_language_loader($store_data[0]['store_locale']);
        $store_id = $store_data[0]['id'];
        $user_id = $store_data[0]['user_id'];

        $ecommerce_config = $this->get_ecommerce_config($store_id);
        $product_sort = isset($ecommerce_config["product_sort"]) ? $ecommerce_config["product_sort"] : "name";
        $product_sort_order = isset($ecommerce_config["product_sort_order"]) ? $ecommerce_config["product_sort_order"] : "asc";

        if($product_sort=="new") $product_sort = "ecommerce_product.id";
        else if($product_sort=="price") $product_sort = "original_price";
        else if($product_sort=="sale") $product_sort = "sales_count";
        else if($product_sort=="random") $product_sort = "rand()";
        else $product_sort = "product_name";

        if($product_sort!="rand()") $product_sort =$product_sort." ".$product_sort_order;

        $subscriber_id = $this->session->userdata($store_id."ecom_session_subscriber_id");
        if($subscriber_id=="") $subscriber_id = $this->input->get("subscriber_id",true);

        $review_data_formatted= array();
        if($this->ecommerce_review_comment_exist)
        {
            $review_data = $this->basic->get_data("ecommerce_product_review",array("where"=>array("store_id"=>$store_id,"hidden"=>"0")),array("product_id","sum(rating) as total_point","count(id) as total_review"),"","",NULL,$order_by='',$group_by='product_id');
            foreach ($review_data as $key => $value)
            {
                $review_data_formatted[$value['product_id']] = $value;
            }
        }

        $fb_app_id = $this->get_app_id();
        $data = array('body'=>"category","page_title"=>$store_data[0]['store_name']." | ".$this->lang->line("Products"),"fb_app_id"=>$fb_app_id,"favicon"=>base_url('upload/ecommerce/'.$store_data[0]['store_favicon']));

        $default_where = array('category_id' => $category);

        $data["review_data"] = $review_data_formatted;
        $data["store_data"] = $store_data[0];
        $data["product_list"] = $this->get_product_list_array($store_id,$default_where,$product_sort);
        $category_list = $this->get_category_list($store_id,true);
        $cat_info=array();
        foreach($category_list as $value)
        {
            $cat_info[$value['id']] = $value['category_name'];
        }

        // check the ecommerce related products exists or not
        $join222 = ['package'=>'users.package_id=package.id,left'];
        $select222 = ['users.id AS userid','users.user_type','users.package_id','package.*'];
        $get_user_info = $this->basic->get_data("users",['where'=>['users.id'=>$user_id]],$select222,$join222);
        $store_user_module_ids = isset($get_user_info[0]['module_ids']) ? explode(",", $get_user_info[0]['module_ids']): [];

        $this->is_ecommerce_related_product_exist = false;
        if($this->basic->is_exist("modules",array("id"=>317))) {
            if((isset($get_user_info[0]) && $get_user_info[0]['user_type'] == 'Admin') || in_array(317,$store_user_module_ids)) {
                $this->is_ecommerce_related_product_exist = true;
            }
        }

        $data["category_list"] = $cat_info;
        $data["category_list_raw"] = $category_list;
        //$data["attribute_list"] = $this->get_attribute_list($store_id);
        $data['currency_icons'] = $this->currency_icon();
        $data['ecommerce_config'] = $ecommerce_config;
        //$data['current_cart'] = $this->get_current_cart($subscriber_id,$store_id);
        $data["social_analytics_codes"] =$store_data[0];
        $data["show_search"] = true;
        $data["show_header"] = true;
        $data['category_id'] = $category;
        $this->load->view('ecommerce/bare-theme', $data);
    }

    public function add_to_cart_modal(){
        $this->ajax_check();
        $product_id = $this->input->post('product_id',true);
        $buy_now = $this->input->post('buy_now',true);
        if($product_id==0) {
            echo '<div class="alert alert-danger text-center">'.$this->lang->line("Product not found.").'</div>';
            exit();
        }
        $where_simple = array("ecommerce_product.id"=>$product_id,"ecommerce_product.status"=>"1","ecommerce_store.status"=>"1");
        $where = array('where'=>$where_simple);
        $join = array('ecommerce_store'=>"ecommerce_product.store_id=ecommerce_store.id,left");
        $select = array("ecommerce_product.*","store_name","store_unique_id","store_logo","store_favicon","terms_use_link","refund_policy_link","store_locale","pixel_id","google_id");
        $product_data = $this->basic->get_data("ecommerce_product",$where,$select,$join);

        if(!isset($product_data[0])) {
            echo '<div class="alert alert-danger text-center">'.$this->lang->line("Product not found.").'</div>';
            exit();
        }
        $subscriber_id = $this->session->userdata($product_data[0]['store_id']."ecom_session_subscriber_id");
        if($subscriber_id=="") $subscriber_id = $this->input->post("subscriber_id",true);

        $user_id = isset($product_data[0]["user_id"]) ? $product_data[0]["user_id"] : 0;
        $attribute_list = $this->get_attribute_list($product_data[0]["store_id"],true);
        $currency_icons = $this->currency_icon();
        $ecommerce_config = $this->get_ecommerce_config($product_data[0]["store_id"]);
        // $current_cart = $this->get_current_cart($subscriber_id,$product_data[0]['store_id']);
        // $social_analytics_codes = $product_data[0];
        if($this->addon_exist('ecommerce_product_price_variation')) $attribute_price_map = $this->basic->get_data("ecommerce_attribute_product_price",array("where"=>array("product_id"=>$product_id)));
        else $attribute_price_map = array();

        $product_data = $product_data[0];
        $hide_add_to_cart = isset($ecommerce_config['hide_add_to_cart']) ? $ecommerce_config['hide_add_to_cart'] : "0";
        $currency = isset($ecommerce_config['currency']) ? $ecommerce_config['currency'] : "USD";
        $currency_position = isset($ecommerce_config['currency_position']) ? $ecommerce_config['currency_position'] : "left";
        $decimal_point = isset($ecommerce_config['decimal_point']) ? $ecommerce_config['decimal_point'] : 0;
        $thousand_comma = isset($ecommerce_config['thousand_comma']) ? $ecommerce_config['thousand_comma'] : '0';
        $currency_icon = isset($currency_icons[$currency]) ? $currency_icons[$currency] : "$";
        $quantity_in_cart = 0;
        $output = "";
        $product_attributes = array_filter(explode(',', $product_data['attribute_ids']));
        if(is_array($product_attributes) && !empty($product_attributes)) $have_attributes = true;
        $map_array = array();
        $currency_left = $currency_right = "";
        if($currency_position=='left') $currency_left = $currency_icon;
        if($currency_position=='right') $currency_right = $currency_icon;
        foreach ($attribute_price_map as $key => $value) {
            $x = $value["amount"]==0 && $value["price_indicator"]=='x' ? 'x' : '';
            $ammount_formatted = $currency_left.mec_number_format($value["amount"],$decimal_point,$thousand_comma).$currency_right;
            $map_array[$value["attribute_id"]][$value["attribute_option_name"]] = $value["amount"]!=0 ? $value["price_indicator"].$ammount_formatted : $x;
        }

        $imgSrc = ($product_data['thumbnail']!='') ? base_url('upload/ecommerce/'.$product_data['thumbnail']) : '';
        if(isset($product_data["woocommerce_product_id"]) && !is_null($product_data["woocommerce_product_id"]) && $product_data['thumbnail']!='')
            $imgSrc = $product_data['thumbnail'];
        $display_price = mec_display_price($product_data['original_price'],$product_data['sell_price'],$currency_icon,'1',$currency_position,$decimal_point,$thousand_comma);

        // $hide_header = $imgSrc=='' ? 'd-none' : '';
        $hide_header = 'd-none';

        $output.='
<div class="product-details">
    <h2 class="product-title">'.$product_data['product_name'].'</h2>
    <div class="product-price">'.$display_price.'</div>
</div>



  <article class="article article-style-b mb-1 mt-1 no_shadow">
  <div class="article-header '.$hide_header.'" style="height:auto !important;">
  <img src="'.$imgSrc.'" class="pb-4" style="width:100% !important;height:auto !impoirtant;">                              
  </div>
  <div class="article-details pt-0 pb-2 pl-1 pr-1">
  <div class="article-title">
  <span class="text-primary" style="font-size:15px !important;"></span>
            
  </div>
  </div>
  </article>';

        $attr_count = 0;
        foreach ($attribute_list as $key => $value) {
            if(in_array($value["id"], $product_attributes)) {
                $attr_count++;
                $name = "attribute_".$attr_count;
                $options_array = json_decode($value["attribute_values"],true);
                $url_option = "option".$value["id"];
                $selected = "";

                $star = ($value['optional']=='0') ? '*' : '';
                $options_print = "";
                $count = 0;
                foreach ($options_array as $key2 => $value2)
                {
                    $selected_attr = "";
                    $count++;
                    $temp_id = $name.$count;
                    $tempu = isset($map_array[$value["id"]][$value2]) ? $map_array[$value["id"]][$value2] : "";
                    $continue = false;
                    if($tempu!='')
                    {
                        $first_char = substr($tempu, 0, 1);
                        if($first_char=='x') $continue = true;
                    }
                    if($continue) continue;
                    if($value['multiselect']=='1')
                    {
                        $options_print.='
                                <fieldset class="size">
                                                            <div class="form-checkbox d-flex align-items-center justify-content-between">
                                  <input type="checkbox" data-attr="'.$value["id"].'" name="'.$name.'"   value="'.$value2.'" class="custom-checkbox options" id="'.$temp_id.'" data-optional="'.$value["optional"].'" '.$selected_attr.'>
                                  <label class="d-inline-block" for="'.$temp_id.'" style="max-width:initial;white-space:nowrap;    padding-left: 2.5rem;">'.$value2.' <strong class="text-dark ml-1">'.$tempu.'</strong></label>
                                </div>
                                                   </fieldset>';
                    }
                    else
                    {
                        $options_print.='                                               
                                                   <fieldset class="size">
                                                            <div>
                                                                <input type="radio" class="custom-checkbox checkbox-round options"  data-attr="'.$value["id"].'"  name="'.$name.'"  id="'.$value2.'"  value="'.$value2.'" id="radio1" data-optional="'.$value["optional"].'" '.$selected_attr.'>
                                                                <label for="'.$value2.'" class="d-inline-block " style="white-space:nowrap;max-width:initial;padding-left:2.5rem;">'.$value2.' <strong class="text-dark ml-1">'.$tempu.'</strong></label>
                                                            </div>
                                                   </fieldset>';
                    }

                }

                $output .= '
       <div class="product-form mb-1">
                                <label class="mb-5 font-weight-bold">
                                  '.$value["attribute_name"].$star.'
                                </label>
                                <div class="flex-wrap d-flex align-items-center product-size-swatch">
                                 '.$options_print.'   
                                </div>
                              </div>';
            }
        }

        $n_eco_builder_config = $this->get_config();
        if($n_eco_builder_config['buy_button_type']=='buy_now'){
            $n_eco_builder_config['buy_button_class'] = 'buy_now';
        }else{
            $n_eco_builder_config['buy_button_class'] = '';
        }
        $buy_now_class = $n_eco_builder_config['buy_button_class'];


        $output .= '<div id="calculated_price_basedon_attribute" class="product-price mt-5"></div>';
        if($hide_add_to_cart=='0'):
            $output .= '<div class="fix-bottom">
                                    <div class="product-form container mt-5 d-flex">
                                        <div class="product-qty-form">
                                            <div class="input-group">
                                                <input class="quantity form-control" type="number" data-toggle="tooltip" title="'.$this->lang->line('Currently added to cart').'" id="item_count"
                                                       value="'.$quantity_in_cart.'" onchange="updateQuantity()">
                                                <button class="quantity-plus w-icon-plus add_to_cart add_to_cart_mdl '.$buy_now_class.'" data-product-id="'.$product_data['id'].'" data-attributes="'.$product_data['attribute_ids'].'" data-action="add" data-toggle="tooltip" title="'.$this->lang->line('Add 1 to Cart').'"></button>
                                                <button class="quantity-minus w-icon-minus add_to_cart add_to_cart_mdl '.$buy_now_class.'" data-product-id="'.$product_data['id'].'" data-attributes="'.$product_data['attribute_ids'].'" data-action="remove" data-toggle="tooltip" title="'.$this->lang->line('Remove 1 from Cart').'"></button>
                                            </div>
                                        </div>
                                        <button id="buttonAddCart" class="btn btn-primary btn-cart add_to_cart add_to_cart_mdl '.$buy_now_class.'" data-product-id="'.$product_data['id'].'" data-attributes="'.$product_data['attribute_ids'].'" data-action="bulk-add">
                                            <i class="w-icon-cart"></i>
                                            <span>'.$this->lang->line('Add to Cart').'</span>
                                        </button>
                                    </div>
                                </div>';
        endif;

        $output .= '
 <script>
 var counter=0;
 var current_product_id = "'.$product_data["id"].'";
 var current_store_id = "'.$product_data["store_id"].'";
 var currency_icon = "'.$currency_icon.'";
 var currency_position = "'.$currency_position.'";
 var decimal_point = "'.$decimal_point.'";
 var thousand_comma = "'.$thousand_comma.'";
 
 function updateQuantity(){
    var value = document.getElementById("item_count").value;
    $(\'#buttonAddCart\').attr("data-quantity_cart",value);
 }
 </script>';

        $output.= file_get_contents(APPPATH.'views/ecommerce/attribute_value.php');

        echo $output;

    }

    public function store($store_unique_id = 0){
        //var_dump($_GET); //fb iframe origin
        $store_unique_id = $this->get_store_id($store_unique_id);
        if($store_unique_id==0) exit();
        $this->ecommerce_review_comment_exist = $this->ecommerce_review_comment_exist();
        $where_simple = array("ecommerce_store.store_unique_id"=>$store_unique_id,"ecommerce_store.status"=>'1');
        $where = array('where'=>$where_simple);
        $store_data = $this->basic->get_data("ecommerce_store",$where);

        if(!isset($store_data[0])){
            echo '<br/><h2 style="border:1px solid red;padding:15px;color:red">'.$this->lang->line("Store not found.").'</h2>';
            exit();
        }

        $this->_language_loader($store_data[0]['store_locale']);
        $store_id = $store_data[0]['id'];
        $user_id = $store_data[0]['user_id'];

        $ecommerce_config = $this->get_ecommerce_config($store_id);
        $product_sort = isset($ecommerce_config["product_sort"]) ? $ecommerce_config["product_sort"] : "name";
        $product_sort_order = isset($ecommerce_config["product_sort_order"]) ? $ecommerce_config["product_sort_order"] : "asc";

        if($product_sort=="new") $product_sort = "ecommerce_product.id";
        else if($product_sort=="price") $product_sort = "original_price";
        else if($product_sort=="sale") $product_sort = "sales_count";
        else if($product_sort=="random") $product_sort = "rand()";
        else $product_sort = "product_name";

        if($product_sort!="rand()") $product_sort =$product_sort." ".$product_sort_order;

        $subscriber_id = $this->session->userdata($store_id."ecom_session_subscriber_id");
        if($subscriber_id=="") $subscriber_id = $this->input->get("subscriber_id",true);

        $review_data_formatted= array();
        if($this->ecommerce_review_comment_exist)
        {
            $review_data = $this->basic->get_data("ecommerce_product_review",array("where"=>array("store_id"=>$store_id,"hidden"=>"0")),array("product_id","sum(rating) as total_point","count(id) as total_review"),"","",NULL,$order_by='',$group_by='product_id');
            foreach ($review_data as $key => $value)
            {
                $review_data_formatted[$value['product_id']] = $value;
            }
        }

        $fb_app_id = $this->get_app_id();
        $data = array('body'=>"ecommerce/store_single","page_title"=>$store_data[0]['store_name']." | ".$this->lang->line("Products"),"fb_app_id"=>$fb_app_id,"favicon"=>base_url('upload/ecommerce/'.$store_data[0]['store_favicon']));

        include(APPPATH.'n_views/default_ecommerce_builder.php');
        if(file_exists(APPPATH . '/n_eco_user/builder/ecommerce_builder_' . $store_unique_id . '.php')){
            include(APPPATH . '/n_eco_user/builder/ecommerce_builder_' . $store_unique_id . '.php');
        }

        $default_where = array();
        $data["review_data"] = $review_data_formatted;
        $data["store_data"] = $store_data[0];
        $data["product_list"] = $this->get_product_list_array($store_id,$default_where,$product_sort);
        $data["store_data"] = $store_data[0];

        $join222 = ['package'=>'users.package_id=package.id,left'];
        $select222 = ['users.id AS userid','users.user_type','users.package_id','package.*'];
        $get_user_info = $this->basic->get_data("users",['where'=>['users.id'=>$user_id]],$select222,$join222);
        $store_user_module_ids = isset($get_user_info[0]['module_ids']) ? explode(",", $get_user_info[0]['module_ids']): [];

        $this->is_ecommerce_related_product_exist = false;
        if($this->basic->is_exist("modules",array("id"=>317))) {
            if((isset($get_user_info[0]) && $get_user_info[0]['user_type'] == 'Admin') || in_array(317,$store_user_module_ids)) {
                $this->is_ecommerce_related_product_exist = true;
            }
        }

        if($this->is_ecommerce_related_product_exist AND $n_eco_builder_config['front_featured_products_show'] =='true'){
            $default_where = array(
                'is_featured' => '1'
            );
            $order_by = '';
            $data['featured_list'] = $this->get_product_list_array($store_id,$default_where,$order_by,$n_eco_builder_config['front_featured_products_limit']);
        }

        if($n_eco_builder_config['front_deals_products_show'] =='true'){
            $default_where = array(
                'deals_mode' => '< original_price',
            );
            $order_by = '';
            $data['deals_list'] = $this->get_product_list_array($store_id,$default_where,$order_by,$n_eco_builder_config['front_deals_products_limit']);
        }

        if($n_eco_builder_config['front_sales_products_show'] =='true'){
            $default_where = array(
            );
            $order_by = 'sales_count DESC';
            $data['sales_list'] = $this->get_product_list_array($store_id,$default_where,$order_by,$n_eco_builder_config['front_sales_products_limit']);

        }

        if($n_eco_builder_config['new_products_show'] =='true'){
            $default_where = array(
            );
            $order_by = 'updated_at DESC';
            $data['new_list'] = $this->get_product_list_array($store_id,$default_where,$order_by,$n_eco_builder_config['new_products_products_limit']);
        }


        $category_list = $this->get_category_list($store_id,true);
        $cat_info=array();
        foreach($category_list as $value)
        {
            $cat_info[$value['id']] = $value['category_name'];
        }
        $data["category_list"] = $cat_info;
        $data["category_list_raw"] = $category_list;

        // check the ecommerce related products exists or not
        $join222 = ['package'=>'users.package_id=package.id,left'];
        $select222 = ['users.id AS userid','users.user_type','users.package_id','package.*'];
        $get_user_info = $this->basic->get_data("users",['where'=>['users.id'=>$user_id]],$select222,$join222);
        $store_user_module_ids = isset($get_user_info[0]['module_ids']) ? explode(",", $get_user_info[0]['module_ids']): [];

        $this->is_ecommerce_related_product_exist = false;
        if($this->basic->is_exist("modules",array("id"=>317))) {
            if((isset($get_user_info[0]) && $get_user_info[0]['user_type'] == 'Admin') || in_array(317,$store_user_module_ids)) {
                $this->is_ecommerce_related_product_exist = true;
            }
        }


        //$data["attribute_list"] = $this->get_attribute_list($store_id);
        $data['currency_icons'] = $this->currency_icon();
        $data['ecommerce_config'] = $ecommerce_config;
        $data['current_cart'] = $this->get_current_cart($subscriber_id,$store_id);
        $data["social_analytics_codes"] = $store_data[0];
        $data["show_search"] = true;
        $data["show_header"] = true;
        $this->load->view('ecommerce/bare-theme', $data);
    }

    public function cart($id=0){
        $store_data_temp = $this->basic->get_data("ecommerce_cart",array("where"=>array("id"=>$id)),"store_id");
        $store_id_temp = isset($store_data_temp[0]['store_id']) ? $store_data_temp[0]['store_id'] : "0";

        $subscriber_id=$this->session->userdata($store_id_temp."ecom_session_subscriber_id");
        if($subscriber_id=="") $subscriber_id = $this->input->get("subscriber_id",true);
        if($subscriber_id==""){
            //echo $this->login_to_continue;
            //var_dump($this->get_store_uq($this->nstore_id));
            redirect($this->return_base_url_php('ecommerce/login_signup/'.$this->nstore_id),'location');
            exit();
        }

        $this->update_cart($id,$subscriber_id);

        $select2 = array("ecommerce_cart.*","first_name","last_name","full_name","profile_pic","email","image_path","phone_number","user_location","store_name","store_type","store_email","store_favicon","store_phone","store_logo","store_address","store_zip","store_city","store_phone","store_email","store_country","store_state","store_unique_id","refund_policy_link","terms_use_link","store_locale","pixel_id","google_id","mercadopago_enabled","sslcommerz_enabled");
        $join2 = array('messenger_bot_subscriber'=>"messenger_bot_subscriber.subscribe_id=ecommerce_cart.subscriber_id,left",'ecommerce_store'=>"ecommerce_store.id=ecommerce_cart.store_id,left");
        $where_simple2 = array("ecommerce_cart.id"=>$id,"action_type !="=>"checkout");
        if($subscriber_id!="") $where_simple2['ecommerce_cart.subscriber_id'] = $subscriber_id;
        else $where_simple2['ecommerce_cart.user_id'] = $this->user_id;
        $where2 = array('where'=>$where_simple2);
        $webhook_data = $this->basic->get_data("ecommerce_cart",$where2,$select2,$join2);
        // echo "<pre>"; print_r($webhook_data); exit;

        if(!isset($webhook_data[0]))
        {
            if($subscriber_id=="" and empty($this->nstore_id)){
                $not_found = $this->lang->line("Sorry, we could not find the cart you are looking for.");
                echo '<br/><h2 style="border:1px solid red;padding:15px;color:red">'.$not_found.'</h2>';
            }else{
                $product_data = $this->initial_load_store($this->nstore_id);
                $product_data = $product_data[0];


                $fb_app_id = $this->get_app_id();
                $data = array('body'=>"empty_cart","page_title"=>$product_data['store_name'],"fb_app_id"=>$fb_app_id,"favicon"=>base_url('upload/ecommerce/'.$product_data['store_favicon']));

                $category_list = $this->get_category_list($product_data['id'],true);
                $cat_info=array();
                foreach($category_list as $value)
                {
                    $cat_info[$value['id']] = $value['category_name'];
                }
                $data["category_list"] = $cat_info;
                $data["category_list_raw"] = $category_list;

                if($subscriber_id==""){
                    $data['not_found'] = $this->lang->line("Sorry, we could not find the cart you are looking for.");
                }

                $data['subscriber_id'] = $subscriber_id;
                $data['currency_icons'] = $this->currency_icon();
                $data['ecommerce_config'] = $this->get_ecommerce_config($this->nstore_id);
                $data['current_cart'] = $this->get_current_cart($subscriber_id,$this->nstore_id);
                $data["social_analytics_codes"] = $product_data;
                $data['current_store_id'] = $this->nstore_id;

                $this->load->view('ecommerce/bare-theme', $data);
                return;
            }
            //todo
            exit();
        }
        $webhook_data_final = $webhook_data[0];
        $ecommerce_config = $this->get_ecommerce_config($webhook_data_final['store_id']);
        $data['ecommerce_config'] = $ecommerce_config;
        $this->_language_loader($webhook_data_final['store_locale']);

        $join = array('ecommerce_product'=>"ecommerce_product.id=ecommerce_cart_item.product_id,left");

        $fb_app_id = $this->get_app_id();

        $data['webhook_data_final'] = $webhook_data_final;
        $data['currency_list'] = $this->currecny_list_all();
        $data['country_names'] = $this->get_country_names();
        $data['phonecodes'] = $this->get_country_iso_phone_currecncy('phonecode');
        $data['currency_icons'] = $this->currency_icon();
        $data['product_list'] = $this->basic->get_data("ecommerce_cart_item",array('where'=>array("cart_id"=>$id)),array("ecommerce_cart_item.*","product_name","thumbnail","taxable","attribute_ids","woocommerce_product_id"),$join);
        $data['fb_app_id'] = $fb_app_id;
        $data['favicon'] = base_url('upload/ecommerce/'.$webhook_data_final['store_favicon']);
        $data['page_title'] = $webhook_data_final['store_name']." | ".$this->lang->line("Checkout");
        $data['body'] = "ecommerce/cart";
        $data['subscriber_id'] = $subscriber_id;

        $data['current_cart'] = $this->get_current_cart($subscriber_id,$webhook_data_final['store_id']);
        $data["social_analytics_codes"] = $webhook_data_final;
        $data["pickup_point_list"] = $this->basic->get_data("ecommerce_cart_pickup_points",array("where"=>array("store_id"=>$webhook_data_final['store_id'])));
        $mercadopago_enabled = isset($webhook_data_final['mercadopago_enabled']) ? $webhook_data_final['mercadopago_enabled'] : '0';
        $marcadopago_country = isset($ecommerce_config['marcadopago_country']) ? $ecommerce_config['marcadopago_country'] : 'br';
        $sslcommerz_enabled = isset($webhook_data_final['sslcommerz_enabled']) ? $webhook_data_final['sslcommerz_enabled'] : '0';
        $payment_amount = isset($webhook_data_final['payment_amount']) ? $webhook_data_final['payment_amount'] : '0';
        $mercadopago_button = $sslcommerz_button = '';
        $postdata_array = array();
        if($mercadopago_enabled=='1')
        {
            $mercadopago_public_key = isset($ecommerce_config['mercadopago_public_key']) ? $ecommerce_config['mercadopago_public_key'] : '';
            $mercadopago_access_token = isset($ecommerce_config['mercadopago_access_token']) ? $ecommerce_config['mercadopago_access_token'] : '';

            $this->load->library("mercadopago");
            $this->mercadopago->public_key=$mercadopago_public_key;
            $this->mercadopago->redirect_url=base_url("ecommerce/mercadopago_action/".$id);
            $this->mercadopago->transaction_amount=$payment_amount;
            $this->mercadopago->secondary_button=false;
            $this->mercadopago->button_lang=$this->lang->line('Pay with Mercado Pago');
            $this->mercadopago->marcadopago_url = 'https://www.mercadopago.com.'.$marcadopago_country;
            $mercadopago_button =  $this->mercadopago->set_button();
        }

        $category_list = $this->get_category_list($store_data_temp[0]["store_id"],true);
        $cat_info=array();
        foreach($category_list as $value)
        {
            $cat_info[$value['id']] = $value['category_name'];
        }
        $data["category_list"] = $cat_info;
        $data["category_list_raw"] = $category_list;

        if($sslcommerz_enabled=='1')
        {
            $postdata_array =
                [
                    'cart_id' => $id
                ];

            $endpoint_url = base_url('ecommerce/sslcommerz_action');
            $sslcommerz_button = '
    <button class="your-button-class" id="sslczPayBtn"
    token="if you have any token validation"
    postdata=""
    order="If you already have the transaction generated for current order"
    endpoint="'.$endpoint_url.'"> Pay With SSLCOMMERZ
    </button>';
        }
        $data["postdata_array"] = $postdata_array;
        $data["mercadopago_button"] = $mercadopago_button;
        $data["sslcommerz_button"] = $sslcommerz_button;

//        var_dump($data);
        $this->load->view('ecommerce/bare-theme', $data);

    }

    public function product($product_id=0)
    {
        if($product_id==0) exit();
        $prodcut_exp = explode('_', $product_id);
        $product_id = $prodcut_exp[0];
        $this->ecommerce_review_comment_exist = $this->ecommerce_review_comment_exist();
        $where_simple = array("ecommerce_product.id"=>$product_id,"ecommerce_product.status"=>"1","ecommerce_store.status"=>"1");
        $where = array('where'=>$where_simple);
        $join = array('ecommerce_store'=>"ecommerce_product.store_id=ecommerce_store.id,left");
        $select = array("ecommerce_product.*","store_name","store_unique_id","store_logo","store_favicon","terms_use_link","refund_policy_link","store_locale","pixel_id","google_id");
        $product_data = $this->basic->get_data("ecommerce_product",$where,$select,$join);

        if(!isset($product_data[0]))
        {
            echo '<br/><h1 style="text-align:center">'.$this->lang->line("Product not found.").'</h1>';
            exit();
        }
        $this->_language_loader($product_data[0]['store_locale']);

        $related_product_ids = $product_data[0]['related_product_ids'];
        $upsell_product_id = $product_data[0]['upsell_product_id'];
        $downsell_product_id = $product_data[0]['downsell_product_id'];
        $related_product_lists = array();
        $upsell_product_lists = array();
        $downsell_product_lists = array();

        if(isset($related_product_ids) && !empty($related_product_ids)) {

            $related_product_ids = explode(",", $related_product_ids);
            $where_in = array('ecommerce_product.id'=>$related_product_ids);
            $where_simple2 = array("ecommerce_product.status"=>"1","ecommerce_store.status"=>"1");
            $where2 = array('where'=>$where_simple2,'where_in'=>$where_in);
            $join2 = array('ecommerce_store'=>"ecommerce_product.store_id=ecommerce_store.id,left");
            $select2 = array("ecommerce_product.*","store_name","store_unique_id","store_logo","store_favicon","terms_use_link","refund_policy_link","store_locale","pixel_id","google_id");
            $related_product_lists = $this->basic->get_data("ecommerce_product",$where2,$select2,$join2);

        }

        if(isset($upsell_product_id) && $upsell_product_id != 0) {
            $where_in3 = array('ecommerce_product.id'=>$upsell_product_id);
            $where_simple3 = array("ecommerce_product.status"=>"1","ecommerce_store.status"=>"1");
            $where3 = array('where'=>$where_simple3,'where_in'=>$where_in3);
            $join3 = array('ecommerce_store'=>"ecommerce_product.store_id=ecommerce_store.id,left");
            $select3 = array("ecommerce_product.*","store_name","store_unique_id","store_logo","store_favicon","terms_use_link","refund_policy_link","store_locale","pixel_id","google_id");
            $upsell_product_lists = $this->basic->get_data("ecommerce_product",$where3,$select3,$join3);
        }

        if(isset($downsell_product_id) && $downsell_product_id != 0) {
            $where_in4 = array('ecommerce_product.id'=>$downsell_product_id);
            $where_simple4 = array("ecommerce_product.status"=>"1","ecommerce_store.status"=>"1");
            $where4 = array('where'=>$where_simple4,'where_in'=>$where_in4);
            $join4 = array('ecommerce_store'=>"ecommerce_product.store_id=ecommerce_store.id,left");
            $select4 = array("ecommerce_product.*","store_name","store_unique_id","store_logo","store_favicon","terms_use_link","refund_policy_link","store_locale","pixel_id","google_id");
            $downsell_product_lists = $this->basic->get_data("ecommerce_product",$where4,$select4,$join4);
        }

        $subscriber_id = $this->session->userdata($product_data[0]['store_id']."ecom_session_subscriber_id");
        if($subscriber_id=="") $subscriber_id = $this->input->get("subscriber_id",true);


        $update_visit_count_sql = "UPDATE ecommerce_product SET visit_count=visit_count+1 WHERE id=".$product_id;
        $this->basic->execute_complex_query($update_visit_count_sql);

        $user_id = isset($product_data[0]["user_id"]) ? $product_data[0]["user_id"] : 0;
        $fb_app_id = $this->get_app_id();
        $data = array('body'=>"ecommerce/product_single","page_title"=>$product_data[0]['store_name']." | ".$product_data[0]['product_name'],"fb_app_id"=>$fb_app_id,"favicon"=>base_url('upload/ecommerce/'.$product_data[0]['store_favicon']));


        $review_data = array();
        $review_list_data = array();
        $xreview = array();
        $has_purchase_array=array();
        if($this->ecommerce_review_comment_exist)
        {
            $review_data = $this->basic->get_data("ecommerce_product_review",array("where"=>array("product_id"=>$product_id,"hidden"=>"0")),array("product_id","sum(rating) as total_point","count(id) as total_review"),"","",NULL,$order_by='',$group_by='product_id');

            $join=array('messenger_bot_subscriber'=>"messenger_bot_subscriber.subscribe_id=ecommerce_product_review.subscriber_id,left");
            $review_list_data = $this->basic->get_data("ecommerce_product_review",array("where"=>array("product_id"=>$product_id,"hidden"=>"0")),array("ecommerce_product_review.*","first_name","last_name","profile_pic","image_path"),$join,"",NULL,$order_by='id DESC');

            if($subscriber_id!='' && $this->user_id=="")
            {
                $join_me = array('ecommerce_cart_item'=>"ecommerce_cart_item.cart_id=ecommerce_cart.id,left");
                $has_purchase_array = $this->basic->get_data("ecommerce_cart",array("where"=>array("subscriber_id"=>$subscriber_id,"product_id"=>$product_id),"where_not_in"=>array("status"=>array("pending","rejected"))),'ecommerce_cart_item.*,count(cart_id) as total_row',$join_me,'',NULL,'','cart_id');
                $xreview = $this->basic->get_data("ecommerce_product_review",array("where"=>array("product_id"=>$product_id,"subscriber_id"=>$subscriber_id)),'');

            }

        }

        // check the ecommerce related products exists or not
        $join222 = ['package'=>'users.package_id=package.id,left'];
        $select222 = ['users.id AS userid','users.user_type','users.package_id','package.*'];
        $get_user_info = $this->basic->get_data("users",['where'=>['users.id'=>$user_id]],$select222,$join222);
        $store_user_module_ids = isset($get_user_info[0]['module_ids']) ? explode(",", $get_user_info[0]['module_ids']): [];

        $this->is_ecommerce_related_product_exist = false;
        if($this->basic->is_exist("modules",array("id"=>317))) {
            if((isset($get_user_info[0]) && $get_user_info[0]['user_type'] == 'Admin') || in_array(317,$store_user_module_ids)) {
                $this->is_ecommerce_related_product_exist = true;
            }
        }
        $data['related_product_lists'] = isset($related_product_lists) ? $related_product_lists:array();
        $data['upsell_product_lists'] = isset($upsell_product_lists) ? $upsell_product_lists:array();
        $data['downsell_product_lists'] = isset($downsell_product_lists) ? $downsell_product_lists:array();
        $data['downsell_product_id'] = isset($downsell_product_id) ? $downsell_product_id:0;

        $data["review_data"] = $review_data;
        $data["review_list_data"] = $review_list_data;
        $data["xreview"] = $xreview;
        $data["has_purchase_array"] = $has_purchase_array;

        $category_list = $this->get_category_list($product_data[0]["store_id"],true);
        $cat_info=array();
        foreach($category_list as $value)
        {
            $cat_info[$value['id']] = $value['category_name'];
        }
        $data["category_list"] = $cat_info;
        $data["category_list_raw"] = $category_list;

        $data["product_data"] = $product_data[0];
        $data["attribute_list"] = $this->get_attribute_list($product_data[0]["store_id"],true);
        $data['currency_icons'] = $this->currency_icon();
        $data['ecommerce_config'] = $this->get_ecommerce_config($product_data[0]["store_id"]);
        $data['current_cart'] = $this->get_current_cart($subscriber_id,$product_data[0]['store_id']);
        $data["social_analytics_codes"] = $product_data[0];
        if($this->addon_exist('ecommerce_product_price_variation')) $data["attribute_price_map"] = $this->basic->get_data("ecommerce_attribute_product_price",array("where"=>array("product_id"=>$product_id)));
        else $data["attribute_price_map"] = array();
        $data['current_product_id'] = isset($product_data[0]['id']) ? $product_data[0]['id'] : 0;
        $data['current_store_id'] = isset($product_data[0]['store_id']) ? $product_data[0]['store_id'] : 0;

        $this->load->view('ecommerce/bare-theme', $data);
    }

    private function get_attribute_list($store_id=0,$raw_data=false)
    {
        if($store_id==0) $store_id = $this->get_store_id($store_id);
        $at_list = $this->basic->get_data("ecommerce_attribute",array("where"=>array("store_id"=>$store_id,"status"=>"1")),$select='',$join='',$limit='',$start=NULL,$order_by='attribute_name ASC');
        if($raw_data) return $at_list;
        $at_info=array();
        foreach($at_list as $value)
        {
            $at_info[$value['id']] = $value['attribute_name'];
        }
        return $at_info;
    }

    private function get_product_list_array($store_id=0,$default_where="",$order_by="",$limit='')
    {
        $store_id = $this->get_store_id($store_id);
        $where_simple = array("store_id"=>$store_id,"status"=>"1");
        if(isset($default_where['product_name'])) {
            $product_name = $default_where['product_name'];
            $this->db->where(" product_name LIKE "."'%".$product_name."%'");
            unset($default_where['product_name']);
        }
        if(isset($default_where['deals_mode'])) {
            $this->db->where(" sell_price < original_price AND sell_price > 0");
            unset($default_where['deals_mode']);
        }

        if(is_array($default_where) && !empty($default_where))
        {
            foreach($default_where as $key => $value)
            {
                $where_simple[$key] = $value;
            }
        }
        if($order_by=="") $order_by = "product_name ASC";
        $product_list = $this->basic->get_data("ecommerce_product",array("where"=>$where_simple),$select='',$join='',$limit,$start=NULL,$order_by);

        // echo $this->db->last_query();
        return $product_list;
    }

    private function get_current_cart($subscriber_id="",$store_id=0,$pickup="")
    {
        $current_cart = array("cart_count"=>0,"cart_id"=>0,"cart_data"=>array());
        $store_id = $this->get_store_id($store_id);
        if($store_id!=0 && $subscriber_id!="")
        {
            $join = array('ecommerce_cart_item'=>"ecommerce_cart.id=ecommerce_cart_item.cart_id,right");
            $where_simple = array("ecommerce_cart.store_id"=>$store_id,"action_type!="=>"checkout");
            if($subscriber_id!="") $where_simple["ecommerce_cart.subscriber_id"] = $subscriber_id;
            else $where_simple["ecommerce_cart.user_id"] = $this->user_id;
            $where = array('where'=>$where_simple);
            $select = array("ecommerce_cart.*","ecommerce_cart_item.id as ecommerce_cart_item_id","cart_id","product_id","unit_price","coupon_info","quantity","attribute_info");
            $cart_data = $this->basic->get_data("ecommerce_cart",$where,$select,$join);
            $cart_id = isset($cart_data[0]['cart_id']) ? $cart_data[0]['cart_id'] : 0;
            $cart_data_final = array();
            foreach ($cart_data as $key => $value)
            {
                if($value["quantity"]<=0)
                {
                    $this->basic->delete_data("ecommerce_cart_item",array("id"=>$value["ecommerce_cart_item_id"]));
                    unset($cart_data[$key]);
                }
                else $cart_data_final[$value['product_id']] = $value;
            }
            $cart_count = count($cart_data);
            $cart_url = base_url("ecommerce/cart/".$cart_id);
            $params = array("subscriber_id"=>$subscriber_id);
            if(isset($pickup) && $pickup!="") $params['pickup'] = $pickup;
            $cart_url = mec_add_get_param($cart_url,$params);
            if($cart_count==0)
            {
                $this->basic->delete_data("ecommerce_cart",array("id"=>$cart_id));
                $cart_id = 0;
                $cart_url= "";
            }
            $current_cart = array("cart_count"=>$cart_count,"cart_id"=>$cart_id,"cart_url"=>$cart_url,"cart_data"=>$cart_data_final,"cart_data_raw"=>$cart_data);
        }
        return $current_cart;
    }

    public function get_buyer_profile()
    {
        $this->ajax_check();
        $store_id = $this->input->post("store_id",true);
        $login_error = '<p>
    <div class="alert alert-danger text-center">
    '.$this->lang->line("Please login to continue.").'<br><br><a href="" id="login_form" class="pointer btn btn-primary">'.$this->lang->line("Login to continue").'</a>
    </div>
    </p>';

        $where_subs = array();
        $subscriber_id=$this->session->userdata($store_id."ecom_session_subscriber_id");
        if($subscriber_id!="") $where_subs = array("subscriber_type"=>"system","subscribe_id"=>$subscriber_id,"store_id"=>$store_id);
        else
        {
            if($subscriber_id=="") $subscriber_id = $this->input->post("subscriber_id",true);
            if($subscriber_id!="") $where_subs = array("subscriber_type!="=>"system","subscribe_id"=>$subscriber_id);
        }
        if($subscriber_id=='')
        {
            echo $login_error;
            exit();
        }
        $subscriber_info = $this->basic->count_row("messenger_bot_subscriber",array("where"=>$where_subs),"id");
        $login_needed = false;
        if($subscriber_info[0]['total_rows']==0){
            echo $login_error;
            exit();
        }

        $where = array("subscriber_id"=>$subscriber_id,'profile_address'=>'1');
        $address_data = $this->basic->get_data("ecommerce_cart_address_saved",array("where"=>$where));
        $country_names =  $this->get_country_names();
        $phonecodes = $this->get_country_iso_phone_currecncy('phonecode');
        $first_name=$last_name=$email=$mobile=$country=$city=$state=$address=$zip=$note='';
        $store_data = $this->basic->get_data("ecommerce_store",array("where"=>array("id"=>$store_id)),"store_country");
        if(!isset($store_data[0]))
        {
            echo '<div class="alert alert-danger text-center">'.$this->lang->line("Store not found.").'</div>';
            exit();
        }

        $is_checkout_country = $is_checkout_state = $is_checkout_city = $is_checkout_zip = $is_checkout_email = $is_checkout_phone = '1';

        if(!isset($address_data[0]))
        {
            $address_data  = $this->basic->get_data("messenger_bot_subscriber",array("where"=>array("subscribe_id"=>$subscriber_id)),"first_name,last_name,full_name,profile_pic,phone_number,user_location,email");
            $user_location = isset($address_data[0]['user_location']) ? json_decode($address_data[0]['user_location'],true) : array();
            $first_name = isset($address_data[0]['first_name']) ? $address_data[0]['first_name'] : '';
            $last_name = isset($address_data[0]['last_name']) ? $address_data[0]['last_name'] : '';
            $email = isset($address_data[0]['email']) ? $address_data[0]['email'] : '';
            $mobile = isset($address_data[0]['phone_number']) ? $address_data[0]['phone_number'] : '';
            $country = isset($user_location['country']) ? $user_location['country'] : '';
            $city = isset($user_location['city']) ? $user_location['city'] : '';
            $state = isset($user_location['state']) ? $user_location['state'] : '';
            $address = isset($user_location['street']) ? $user_location['street'] : '';
            $zip = isset($user_location['zip']) ? $user_location['zip'] : '';
        }
        else
        {
            $first_name = isset($address_data[0]['first_name']) ? $address_data[0]['first_name'] : '';
            $last_name = isset($address_data[0]['last_name']) ? $address_data[0]['last_name'] : '';
            $email = isset($address_data[0]['email']) ? $address_data[0]['email'] : '';
            $mobile = isset($address_data[0]['mobile']) ? $address_data[0]['mobile'] : '';
            $country = isset($address_data[0]['country']) ? $address_data[0]['country'] : '';
            $city = isset($address_data[0]['city']) ? $address_data[0]['city'] : '';
            $state = isset($address_data[0]['state']) ? $address_data[0]['state'] : '';
            $address = isset($address_data[0]['address']) ? $address_data[0]['address'] : '';
            $zip = isset($address_data[0]['zip']) ? $address_data[0]['zip'] : '';
            $note = isset($address_data[0]['note']) ? $address_data[0]['note'] : '';
        }
        $options = "";
        foreach ($country_names as $key => $value) {
            if($country!='')  $selected_country = ($key==$country) ? 'selected' : '';
            else $selected_country = ($key==$store_data[0]['store_country']) ? 'selected' : '';
            $phonecode_attr = isset($phonecodes[$key]) ? $phonecodes[$key] : '';
            $options .='<option phonecode="'.$phonecode_attr.'" value="'.$key.'" '.$selected_country.'>'.$value.'</option>';
        }

        $state_city_street_html = $country_html = $email_html = $phone_html = '';

        $state_var = ($is_checkout_state=='1') ? '
        <div class="col-md-4">
        <div class="form-group mb-3">
    <label for="firstname">'.$this->lang->line('State').'</label>
    <input type="text" class="form-control form-control-md"  name="state" value="'.$state.'" placeholder="'.$this->lang->line('State').'">
</div></div>
        ':'';
        $city_var = ($is_checkout_city=='1') ? '
                <div class="col-md-4">
        <div class="form-group mb-3">
    <label for="firstname">'.$this->lang->line('City').'</label>
    <input type="text" class="form-control form-control-md"  name="city" value="'.$city.'" placeholder="'.$this->lang->line('City').'">
</div></div>
        
        
        ':'';
        $zip_var = ($is_checkout_zip=='1') ? '
                        <div class="col-md-4">
        <div class="form-group mb-3">
    <label for="firstname">'.$this->lang->line('Zip').'</label>
    <input type="text" class="form-control form-control-md"  name="zip" value="'.$zip.'" placeholder="'.$this->lang->line('Zip').'">
</div></div>
        
        ':'';

        if($state_var!='' || $city_var!='' || $zip_var!='')
            $state_city_street_html .=
                '<div class="row">      
  '.$state_var.$city_var.$zip_var.'
  </div>';

        if($is_checkout_country=='1')
            $country_html .=
                '
<div class="form-group mb-3">
    <label for="firstname">'.$this->lang->line('country').'</label>
      <select id="country" name="country" class="form-control form-control-md"> 
  '.$options.'
  </select>
</div>
';

        $email_var = ($is_checkout_email=='1') ? '
        <div class="form-group mb-3">
    <label for="firstname">'.$this->lang->line('Email').'</label>
    <input type="text" class="form-control form-control-md" name="email" value="'.$email.'" placeholder="'.$this->lang->line("Email").'">
</div> ' : '';
        $mobile_var = ($is_checkout_phone=='1') ? '

        <div class="form-group mb-3">
    <label for="firstname">'.$this->lang->line('Phone Number').'</label>
    <span class="input-group-text" id="phonecode_val"></span>
<input type="text" class="form-control form-control-md" name="mobile" value="'.$mobile.'" placeholder="'.$this->lang->line("Phone Number").'">
</div> ' : '';

        if($is_checkout_email=='1')
            $email_html .= '

  '.$email_var.'
';

        if($is_checkout_phone=='1')
            $phone_html .= '

  '.$mobile_var.'
';

        echo
            '
<div class="row">

<div class="col-md-6">
        <div class="form-group mb-3">
    <label for="firstname">'.$this->lang->line('First Name').'*</label>
      <input type="text" class="form-control form-control-md" name="first_name" placeholder="'.$this->lang->line("First Name").'*"  class="form-control-plaintext" value="'.$first_name.'">
</div>
</div>

<div class="col-md-6">
        <div class="form-group mb-3">
    <label for="firstname">'.$this->lang->line('Last Name').'*</label>
  <input type="text" class="form-control form-control-md" name="last_name" placeholder="'.$this->lang->line("Last Name").'*"  class="form-control-plaintext" value="'.$last_name.'">
</div>
</div>


</div>            


        <div class="form-group mb-3">
    <label for="firstname">'.$this->lang->line('Street').'*</label>
    <input type="text" class="form-control form-control-md"  name="street" value="'.$address.'" placeholder="'.$this->lang->line('Street').'*">
</div>

  
  '.$state_city_street_html.'
  '.$country_html.'
  '.$email_html.'
  '.$phone_html;
    }

    public function get_buyer_address()
    {
        $this->ajax_check();


        $id=0;
        $address_data = array();
        $subscriber_id = $this->input->post("subscriber_id",true);
        $store_id = $this->input->post("store_id",true);
        $operation = $this->input->post("operation",true);

        $country_names =  $this->get_country_names();
        $phonecodes = $this->get_country_iso_phone_currecncy('phonecode');
        $first_name=$last_name=$email=$mobile=$country=$city=$state=$address=$zip=$title='';
        $store_data = $this->basic->get_data("ecommerce_store",array("where"=>array("id"=>$store_id)),array("store_country", "store_locale"));
        if(!isset($store_data[0]))
        {
            echo '<div class="alert alert-danger text-center">'.$this->lang->line("Store not found.").'</div>';
            exit();
        }

        $this->_language_loader($store_data[0]['store_locale']);
        $delete='';

        if($operation=='edit')
        {
            $id = $this->input->post("id",true);
            $where = array("subscriber_id"=>$subscriber_id,'id'=>$id);
            $address_data = $this->basic->get_data("ecommerce_cart_address_saved",array("where"=>$where));
            if(isset($address_data[0]))
            {
                $first_name = isset($address_data[0]['first_name']) ? $address_data[0]['first_name'] : '';
                $last_name = isset($address_data[0]['last_name']) ? $address_data[0]['last_name'] : '';
                $email = isset($address_data[0]['email']) ? $address_data[0]['email'] : '';
                $mobile = isset($address_data[0]['mobile']) ? $address_data[0]['mobile'] : '';
                $country = isset($address_data[0]['country']) ? $address_data[0]['country'] : '';
                $city = isset($address_data[0]['city']) ? $address_data[0]['city'] : '';
                $state = isset($address_data[0]['state']) ? $address_data[0]['state'] : '';
                $address = isset($address_data[0]['address']) ? $address_data[0]['address'] : '';
                $zip = isset($address_data[0]['zip']) ? $address_data[0]['zip'] : '';
                $title = isset($address_data[0]['title']) ? $address_data[0]['title'] : '';
            }
            $delete = '<a href="#" id="delete_address" data-id="'.$id.'" class="text-danger float-right pb-2"><i class="fas fa-trash"></i> '.$this->lang->line("Delete").'</a>';
        }
        else
        {
            $address_data = $this->basic->get_data("messenger_bot_subscriber",array("where"=>array("subscribe_id"=>$subscriber_id)));
            if(isset($address_data[0]))
            {
                $first_name = isset($address_data[0]['first_name']) ? $address_data[0]['first_name'] : '';
                $last_name = isset($address_data[0]['last_name']) ? $address_data[0]['last_name'] : '';
                $email = isset($address_data[0]['email']) ? $address_data[0]['email'] : '';
                $mobile = isset($address_data[0]['phone_number']) ? $address_data[0]['phone_number'] : '';
                $user_location = isset($address_data[0]['user_location']) ? json_decode($address_data[0]['user_location'],true) : array();
                $country = isset($user_location['country']) ? $user_location['country'] : '';
                $city = isset($user_location['city']) ? $user_location['city'] : '';
                $state = isset($user_location['state']) ? $user_location['state'] : '';
                $address = isset($user_location['street']) ? $user_location['street'] : '';
                $zip = isset($user_location['zip']) ? $user_location['zip'] : '';
            }
        }

        $ecommerce_config = $this->get_ecommerce_config($store_id);
        $is_checkout_country = isset($ecommerce_config['is_checkout_country']) ? $ecommerce_config['is_checkout_country'] : '1';
        $is_checkout_state = isset($ecommerce_config['is_checkout_state']) ? $ecommerce_config['is_checkout_state'] : '1';
        $is_checkout_city = isset($ecommerce_config['is_checkout_city']) ? $ecommerce_config['is_checkout_city'] : '1';
        $is_checkout_zip = isset($ecommerce_config['is_checkout_zip']) ? $ecommerce_config['is_checkout_zip'] : '1';
        $is_checkout_email = isset($ecommerce_config['is_checkout_email']) ? $ecommerce_config['is_checkout_email'] : '1';
        $is_checkout_phone = isset($ecommerce_config['is_checkout_phone']) ? $ecommerce_config['is_checkout_phone'] : '1';
        $is_delivery_note = isset($ecommerce_config['is_delivery_note']) ? $ecommerce_config['is_delivery_note'] : '1';


        $options = "";
        foreach ($country_names as $key => $value) {
            if($country!='')  $selected_country = ($key==$country) ? 'selected' : '';
            else $selected_country = ($key==$store_data[0]['store_country']) ? 'selected' : '';
            $phonecode_attr = isset($phonecodes[$key]) ? $phonecodes[$key] : '';
            $options .='<option phonecode="'.$phonecode_attr.'" value="'.$key.'" '.$selected_country.'>'.$value.'</option>';
        }

        $state_city_street_html = $country_html = $email_html = $phone_html = '';

        $state_var = ($is_checkout_state=='1') ? ' <div class="col-md-4">
        <div class="form-group mb-3">
    <label for="firstname">'.$this->lang->line('State').'</label>
    <input type="text" class="form-control form-control-md"  name="state" value="'.$state.'" placeholder="'.$this->lang->line('State').'">
</div></div>':'';
        $city_var = ($is_checkout_city=='1') ? ' <div class="col-md-4">
        <div class="form-group mb-3">
    <label for="firstname">'.$this->lang->line('City').'</label>
    <input type="text" class="form-control form-control-md"  name="city" value="'.$city.'" placeholder="'.$this->lang->line('City').'">
</div></div>':'';
        $zip_var = ($is_checkout_zip=='1') ? '<div class="col-md-4">
        <div class="form-group mb-3">
    <label for="firstname">'.$this->lang->line('Zip').'</label>
    <input type="text" class="form-control form-control-md"  name="zip" value="'.$zip.'" placeholder="'.$this->lang->line('Zip').'">
</div></div>':'';

        if($state_var!='' || $city_var!='' || $zip_var!='')
            $state_city_street_html .=
                '<div class="row">      
  '.$state_var.$city_var.$zip_var.'
  </div>';

        if($is_checkout_country=='1')
            $country_html .=
                '<div class="form-group mb-3">
    <label for="firstname">'.$this->lang->line('country').'</label>
      <select id="country" name="country" class="form-control form-control-md"> 
  '.$options.'
  </select>
</div>';

        $email_var = ($is_checkout_email=='1') ? '<div class="form-group mb-3">
    <label for="firstname">'.$this->lang->line('Email').'</label>
    <input type="text" class="form-control form-control-md" name="email" value="'.$email.'" placeholder="'.$this->lang->line("Email").'">
</div> ' : '';
        $mobile_var = ($is_checkout_phone=='1') ? ' <div class="form-group mb-3">
    <label for="firstname">'.$this->lang->line('Phone Number').'</label>
    <span class="input-group-text" id="phonecode_val"></span>
<input type="text" class="form-control form-control-md" name="mobile" value="'.$mobile.'" placeholder="'.$this->lang->line("Phone Number").'">
</div>' : '';

        if($is_checkout_email=='1')
            $email_html .= '
      
  '.$email_var.'
';

        if($is_checkout_phone=='1')
            $phone_html .= '
  
  '.$mobile_var.'
';

        echo
            $delete.'
  <input type="hidden" name="id" class="form-control-plaintext" value="'.$id.'">

          <div class="form-group mb-3">
    <label for="firstname">'.$this->lang->line('Title').'</label>
        <input type="text" class="form-control form-control-md""  name="title" value="'.$title.'" placeholder="'.$this->lang->line('Title').'">
</div>
  

<div class="row">

<div class="col-md-6">
        <div class="form-group mb-3">
    <label for="firstname">'.$this->lang->line('First Name').'*</label>
      <input type="text" class="form-control form-control-md" name="first_name" placeholder="'.$this->lang->line("First Name").'*"  class="form-control-plaintext" value="'.$first_name.'">
</div>
</div>

<div class="col-md-6">
        <div class="form-group mb-3">
    <label for="firstname">'.$this->lang->line('Last Name').'*</label>
  <input type="text" class="form-control form-control-md" name="last_name" placeholder="'.$this->lang->line("Last Name").'*"  class="form-control-plaintext" value="'.$last_name.'">
</div>
</div>


</div>         
  
        <div class="form-group mb-3">
    <label for="firstname">'.$this->lang->line('Street').'*</label>
    <input type="text" class="form-control form-control-md"  name="street" value="'.$address.'" placeholder="'.$this->lang->line('Street').'*">
</div>
  
  '.$state_city_street_html.'
  '.$country_html.'
  '.$email_html.'
  '.$phone_html;
    }

    public function save_address()
    {
        $this->ajax_check();
        $data = array();
        foreach ($_POST as $key => $value)
        {
            $$key = strip_tags($this->input->post($key,true));
            $data[$key] = $$key;
        }
        if($subscriber_id==''){
            echo json_encode(array('status'=>'0','message'=>$this->login_to_continue));
            exit();
        }
        if(isset($mobile) && isset($country_code))
        {
            $pos = strpos($mobile, $country_code);
            $country_code_embeded = ($pos!==false && $pos===0) ? true : false;
            if($mobile!=='' && !$country_code_embeded) $mobile = $country_code.$mobile;
            if(isset($data["mobile"])) $data["mobile"] = $mobile;
        }

        $data["address"] = $data["street"];
        $data["profile_address"] = "0";
        unset($data['store_id']);
        if(isset($data['country_code'])) unset($data['country_code']);
        unset($data['street']);
        //if(isset($data['id']))unset($data['id']);

        if(isset($data['id']) && $data['id']!=0)
            $this->basic->update_data("ecommerce_cart_address_saved",array("subscriber_id"=>$subscriber_id,"id"=>$data['id']),$data);
        else
        {
            $this->basic->update_data("ecommerce_cart_address_saved",array("subscriber_id"=>$subscriber_id),array("is_default"=>"0"));
            $data["is_default"] = "1";
            $this->basic->insert_data("ecommerce_cart_address_saved",$data);
        }

        echo json_encode(array('status'=>'1','message'=>$this->lang->line("Delivery address has been saved successfully.")));

    }

    public function comment_list_data(){
        $this->ajax_check();
        $subscriber_id = $this->input->post("subscriber_id",true); // to load a single comment
        $comment_id = $this->input->post("comment_id",true); // to load a single comment
        $product_id = $this->input->post("product_id",true);
        $store_id = $this->input->post("store_id",true);
        $store_favicon = $this->input->post("store_favicon",true);
        $store_name = strip_tags($this->input->post("store_name",true));
        if($store_name=="") $store_name = $this->lang->line("Administrator");
        $start = $this->input->post("start",true);
        $limit = $this->input->post("limit",true);

        $select=array("ecommerce_product_comment.*","first_name","last_name","profile_pic","image_path");
        if(empty($comment_id)) $where=array("where"=>array("ecommerce_product_comment.product_id"=>$product_id,"hidden"=>"0","parent_product_comment_id"=>0));
        else $where=array("where"=>array("ecommerce_product_comment.id"=>$comment_id,"hidden"=>"0","parent_product_comment_id"=>0));
        $join=array('messenger_bot_subscriber'=>"messenger_bot_subscriber.subscribe_id=ecommerce_product_comment.subscriber_id,left");
        $parent_comment_info=$this->basic->get_data("ecommerce_product_comment",$where,$select,$join,$limit,$start,$order_by="id DESC");
        $total_rows_array = $this->basic->count_row("ecommerce_product_comment", $where,$count='ecommerce_product_comment.id',$join);
        $total_rows =$total_rows_array[0]['total_rows'];
        $parent_ids=array();
        foreach ($parent_comment_info as $key => $value)
        {
            array_push($parent_ids, $value['id']);
        }
        $parent_ids=array_unique($parent_ids);

        // getting child comments (using same join param and derived parent array)
        $total_comment=count($parent_comment_info);
        $child_comment_info_formatted=array();
        if(!empty($parent_ids))
        {
            $child_comment_info=$this->basic->get_data("ecommerce_product_comment",array("where"=>array("ecommerce_product_comment.product_id"=>$product_id,"hidden"=>"0"),"where_in"=>array("parent_product_comment_id"=>$parent_ids)),$select,$join,$limit='',$start=NULL,$order_by="id ASC");
            foreach ($child_comment_info as $key => $value)
            {
                $total_comment++;
                $child_comment_info_formatted[$value['parent_product_comment_id']][]=$value;
            }
        }

        $html='';
        $border_bottom = empty($comment_id) ? "border-bottom" : "";
        foreach ($parent_comment_info as $key => $value)
        {
            $sub_comments = '';
            $divId = "collapse".$value['id'];
            if(isset($child_comment_info_formatted[$value['id']]))
                foreach ($child_comment_info_formatted[$value['id']] as $key2 => $value2)
                {
                    if($value2['subscriber_id']=='') $commenter2 = $store_name." <i class='fas fa-user-circle text-primary'></i>";
                    else $commenter2 = $value2["first_name"]." ".$value2["last_name"];
                    $profile_pic2 = ($value2['profile_pic']!="") ? $value2["profile_pic"] :  base_url('assets/img/avatar/avatar-1.png');
                    $image_path2=($value2["image_path"]!="") ? base_url($value2["image_path"]) : $profile_pic2;

                    if($value2['subscriber_id']=='' && !empty($store_favicon)) $image_path2 = "<img class='rounded-circle mr-3' style='height:50px;width:50px;' src='".base_url('upload/ecommerce/'.$store_favicon)."'>";

                    $hide_link2 = "";
                    if($this->user_id!='') $hide_link2 = '
                    <a href="#" data-id="'.$value2['id'].'"  class="btn btn-link btn-underline sm btn-icon-left font-weight-normal text-capitalize hide-comment">
                <i class="fa fa-eye-slash"></i>'.$this->lang->line("Hide").'
            </a>';

                    $new_comment_formatted2 = preg_replace("/(https?:\/\/[a-zA-Z0-9\-._~\:\/\?#\[\]@!$&'\(\)*+,;=]+)/", '<a target="_BLANK" href="$1">$1</a>', $value2["comment_text"]); // find and replace links with ancor tag
                    $sub_comments .= '
                        <li class="comment" id="comment-'.$value2['id'].'">
                            <div class="comment-body">
                                <figure class="comment-avatar">
                                    <img src="'.$image_path2.'" onerror="this.onerror=null;this.src=\''.base_url('assets/img/avatar/avatar-1.png').'\';" style="border-radius: 50%" alt="Commenter Avatar" width="90" height="90">
                                </figure>
                                <div class="comment-content" style="width: 100%;">
                                    <h4 class="comment-author">
                                        <span>'.$commenter2.'</span>
                                        <span class="comment-date">'.date("d M,y H:i",strtotime($value2['inserted_at'])).'</span>
                                    </h4>
                                    <p>'.nl2br($new_comment_formatted2).'</p>
                                    <div class="comment-action">
                                        '.$hide_link2.'
                                    </div>
                                </div>
                            </div>
                        </li>';
                }

            if($value['subscriber_id']=='') $commenter = $store_name." <i class='fas fa-user-circle text-primary'></i>";
            else $commenter = $value["first_name"]." ".$value["last_name"];
            $profile_pic = ($value['profile_pic']!="") ? $value["profile_pic"] :  base_url('assets/img/avatar/avatar-1.png');
            $image_path=($value["image_path"]!="") ? base_url($value["image_path"]) : $profile_pic;
            if($value['subscriber_id']=='' && !empty($store_favicon)) $image_path = "<img class='rounded-circle mr-3' style='height:50px;width:50px;' src='".base_url('upload/ecommerce/'.$store_favicon)."'>";
            $hide_link = "";
            if($this->user_id!='') $hide_link = '
            <a href="#" data-id="'.$value['id'].'"  class="btn btn-link btn-underline sm btn-icon-left font-weight-normal text-capitalize hide-comment">
                <i class="fa fa-eye-slash"></i>'.$this->lang->line("Hide").'
            </a>';
            $new_comment_formatted = preg_replace("/(https?:\/\/[a-zA-Z0-9\-._~\:\/\?#\[\]@!$&'\(\)*+,;=]+)/", '<a target="_BLANK" href="$1">$1</a>', $value["comment_text"]); // find and replace links with ancor tag
            $direct_link = $this->return_base_url_php("ecommerce/comment/".$value['id']);
            if($subscriber_id!="") $direct_link.="?subscriber_id=".$subscriber_id;
            $html.='
            <li class="comment" id="comment-'.$value['id'].'">
                <div class="comment-body">
                    <figure class="comment-avatar">
                        <img src="'.$image_path.'" style="border-radius: 50%" alt="Commenter Avatar" width="90" height="90">
                    </figure>
                    <div class="comment-content" style="width: 100%;">
                        <h4 class="comment-author">
                            <a href="'.$direct_link.'">'.$commenter.'</a>
                            <span class="comment-date">'.date("d M,y H:i",strtotime($value['inserted_at'])).'</span>
                        </h4>
                        <p>'.nl2br($new_comment_formatted).'</p>
                        <div class="comment-action">
                            <a href="#" data-id="#collapsecom'.$divId.'"  class="collapse_link  btn btn-link btn-underline sm btn-icon-left font-weight-normal text-capitalize" role="button" aria-expanded="false" aria-controls="collapsecom'.$divId.'">
                                <i class="far fa-comment"></i>'.$this->lang->line("Reply").'
                            </a>
                            '.$hide_link.'
                            <div class="pt-2" id="collapsecom'.$divId.'" style="display: none;">
                                <textarea class="form-control comment_reply" name="comment_reply" style="height:50px !important;"></textarea>
                                <button class="btn btn-dark leave_comment no_radius" parent-id='.$value["id"].'><i class="fa fa-reply"></i> '.$this->lang->line("Reply").'</button>              
                            </div>
                        </div>
                    </div>
                </div>
                <ul style="margin-top:50px; margin-left: 50px;">
                '.$sub_comments.'
                </ul>
            </li>

                  
';
        }
        echo json_encode(array("html"=>$html,"found"=>count($parent_comment_info)));
    }

    public function comment($comment_id=0){
        if($comment_id==0) exit();
        $this->ecommerce_review_comment_exist = $this->ecommerce_review_comment_exist();
        if(!$this->ecommerce_review_comment_exist) exit();
        $where_simple = array("ecommerce_product_comment.id"=>$comment_id,"ecommerce_product_comment.hidden"=>"0","ecommerce_product.deleted"=>"0");
        $where = array('where'=>$where_simple);
        $join = array('ecommerce_product'=>"ecommerce_product.id=ecommerce_product_comment.product_id,left",'ecommerce_store'=>"ecommerce_product_comment.store_id=ecommerce_store.id,left");
        $select = array("ecommerce_product_comment.*","product_name","ecommerce_product.user_id","store_name","store_unique_id","store_logo","store_favicon","terms_use_link","refund_policy_link","store_locale","pixel_id","google_id");
        $product_data = $this->basic->get_data("ecommerce_product_comment",$where,$select,$join);

        if(!isset($product_data[0]))
        {
            echo '<br/><h2 style="border:1px solid red;padding:15px;color:red">'.$this->lang->line("Comment not found.").'</h2>';
            exit();
        }
        $this->_language_loader($product_data[0]['store_locale']);

        $subscriber_id = $this->session->userdata($product_data[0]['store_id']."ecom_session_subscriber_id");
        if($subscriber_id=="") $subscriber_id = $this->input->get("subscriber_id",true);

        $user_id = isset($product_data[0]["user_id"]) ? $product_data[0]["user_id"] : 0;
        $fb_app_id = $this->get_app_id();
        $page_title = $product_data[0]['store_name']." | ".$product_data[0]['product_name']." | ".$this->lang->line("Comment")."#".$comment_id;
        $data = array('body'=>"ecommerce/comment_single","page_title"=>$page_title,"fb_app_id"=>$fb_app_id,"favicon"=>base_url('upload/ecommerce/'.$product_data[0]['store_favicon']));

        $category_list = $this->get_category_list($product_data[0]['store_id'],true);
        $cat_info=array();
        foreach($category_list as $value)
        {
            $cat_info[$value['id']] = $value['category_name'];
        }
        $data["category_list"] = $cat_info;
        $data["category_list_raw"] = $category_list;

        $data["product_data"] = $product_data[0];
        $data['current_cart'] = $this->get_current_cart($subscriber_id,$product_data[0]['store_id']);
        $data["social_analytics_codes"] = $product_data[0];
        $data['current_product_id'] = isset($product_data[0]['product_id']) ? $product_data[0]['product_id'] : 0;
        $data['current_store_id'] = isset($product_data[0]['store_id']) ? $product_data[0]['store_id'] : 0;
        $data['comment_id'] = $comment_id;
        $data['ecommerce_config'] = $this->get_ecommerce_config($product_data[0]["store_id"]);
        $this->load->view('ecommerce/bare-theme', $data);
    }

    private function check_access(){
        $eco_host = $_SERVER['HTTP_HOST'];

        $n_cd_data = $this->basic->get_data("n_custom_domain", array("where" => array(
            "host_url" => $eco_host,
            "active" => 1,
        )));
        //var_dump(empty($n_cd_data[0]) AND !isset($n_cd_data[0]));
        if (empty($n_cd_data[0]) AND !isset($n_cd_data[0])){
            include(APPPATH . 'custom_domain.php');
            $page_title = $this->lang->line("404 | Domain not recognized");
            $message = $this->lang->line("The domain you were looking for could not be found.");
            $orig_domain = $ncd_config['custom_domain_host'];
            include(APPPATH.'n_views/page/error_cname_new.php');
//            $this->load->view('page/error_cname', $data);
            exit;
        }

        $n_cd_data = $this->basic->get_data("ecommerce_store", array("where" => array("id" => $n_cd_data[0]['custom_id'])));
        if (!isset($n_cd_data[0])){
            include(APPPATH . 'custom_domain.php');
            $page_title = $this->lang->line("404 | Domain not recognized");
            $message = $this->lang->line("The domain you were looking for could not be found.");
            $orig_domain = $ncd_config['custom_domain_host'];

            include(APPPATH.'n_views/page/error_cname_new.php');
//            $this->load->view('page/error_cname', $data);
            exit;
        }

        $where_q = array(
            "where"=>array("id"=>$n_cd_data[0]['user_id'])
        );

        $info = $this->basic->get_data('users', $where_q, $select = 'user_type, expired_date, package_id', $join = '', $limit = '', $start = '', $order_by = '', $group_by = '', $num_rows = 1);

        $count = $info['extra_index']['num_rows'];
        if ($count == 0) {
            $page_title = $this->lang->line("404 | Domain not recognized");
            $message = $this->lang->line("The domain you were looking for could not be found.");
            include(APPPATH.'n_views/page/error_cname.php');
//            $this->load->view('page/error_cname', $data);
            exit;
        }

        $expire_date = strtotime($info[0]['expired_date']);
        $current_date = strtotime(date("Y-m-d"));
        if($expire_date < $current_date AND $info[0]['user_type']=='MEMBER'){
            $page_title = $this->lang->line("404 | Domain not recognized");
            $message = $this->lang->line("The domain you were looking has user expired.");
            include(APPPATH.'n_views/page/error_cname.php');
//            $this->load->view('page/error_cname', $data);
            exit;
        }

        $where_q = array(
            "where"=>array("id"=>$info[0]['package_id'])
        );
        $info_p = $this->basic->get_data('package', $where_q, $select = 'module_ids', $join = '', $limit = '', $start = '', $order_by = '', $group_by = '', $num_rows = 1);

        $count = $info_p['extra_index']['num_rows'];
        if ($count == 0 AND $info[0]['user_type']=='MEMBER') {
            $page_title = $this->lang->line("404 | Domain not recognized");
            $message = $this->lang->line("The domain you were looking has user no access to use custom domain.");
            include(APPPATH.'n_views/page/error_cname.php');
//            $this->load->view('page/error_cname', $data);
            exit;
        }

        if(!empty($info_p[0]) AND $info[0]['user_type']=='MEMBER'){
            $access_q = explode(',', $info_p[0]['module_ids']);

            if(!in_array(3100,$access_q)){
                $page_title = $this->lang->line("404 | Domain not recognized");
                $message = $this->lang->line("The domain you were looking has user no access to use custom domain.");
                include(APPPATH.'n_views/page/error_cname.php');
//            $this->load->view('page/error_cname', $data);
                exit;
            }
        }

        return $n_cd_data;
    }

    private function initial_load_store($store_unique_id){
        if($store_unique_id==0) exit();
        $this->ecommerce_review_comment_exist = $this->ecommerce_review_comment_exist();
        $where_simple = array("ecommerce_store.store_unique_id"=>$store_unique_id,"ecommerce_store.status"=>'1');
        $where = array('where'=>$where_simple);
        $store_data = $this->basic->get_data("ecommerce_store",$where);

        if(!isset($store_data[0])) {
            echo '<br/><h2 style="border:1px solid red;padding:15px;color:red">'.$this->lang->line("Store not found.").'</h2>';
            exit();
        }

        $this->_language_loader($store_data[0]['store_locale']);
        $store_id = $store_data[0]['id'];
        $user_id = $store_data[0]['user_id'];

        return $store_data;
    }

    private function get_app_id(){
        $fb_app_id_info=$this->basic->get_data('facebook_rx_config',$where=array('where'=>array('status'=>'1')),"api_id");
        $fb_app_id = isset($fb_app_id_info[0]['api_id']) ? $fb_app_id_info[0]['api_id'] : "";
        return $fb_app_id;
    }

    private function get_ecommerce_config($store_id='0'){
        if($store_id=='0') $store_id = $this->session->userdata("ecommerce_selected_store");
        $data = $this->basic->get_data("ecommerce_config",array("where"=>array("store_id"=>$store_id)));
        if(isset($data[0])) return $data[0];
        else return array();
    }

    private function get_category_list($store_id=0,$raw_data=false){
        if($store_id==0) $store_id = $this->session->userdata("ecommerce_selected_store");
        $cat_list = $this->basic->get_data("ecommerce_category",array("where"=>array("store_id"=>$store_id,"status"=>"1")),$select='',$join='',$limit='',$start=NULL,$order_by='serial asc, category_name asc');
        if($raw_data) return $cat_list;
        $cat_info=array();
        foreach($cat_list as $value)
        {
            $cat_info[$value['id']] = $value['category_name'];
        }
        return $cat_info;
    }

    private function get_config(){
        include(APPPATH.'n_views/default_ecommerce_builder.php');
        if(file_exists(APPPATH . '/n_eco_user/builder/ecommerce_builder_' . $this->nstore_id . '.php')){
            include(APPPATH . '/n_eco_user/builder/ecommerce_builder_' . $this->nstore_id . '.php');
        }
        return $n_eco_builder_config;
    }

    private function get_store_id($store_id){
        if($store_id==0){
            $store_id = $this->nstore_id;
        }
        if($store_id==0){
            exit();
        }
        return $store_id;
    }

    private function get_store_uq($store_id){
        $store_data_temp = $this->basic->get_data("ecommerce_store",array("where"=>array("id"=>$store_id)),"store_unique_id");
        if(empty($store_data_temp)){return 0;}
        return $store_data_temp[0]['store_unique_id'];
    }

    private function get_store_ids($store_id){
        $store_data_temp = $this->basic->get_data("ecommerce_store",array("where"=>array("store_unique_id"=>$store_id)),"id");
        if(empty($store_data_temp[0])){
            return 0;
        }
        return $store_data_temp[0]['id'];
    }

    private function get_payment_status()
    {
        return array('pending'=>$this->lang->line('Pending'),'approved'=>$this->lang->line('Approved'),'rejected'=>$this->lang->line('Rejected'),'shipped'=>$this->lang->line('Shipped'),'delivered'=>$this->lang->line('Delivered'),'completed'=>$this->lang->line('Completed'));
    }

    public function new_review(){
        $this->ajax_check();
        $this->load->helper("ecommerce");
        $insert_id = $this->input->post("insert_id",true);
        $cart_id = $this->input->post("cart_id",true);
        $product_id = $this->input->post("product_id",true);
        $store_id = $this->input->post("store_id",true);
        $reason = strip_tags($this->input->post("reason",true));
        $rating = $this->input->post("rating",true);
        $review = strip_tags($this->input->post("review",true));
        $product_name = strip_tags($this->input->post("product_name",true));
        $need_to_login =false;

        $where_subs = array();
        $subscriber_id=$this->session->userdata($store_id."ecom_session_subscriber_id");
        if($subscriber_id!="") $where_subs = array("subscriber_type"=>"system","subscribe_id"=>$subscriber_id,"store_id"=>$store_id);
        else
        {
            if($subscriber_id=="") $subscriber_id = $this->input->post("subscriber_id",true);
            if($subscriber_id!="") $where_subs = array("subscriber_type!="=>"system","subscribe_id"=>$subscriber_id);
        }
        if($subscriber_id=="") $need_to_login = true;
        else
        {
            $subscriber_info = $this->basic->count_row("messenger_bot_subscriber",array("where"=>$where_subs),"id");
            if($subscriber_info[0]['total_rows']==0) $need_to_login = true;
        }


        if($need_to_login)
        {
            echo json_encode(array('status'=>'0','message'=>$this->login_to_continue,"login_popup"=>'1'));
            exit();
        }

        $join_me = array('ecommerce_cart_item'=>"ecommerce_cart_item.cart_id=ecommerce_cart.id,left");
        $has_purchase_array = $this->basic->count_row("ecommerce_cart",array("where"=>array("subscriber_id"=>$subscriber_id,"product_id"=>$product_id),"where_not_in"=>array("status"=>array("pending","rejected"))),'count(cart_id) as total_row',$join_me,'cart_id');
        if($has_purchase_array[0]['total_rows']==0)
        {
            echo json_encode(array('status'=>'0','message'=>$this->lang->line("You have not purchased this item.")));
            exit();
        }


        $datetime = date("Y-m-d H:i:s");
        if($cart_id=='') $cart_id = 0;

        $insert_data = array
        (
            "store_id"=>$store_id,
            "cart_id"=>$cart_id,
            "product_id"=>$product_id,
            "subscriber_id"=>$subscriber_id,
            "reason"=>$reason,
            "rating"=>$rating,
            "review"=>$review,
            "review_reply" => "",
            "replied_by_user_id" => 0,
            "inserted_at"=>$datetime
        );

        if($this->basic->is_exist("ecommerce_product_review",array("product_id"=>$product_id,"subscriber_id"=>$subscriber_id),'id'))
            $this->basic->update_data("ecommerce_product_review",array("product_id"=>$product_id,"subscriber_id"=>$subscriber_id),$insert_data);
        else
        {
            $this->basic->insert_data("ecommerce_product_review",$insert_data);
            $insert_id = $this->db->insert_id();
        }


        $store_data = $this->basic->get_data("ecommerce_store",array("where"=>array("id"=>$store_id)),array("user_id","store_name","store_favicon"));
        $store_name = isset($store_data[0]["store_name"])?$store_data[0]["store_name"]:"";
        $store_favicon = isset($store_data[0]["store_favicon"])?$store_data[0]["store_favicon"]:"";
        $store_admin_user_id = isset($store_data[0]["user_id"])?$store_data[0]["user_id"]:"0";

        $subscriber_info=$this->basic->get_data("messenger_bot_subscriber",array("where"=>array("subscribe_id"=>$subscriber_id)),array("first_name","last_name","profile_pic","image_path"));
        $first_name = isset($subscriber_info[0]['first_name']) ? $subscriber_info[0]['first_name'] : "";
        $last_name = isset($subscriber_info[0]['last_name']) ? $subscriber_info[0]['last_name'] : "";
        $commenter = $first_name." ".$last_name;

        $stars = mec_display_rating_starts($rating);

        $direct_link = base_url("ecommerce/review/".$insert_id);
        $invoice_link = base_url("ecommerce/order/".$cart_id);

        $description = $this->lang->line("Hello")." ".$store_name." ".$this->lang->line("admin").",<br><br>";
        $description .= "<b>".$commenter."</b> ".$this->lang->line("just posted a review on ecommerce store")." <b>".$store_name."</b> <i>@".$product_name."</i><br><br><blockquote>".$stars."<b>".$reason."</b> : ".$review."</blockquote>";
        $description .= $this->lang->line("You can reply this review here")." : ".$direct_link."<br><br>";
        $description .= $this->lang->line("You can see the invoice here")." : ".$invoice_link."<br><br>".$this->lang->line("Thanks");

        if($this->user_id != $store_admin_user_id)
        {
            $announcement_insert = array
            (
                'title'=> $this->lang->line("New review on ")." : ".$product_name,
                'description'=> $description,
                'status'=>"published",
                'created_at'=>$datetime,
                'user_id' => $store_admin_user_id,
                'color_class' => 'dark',
                'icon' => 'fas fa-star orange'
            );
            $this->basic->insert_data("announcement",$announcement_insert);
        }


        echo json_encode(array('status'=>'1','message'=>$this->lang->line("Review has been submitted successfully.")));
    }

    public function review($review_id=0)
    {
        if($review_id==0) exit();
        $this->ecommerce_review_comment_exist = $this->ecommerce_review_comment_exist();
        if(!$this->ecommerce_review_comment_exist) exit();

        $where_simple = array("ecommerce_product_review.id"=>$review_id,"ecommerce_product_review.hidden"=>"0","ecommerce_product.deleted"=>"0");
        $where = array('where'=>$where_simple);
        $join = array('ecommerce_product'=>"ecommerce_product.id=ecommerce_product_review.product_id,left",'ecommerce_store'=>"ecommerce_product_review.store_id=ecommerce_store.id,left",'messenger_bot_subscriber'=>"messenger_bot_subscriber.subscribe_id=ecommerce_product_review.subscriber_id,left");
        $select = array("ecommerce_product_review.*","product_name","ecommerce_product.user_id","store_name","store_unique_id","store_logo","store_favicon","terms_use_link","refund_policy_link","store_locale","pixel_id","google_id","first_name","last_name","profile_pic","image_path");
        $product_data = $this->basic->get_data("ecommerce_product_review",$where,$select,$join);

        if(!isset($product_data[0]))
        {
            echo '<br/><h2 style="border:1px solid red;padding:15px;color:red">'.$this->lang->line("Review not found.").'</h2>';
            exit();
        }
        $this->_language_loader($product_data[0]['store_locale']);

        $subscriber_id = $this->session->userdata($product_data[0]['store_id']."ecom_session_subscriber_id");
        if($subscriber_id=="") $subscriber_id = $this->input->get("subscriber_id",true);

        $user_id = isset($product_data[0]["user_id"]) ? $product_data[0]["user_id"] : 0;
        $fb_app_id = $this->get_app_id();
        $page_title = $product_data[0]['store_name']." | ".$product_data[0]['product_name']." | ".$this->lang->line("Review")."#".$review_id;
        $data = array('body'=>"ecommerce/review_single","page_title"=>$page_title,"fb_app_id"=>$fb_app_id,"favicon"=>base_url('upload/ecommerce/'.$product_data[0]['store_favicon']));

        $category_list = $this->get_category_list($product_data[0]['store_id'],true);
        $cat_info=array();
        foreach($category_list as $value)
        {
            $cat_info[$value['id']] = $value['category_name'];
        }
        $data["category_list"] = $cat_info;
        $data["category_list_raw"] = $category_list;

        $data["review_data"] = $product_data;
        $data['current_cart'] = $this->get_current_cart($subscriber_id,$product_data[0]['store_id']);
        $data["social_analytics_codes"] = $product_data[0];
        $data['current_product_id'] = isset($product_data[0]['product_id']) ? $product_data[0]['product_id'] : 0;
        $data['current_store_id'] = isset($product_data[0]['store_id']) ? $product_data[0]['store_id'] : 0;
        $data['review_id'] = $review_id;
        $data['ecommerce_config'] = $this->get_ecommerce_config($product_data[0]["store_id"]);
        $this->load->view('ecommerce/bare-theme', $data);
    }

    public function _language_loader($default_lang = ''){

        if(!empty($this->session->userdata("selected_language")) AND file_exists(APPPATH.'language/'.$this->session->userdata("selected_language").'/v_5_0_a_lang.php') ){
            $default_lang = $this->session->userdata("selected_language");
        }

        HOME::_language_loader($default_lang);

    }

    public function proceed_checkout()
    {
        $this->ajax_check();
        $mydata = json_decode($this->input->post("mydata"),true);
        $tmp_store_id = isset($mydata['store_id']) ? $mydata['store_id'] : '0';

        $cart_id = isset($mydata["cart_id"]) ? $mydata["cart_id"] : 0;

        $where_subs = array();
        $subscriber_id=$this->session->userdata($tmp_store_id."ecom_session_subscriber_id");
        if($subscriber_id!="") $where_subs = array("subscriber_type"=>"system","subscribe_id"=>$subscriber_id,"store_id"=>$tmp_store_id);
        else
        {
            if($subscriber_id=="") $subscriber_id = isset($mydata['subscriber_id']) ? $mydata['subscriber_id'] : '';
            if($subscriber_id!="") $where_subs = array("subscriber_type!="=>"system","subscribe_id"=>$subscriber_id);
        }
        if($subscriber_id=="")
        {
            echo json_encode(array('status'=>'0','message'=>$this->login_to_continue,'login_popup'=>'1'));
            exit();
        }
        $subscriber_info = $this->basic->count_row("messenger_bot_subscriber",array("where"=>$where_subs),"id");
        if($subscriber_info[0]['total_rows']==0){
            echo json_encode(array('status'=>'0','message'=>$this->login_to_continue,'login_popup'=>'1'));
            exit();
        }

        $select = array("store_name","store_id","store_unique_id","store_favicon","store_locale","paypal_enabled","stripe_enabled","razorpay_enabled","paystack_enabled","mollie_enabled","mercadopago_enabled","sslcommerz_enabled","toyyibpay_enabled","myfatoorah_enabled","paymaya_enabled","manual_enabled","senangpay_enabled","instamojo_enabled","xendit_enabled","cod_enabled","ecommerce_cart.user_id as user_id","payment_amount");

        if(file_exists(APPPATH.'modules/n_omise/controllers/N_omise.php')) {
            $select[] = 'n_omise_enabled';
        }

        if(file_exists(APPPATH.'modules/n_paymongo/controllers/N_paymongo.php')) {
            $select[] = 'n_paymongo_enabled';
            $select[] = 'n_paymongo_paymaya_en';
            $select[] = 'n_paymongo_grab_en';
            $select[] = 'n_paymongo_gcash_en';
        }

        if(file_exists(APPPATH.'modules/n_paymentwall/controllers/N_paymentwall.php')) {
            $select[] = 'n_paymentwall_enabled';
        }

        $cart_data = $this->valid_cart_data($cart_id,$subscriber_id,$select);

        if(!isset($cart_data[0]) || (isset($cart_data[0]) && $cart_data[0]["store_id"]=="") )
        {
            echo json_encode(array('status'=>'0','message'=>$this->lang->line('Order data not found.')));
            exit();
        }
        $this->_language_loader($cart_data[0]['store_locale']);

        $subscriber_first_name = isset($mydata['subscriber_first_name']) ? $mydata['subscriber_first_name'] : '';
        $subscriber_last_name = isset($mydata['subscriber_last_name']) ? $mydata['subscriber_last_name'] : '';
        $subscriber_country = isset($mydata['subscriber_country']) ? $mydata['subscriber_country'] : '';
        $delivery_address_id = isset($mydata['delivery_address_id']) ? $mydata['delivery_address_id'] : '';
        $store_pickup = isset($mydata['store_pickup']) ? $mydata['store_pickup'] : '0';
        $pickup_point_details = isset($mydata['pickup_point_details']) ? strip_tags($mydata['pickup_point_details']) : '';
        $delivery_note = isset($mydata['delivery_note']) ? $this->security->xss_clean(strip_tags($mydata['delivery_note'])) : '';
        $delivery_time = isset($mydata['delivery_time']) ? $this->security->xss_clean(strip_tags($mydata['delivery_time'])) : '';

        $store_id = $cart_data[0]["store_id"];
        $user_id = $cart_data[0]["user_id"];
        $payment_amount = $cart_data[0]["payment_amount"];
        $store_name = $cart_data[0]["store_name"];
        $paypal_enabled = $cart_data[0]["paypal_enabled"];
        $stripe_enabled = $cart_data[0]["stripe_enabled"];
        $toyyibpay_enabled = $cart_data[0]["toyyibpay_enabled"];
        $paymaya_enabled = $cart_data[0]["paymaya_enabled"];
        $myfatoorah_enabled = $cart_data[0]["myfatoorah_enabled"];
        $manual_enabled = $cart_data[0]["manual_enabled"];
        $cod_enabled = $cart_data[0]["cod_enabled"];
        $razorpay_enabled = $cart_data[0]["razorpay_enabled"];
        $paystack_enabled = $cart_data[0]["paystack_enabled"];
        $mollie_enabled = $cart_data[0]["mollie_enabled"];
        $mercadopago_enabled = $cart_data[0]["mercadopago_enabled"];
        $sslcommerz_enabled = $cart_data[0]["sslcommerz_enabled"];
        $senangpay_enabled = $cart_data[0]["senangpay_enabled"];
        $n_omise_enabled = isset($cart_data[0]["n_omise_enabled"]) ? $cart_data[0]["n_omise_enabled"] : 0;
        $n_paymongo_enabled = isset($cart_data[0]["n_paymongo_enabled"]) ? $cart_data[0]["n_paymongo_enabled"] : 0;
        $n_paymongo_paymaya_en = isset($cart_data[0]["n_paymongo_paymaya_en"]) ? $cart_data[0]["n_paymongo_paymaya_en"] : 0;
        $n_paymongo_grab_en = isset($cart_data[0]["n_paymongo_grab_en"]) ? $cart_data[0]["n_paymongo_grab_en"] : 0;
        $n_paymongo_gcash_en = isset($cart_data[0]["n_paymongo_gcash_en"]) ? $cart_data[0]["n_paymongo_gcash_en"] : 0;
        $n_paymentwall_enabled = isset($cart_data[0]["n_paymentwall_enabled"]) ? $cart_data[0]["n_paymentwall_enabled"] : 0;

        $instamojo_enabled = $cart_data[0]["instamojo_enabled"];
        $xendit_enabled = $cart_data[0]["xendit_enabled"];
        $store_favicon = $cart_data[0]["store_favicon"];
        if($store_favicon!="") $store_favicon = base_url("upload/ecommerce/".$store_favicon);

        $ecommerce_config =  $this->get_ecommerce_config($store_id);
        $paypal_email = isset($ecommerce_config['paypal_email']) ? $ecommerce_config['paypal_email'] : '';
        $paypal_mode = isset($ecommerce_config['paypal_mode']) ? $ecommerce_config['paypal_mode'] : 'live';
        $stripe_secret_key = isset($ecommerce_config['stripe_secret_key']) ? $ecommerce_config['stripe_secret_key'] : '';
        $stripe_publishable_key = isset($ecommerce_config['stripe_publishable_key']) ? $ecommerce_config['stripe_publishable_key'] : '';
        $stripe_billing_address = isset($ecommerce_config['stripe_billing_address']) ? $ecommerce_config['stripe_billing_address'] : '0';
        $razorpay_key_id = isset($ecommerce_config['razorpay_key_id']) ? $ecommerce_config['razorpay_key_id'] : '';
        $razorpay_key_secret = isset($ecommerce_config['razorpay_key_secret']) ? $ecommerce_config['razorpay_key_secret'] : '';
        $paystack_secret_key = isset($ecommerce_config['paystack_secret_key']) ? $ecommerce_config['paystack_secret_key'] : '';
        $paystack_public_key = isset($ecommerce_config['paystack_public_key']) ? $ecommerce_config['paystack_public_key'] : '';
        $mollie_api_key = isset($ecommerce_config['mollie_api_key']) ? $ecommerce_config['mollie_api_key'] : '';
        $mercadopago_public_key = isset($ecommerce_config['mercadopago_public_key']) ? $ecommerce_config['mercadopago_public_key'] : '';
        $mercadopago_access_token = isset($ecommerce_config['mercadopago_access_token']) ? $ecommerce_config['mercadopago_access_token'] : '';
        $sslcommerz_store_id = isset($ecommerce_config['sslcommerz_store_id']) ? $ecommerce_config['sslcommerz_store_id'] : '';
        $sslcommerz_store_password = isset($ecommerce_config['sslcommerz_store_password']) ? $ecommerce_config['sslcommerz_store_password'] : '';
        $marcadopago_country = isset($ecommerce_config['marcadopago_country']) ? $ecommerce_config['marcadopago_country'] : '';
        $senangpay_merchent_id = isset($ecommerce_config['senangpay_merchent_id']) ? $ecommerce_config['senangpay_merchent_id'] : '';
        $senangpay_secret_key = isset($ecommerce_config['senangpay_secret_key']) ? $ecommerce_config['senangpay_secret_key'] : '';
        $senangpay_mode = isset($ecommerce_config['senangpay_mode']) ? $ecommerce_config['senangpay_mode'] : 'live';
        $instamojo_api_key = isset($ecommerce_config['instamojo_api_key']) ? $ecommerce_config['instamojo_api_key'] : '';
        $instamojo_auth_token = isset($ecommerce_config['instamojo_auth_token']) ? $ecommerce_config['instamojo_auth_token'] : '';
        $instamojo_mode = isset($ecommerce_config['instamojo_mode']) ? $ecommerce_config['instamojo_mode'] : 'live';
        $myfatoorah_api_key = isset($ecommerce_config['myfatoorah_api_key']) ? $ecommerce_config['myfatoorah_api_key'] : '';
        $myfatoorah_mode = isset($ecommerce_config['myfatoorah_mode']) ? $ecommerce_config['myfatoorah_mode'] : 'live';
        $toyyibpay_secret_key = isset($ecommerce_config['toyyibpay_secret_key']) ? $ecommerce_config['toyyibpay_secret_key'] : '';
        $toyyibpay_category_code = isset($ecommerce_config['toyyibpay_category_code']) ? $ecommerce_config['toyyibpay_category_code'] : '';
        $toyyibpay_mode = isset($ecommerce_config['toyyibpay_mode']) ? $ecommerce_config['toyyibpay_mode'] : 'live';

        $paymaya_public_key = isset($ecommerce_config['paymaya_public_key']) ? $ecommerce_config['paymaya_public_key'] : '';
        $paymaya_secret_key = isset($ecommerce_config['paymaya_secret_key']) ? $ecommerce_config['paymaya_secret_key'] : '';
        $paymaya_mode = isset($ecommerce_config['paymaya_mode']) ? $ecommerce_config['paymaya_mode'] : 'live';
        $xendit_secret_api_key = isset($ecommerce_config['xendit_secret_api_key']) ? $ecommerce_config['xendit_secret_api_key'] : '';
        $manual_payment_instruction = isset($ecommerce_config['manual_payment_instruction']) ? $ecommerce_config['manual_payment_instruction'] : '';
        $currency = isset($ecommerce_config['currency']) ? $ecommerce_config['currency'] : 'USD';
        $is_order_schedule = isset($ecommerce_config['is_order_schedule']) ? $ecommerce_config['is_order_schedule'] : '0';

        $n_omise_pubkey = isset($ecommerce_config['n_omise_pubkey']) ? $ecommerce_config['n_omise_pubkey'] : '';
        $n_omise_seckey = isset($ecommerce_config['n_omise_seckey']) ? $ecommerce_config['n_omise_seckey'] : '';

        $n_paymongo_pub = isset($ecommerce_config['n_paymongo_pub']) ? $ecommerce_config['n_paymongo_pub'] : '';
        $n_paymongo_sec = isset($ecommerce_config['n_paymongo_sec']) ? $ecommerce_config['n_paymongo_sec'] : '';

        $n_paymentwall_pub = isset($ecommerce_config['n_paymentwall_pub']) ? $ecommerce_config['n_paymentwall_pub'] : '';
        $n_paymentwall_sec = isset($ecommerce_config['n_paymentwall_sec']) ? $ecommerce_config['n_paymentwall_sec'] : '';


        if($is_order_schedule=='1')
        {
            $check_delivery_time = $delivery_time;
            if($check_delivery_time=="") $check_delivery_time = date("Y-m-d H:i:s");
            $check_date  = date('Y-m-d',strtotime($check_delivery_time));
            $check_day  = date('l',strtotime($check_delivery_time));
            $check_time  = date('H:i:s',strtotime($check_delivery_time));
            $ecommerce_store_business_hours = array();
            if(!empty($check_day))
                $ecommerce_store_business_hours  = $this->basic->get_data("ecommerce_store_business_hours",array("where"=>array("store_id"=>$store_id,"schedule_day"=>$check_day)));
            if(isset($ecommerce_store_business_hours[0]))
            {
                if($ecommerce_store_business_hours[0]['off_day']=='1')
                {
                    echo json_encode(array('status'=>'2','message'=>$this->lang->line('Sorry, but we cannot take the order. We are closed on')." ".$this->lang->line($check_day).'!'));
                    exit();
                }
                else
                {
                    $start_time = $check_date." ".$ecommerce_store_business_hours[0]['start_time'].":00";
                    $end_time = $check_date." ".$ecommerce_store_business_hours[0]['end_time'].":00";

                    $time_ok = false;
                    if(strtotime($check_delivery_time) >= strtotime($start_time) && strtotime($check_delivery_time) <= strtotime($end_time))
                        $time_ok = true;

                    if(!$time_ok)
                    {
                        if($delivery_time=='') echo json_encode(array('status'=>'2','message'=>$this->lang->line('Sorry, but we cannot take the order. We are closed now.')));
                        else echo json_encode(array('status'=>'2','message'=>$this->lang->line('Sorry, but we cannot take the order. We will be closed at the selected time. Please try selecting a new delivery time.')));
                        exit();
                    }

                }
            }
        }

        $buyer_first_name  = $bill_first_name = $subscriber_first_name;
        $buyer_last_name = $bill_last_name = $subscriber_last_name;
        $buyer_country =  $bill_country = $subscriber_country;
        $buyer_email = $buyer_mobile = $buyer_address = $buyer_state = $buyer_city = $buyer_zip = "";
        $bill_email = $bill_mobile = $bill_city = $bill_state = $bill_address = $bill_zip = "";


        $billing_data = $this->basic->get_data("ecommerce_cart_address_saved",array("where"=>array("subscriber_id"=>$subscriber_id,"profile_address"=>"1")));
        if(isset($billing_data[0])){
            $bill_first_name = $billing_data[0]['first_name'];
            $bill_last_name = $billing_data[0]['last_name'];
            $bill_country = $billing_data[0]['country'];
            $bill_email = $billing_data[0]['email'];
            $bill_mobile = $billing_data[0]['mobile'];
            $bill_city = $billing_data[0]['city'];
            $bill_state = $billing_data[0]['state'];
            $bill_address = $billing_data[0]['address'];
            $bill_zip = $billing_data[0]['zip'];
        }
        if(!empty($delivery_address_id) && $store_pickup=='0'){
            $delivery_data = $this->basic->get_data("ecommerce_cart_address_saved",array("where"=>array("subscriber_id"=>$subscriber_id,"id"=>$delivery_address_id)));
            if(isset($delivery_data[0]))
            {
                $this->basic->update_data("ecommerce_cart_address_saved",array("subscriber_id"=>$subscriber_id),array("is_default"=>'0'));
                $this->basic->update_data("ecommerce_cart_address_saved",array("subscriber_id"=>$subscriber_id,"id"=>$delivery_address_id),array("is_default"=>'1'));
                $buyer_first_name = $delivery_data[0]['first_name'];
                $buyer_last_name = $delivery_data[0]['last_name'];
                $buyer_country = $delivery_data[0]['country'];
                $buyer_email = $delivery_data[0]['email'];
                $buyer_mobile = $delivery_data[0]['mobile'];
                $buyer_city = $delivery_data[0]['city'];
                $buyer_state = $delivery_data[0]['state'];
                $buyer_address = $delivery_data[0]['address'];
                $buyer_zip = $delivery_data[0]['zip'];
            }
        }
        else $buyer_first_name = $buyer_last_name = $buyer_country = "";

        if($n_omise_enabled=='1'  && ($n_omise_pubkey=='' || $n_omise_seckey==''))
        {
            echo json_encode(array('status'=>'0','message'=>$this->lang->line('Omise payment settings not found.')));
            exit();
        }

        if($n_paymongo_enabled=='1'  && ($n_paymongo_pub=='' || $n_paymongo_sec==''))
        {
            echo json_encode(array('status'=>'0','message'=>$this->lang->line('Paymongo payment settings not found.')));
            exit();
        }

        if($n_paymentwall_enabled=='1'  && ($n_paymentwall_pub=='' || $n_paymentwall_sec==''))
        {
            echo json_encode(array('status'=>'0','message'=>$this->lang->line('Paymentwall payment settings not found.')));
            exit();
        }

        if($paypal_enabled=='1'  && $paypal_email=='')
        {
            echo json_encode(array('status'=>'0','message'=>$this->lang->line('PayPal payment settings not found.')));
            exit();
        }

        if($stripe_enabled=='1'  && ($stripe_secret_key=='' || $stripe_publishable_key==''))
        {
            echo json_encode(array('status'=>'0','message'=>$this->lang->line('Stripe payment settings not found.')));
            exit();
        }
        if($razorpay_enabled=='1'  && ($razorpay_key_id=='' || $razorpay_key_secret==''))
        {
            echo json_encode(array('status'=>'0','message'=>$this->lang->line('Razorpay payment settings not found.')));
            exit();
        }
        if($paystack_enabled=='1'  && ($paystack_secret_key=='' || $paystack_public_key==''))
        {
            echo json_encode(array('status'=>'0','message'=>$this->lang->line('Paystack payment settings not found.')));
            exit();
        }
        if($mollie_enabled=='1'  && $mollie_api_key=='')
        {
            echo json_encode(array('status'=>'0','message'=>$this->lang->line('Mollie payment settings not found.')));
            exit();
        }
        if($mercadopago_enabled=='1'  && ($mercadopago_public_key=='' || $mercadopago_access_token==''))
        {
            echo json_encode(array('status'=>'0','message'=>$this->lang->line('Mercado Pago payment settings not found.')));
            exit();
        }
        if($sslcommerz_enabled=='1'  && ($sslcommerz_store_id=='' || $sslcommerz_store_password==''))
        {
            echo json_encode(array('status'=>'0','message'=>$this->lang->line('Sslcommerz payment settings not found.')));
            exit();
        }
        if($senangpay_enabled=='1'  && ($senangpay_merchent_id=='' || $senangpay_secret_key==''))
        {
            echo json_encode(array('status'=>'0','message'=>$this->lang->line('Senangpay payment settings not found.')));
            exit();
        }
        if($instamojo_enabled=='1'  && ($instamojo_api_key=='' || $instamojo_auth_token==''))
        {
            echo json_encode(array('status'=>'0','message'=>$this->lang->line('Instamojo payment settings not found.')));
            exit();
        }
        if($toyyibpay_enabled=='1'  && ($toyyibpay_secret_key=='' || $toyyibpay_category_code==''))
        {
            echo json_encode(array('status'=>'0','message'=>$this->lang->line('Toyyibpay payment settings not found.')));
            exit();
        }

        if($paymaya_enabled=='1'  && ($paymaya_secret_key=='' || $paymaya_public_key==''))
        {
            echo json_encode(array('status'=>'0','message'=>$this->lang->line('Paymaya payment settings not found.')));
            exit();
        }

        if($myfatoorah_enabled=='1'  && $myfatoorah_api_key=='')
        {
            echo json_encode(array('status'=>'0','message'=>$this->lang->line('Myfatoorah payment settings not found.')));
            exit();
        }

        if($xendit_enabled=='1'  && $xendit_secret_api_key=='')
        {
            echo json_encode(array('status'=>'0','message'=>$this->lang->line('Xendit payment settings not found.')));
            exit();
        }
        if($manual_enabled=='1'  && $manual_payment_instruction=='')
        {
            echo json_encode(array('status'=>'0','message'=>$this->lang->line('Manual payment settings not found.')));
            exit();
        }

        $curtime = date("Y-m-d H:i:s");
        $update_data = array
        (
            "store_pickup"=>$store_pickup,
            "buyer_first_name"=>$buyer_first_name,
            "buyer_last_name"=>$buyer_last_name,
            "buyer_email"=>$buyer_email,
            "buyer_mobile"=>$buyer_mobile,
            "buyer_country"=>$buyer_country,
            "buyer_state"=>$buyer_state,
            "buyer_city"=>$buyer_city,
            "buyer_address"=>$buyer_address,
            "buyer_zip"=>$buyer_zip,
            "updated_at"=>$curtime,
            "buyer_zip"=>$buyer_zip,
            "bill_first_name"=>$bill_first_name,
            "bill_last_name"=>$bill_last_name,
            "bill_country"=>$bill_country,
            "bill_email"=>$bill_email,
            "bill_mobile"=>$bill_mobile,
            "bill_city"=>$bill_city,
            "bill_state"=>$bill_state,
            "bill_address"=>$bill_address,
            "bill_zip"=>$bill_zip,
            "pickup_point_details"=>$pickup_point_details,
            "delivery_note"=>$delivery_note,
            "delivery_time"=>$delivery_time
        );

        $this->basic->update_data("ecommerce_cart",array("id"=>$cart_id,"subscriber_id"=>$subscriber_id,"action_type !="=>"checkout"),$update_data);

        $customer_mobile = $bill_mobile!='' ? $bill_mobile : $buyer_mobile;
        $customer_email = $bill_email!='' ? $bill_email : $buyer_email;
        $customer_first_name = $bill_first_name!='' ? $bill_first_name : $buyer_first_name;
        $customer_last_name = $bill_last_name!='' ? $bill_last_name : $buyer_last_name;
        $customer_name = $customer_first_name." ".$customer_last_name;
        $only_domain = get_domain_only(base_url());
        $fake_email = "ecommerce@".$only_domain;

        $paypal_button = $stripe_button = $razorpay_button = $paystack_button = $mollie_button = $manual_button = $mercadopago_button = $sslcommerz_button = $senangpay_button = $instamojo_button = $xendit_button = $cod_button = $toyyibpay_button = $myfatoorah_button = $paymaya_button = "";
        $product_name  = $store_name." : ".$this->lang->line("Order")." #".$cart_id;


        if($paypal_enabled=="1"){
            $this->load->library('paypal_class_ecommerce');
            $cancel_url=base_url()."ecommerce/order/".$cart_id."?subscriber_id=".$subscriber_id."&action=cancel";
            $success_url=base_url()."ecommerce/order/".$cart_id."?subscriber_id=".$subscriber_id."&action=success";

            $this->paypal_class_ecommerce->mode=$paypal_mode;
            $this->paypal_class_ecommerce->cancel_url=$cancel_url;
            $this->paypal_class_ecommerce->success_url=$success_url;
            $this->paypal_class_ecommerce->notify_url=base_url("ecommerce/paypal_action/".$store_id);
            $this->paypal_class_ecommerce->business_email=$paypal_email;
            $this->paypal_class_ecommerce->amount=$payment_amount;
            $this->paypal_class_ecommerce->user_id=$user_id;
            $this->paypal_class_ecommerce->currency=$currency;
            $this->paypal_class_ecommerce->cart_id=$cart_id;
            $this->paypal_class_ecommerce->subscriber_id=$subscriber_id;
            $this->paypal_class_ecommerce->product_name=$product_name;
            $this->paypal_class_ecommerce->button_lang=$this->lang->line("Pay with PayPal");
            $this->paypal_class_ecommerce->secondary_button = true;
            $paypal_button = $this->paypal_class_ecommerce->set_button();
            $paypal_button = '<div class="col-12 col-md-6">'.$paypal_button.'</div>';
        }

        if($stripe_enabled=="1"){
            $this->load->library('stripe_class_ecommerce');
            $this->stripe_class_ecommerce->secret_key=$stripe_secret_key;
            $this->stripe_class_ecommerce->publishable_key=$stripe_publishable_key;
            $this->stripe_class_ecommerce->title=$store_name;
            $this->stripe_class_ecommerce->description=$this->lang->line("Order")." #".$cart_id;
            $this->stripe_class_ecommerce->amount=$payment_amount;
            $this->stripe_class_ecommerce->action_url=base_url("ecommerce/stripe_action/".$store_id.'/'.$cart_id.'/'.$subscriber_id.'/'.$payment_amount.'/'.$currency.'/'.urlencode($store_name));
            $this->stripe_class_ecommerce->currency=$currency;
            $this->stripe_class_ecommerce->img_url=$store_favicon;
            $this->stripe_class_ecommerce->button_lang=$this->lang->line("Pay with Stripe");
            $this->stripe_class_ecommerce->stripe_billing_address=$stripe_billing_address;
            $this->stripe_class_ecommerce->secondary_button = true;
            $stripe_button = $this->stripe_class_ecommerce->set_button();
            $stripe_button = '<div class="col-12 col-md-6">'.$stripe_button.'</div>';
        }

        if($razorpay_enabled=="1"){
            $this->load->library("razorpay_class_ecommerce");

            $this->razorpay_class_ecommerce->key_id=$razorpay_key_id;
            $this->razorpay_class_ecommerce->key_secret=$razorpay_key_secret;
            $this->razorpay_class_ecommerce->title=$store_name;
            $this->razorpay_class_ecommerce->description=$this->lang->line("Order")." #".$cart_id;
            $this->razorpay_class_ecommerce->amount=$payment_amount;
            $this->razorpay_class_ecommerce->action_url=base_url("ecommerce/razorpay_action/".$store_id.'/'.$cart_id.'/'.$subscriber_id);
            $this->razorpay_class_ecommerce->currency=$currency;
            $this->razorpay_class_ecommerce->img_url=$store_favicon;
            $this->razorpay_class_ecommerce->customer_name=$customer_name;
            $this->razorpay_class_ecommerce->customer_email=$customer_email;
            $this->razorpay_class_ecommerce->button_lang=$this->lang->line("Pay with Razorpay");
            $this->razorpay_class_ecommerce->secondary_button = true;
            $razorpay_button =  $this->razorpay_class_ecommerce->set_button();
            $razorpay_button = '<div class="col-12 col-md-6">'.$razorpay_button.'</div>';
        }

        if($paystack_enabled=="1"){
            $this->load->library("paystack_class_ecommerce");

            $this->paystack_class_ecommerce->secret_key=$paystack_secret_key;
            $this->paystack_class_ecommerce->public_key=$paystack_public_key;
            $this->paystack_class_ecommerce->title=$store_name;
            $this->paystack_class_ecommerce->description=$this->lang->line("Order")." #".$cart_id;
            $this->paystack_class_ecommerce->amount=$payment_amount;
            $this->paystack_class_ecommerce->action_url=base_url("ecommerce/paystack_action/".$store_id.'/'.$cart_id.'/'.$subscriber_id);
            $this->paystack_class_ecommerce->currency=$currency;
            $this->paystack_class_ecommerce->img_url=$store_favicon;
            $this->paystack_class_ecommerce->customer_first_name=$customer_first_name;
            $this->paystack_class_ecommerce->customer_last_name=$customer_last_name;
            if($customer_email!="")$this->paystack_class_ecommerce->customer_email=$customer_email;
            else $this->paystack_class_ecommerce->customer_email=$fake_email;
            $this->paystack_class_ecommerce->button_lang=$this->lang->line("Pay with Paystack");
            $this->paystack_class_ecommerce->secondary_button = true;
            $paystack_button =  $this->paystack_class_ecommerce->set_button();
            $paystack_button = '<div class="col-12 col-md-6">'.$paystack_button.'</div>';
        }

        if($mollie_enabled=="1"){
            $this->load->library("mollie_class_ecommerce");

            $this->mollie_class_ecommerce->api_key=$mollie_api_key;
            $this->mollie_class_ecommerce->title=$store_name;
            $this->mollie_class_ecommerce->description=$this->lang->line("Order")." #".$cart_id;
            $this->mollie_class_ecommerce->amount=$payment_amount;
            $this->mollie_class_ecommerce->action_url=base_url("ecommerce/mollie_action/".$store_id.'/'.$cart_id.'/'.$subscriber_id);
            $this->mollie_class_ecommerce->currency=$currency;
            $this->mollie_class_ecommerce->img_url=$store_favicon;
            $this->mollie_class_ecommerce->customer_name=$customer_name;
            $this->mollie_class_ecommerce->customer_email=$customer_email;
            $this->mollie_class_ecommerce->ec_order_id=$cart_id;
            $this->mollie_class_ecommerce->button_lang=$this->lang->line("Pay with Mollie");
            $this->mollie_class_ecommerce->secondary_button = true;
            $mollie_button =  $this->mollie_class_ecommerce->set_button_ecommerce();
            $mollie_button = '<div class="col-12 col-md-6">'.$mollie_button.'</div>';
        }

        if($mercadopago_enabled=='1'){
            $mercadopago_button = '
  <div class="col-12 col-md-6 text-center">
  <a href="#" class="list-group-item list-group-item-action flex-column align-items-start" onclick="document.querySelector(\'.mercadopago-button\').click();">
  <div class="d-flex w-100 align-items-center">
  <small class="text-muted"><img class="rounded" width="60" height="60" src="'.base_url("assets/img/payment/mercadopago.png").'"></small>
  <h6 class="mb-1">'.$this->lang->line("Pay with Mercado Pago").'</h6>
  </div>
  </a>
  </div>';
        }

        if($sslcommerz_enabled=='1'){
            $sslcommerz_button = '
  <div class="col-12 col-md-6 text-center">
  <a href="#" class="list-group-item list-group-item-action flex-column align-items-start" onclick="document.getElementById(\'sslczPayBtn\').click();">
  <div class="d-flex w-100 align-items-center">
  <small class="text-muted"><img class="rounded" width="60" height="60" src="'.base_url("assets/img/payment/sslcommerz.png").'"></small>
  <h6 class="mb-1">'.$this->lang->line("Pay With SSLCOMMERZ").'</h6>
  </div>
  </a>
  </div>';

        }

        if($senangpay_enabled=='1'){
            $details = "#".$cart_id;
            // $hashed_string = md5($senangpay_secret_key.urldecode($details).urldecode($payment_amount).urldecode($cart_id));
            $hashed_string = hash_hmac('sha256', $senangpay_secret_key.urldecode($details).urldecode($payment_amount).urldecode($cart_id), $senangpay_secret_key);

            $this->load->library('senangpay');
            $this->senangpay->merchant_id = $senangpay_merchent_id;
            $this->senangpay->secretkey = $senangpay_secret_key;
            $this->senangpay->detail = $details;
            $this->senangpay->amount = $payment_amount;
            $this->senangpay->order_id = $cart_id;
            $this->senangpay->name = $customer_name;
            $this->senangpay->email = $customer_email;
            $this->senangpay->phone = $customer_mobile;
            $this->senangpay->senangpay_mode = $senangpay_mode;
            $this->senangpay->hashed_string = $hashed_string;
            $this->senangpay->secondary_button = true;
            $this->senangpay->button_lang = $this->lang->line('Pay with Senangpay');
            $senangpay_button = $this->senangpay->set_button();
            $senangpay_button = '<div class="col-12 col-md-6">'.$senangpay_button.'</div>';
        }

        if($instamojo_enabled=='1'){
            $redirect_url_instamojo = base_url('ecommerce/instamojo_action/').$cart_id;
            $this->load->library('instamojo');
            $this->instamojo->redirect_url = $redirect_url_instamojo;
            $this->instamojo->button_lang = $this->lang->line('Pay with Instamojo');
            $instamojo_button = $this->instamojo->set_button();
            $instamojo_button = '<div class="col-12 col-md-6">'.$instamojo_button.'</div>';
        }

        if($toyyibpay_enabled=='1'){
            $redirect_url_toyyibpay = base_url('ecommerce/toyyibpay_action/').$cart_id;
            $this->load->library('toyyibpay');
            $this->toyyibpay->redirect_url = $redirect_url_toyyibpay;
            $this->toyyibpay->button_lang = $this->lang->line('Pay with toyyibpay');
            $toyyibpay_button = $this->toyyibpay->set_button();
            $toyyibpay_button = '<div class="col-12 col-md-6">'.$toyyibpay_button.'</div>';
        }

        if($paymaya_enabled=='1'){
            $redirect_url_paymaya = base_url('ecommerce/paymaya_action/').$cart_id;
            $this->load->library('paymaya');
            $this->paymaya->redirect_url = $redirect_url_paymaya;
            $this->paymaya->button_lang = $this->lang->line('Pay with paymaya');
            $paymaya_button = $this->paymaya->set_button();
            $paymaya_button = '<div class="col-12 col-md-6">'.$paymaya_button.'</div>';
        }

        if($myfatoorah_enabled=='1'){
            $redirect_url_myfatoorah = base_url('ecommerce/myfatoorah_action/').$cart_id;
            $this->load->library('myfatoorah');
            $this->myfatoorah->redirect_url = $redirect_url_myfatoorah;
            $this->myfatoorah->button_lang = $this->lang->line('Pay with myfatoorah');
            $myfatoorah_button = $this->myfatoorah->set_button();
            $myfatoorah_button = '<div class="col-12 col-md-6">'.$myfatoorah_button.'</div>';
        }

        if($xendit_enabled=='1'){
            $xendit_redirect_url = base_url('ecommerce/xendit_action/').$cart_id;
            $xendit_success_redirect_url = base_url('ecommerce/xendit_success/');
            $xendit_failure_redirect_url = base_url('ecommerce/xendit_fail/');
            $this->load->library('xendit');
            $this->xendit->xendit_redirect_url = $xendit_redirect_url;
            $this->xendit->xendit_success_redirect_url = $xendit_success_redirect_url;
            $this->xendit->xendit_failure_redirect_url = $xendit_failure_redirect_url;
            $this->xendit->button_lang = $this->lang->line('Pay with Xendit');
            $xendit_button = $this->xendit->set_button();
            $xendit_button = '<div class="col-12 col-md-6">'.$xendit_button.'</div>';
        }

        if($manual_enabled=='1'){
            $manual_button = '
  <div class="col-12 col-md-6 text-center">
  <a href="#" class="list-group-item list-group-item-action flex-column align-items-start" id="manual-payment-button">
  <div class="d-flex w-100 align-items-center">
  <small class="text-muted"><img class="rounded" width="60" height="60" src="'.base_url("assets/img/payment/manual.png").'"></small>
  <h6 class="mb-1">'.$this->lang->line("Manual Payment").'</h6>
  </div>
  </a>
  </div>';
        }

        if($cod_enabled=='1'){
            $cod_button = '
  <div class="col-12 col-md-6 text-center">
  <a href="#" class="list-group-item list-group-item-action flex-column align-items-start" id="cod-payment-button">
  <div class="d-flex w-100 align-items-center">
  <small class="text-muted"><img class="rounded" width="60" height="60" src="'.base_url("assets/img/payment/cod.png").'"></small>
  <h6 class="mb-1">'.$this->lang->line("Cash on Delivery").'</h6>
  </div>
  </a>
  </div>';
        }

        $n_omise_btn = '';
        if($n_omise_enabled=='1'){
            $n_omise_btn = ' 
                <div class="col-12 col-md-6 text-center">
                  <a href="#" class="list-group-item list-group-item-action flex-column align-items-start" onclick="document.getElementById(\'omise-checkout-button-1\').click();">
                  <div class="d-flex w-100 align-items-center">
                  <small class="text-muted"><img class="rounded" style="padding: 10px;" width="60" height="60" src="https://cdn.omise.co/assets/dashboard/images/omise-logo.png"></small>
                  <h6 class="mb-1">'.$this->lang->line("Pay with Omise").'</h6>
                  </div>
                  </a>
                  </div>
                
                                      <form name="checkoutForm" method="POST" action="'.base_url('n_omise/charge_eco/'.$store_id.'/'.$cart_id.'/'.$subscriber_id.'/'.$payment_amount.'/'.$currency.'/'.urlencode($store_name).'/'.urlencode($buyer_email)).'" class="d-none">
                                        <p><button type="submit" id="omise-checkout-button-1"></button></p>
                                    </form>';
        }

        $n_paymongo_btn = '';
        $paymongo_js = '';
        if($n_paymongo_sec!='' AND $currency=='PHP'){
            include(APPPATH.'modules/n_paymongo/lib/paymongo_lib.php');
        }

        $html ='<div class="row">'.$cod_button.$paypal_button.$stripe_button.$razorpay_button.$paystack_button.$mollie_button.$mercadopago_button.$sslcommerz_button.$senangpay_button.$instamojo_button.$xendit_button.$manual_button.$toyyibpay_button.$myfatoorah_button.$paymaya_button.$n_omise_btn.$n_paymongo_btn.'</div>';

        echo json_encode(array('status'=>'1','message'=>'','html'=>$html));

    }

    private function valid_cart_data($cart_id=0,$subscriber_id="",$select="")
    {
        $join = array('ecommerce_store'=>"ecommerce_cart.store_id=ecommerce_store.id,left");
        $where = array('where'=>array("ecommerce_cart.subscriber_id"=>$subscriber_id,"ecommerce_cart.id"=>$cart_id,"action_type!="=>"checkout","ecommerce_store.status"=>"1"));
        if($select=="") $select = array("ecommerce_cart.*","tax_percentage","shipping_charge","store_unique_id","store_locale");
        return $cart_data = $this->basic->get_data("ecommerce_cart",$where,$select,$join);
    }

    public function payment_accounts_action()
    {
        if($this->is_demo == '1')
        {
            echo "<h2 style='text-align:center;color:red;border:1px solid red; padding: 10px'>This feature is disabled in this demo.</h2>";
            exit();
        }
        if ($_SERVER['REQUEST_METHOD'] === 'GET') redirect('home/access_forbidden', 'location');
        if ($_POST)
        {
            $this->form_validation->set_rules('paypal_email','<b>'.$this->lang->line("Paypal Email").'</b>','trim|valid_email');
            $this->form_validation->set_rules('paypal_mode','<b>'.$this->lang->line("Paypal Sandbox Mode").'</b>','trim');
            $this->form_validation->set_rules('stripe_secret_key','<b>'.$this->lang->line("Stripe Secret Key").'</b>','trim');
            $this->form_validation->set_rules('stripe_publishable_key','<b>'.$this->lang->line("Stripe Publishable Key").'</b>','trim');
            $this->form_validation->set_rules('razorpay_key_id','<b>'.$this->lang->line("Razorpay Key ID").'</b>','trim');
            $this->form_validation->set_rules('razorpay_key_secret','<b>'.$this->lang->line("Razorpay Key Secret").'</b>','trim');
            $this->form_validation->set_rules('paystack_secret_key','<b>'.$this->lang->line("Paystack Secret Key").'</b>','trim');
            $this->form_validation->set_rules('paystack_public_key','<b>'.$this->lang->line("Paystack Public Key").'</b>','trim');
            $this->form_validation->set_rules('mollie_api_key','<b>'.$this->lang->line("Mollie API Key").'</b>','trim');
            $this->form_validation->set_rules('mercadopago_public_key','<b>'.$this->lang->line("Mercado Pago Public Key").'</b>','trim');
            $this->form_validation->set_rules('mercadopago_access_token','<b>'.$this->lang->line("Mercado Pago Acceess Token").'</b>','trim');
            $this->form_validation->set_rules('sslcommerz_store_id','<b>'.$this->lang->line("Sslcommerz Store ID").'</b>','trim');
            $this->form_validation->set_rules('sslcommerz_store_password','<b>'.$this->lang->line("Sslcommerz Store Password").'</b>','trim');
            $this->form_validation->set_rules('sslcommerz_mode','<b>'.$this->lang->line("Sslcommerz Sandbox Mode").'</b>','trim');
            $this->form_validation->set_rules('marcadopago_country','<b>'.$this->lang->line("marcadopago_country").'</b>','trim');
            $this->form_validation->set_rules('senangpay_merchent_id','<b>'.$this->lang->line("senangpay_merchent_id").'</b>','trim');
            $this->form_validation->set_rules('senangpay_secret_key','<b>'.$this->lang->line("senangpay_secret_key").'</b>','trim');
            $this->form_validation->set_rules('senangpay_mode','<b>'.$this->lang->line("senangpay_mode").'</b>','trim');
            $this->form_validation->set_rules('instamojo_api_key','<b>'.$this->lang->line("instamojo_api_key").'</b>','trim');
            $this->form_validation->set_rules('instamojo_auth_token','<b>'.$this->lang->line("instamojo_auth_token").'</b>','trim');
            $this->form_validation->set_rules('instamojo_mode','<b>'.$this->lang->line("instamojo_mode").'</b>','trim');
            $this->form_validation->set_rules('xendit_secret_api_key','<b>'.$this->lang->line("xendit_secret_api_key").'</b>','trim');
            $this->form_validation->set_rules('myfatoorah_mode','<b>'.$this->lang->line("myfatoorah_mode").'</b>','trim');
            $this->form_validation->set_rules('myfatoorah_api_key','<b>'.$this->lang->line("myfatoorah_api_key").'</b>','trim');
            $this->form_validation->set_rules('senangpay_enabled','<b>'.$this->lang->line("senangpay_enabled").'</b>','trim');
            $this->form_validation->set_rules('toyyibpay_secret_key','<b>'.$this->lang->line("toyyibpay_secret_key").'</b>','trim');
            $this->form_validation->set_rules('toyyibpay_mode','<b>'.$this->lang->line("toyyibpay_mode").'</b>','trim');
            $this->form_validation->set_rules('toyyibpay_category_code','<b>'.$this->lang->line("toyyibpay_category_code").'</b>','trim');
            $this->form_validation->set_rules('paymaya_secret_key','<b>'.$this->lang->line("paymaya_secret_key").'</b>','trim');
            $this->form_validation->set_rules('paymaya_public_key','<b>'.$this->lang->line("paymaya_public_key").'</b>','trim');
            $this->form_validation->set_rules('paymaya_mode','<b>'.$this->lang->line("paymaya_mode").'</b>','trim');
            $this->form_validation->set_rules('senangpay_enabled','<b>'.$this->lang->line("senangpay_enabled").'</b>','trim');
            $this->form_validation->set_rules('instamojo_enabled','<b>'.$this->lang->line("instamojo_enabled").'</b>','trim');
            $this->form_validation->set_rules('xendit_enabled','<b>'.$this->lang->line("xendit_enabled").'</b>','trim');
            $this->form_validation->set_rules('toyyibpay_enabled','<b>'.$this->lang->line("toyyibpay_enabled").'</b>','trim');
            $this->form_validation->set_rules('paymaya_enabled','<b>'.$this->lang->line("paymaya_enabled").'</b>','trim');
            $this->form_validation->set_rules('currency','<b>'.$this->lang->line("Currency").'</b>',  'trim');
            $this->form_validation->set_rules('manual_payment_instruction','<b>'.$this->lang->line("Manual Payment Instruction").'</b>',  'trim');
            $this->form_validation->set_rules('currency_position','<b>'.$this->lang->line("Currency Position").'</b>',  'trim');
            $this->form_validation->set_rules('decimal_point','<b>'.$this->lang->line("Decimal Point").'</b>',  'trim|integer');
            $this->form_validation->set_rules('thousand_comma','<b>'.$this->lang->line("Thousand Comma").'</b>',  'trim');
            $this->form_validation->set_rules('is_preparation_time','<b>'.$this->lang->line("Preparation time").'</b>',  'trim');
            $this->form_validation->set_rules('preparation_time','<b>'.$this->lang->line("Preparation time").'</b>',  'trim');
            $this->form_validation->set_rules('preparation_time_unit','<b>'.$this->lang->line("Preparation time").'</b>',  'trim');
            $this->form_validation->set_rules('is_order_schedule','<b>'.$this->lang->line("Scheduled order").'</b>',  'trim');
            $this->form_validation->set_rules('order_schedule','<b>'.$this->lang->line("Scheduled order").'</b>',  'trim');
            $this->form_validation->set_rules('is_guest_login','<b>'.$this->lang->line("Guest login").'</b>',  'trim');
            $this->form_validation->set_rules('n_omise_pubkey','<b>'.$this->lang->line("n_omise_pubkey").'</b>',  'trim');
            $this->form_validation->set_rules('n_omise_seckey','<b>'.$this->lang->line("n_omise_seckey").'</b>',  'trim');
            $this->form_validation->set_rules('n_omise_enabled','<b>'.$this->lang->line("n_omise_enabled").'</b>',  'trim');
            $this->form_validation->set_rules('n_omise_pubkey','<b>'.$this->lang->line("n_omise_pubkey").'</b>',  'trim');
            $this->form_validation->set_rules('n_omise_seckey','<b>'.$this->lang->line("n_omise_seckey").'</b>',  'trim');
            $this->form_validation->set_rules('n_omise_enabled','<b>'.$this->lang->line("n_omise_enabled").'</b>',  'trim');
            $this->form_validation->set_rules('n_omise_pubkey','<b>'.$this->lang->line("n_omise_pubkey").'</b>',  'trim');
            $this->form_validation->set_rules('n_omise_seckey','<b>'.$this->lang->line("n_omise_seckey").'</b>',  'trim');
            $this->form_validation->set_rules('n_omise_enabled','<b>'.$this->lang->line("n_omise_enabled").'</b>',  'trim');

            $this->form_validation->set_rules('n_paymongo_pub','<b>'.$this->lang->line("n_paymongo_pub").'</b>',  'trim');
            $this->form_validation->set_rules('n_paymongo_sec','<b>'.$this->lang->line("n_paymongo_sec").'</b>',  'trim');
            $this->form_validation->set_rules('n_paymongo_enabled','<b>'.$this->lang->line("n_paymongo_enabled").'</b>',  'trim');
            $this->form_validation->set_rules('n_paymongo_paymaya_en','<b>'.$this->lang->line("n_paymongo_paymaya_en").'</b>',  'trim');
            $this->form_validation->set_rules('n_paymongo_grab_en','<b>'.$this->lang->line("n_paymongo_grab_en").'</b>',  'trim');
            $this->form_validation->set_rules('n_paymongo_gcash_en','<b>'.$this->lang->line("n_paymongo_gcash_en").'</b>',  'trim');

            $this->form_validation->set_rules('n_paymentwall_pub','<b>'.$this->lang->line("n_paymentwall_pub").'</b>',  'trim');
            $this->form_validation->set_rules('n_paymentwall_sec','<b>'.$this->lang->line("n_paymentwall_sec").'</b>',  'trim');
            $this->form_validation->set_rules('n_paymentwall_enabled','<b>'.$this->lang->line("n_paymentwall_enabled").'</b>',  'trim');

            // go to config form page if validation wrong
            if ($this->form_validation->run() == false)
            {
                return $this->payment_accounts();
            }
            else
            {
                // assign
                $paypal_email=strip_tags($this->input->post('paypal_email',true));
                $paypal_payment_type=strip_tags($this->input->post('paypal_payment_type',true));
                $paypal_mode=strip_tags($this->input->post('paypal_mode',true));
                $stripe_billing_address=strip_tags($this->input->post('stripe_billing_address',true));
                $stripe_secret_key=strip_tags($this->input->post('stripe_secret_key',true));
                $stripe_publishable_key=strip_tags($this->input->post('stripe_publishable_key',true));
                $razorpay_key_id=strip_tags($this->input->post('razorpay_key_id',true));
                $razorpay_key_secret=strip_tags($this->input->post('razorpay_key_secret',true));
                $paystack_secret_key=strip_tags($this->input->post('paystack_secret_key',true));
                $paystack_public_key=strip_tags($this->input->post('paystack_public_key',true));
                $mollie_api_key=strip_tags($this->input->post('mollie_api_key',true));
                $mercadopago_public_key=strip_tags($this->input->post('mercadopago_public_key',true));
                $mercadopago_access_token=strip_tags($this->input->post('mercadopago_access_token',true));
                $sslcommerz_store_id=strip_tags($this->input->post('sslcommerz_store_id',true));
                $sslcommerz_store_password=strip_tags($this->input->post('sslcommerz_store_password',true));
                $sslcommerz_mode=strip_tags($this->input->post('sslcommerz_mode',true));
                $myfatoorah_api_key=strip_tags($this->input->post('myfatoorah_api_key',true));
                $myfatoorah_mode=strip_tags($this->input->post('myfatoorah_mode',true));
                $myfatoorah_enabled=strip_tags($this->input->post('myfatoorah_enabled',true));
                $marcadopago_country=strip_tags($this->input->post('marcadopago_country',true));
                $senangpay_merchent_id=strip_tags($this->input->post('senangpay_merchent_id',true));
                $senangpay_secret_key=strip_tags($this->input->post('senangpay_secret_key',true));
                $senangpay_mode=strip_tags($this->input->post('senangpay_mode',true));
                $instamojo_api_key=strip_tags($this->input->post('instamojo_api_key',true));
                $instamojo_auth_token=strip_tags($this->input->post('instamojo_auth_token',true));
                $instamojo_mode=strip_tags($this->input->post('instamojo_mode',true));
                $xendit_secret_api_key=strip_tags($this->input->post('xendit_secret_api_key',true));

                $paymaya_secret_key=strip_tags($this->input->post('paymaya_secret_key',true));
                $paymaya_public_key=strip_tags($this->input->post('paymaya_public_key',true));
                $paymaya_mode=strip_tags($this->input->post('paymaya_mode',true));

                $toyyibpay_secret_key=strip_tags($this->input->post('toyyibpay_secret_key',true));
                $toyyibpay_category_code=strip_tags($this->input->post('toyyibpay_category_code',true));
                $toyyibpay_mode=strip_tags($this->input->post('toyyibpay_mode',true));
                $currency=strip_tags($this->input->post('currency',true));
                $currency_position=strip_tags($this->input->post('currency_position',true));
                $decimal_point=strip_tags($this->input->post('decimal_point',true));
                $thousand_comma=strip_tags($this->input->post('thousand_comma',true));
                $paypal_enabled=strip_tags($this->input->post('paypal_enabled',true));
                $stripe_enabled=strip_tags($this->input->post('stripe_enabled',true));
                $toyyibpay_enabled=strip_tags($this->input->post('toyyibpay_enabled',true));
                $paymaya_enabled=strip_tags($this->input->post('paymaya_enabled',true));
                $manual_enabled=strip_tags($this->input->post('manual_enabled',true));
                $cod_enabled=strip_tags($this->input->post('cod_enabled',true));
                $razorpay_enabled=strip_tags($this->input->post('razorpay_enabled',true));
                $paystack_enabled=strip_tags($this->input->post('paystack_enabled',true));
                $mollie_enabled=strip_tags($this->input->post('mollie_enabled',true));
                $senangpay_enabled=strip_tags($this->input->post('senangpay_enabled',true));
                $instamojo_enabled=strip_tags($this->input->post('instamojo_enabled',true));
                $xendit_enabled=strip_tags($this->input->post('xendit_enabled',true));
                $mercadopago_enabled=strip_tags($this->input->post('mercadopago_enabled',true));
                $sslcommerz_enabled=strip_tags($this->input->post('sslcommerz_enabled',true));
                $tax_percentage=strip_tags($this->input->post('tax_percentage',true));
                $shipping_charge=strip_tags($this->input->post('shipping_charge',true));
                $is_store_pickup=strip_tags($this->input->post('is_store_pickup',true));
                $is_home_delivery=strip_tags($this->input->post('is_home_delivery',true));
                $is_checkout_country=strip_tags($this->input->post('is_checkout_country',true));
                $is_checkout_state=strip_tags($this->input->post('is_checkout_state',true));
                $is_checkout_city=strip_tags($this->input->post('is_checkout_city',true));
                $is_checkout_zip=strip_tags($this->input->post('is_checkout_zip',true));
                $is_checkout_email=strip_tags($this->input->post('is_checkout_email',true));
                $is_checkout_phone=strip_tags($this->input->post('is_checkout_phone',true));
                $is_delivery_note=strip_tags($this->input->post('is_delivery_note',true));
                $is_preparation_time=strip_tags($this->input->post('is_preparation_time',true));
                $preparation_time=strip_tags($this->input->post('preparation_time',true));
                $preparation_time_unit=strip_tags($this->input->post('preparation_time_unit',true));
                $is_order_schedule=strip_tags($this->input->post('is_order_schedule',true));
                $order_schedule=strip_tags($this->input->post('order_schedule',true));
                $is_guest_login=strip_tags($this->input->post('is_guest_login',true));
                // $manual_payment=$this->input->post('manual_payment');
                $manual_payment='1';
                $manual_payment_instruction=$this->input->post('manual_payment_instruction',true);

                $n_omise_pubkey=strip_tags($this->input->post('n_omise_pubkey',true));
                $n_omise_seckey=strip_tags($this->input->post('n_omise_seckey',true));
                $n_omise_enabled=strip_tags($this->input->post('n_omise_enabled',true));

                $n_paymongo_pub=strip_tags($this->input->post('n_paymongo_pub',true));
                $n_paymongo_sec=strip_tags($this->input->post('n_paymongo_sec',true));
                $n_paymongo_enabled=strip_tags($this->input->post('n_paymongo_enabled',true));
                $n_paymongo_paymaya_en=strip_tags($this->input->post('n_paymongo_paymaya_en',true));
                $n_paymongo_grab_en=strip_tags($this->input->post('n_paymongo_grab_en',true));
                $n_paymongo_gcash_en=strip_tags($this->input->post('n_paymongo_gcash_en',true));

                $n_paymentwall_pub=strip_tags($this->input->post('n_paymentwall_pub',true));
                $n_paymentwall_sec=strip_tags($this->input->post('n_paymentwall_sec',true));
                $n_paymentwall_enabled=strip_tags($this->input->post('n_paymentwall_enabled',true));

                $store_type=$this->input->post('store_type',true);
                if($store_type == 'digital') {
                    $manual_payment_instruction = '';
                    $cod_enabled = '0';
                    $manual_payment = '0';
                }


                if($paypal_mode=="") $paypal_mode="live";
                if($stripe_billing_address=="") $stripe_billing_address="0";
                if($sslcommerz_mode=="") $sslcommerz_mode="live";
                if($senangpay_mode=="") $senangpay_mode="live";
                if($myfatoorah_mode=="") $myfatoorah_mode="live";
                if($instamojo_mode=="") $instamojo_mode="live";
                if($toyyibpay_mode=="") $toyyibpay_mode="live";
                if($paymaya_mode=="") $paymaya_mode="live";
                if($manual_payment=="") $manual_payment="0";
                if($currency_position=="") $currency_position="left";
                if($thousand_comma=="") $thousand_comma="0";
                if($decimal_point=="") $decimal_point="0";

                if($is_store_pickup=="") $is_store_pickup="0";
                if($is_home_delivery=="") $is_home_delivery="0";
                if($is_checkout_country=="") $is_checkout_country="0";
                if($is_checkout_state=="") $is_checkout_state="0";
                if($is_checkout_city=="") $is_checkout_city="0";
                if($is_checkout_zip=="") $is_checkout_zip="0";
                if($is_checkout_email=="") $is_checkout_email="0";
                if($is_checkout_phone=="") $is_checkout_phone="0";
                if($is_delivery_note=="") $is_delivery_note="0";
                if($is_preparation_time=="") $is_preparation_time="0";
                if($is_order_schedule=="") $is_order_schedule="0";
                if($is_guest_login=="") $is_guest_login="0";

                if($is_preparation_time=='1')
                {
                    if(!isset($preparation_time) || $preparation_time=="") $preparation_time="30";
                    if(!isset($preparation_time_unit) || $preparation_time_unit=="") $preparation_time_unit="minutes";
                }

                if(!isset($order_schedule) || $order_schedule=="") $order_schedule="any";

                if($paypal_enabled=='0' && $stripe_enabled=='0' && $manual_enabled=='0' && $cod_enabled=='0' && $razorpay_enabled=='0' && $paystack_enabled=='0' && $mollie_enabled=='0' && $mercadopago_enabled=='0' && $sslcommerz_enabled=='0' && $senangpay_enabled=='0' && $instamojo_enabled=='0' && $xendit_enabled=='0' && $toyyibpay_enabled == '0' && $myfatoorah_enabled == '0' && $paymaya_enabled == '0' && $n_omise_enabled == '0' && $n_paymongo_enabled == '0' && $n_paymentwall_enabled == '0' )
                {
                    $this->session->set_flashdata('error_message', 1);
                    redirect('ecommerce/payment_accounts', 'location');
                    exit();
                }

                if($store_type == 'physical') {

                    if($is_store_pickup=='0' && $is_home_delivery=='0')
                    {
                        $this->session->set_flashdata('error_message2', 1);
                        redirect('ecommerce/payment_accounts', 'location');
                        exit();
                    }
                }

                $update_data =
                    array
                    (
                        'paypal_email'=>$paypal_email,
                        'paypal_mode'=>$paypal_mode,
                        'stripe_billing_address'=>$stripe_billing_address,
                        'stripe_secret_key'=>$stripe_secret_key,
                        'stripe_publishable_key'=>$stripe_publishable_key,
                        'razorpay_key_id'=>$razorpay_key_id,
                        'razorpay_key_secret'=>$razorpay_key_secret,
                        'paystack_secret_key'=>$paystack_secret_key,
                        'paystack_public_key'=>$paystack_public_key,
                        'mollie_api_key'=>$mollie_api_key,
                        'mercadopago_public_key'=>$mercadopago_public_key,
                        'mercadopago_access_token'=>$mercadopago_access_token,
                        'sslcommerz_store_id'=>$sslcommerz_store_id,
                        'sslcommerz_store_password'=>$sslcommerz_store_password,
                        'sslcommerz_mode'=>$sslcommerz_mode,
                        'marcadopago_country'=>$marcadopago_country,
                        'senangpay_merchent_id'=>$senangpay_merchent_id,
                        'senangpay_secret_key'=>$senangpay_secret_key,
                        'senangpay_mode'=>$senangpay_mode,
                        'instamojo_api_key'=>$instamojo_api_key,
                        'instamojo_auth_token'=>$instamojo_auth_token,
                        'instamojo_mode'=>$instamojo_mode,
                        'paymaya_public_key'=>$paymaya_public_key,
                        'paymaya_secret_key'=>$paymaya_secret_key,
                        'paymaya_mode'=>$paymaya_mode,
                        'myfatoorah_api_key'=>$myfatoorah_api_key,
                        'myfatoorah_mode'=>$myfatoorah_mode,
                        'xendit_secret_api_key'=>$xendit_secret_api_key,
                        'toyyibpay_secret_key'=>$toyyibpay_secret_key,
                        'toyyibpay_category_code'=>$toyyibpay_category_code,
                        'toyyibpay_mode'=>$toyyibpay_mode,
                        'currency'=>$currency,
                        'manual_payment'=> $manual_payment,
                        'manual_payment_instruction'=>$manual_payment_instruction,
                        'user_id'=>$this->user_id,
                        'store_id'=>$this->session->userdata("ecommerce_selected_store"),
                        'currency_position'=>$currency_position,
                        'decimal_point'=>$decimal_point,
                        'thousand_comma'=>$thousand_comma,
                        'updated_at'=>date("Y-m-d H:i:s"),
                        'is_store_pickup'=>$is_store_pickup,
                        'is_home_delivery'=>$is_home_delivery,
                        'is_checkout_country'=>$is_checkout_country,
                        'is_checkout_state'=>$is_checkout_state,
                        'is_checkout_city'=>$is_checkout_city,
                        'is_checkout_zip'=>$is_checkout_zip,
                        'is_checkout_email'=>$is_checkout_email,
                        'is_checkout_phone'=>$is_checkout_phone,
                        'is_delivery_note'=>$is_delivery_note,
                        'is_preparation_time'=>$is_preparation_time,
                        'preparation_time'=>$preparation_time,
                        'preparation_time_unit'=>$preparation_time_unit,
                        'is_order_schedule'=>$is_order_schedule,
                        'order_schedule'=>$order_schedule,
                        'is_guest_login'=>$is_guest_login
                    );

                if(file_exists(APPPATH.'modules/n_omise/controllers/N_omise.php')) {
                    $update_data['n_omise_pubkey'] = $n_omise_pubkey;
                    $update_data['n_omise_seckey'] = $n_omise_seckey;
                }

                if(file_exists(APPPATH.'modules/n_paymongo/controllers/N_paymongo.php')) {
                    $update_data['n_paymongo_sec'] = $n_paymongo_sec;
                    $update_data['n_paymongo_pub'] = $n_paymongo_pub;
                }

                if(file_exists(APPPATH.'modules/n_paymentwall/controllers/N_paymentwall.php')) {
                    $update_data['n_paymentwall_sec'] = $n_paymentwall_sec;
                    $update_data['n_paymentwall_pub'] = $n_paymentwall_pub;
                }


                $get_data = $this->basic->get_data("ecommerce_config",array("where"=>array("store_id"=>$this->session->userdata("ecommerce_selected_store"))));
                if(isset($get_data[0]))
                    $this->basic->update_data("ecommerce_config",array("store_id"=>$this->session->userdata("ecommerce_selected_store")),$update_data);
                else $this->basic->insert_data("ecommerce_config",$update_data);

                $update_store = array
                (
                    "paypal_enabled"=> $paypal_enabled,
                    "stripe_enabled"=> $stripe_enabled,
                    "manual_enabled"=> $manual_enabled,
                    "razorpay_enabled"=> $razorpay_enabled,
                    "paystack_enabled"=> $paystack_enabled,
                    "paymaya_enabled"=> $paymaya_enabled,
                    "mollie_enabled"=> $mollie_enabled,
                    "mercadopago_enabled"=> $mercadopago_enabled,
                    "sslcommerz_enabled"=> $sslcommerz_enabled,
                    "senangpay_enabled"=> $senangpay_enabled,
                    "instamojo_enabled"=> $instamojo_enabled,
                    "xendit_enabled"=> $xendit_enabled,
                    "toyyibpay_enabled"=> $toyyibpay_enabled,
                    "myfatoorah_enabled"=> $myfatoorah_enabled,
                    "cod_enabled"=> $cod_enabled,
                    "tax_percentage"=> $tax_percentage,
                    "shipping_charge"=> $shipping_charge
                );
                if(file_exists(APPPATH.'modules/n_omise/controllers/N_omise.php')) {
                    $update_store['n_omise_enabled'] = $n_omise_enabled;
                }

                if(file_exists(APPPATH.'modules/n_paymongo/controllers/N_paymongo.php')) {
                    $update_store['n_paymongo_enabled'] = $n_paymongo_enabled;
                    $update_store['n_paymongo_paymaya_en'] = $n_paymongo_paymaya_en;
                    $update_store['n_paymongo_grab_en'] = $n_paymongo_grab_en;
                    $update_store['n_paymongo_gcash_en'] = $n_paymongo_gcash_en;
                }

                if(file_exists(APPPATH.'modules/n_paymentwall/controllers/N_paymentwall.php')) {
                    $update_store['n_paymentwall_enabled'] = $n_paymentwall_enabled;
                }

                $this->basic->update_data("ecommerce_store",array("id"=>$this->session->userdata("ecommerce_selected_store")),$update_store);

                $this->session->set_flashdata('success_message', 1);
                redirect('ecommerce/payment_accounts', 'location');
            }
        }
    }
}
